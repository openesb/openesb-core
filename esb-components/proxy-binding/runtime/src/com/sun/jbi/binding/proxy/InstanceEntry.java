/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)InstanceEntry.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.proxy;

import java.util.Date;

import java.text.SimpleDateFormat;

/**
 * Records information about JBI Instances in an ESB configuration.
 * @author Sun Microsystems, Inc
 */
class InstanceEntry
        implements java.io.Serializable
{
    private final String        mInstanceId;
    private final long          mBirthTime;
    private long                mHeartbeatTime;
    
    InstanceEntry(String instanceId, long birthTime, long heartbeatTime)
    {
        mInstanceId = instanceId;
        mBirthTime = birthTime;
        mHeartbeatTime = heartbeatTime;
    }
    
    String getInstanceId()
    {
        return (mInstanceId);
    }
    
    long getBirthTime()
    {
        return (mBirthTime);
    }
    
    long getHeartbeatTime()
    {
        return (mHeartbeatTime);
    }
    
    void setHeartbeatTime(long heartbeat)
    {
        mHeartbeatTime = heartbeat;
    }
    
    public String toString()
    {
        StringBuffer        sb = new StringBuffer();
        SimpleDateFormat    sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
        
        sb.append("      ESB  InstanceId    (");
        sb.append(mInstanceId);
        sb.append(")\n        Birthtime     (");
        sb.append(sdf.format(new Date(mBirthTime)));
        sb.append(")\n        NextHeartBeat (");
        sb.append(sdf.format(new Date(mHeartbeatTime)));
        sb.append(")\n");
        
        return (sb.toString());
    }
    
}
