/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ServiceEndpointImpl.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.proxy;

import javax.xml.namespace.QName;

/**
 * Implementation of ServiceEndpoint for junit test cases.
 *
 * @author Sun Microsystems, Inc.
 */
public class ServiceEndpointImpl implements javax.jbi.servicedesc.ServiceEndpoint
{    
    QName       mServiceName;
    String      mEndpointName;
    
    ServiceEndpointImpl(QName service, String endpoint)
    {
        mServiceName = service;
        mEndpointName = endpoint;
    }
    
    public org.w3c.dom.DocumentFragment getAsReference(QName operationName)
    {
        return (null);
    }

    /**
     * Returns the name of this endpoint.
     * @return the endpoint name.
     */
    public String getEndpointName()
    {
        return (mEndpointName);
    }

    /**
     * Get the qualified names of all the interfaces implemented by this
     * service endpoint.
     * @return array of all interfaces implemented by this service endpoint;
     * must be non-null and non-empty.
     */
    public javax.xml.namespace.QName[] getInterfaces()
    {
        return (null);
    }

    /**
     *  Returns the service name of this endpoint.
     *  @return the qualified service name.
     */
    public javax.xml.namespace.QName getServiceName()
    {
        return (mServiceName);
   
    }
}
  
