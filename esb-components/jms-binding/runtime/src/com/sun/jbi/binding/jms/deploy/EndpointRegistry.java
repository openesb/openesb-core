/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)EndpointRegistry.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.jms.deploy;

import com.sun.jbi.binding.jms.EndpointBean;
import com.sun.jbi.binding.jms.JMSBindingContext;

import com.sun.jbi.binding.jms.config.ConfigConstants;
import com.sun.jbi.binding.jms.config.Config;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import java.util.logging.Logger;

import javax.jbi.JBIException;

import javax.jbi.servicedesc.ServiceEndpoint;

import javax.xml.namespace.QName;

/**
 * Simple Deployment Registry implementation. The Registry is currently loaded
 * from a scaffolded registry data file. This will be improved later.
 *
 * @author Sun Microsystems Inc.
 */
public class EndpointRegistry
{
    /**
     * Internal handle to the logger object.
     */
    private Logger mLogger;

    /**
     * Internal handle to the registry implementation.
     */
    private RegistryImplementor mRegistryImplementor;

    /**
     * Creates a new instance of EndpointRegistry.
     */
    public EndpointRegistry()
    {
        mLogger = 
            JMSBindingContext.getInstance().getLogger();
    }

    /**
     * Returns an iterator for endpoint names.
     *
     * @return iterator of endpoint names.
     */
    public Iterator getAllEndpointNames()
    {
        return mRegistryImplementor.getAllKeys();
    }

    /**
     * Returns all the endpoints.
     *
     * @return collection of endpoints.
     */
    public Collection getAllEndpoints()
    {
        return mRegistryImplementor.getAllValues();
    }

    /**
     * Gets the inbound deployment for a given service URL.
     *
     * @param key - service address.
     *
     * @return deployed endpoint.
     *
     * @throws JBIException when the endpoint could not be deployed.
     */
    public EndpointBean getEndpoint(Object key)
        throws JBIException
    {
        String regkey = getRegistryKey(key);

        return (EndpointBean) mRegistryImplementor.getValue(regkey);
    }

    /**
     * Returns the endpoints corresponding to a deployment.
     *
     * @param asid SU id.
     *
     * @return collection of endpoints.
     */
    public Collection getEndpoints(String asid)
    {
        Collection allvalues = new ArrayList();

        EndpointBean eb = null;

        //Get the Inbound registry endpoints and unregister them
        Iterator keyIter = getAllEndpointNames();

        try
        {
            while (keyIter.hasNext())
            {
                //Get the registry key
                String regkey = (String) keyIter.next();

                //Get the endpoint for this key
                eb = (EndpointBean) getEndpoint(regkey);

                if (eb == null)
                {
                    continue;
                }

                if (eb.getDeploymentId().trim().equals(asid))
                {
                    allvalues.add(eb);
                }
            }
        }
        catch (Exception e)
        {
            allvalues = null;
        }

        //end while
        return allvalues;
    }

    /**
     * Clears all deployments.
     */
    public void clearEndpoints()
    {
        if (mRegistryImplementor != null)
        {
            mRegistryImplementor.clearRegistry();
        }
    }

    /**
     * Indicates if the current inbound deployment exists.
     *
     * @param key - registry key.
     *
     * @return true if a deployment exists for this address;false otherwise.
     */
    public boolean containsEndpoint(Object key)
    {
        String registryKey = getRegistryKey(key);

        return mRegistryImplementor.containsKey(registryKey);
    }

    /**
     * Initializes the deployment registry.
     *
     * @throws JBIException jbi exception.
     */
    public void init()
        throws JBIException
    {
        // Load it from a file for maximum flexibility for vertical slice 
        loadDeployments();
    }

    /**
     * Loads deployemnts from the deployment file. this is a hack and will be
     * gone.
     *
     * @throws JBIException jbi exception.
     */
    public void loadDeployments()
        throws JBIException
    {
        try
        {
            Config config = JMSBindingContext.getInstance().getConfig();
            String implementationClassName = config.getDeploymentImplClass();
            ClassLoader bindingClassLoader = this.getClass().getClassLoader();
            mLogger.fine("Loading " + implementationClassName
                + " using the thread context class loader");

            Class implementationClass =
                bindingClassLoader.loadClass(implementationClassName);
            mLogger.fine("Creating a new instance of "
                + implementationClassName);
            mRegistryImplementor =
                (RegistryImplementor) implementationClass.newInstance();
            mRegistryImplementor.init(config.getDeploymentProperties());
        }
        catch (Exception exception)
        {
            exception.printStackTrace();

            JBIException jbiException =
                new JBIException(exception.getMessage());
            jbiException.initCause(exception);
            throw jbiException;
        }
        catch (Throwable t)
        {
            t.printStackTrace();
        }
    }

    /**
     * Persists the deployments.
     *
     * @throws JBIException jbi exception.
     */
    public void persist()
        throws JBIException
    {
        mRegistryImplementor.serialize();
    }

    /**
     * Adds a new inbound deployment to the deployment registry.
     *
     * @param endpoint to be deployed.
     *
     * @return registered key.
     */
    public String registerEndpoint(EndpointBean endpoint)
    {
        String key = endpoint.getUniqueName();

        return mRegistryImplementor.registerKey(key, endpoint);
    }

    /**
     * Removes the endpoint from the registry.
     * 
     * @param endpoint endpoint to be removed.
     */
    
    public void deregisterEndpoint(String endpoint)
    {
        mRegistryImplementor.removeKey(endpoint);
    }
    /**
     * Removes the inbound deployment from the deployment registry.
     *
     * @param key to be undeployed.
     *
     * @throws JBIException when the deployment could not be undeployed.
     */
    public void removeEndpoint(Object key)
        throws JBIException
    {
        String regkey = getRegistryKey(key);
        mRegistryImplementor.removeKey(regkey);
    }

    /**
     * Returns a registry key.
     *
     * @param key registry key.
     *
     * @return string value of the key.
     */
    private String getRegistryKey(Object key)
    {
        String regkey = null;

        if (key == null)
        {
            return regkey;
        }

        try
        {
            if (key instanceof ServiceEndpoint)
            {
                ServiceEndpoint epref = (ServiceEndpoint) key;
                if (epref.getEndpointName() != null)
                {
                    regkey = 
                        epref.getServiceName().toString() + 
                        epref.getEndpointName();
                }
                else
                {
                   regkey =  epref.getServiceName().toString();
                }            
            }
            else if (key instanceof EndpointBean)
            {
                EndpointBean eb = (EndpointBean) key;
                regkey = eb.getUniqueName();
            }
            else if (key instanceof String)
            {
                regkey = (String) key;
            }
        }
        catch (Exception e)
        {
            ;
        }

        return regkey;
    }
    /**
     * Returns all the deployments.
     *
     * @return String [] array of deployments.
     *
     */
    public String [] getAllDeployments()
    {
        
        List deps = new ArrayList();
        Collection col = getAllEndpoints();
        Iterator iter = col.iterator();
        while (iter.hasNext())
        {
            EndpointBean eb = (EndpointBean) iter.next();
            String depid = eb.getDeploymentId();
            if (!deps.contains(depid))
            {
                deps.add(depid);
            }
        }
        String [] deployments = new String[deps.size()];
        Iterator depiter = deps.iterator();
        int i = 0;
        while (depiter.hasNext())
        {
            deployments[i] = (String) depiter.next();
            i++;
        }
        return deployments;
    }
    
    /**
     * Used to find a registered endpoint which matches the specified interface.
     *
     * @param interfacename the interface to match against.
     *
     * @return the appropriate endpoint bean, or no such endpoint exists.
     */
    public EndpointBean findEndpointByInterface(String interfacename)
    {
        //Set keyset = 
        Iterator iter = mRegistryImplementor.getAllKeys();

        while (iter.hasNext())
        {
            String ser = (String) iter.next();
            EndpointBean eb = (EndpointBean) mRegistryImplementor.getValue(ser);
            QName in = new QName(eb.getValue(ConfigConstants.INTERFACE_NAMESPACE),
                eb.getValue(ConfigConstants.INTERFACE_LOCALNAME));
            if ((in.toString().trim().equals(interfacename)) && 
                 (eb.getRole() == ConfigConstants.CONSUMER))
            {
                return eb;
            }
        }
        return null;
    }   

    /**
     * Used to check if the q or topic is already deployed.
     *
     * @param eb bean.
     *
     * @return true or false.
     */
    public boolean containsDestination(Object eb)
    {
        EndpointBean bean = (EndpointBean)eb;
        Iterator iter = mRegistryImplementor.getAllKeys();
        while (iter.hasNext())
        {
            String ser = (String) iter.next();
            EndpointBean epbean  = (EndpointBean) mRegistryImplementor.getValue(ser);
            if ((epbean.getValue(ConfigConstants.DESTINATION_NAME).equals
                (bean.getValue(ConfigConstants.DESTINATION_NAME))) &&  
                (epbean.getStyle() == bean.getStyle()))
            {
                return true;
            }
        }
        return false;
    }    

}
