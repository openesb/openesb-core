/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ConfigElement.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.jms.config;

import java.util.Iterator;

/**
 * Represents a configuration element. The configuration data can be visually
 * represented as a tree, with several nodes and leaves. A configuration
 * element represents a node element. All children of this node are the
 * properties of the element and can be fetched using their name. All leaf
 * children are stored as String instances and all node children are stored as
 * ConfigElement instances.
 *
 * @author Sun Microsystems Inc.
  */
public interface ConfigElement
{
    /**
     * Gets the config element associated with property name.
     *
     * @param propertyName element's property name.
     *
     * @return associated property value as a ConfigElement instance.
     */
    ConfigElement getElement(String propertyName);

    /**
     * Gets the values associated with property name.
     *
     * @param propertyName element's property name.
     *
     * @return associated ConfigElement instance list.
     */
    ConfigElement [] getElementList(String propertyName);

    /**
     * Returns the configuration element name.
     *
     * @return the element name as a String.
     */
    String getElementName();

    /**
     * Get the iterator containing the element's property names.
     *
     * @return an property name interator.
     */
    Iterator getKeys();

    /**
     * Gets the value associated with property name.
     *
     * @param propertyName elements' property name.
     *
     * @return associated property value as a String.
     */
    String getProperty(String propertyName);

    /**
     * Gets the values associated with property name.
     *
     * @param propertyName element's property name.
     *
     * @return associated property value list.
     */
    String [] getPropertyList(String propertyName);
}
