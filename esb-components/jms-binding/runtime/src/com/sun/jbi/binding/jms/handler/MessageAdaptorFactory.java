/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)MessageAdaptorFactory.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.jms.handler;

import javax.jms.Session;
import javax.jms.Message;


/**
 * Factory class for message adaptors.
 *
 * @author Sun Microsystems Inc.
 */
class MessageAdaptorFactory
{
    /**
     * Creates a new MessageAdaptorFactory object.
     */
    public MessageAdaptorFactory()
    {
    }

    /**
     * Returs the appropriate adaptor.
     *
     * @param msg JMS message.
     *
     * @return message adaptor implementation.
     */
    public static MessageAdaptor getAdaptor(Message msg, String deptype)
    {
        MessageAdaptor adaptor = null;

        if (msg instanceof javax.jms.TextMessage)
        {
		if (deptype.trim().equals("WSDL11"))
                {
                    adaptor = new Wsdl11TextMessageAdaptor();
                }
                else
                {
                    adaptor = new TextMessageAdaptor();
                }

            // process text
        }
        else if (msg instanceof javax.jms.BytesMessage)
        {
            ;// processByteMessage();
        }
        else if (msg instanceof javax.jms.StreamMessage)
        {
            ;// processStreamMessage();
        }
        else if (msg instanceof javax.jms.MapMessage)
        {
            ;// processMapMessage();
        }
        else if (msg instanceof javax.jms.ObjectMessage)
        {
            ;// processObjectMessage();
        }
        else
        {
            ;// seend some error back
        }

        return adaptor;
    }

    /**
     * Gets the adaptor based on the type.
     *
     * @param outputtype type of output message.
     *
     * @return message adaptor implementation.
     */
    public static MessageAdaptor getAdaptor(String outputtype, String deptype)
    {
        MessageAdaptor adaptor = null;

        if (outputtype.trim().equals(MessageProperties.TEXT_MESSAGE))
        {
		if (deptype.trim().equals("WSDL11"))
                {
                    adaptor = new Wsdl11TextMessageAdaptor();
                }
                else
                {
                    adaptor = new TextMessageAdaptor();
                }

        }
        else if (outputtype.trim().equals(MessageProperties.STREAM_MESSAGE))
        {
            ;
        }
        else if (outputtype.trim().equals(MessageProperties.OBJECT_MESSAGE))
        {
            ;
        }
        else if (outputtype.trim().equals(MessageProperties.MAP_MESSAGE))
        {
            ;
        }
        else if (outputtype.trim().equals(MessageProperties.BYTE_MESSAGE))
        {
            ;
        }

        return adaptor;
    }
}
