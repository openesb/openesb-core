#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)jms00004.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#
echo "jmsbinding00004.ksh- DEPLOY jms binding test"

. ./regress_defs.ksh

# first create a jar by replacing tokens

# jar and zip 

root=`echo $JV_SVC_ROOT|sed s'-/runtime$--'`
z=`echo $root/deployment*/jms-regress11.zip`

$AS8BASE/bin/asant -emacs -q -f $JBI_ADMIN_XML -Djbi.port=$JBI_ADMIN_PORT -Djbi.deploy.file=$z deploy-service-assembly

$AS8BASE/bin/asant -emacs -q -f $JBI_ADMIN_XML -Djbi.username=$AS_ADMIN_USER -Djbi.password=$AS_ADMIN_PASSWD -Djbi.port=$JBI_ADMIN_PORT -Djbi.service.assembly.name=JMS_SAMPLE_WSDL_UNIT start-service-assembly

echo Completed Deploying to jms bindin
