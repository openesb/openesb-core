/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)SecurityServiceConfigMBean.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  SecurityServiceConfigMBean.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on December 16, 2004, 1:45 PM
 */

package com.sun.jbi.internal.security.mbeans;

import com.sun.jbi.management.common.GenericConfigurationMBean;

/**
 * This MBean interface extends the Configuration MBean interface for the
 * Security Service installation time configuration.
 *
 * @author Sun Microsystems, Inc.
 */
public interface SecurityServiceConfigMBean
    extends GenericConfigurationMBean
{
    
   /*-------------------------------------------------------------------------------*\
    *                           User Domain Operations                              *
   \*-------------------------------------------------------------------------------*/

    /**
     * @param defUserDomain is the default User Domain.
     */
    void setDefaultUserDomainName (String defUserDomain);

    /**
     * @return the default User Domain name.
     */
    String getDefaultUserDomainName ();

     /**
     * Add a new User Domain. If a User Domain by the specified name exists, it is
     * overwritten with the new one.
     *
     * @param name of the domain.
     * @param domain is the implementation class that implements the UserDomain interface.
     */
    void addUserDomain(String name, String domain);
    
    /**
     * Remove a User Domain. 
     *
     * @param name of the domain.
     */
    void removeUserDomain(String name);
    
    /**
     * Add a parameter to a User Domain, if the domain does not exist, the parameter is
     * not added.
     *
     * @param domain name of the UserDomain.
     * @param name is the name of the parameter.
     * @param parameter is the value for the parameter.
     */
    void addParameterToUserDomain(String domain, String name, String parameter);
    
        /**
     * Remove a parameter to a User Domain.
     *
     * @param domain name of the UserDomain.
     * @param name is the name of the parameter.
     */
    void removeParameterFromUserDomain(String domain, String name);

   /*-------------------------------------------------------------------------------*\
    *                           KeyStore Manager Operations                         *
   \*-------------------------------------------------------------------------------*/
        
        
    /**
     * @param defKSMgr is the default KeyStore Manager.
     */
    void setDefaultKeyStoreManagerName(String defKSMgr);
    
    /**
     * @return the default KeyStore Manager.
     */
    String getDefaultKeyStoreManagerName ();
    
    /**
     * Add a new KeyStore Manager. If a KeyStoreManager by the specified name exists,
     * it is overwritten with the new one.
     *
     * @param name of the manager.
     * @param manager is the implementation class that implements the KeyStoreManager
     * interface.
     */
    void addKeyStoreManager (String name, String manager);
    
    
    /**
     * Remove a KeyStore Manager.
     *
     * @param name of the manager.
     */
    void removeKeyStoreManager (String name);

    
    /**
     * Add a parameter to a KeyStoreManager, if it does not exist, the parameter is
     * not added.
     *
     * @param manager is the name of the KeyStoreManager.
     * @param name is the name of the parameter.
     * @param value is the value for the parameter.
     */
    void addParameterToKeyStoreManager(String manager, String name, String value);
    
    /**
     * Remove a parameter from the KeyStoreManager.
     *
     * @param manager is the name of the KeyStoreManager.
     * @param name is the name of the parameter.
     */
    void removeParameterFromKeyStoreManager(String manager, String name);
   
    
   /*-------------------------------------------------------------------------------*\
    *                           Security Context Operations                         *
   \*-------------------------------------------------------------------------------*/

    /**
     * Set the Client Alias from the TransportSecurity Context.
     * @param alias is the SSL Client alias.
     */
    void setSSLClientAlias(String alias); 
    
    /**
     * Get the Client Alias from the TransportSecurity Context.
     * @return the SSL Client alias.
     */
    String getSSLClientAlias();
  
    /**
     * Set Client Alias from the TransportSecurity Context.
     *
     * @param protocol is the SSL Client protocol, allowed values are SSLv3, TLS and TLSv1
     */
    void setSSLClientProtocol (String protocol);  
    
    /**
     * Get the Client SSL Protocol  from the TransportSecurity Context.
     *
     * @return the SSL Client Protocol.
     */
    String getSSLClientProtocol();
    
    /**
     * Set the SSL Client Use Default parameter.
     *
     * @param flag - true/false inducates whether to use the
     * default Application Server SSL context or not.
     */
    void setSSLClientUseDefault(boolean flag);
    
    /**
     * Get the SSL Client Use Default parameter.
     *
     * @return true/false inducates whether the
     * default Application Server SSL context is being used or not.
     */
    boolean getSSLClientUseDefault();
    
    /**
     * Set the SSL Server Req. Client Auth flag.
     *
     * @param flag - true/false inducates whether Client Auth 
     * is required by default.
     */
    void setSSLServerRequireClientAuth(boolean flag);
    
    /**
     * Get the SSL Server Req. Client Auth flag.
     *
     * @return true/false inducates whether Client Auth 
     * is required by default.
     */
    boolean getSSLServerRequireClientAuth();
    
   /*-------------------------------------------------------------------------------*\
    *         Operations to display Configuration information                       *
   \*-------------------------------------------------------------------------------*/

    /**
     * Get an XML String which has the meta-data for a particular user domain
     * User Domain.
     *
     * @param name is the logical name of the UserDomain. If there is no such UserDomain
     * or the name is null then a empty string "" is returned.
     * @return an XML String with information for a particular UserDomain.
     */
    String getUserDomain(String name);
    
    /**
     * Get an XML String listing all the UserDomains and their meta-data.
     *
     * @return an XML String with information for all the UserDomains.
     */
    String getUserDomains();
    
    /**
     * Get an XML String which has the meta-data for a particular key store manager.
     *
     * @param name is the logical name of the KeyStoreManager. If there is no such 
     * KeyStoreManager or the name is null then a empty string "" is returned.
     * @return an XML String with information for a particular KeyStoreManager.
     */
    String getKeyStoreManager(String name);
    
    /**
     * Get an XML String listing all the KeyStoreManagers and their meta-data.
     *
     * @return an XML String with information for all the KeyStoreManagers.
     */
    String getKeyStoreManagers();
    
    
    /**
     * Get an XML String which has the meta-data for the Transport Security Context.
     *
     * @return an XML String with information for the Transport Security Context.
     */
    String getTransportSecurityConfig();
    

}
