/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ComponentContext.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  BindingComponentContext.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on November 4, 2004, 12:43 PM
 */

package com.sun.jbi.internal.security;

import com.sun.jbi.StringTranslator;

/**
 * This interface is a wrapper around the com.sun.jbi.BindingContext. 
 *
 * @author Sun Microsystems, Inc.
 */
public interface ComponentContext
{
    
    /**
     * Get the StringTranslator required for i18N based on the package name.
     *
     * @param packageName is the name of the package
     * @return the StringTranslator for the package
     */
    StringTranslator getStringTranslator(String packageName);
    
     /**
     * Get the StringTranslator required for i18N based on the objects package name.
     *
     * @param object is the Object.
     * @return the StringTranslator for the object.
     */
    StringTranslator getStringTranslatorFor(Object object);
    
    /**
     * Get the Component Id of the Binding Component.
     *
     * @return the ComponentId as String
     */
    String getComponentName();
    
    /**
     * Get the SecurityHandler.
     *
     * @return the SecurityHandler instance for the component.
     */
    com.sun.jbi.binding.security.SecurityHandler getSecurityHandler();
    
}
