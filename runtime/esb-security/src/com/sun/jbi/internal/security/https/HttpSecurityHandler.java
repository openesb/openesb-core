/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)HttpSecurityHandler.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  HttpSecurityHandler.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on October 19, 2004, 5:53 PM
 */
package com.sun.jbi.internal.security.https;

import com.sun.jbi.StringTranslator;
import com.sun.jbi.binding.security.Endpoint;
import com.sun.jbi.binding.security.HttpErrorResponseException; 
import com.sun.jbi.internal.security.SecurityService;
import com.sun.jbi.internal.security.ThreadLocalContext;

import java.net.URL;
import java.net.URLConnection;
import java.security.cert.X509Certificate;
import java.util.logging.Logger;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;

import javax.security.auth.Subject;
import javax.servlet.http.HttpServletRequest;



/**
 * HttpsSecurityHandler defines a set of methods which can be used by a Http binding.
 * 
 * @author Sun Microsystems, Inc.
 */
public class HttpSecurityHandler
    implements com.sun.jbi.binding.security.HttpSecurityHandler
{
    /** The Logger to use */
    private  Logger mLogger = null;
    
    /** The SSL Helper. */
    private SSLHelper mSSLHelper;
    
    /** The String Translator to use. */
    private StringTranslator mTranslator;
    
    /**
     * Constructor.
     *
     * @param sslHelper is the SSLHelper instance.
     */
    public HttpSecurityHandler(SSLHelper sslHelper)
    {
        mSSLHelper = sslHelper;
        mTranslator = SecurityService.getStringTranslator(HttpConstants.PACKAGE);
        mLogger = Logger.getLogger(
            com.sun.jbi.internal.security.Constants.PACKAGE);
    }
    
    /**
     * Authenticate a HttpServletRequest.
     *
     * If the Endpoint requires SSL Client Authentication, this method gets the Client 
     * Certificate from the request and authenticates the Sender. If a Client Certificate
     * is missing an exception is thrown. 
     *
     * If the Endpoint does not require SSL Client Authentication none of the above steps
     * are performed and an empty Subject is returned. This method does not return a null
     * Subject to avoid NullPOinterExceptions.
     *
     * @param request is the HttpServletRequest.
     * @param endpoint is the targeted Endpoint 
     * @param subject is the Sender subject, if the subject is null a new one is 
     * created.
     * @throws HttpErrorResponseException if a HttpErrorResponse is to be sent back to the
     * client.
     * @return the authenticated Sender Subject.
     */
    public Subject authenticateSenderRequest(HttpServletRequest request, 
        Endpoint endpoint, Subject subject)
        throws HttpErrorResponseException
    {
        
        if ( subject == null )
        {
            // -- If there is a Subject associated with the current thread use that.
            Subject subj = ThreadLocalContext.getLocalSubject();
            subject = ( (subj == null) ? new Subject() : subj );
        }    
        
        TlsContext endptTlsCtx = mSSLHelper.getTlsContext(endpoint);
        
        if ( endptTlsCtx != null )
        {  
            if ( endptTlsCtx.isClientAuthRequired() )
            {
                X509Certificate cert = getClientCertificate(request);
                if (cert == null )
                {
                    throwClientCertRequiredException();
                }
                updateSenderSubject(cert, subject);
            }
        }

        return subject;
        
    }
    
    /**
     * Authenticate the Sender Request by getting the Sender identity from the
     * Certificate.
     *
     * @param cert is the trusted X.509 Certificate.
     * @param endpoint is the targeted Endpoint 
     * @param subject is the Subject to be updated. 
     * @throws HttpErrorResponseException if authentication of the request fails.
     * @return the authenticated Sender Subject
     */
    public Subject authenticateSenderRequest(X509Certificate cert, 
        Endpoint endpoint, Subject subject)
        throws HttpErrorResponseException
    {
        if ( subject == null )
        {
            subject = new Subject();
        }     
        
        TlsContext endptTlsCtx = mSSLHelper.getTlsContext(endpoint);
        
        if ( endptTlsCtx != null )
        {  
            if ( endptTlsCtx.isClientAuthRequired() )
            {
                if (cert == null )
                {
                    throwClientCertRequiredException();
                }
                updateSenderSubject(cert, subject);
            }
        }

        return subject;
        
    }
    
    /**
     * Get the Client Certificate from the request
     *
     * @param request is the HttpServletRequest
     * @return the X.509 Client certificate
     * @throws HttpErrorResponseException if the Client Certificate is not found.
     */
    private X509Certificate getClientCertificate(HttpServletRequest request)
        throws HttpErrorResponseException
    {
        if (request.isSecure())
        {
            X509Certificate[] certs = (X509Certificate[]) request.
                getAttribute(HttpConstants.X509CERT_SERVLET_REQUEST_ATTRIB);  
            if (certs != null) 
            { 
                if (certs[0] == null) 
                { 
                    throwClientCertRequiredException(request);
                }
                
                return certs[0];
                
            }
        } 
        throwClientCertRequiredException(request);
        
        // -- Should never reach here
        return null;
    }
    
    /**
     * Throws a HttpErrorResponseException for Client Certificate Required.
     *
     * @param request is the HttpServletRequest which does not have the certificate.
     * @throws HttpErrorResponseException with the required ErrorCode and ErrMsg
     */
    private void throwClientCertRequiredException(HttpServletRequest request)
        throws HttpErrorResponseException
    {
        HttpErrorResponseException httpError = 
            new HttpErrorResponseException(mTranslator.getString(
                HttpConstants.BC_ERR_CLIENT_CERT_REQUIRED_DETAIL, 
                    request.getRequestURL().toString() ) );
        
        httpError.setErrorCode(HttpConstants.CLIENT_CERTIFICATE_REQUIRED);
        httpError.setErrorMessage( mTranslator.getString(
                HttpConstants.BC_ERR_CLIENT_CERT_REQUIRED) );
        
        throw httpError;
    }
    
    /**
     * Throws a HttpErrorResponseException for Client Certificate Required.
     *
     * @throws HttpErrorResponseException with the required ErrorCode and ErrMsg
     */
    private void throwClientCertRequiredException()
        throws HttpErrorResponseException
    {
        HttpErrorResponseException httpError = 
            new HttpErrorResponseException(mTranslator.getString(
                HttpConstants.BC_ERR_CLIENT_CERT_REQUIRED) );
        
        httpError.setErrorCode(HttpConstants.CLIENT_CERTIFICATE_REQUIRED);
        httpError.setErrorMessage( mTranslator.getString(
                HttpConstants.BC_ERR_CLIENT_CERT_REQUIRED) );
        
        throw httpError;
    }
    
    /**
     * Throws a HttpErrorResponseException for User Authentication failures.
     *
     * @param userName is the name of the User that failed authentication.
     * @throws HttpErrorResponseException with the required ErrorCode and ErrMsg
     */
    private void throwAuthenticationFailedException(String userName)
        throws HttpErrorResponseException
    {
        HttpErrorResponseException httpError = 
            new HttpErrorResponseException(mTranslator.getString(
                HttpConstants.BC_ERR_CLIENT_CERT_AUTH_FAILED_DETAIL, 
                    userName ) );
        
        httpError.setErrorCode(HttpConstants.CLIENT_CERTIFICATE_INVALID);
        httpError.setErrorMessage( mTranslator.getString(
                HttpConstants.BC_ERR_CLIENT_CERT_AUTH_FAILED) );
        
        throw httpError;
        
    }
    
    /** 
     * Update the Subject based on the Certificate.
     *
     * @param cert is the X.509 Certificate
     * @param subject is the Subject to update.
     */
    private void updateSenderSubject(X509Certificate cert, Subject subject)
    {
        subject.getPrincipals().add(
            new com.sun.jbi.security.SSLClientPrincipal(
                cert.getSubjectX500Principal()));
        subject.getPublicCredentials().add(cert);    
        
        // -- Associate the Subject with the Current Thread.
        if ( ThreadLocalContext.getLocalSubject() == null ) 
        {
            ThreadLocalContext.setLocalSubject(subject);
        }
    }
    
    /**
     * Make this a secure Connection. The choice of using SSL3.0/TLS and the TrustStore
     * Keystore details should come from the endpoint/operation details.
     *
     * @param serverURL is the URL of the server to which a secure HTTPs connection is
     * to be established.
     * @param endpoint is the Endpoint on behalf of which the secure connection 
     * is being made.
     * @throws java.io.IOException on IO realted errors.
     * @return the secure HTTPs URL Connection
     */
    public URLConnection createSecureClientConnection(URL serverURL, Endpoint endpoint) 
        throws java.io.IOException         
    {
        URLConnection connection = serverURL.openConnection();

        if ( connection instanceof HttpsURLConnection )
        {    

            // -- Set the SSL Socket Factory for this instance
            SSLContext sslCtx = getSSLContext(endpoint);

            if (sslCtx != null )
            {
                ((HttpsURLConnection) connection).setSSLSocketFactory(
                    sslCtx.getSocketFactory());
            }

            HostnameVerifier hv = new DefaultHostnameVerifier(mTranslator);

            // -- Set the HostNameVerifier for the instance 
            ( (HttpsURLConnection) connection).setHostnameVerifier(hv);
        }        
        return connection;
    }
   
    
    /**
     *
     * @param endpoint is the Endpoint whose SSL Context is to be retrieved.
     * @return a initialized SSLContext, null if the default SSLContext is to be
     * used for the endpoint. If the Endpoint deployment registration has not yet
     * occured, there would be no TLS Context for it, in whicha case too a null is 
     * returned.
     */
    private SSLContext getSSLContext(Endpoint endpoint)
    {    
        TlsContext tlsCtx = mSSLHelper.getTlsContext(endpoint);
        
        SSLContext sslc = null;
        
        if ( tlsCtx != null )
        {
            sslc =  tlsCtx.getSSLContext();
        }
        return sslc;
    }
    
    
}
