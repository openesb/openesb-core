/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)SecurityProcessor.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  SecurityProcessor.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 */

package com.sun.jbi.internal.security.msg;

// -- Pre-JSR 196 classes
import com.sun.enterprise.security.jauth.AuthConfig;
import com.sun.enterprise.security.jauth.AuthParam;
import com.sun.enterprise.security.jauth.SOAPAuthParam;
import com.sun.enterprise.security.jauth.ClientAuthContext;
import com.sun.enterprise.security.jauth.ServerAuthContext;

import com.sun.jbi.internal.security.ComponentContext;
import com.sun.jbi.StringTranslator;
import com.sun.jbi.binding.security.Endpoint;
import com.sun.jbi.binding.security.MessageContext;
import com.sun.jbi.binding.security.MessageHandlerException;
import com.sun.jbi.binding.security.Interceptor;

import com.sun.jbi.internal.security.config.EndpointInfo;
import com.sun.jbi.internal.security.config.EndpointInfoImpl;
import com.sun.jbi.internal.security.config.EndpointSecurityConfig;
import com.sun.jbi.internal.security.config.MessageSecPolicy;

import com.sun.jbi.internal.security.KeyStoreManager;
import com.sun.jbi.internal.security.UserDomain;
import com.sun.jbi.internal.security.Constants;
import com.sun.jbi.internal.security.DeploymentObserver;
import com.sun.jbi.internal.security.LocalStringConstants;
import com.sun.jbi.internal.security.ThreadLocalContext;



import java.util.logging.Logger;
import java.util.HashMap;
import javax.security.auth.Subject;


/**
 * Implementation of the Deployment Listener and Interceptor.
 *
 * @author Sun Microsystems, Inc.
 */
public class SecurityProcessor
    implements DeploymentObserver, 
        Interceptor
{
    
    /**
     * Client Auth Modules
     */
    private HashMap mClientAuthContexts;
    
    /**
     * Server Auth Modules
     */
    private HashMap mServerAuthContexts;
    
    /** The Logger */
    private Logger mLogger = null;
    
    /** The Component Context. */
    private ComponentContext mCmpCtx;
    
    /** The StringTranslator Used for getting localized strings. */
    private StringTranslator mTranslator;
    
    /** The AuthType. */
    private String mAuthType;
    
    /** 
     * Creates a new instance of SecurityProcessor.
     *
     * @param cmpCtx is the ComponentContext
     */
    public SecurityProcessor(ComponentContext cmpCtx)
    {
        this( cmpCtx, "SOAP");
    }
    
    /** 
     * Creates a new instance of SecurityProcessor.
     *
     * @param cmpCtx is the ComponentContext
     * @param authLayer is the authnetication layer ( ex. SOAP )
     */
    public SecurityProcessor(ComponentContext cmpCtx, String authLayer)
    {
        mLogger = Logger.getLogger(
            com.sun.jbi.internal.security.Constants.PACKAGE);
        
        mAuthType = authLayer;
        mClientAuthContexts = new HashMap();
        mServerAuthContexts = new HashMap();
        mCmpCtx = cmpCtx;
        mTranslator = cmpCtx.getStringTranslator(Constants.PACKAGE);
    }
    
    /**
     * Process an incoming message. For an Inbound endpoint the incoming message is
     * a request being sent to invoke a particular operation in a Service. For an
     * Outbound endpoint the incoming message is a Response to a earlier message.
     *
     * @param endpoint is the deployed Endpoint which is the sink of the Message
     * @param operation is the operation being invoked
     * @param subject is the Subject to be updated with the senders Identity.
     * @param msgCtx is the MessageContext which is a wrapper around the message.
     * @throws MessageHandlerException on Errors
     */
    public void processIncomingMessage(final Endpoint endpoint, final String operation, 
        final MessageContext msgCtx, final Subject subject)
        throws MessageHandlerException
    {
        try
        {
            // -- Run in the Security Service Protection domain.
            java.security.AccessController.doPrivileged( 
                new java.security.PrivilegedExceptionAction() 
            {
                /**
                 * @return null
                 * @throws MessageHandlerException on execution errors.
                 */
                public Object run() throws Exception
                {
                    privProcessIncomingMessage(endpoint, operation, msgCtx, subject);
                    return null;
                }
            } );
        }
        catch ( java.security.PrivilegedActionException pex )
        {
            throw (MessageHandlerException) pex.getException();
        }
    }
    
    /**
     * Process an incoming message. For an Inbound endpoint the incoming message is
     * a request being sent to invoke a particular operation in a Service. For an
     * Outbound endpoint the incoming message is a Response to a earlier message.
     *
     * @param endpoint is the deployed Endpoint which is the sink of the Message
     * @param operation is the operation being invoked
     * @param subject is the Subject to be updated with the senders Identity.
     * @param msgCtx is the MessageContext which is a wrapper around the message.
     * @throws MessageHandlerException on Errors
     */
    private void privProcessIncomingMessage(Endpoint endpoint, String operation, 
        MessageContext msgCtx, Subject subject) 
        throws MessageHandlerException
    {
        
        // -- If the Subject is null, create a new one and set it on the thread ctx
        // -- this way JAAS Authenticator updates the same subject
        if ( subject == null )
        {
            if ( ThreadLocalContext.getLocalSubject() != null )
            {
                // -- This is an incoming request over HTTPS with Client Auth
                subject = ThreadLocalContext.getLocalSubject();
            }
            else
            {
                subject = new Subject();
            }
        }
        ThreadLocalContext.setLocalSubject(subject);
        
        HashMap options = new HashMap();
        try
        {
            if ( endpoint.getRole() == javax.jbi.messaging.MessageExchange.Role.CONSUMER)
            {
                mLogger.finest("Retrieving a Server Auth Context for Endpoint "
                    + endpoint.getEndpointName() + ":" 
                    + endpoint.getServiceName().toString()  
                    + " and operation " + operation);
                ServerAuthContext authModule = (ServerAuthContext) mServerAuthContexts
                    .get(getKey(endpoint, operation));

                if ( authModule != null )
                {
                    AuthParam authParam = getAuthParam(msgCtx, true);
                    authModule.validateRequest( authParam, subject, options);  
                    checkAuthParamForResponse(authParam, msgCtx);
                    
                }
                else
                {
                    mLogger.finest("No Server Auth Context defined for Endpoint "
                        + endpoint.getEndpointName() + ":" 
                        + endpoint.getServiceName().toString()  
                        + " and operation " + operation);
                }
                
            }
            else
            {
                mLogger.finest("Retrieving a Client Auth Context for Endpoint "
                    + endpoint.getEndpointName() + ":" 
                    + endpoint.getServiceName().toString() 
                    + " and operation " + operation);
                ClientAuthContext authModule = (ClientAuthContext) mClientAuthContexts
                    .get(getKey(endpoint, operation));
                
                if ( authModule != null )
                {
                    AuthParam authParam = getAuthParam(msgCtx, false);
                    authModule.validateResponse( authParam, subject, options);
                    checkAuthParamForResponse(authParam, msgCtx);
                }
                else
                {
                    mLogger.finest("No Client Auth Context defined for Endpoint "
                        + endpoint.getEndpointName() + ":" 
                        + endpoint.getServiceName().toString()  
                        + " and operation " + operation);
                }
            }  
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            String errMsg = mTranslator.getString(
                LocalStringConstants.ERR_PROCESSING_INCOMING_MESSAGE,
                    new Object[]{endpoint.getEndpointName(), 
                        endpoint.getServiceName().toString(), 
                        ex.getMessage(), });
                
            mLogger.severe(errMsg);
            
            throw new MessageHandlerException(errMsg, ex);
        }
    }
    
    /**
     * Process an incoming message. For an Inbound endpoint the incoming message is
     * a request being sent to invoke a particular operation in a Service. For an
     * Outbound endpoint the incoming message is a Response to a earlier message.
     *
     * @param endpoint is the deployed Endpoint which is the sink of the Message
     * @param operation is the operation being invoked
     * @param subject is the Subject which identifies the Sender.
     * @param msgCtx is the MessageContext which is a wrapper around the message.
     * @throws MessageHandlerException on Errors
     */
    public void processOutgoingMessage(final Endpoint endpoint, final String operation, 
        final MessageContext msgCtx, final Subject subject) 
        throws MessageHandlerException
    {
        try
        {
            // -- Run in the Security Service Protection domain.
            java.security.AccessController.doPrivileged( 
                new java.security.PrivilegedExceptionAction() 
            {
                /**
                 * @return null
                 * @throws MessageHandlerException on execution errors.
                 */
                public Object run() throws Exception
                {
                    privProcessOutgoingMessage(endpoint, operation, 
                        msgCtx, subject);
                    return null;
                }
            } );
        }
        catch ( java.security.PrivilegedActionException pex )
        {
            throw (MessageHandlerException) pex.getException();
        }
    }
    
    /**
     * Process an outbound message. For an Inbound endpoint the outgoind message is
     * a response being sent to an earlier request to the Endpoint to invoke a particular
     * operation in a Service. For an Outbound endpoint the outbound message is a Request
     * being sent to a remote Service to invoke a operation.
     *
     * @param endpoint is the deployed Endpoint which is the source of the Message
     * @param operation is the operation being invoked
     * @param subj is the Subject which identifies the Sender.
     * @param msgCtx is the MessageContext which is a wrapper around the message.
     * @throws MessageHandlerException on Errors
     */
    private void privProcessOutgoingMessage(Endpoint endpoint, String operation, 
        MessageContext msgCtx, Subject subj) 
        throws MessageHandlerException
    {
        ThreadLocalContext.setLocalSubject(subj);

        HashMap options = new HashMap();
        try
        {
            if ( endpoint.getRole() == javax.jbi.messaging.MessageExchange.Role.CONSUMER )
            {
                mLogger.finest("Retrieving a Server Auth Module for Endpoint "
                    + endpoint.getEndpointName() + ":" 
                    + endpoint.getServiceName().toString() 
                    + " and operation " + operation);
                ServerAuthContext authModule = (ServerAuthContext) mServerAuthContexts
                    .get(getKey(endpoint, operation));

                if ( authModule != null )
                {
                    authModule.secureResponse(getAuthParam(msgCtx, false), subj, options);
                }
                else
                {
                    mLogger.finest("No Server Auth Context defined for Endpoint "
                        + endpoint.getEndpointName() + ":" 
                        + endpoint.getServiceName().toString()  
                        + " and operation " + operation);
                }
            }
            else
            {
                mLogger.finest("Retrieving a Client Auth Context for Endpoint "
                    + endpoint.getEndpointName() + ":" 
                    + endpoint.getServiceName().toString() 
                    + " and operation " + operation);
                ClientAuthContext authModule = (ClientAuthContext) mClientAuthContexts
                    .get(getKey(endpoint, operation));
                
                if ( authModule != null )
                {
                    authModule.secureRequest( getAuthParam(msgCtx, true), subj, options);
                }
                else
                {
                    mLogger.finest("No Client Auth Context defined for Endpoint "
                        + endpoint.getEndpointName() + ":" 
                        + endpoint.getServiceName().toString()  
                        + " and operation " + operation);
                }
            }   
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            String errMsg = mTranslator.getString(
                LocalStringConstants.ERR_PROCESSING_OUTGOING_MESSAGE,
                new Object[]{endpoint.getEndpointName(), 
                    endpoint.getServiceName().toString(), 
                    ex.getMessage(), });
            mLogger.severe(errMsg);
            
            throw new MessageHandlerException(errMsg, ex);
        }
    }
    
    /**
     * Unregister an Endpoint deployment.
     *
     * @param endpoint is the Endpoint being undeployed
     */
    public void unregisterEndpointDeployment(Endpoint endpoint)
    {
        
        java.util.Iterator opItr = endpoint.getOperationNames();
        while ( opItr.hasNext() )
        {
            String opName = (String) opItr.next();
            if ( endpoint.getRole() == javax.jbi.messaging.MessageExchange.Role.CONSUMER )
            {
                mServerAuthContexts.remove(getKey(endpoint, opName));
                mServerAuthContexts.remove(getKey(endpoint, 
                    Constants.DEFAULT_OPERATION_NAME));
            }
            else
            {
                mClientAuthContexts.remove(getKey(endpoint, opName));
                mClientAuthContexts.remove(getKey(endpoint, 
                    Constants.DEFAULT_OPERATION_NAME));
            }   
        } 
    }
    
    /**
     * Register an endpoint deployment. 
     *
     * @param endpoint is the Endpoint being deployed.
     * @param epDeployConfig is the Endpoint Security Configuration.
     * @throws Exception on Errors.
     */
    public void registerEndpointDeployment(final Endpoint endpoint, 
        final EndpointSecurityConfig epDeployConfig)
        throws Exception
    {
        try
        {
            // -- Run in the Security Service Protection domain.
            java.security.AccessController.doPrivileged( 
                new java.security.PrivilegedExceptionAction() 
            {
                /**
                 * @return null
                 * @throws MessageHandlerException on execution errors.
                 */
                public Object run() throws Exception
                {
                    privRegisterEndpointDeployment(endpoint, epDeployConfig);
                    return null;
                }
            } );
        }
        catch ( java.security.PrivilegedActionException pex )
        {
            throw (Exception) pex.getException();
        }
    }
    
    /**
     * Register an endpoint deployment. 
     *
     * @param endpoint is the Endpoint being deployed.
     * @param epDeployConfig is the Endpoint Security Configuration.
     * @throws Exception on Errors.
     */
    private void privRegisterEndpointDeployment(Endpoint endpoint, 
        EndpointSecurityConfig epDeployConfig)
        throws Exception
    {
        // -- Create the CallbackHandler based on the deployment config
        javax.security.auth.callback.CallbackHandler handler = 
            createCallbackHandler(epDeployConfig.getSecurityContext());
        
        // -- Get the AuthModule Factory
        AuthConfig factory = AuthContextFactoryLocator.getAuthContextFactory();
        
        // -- Create and cache the handler for each endpoint operation
        java.util.Iterator opItr = endpoint.getOperationNames();
        while ( opItr.hasNext() )
        {
            String opName = (String) opItr.next();

            MessageSecPolicy appPolicy = 
                getMsgSecPolicy(endpoint, epDeployConfig, opName);
            
            // -- Determine the Provider Id to be used, order of priority
            //    1. Operation Level Provider Id
            //    2. Endpoint Level Provider Id
            //    3. Default Container Provider Id.
            String providerId = ( appPolicy.getMessageProviderId() == null ? 
                epDeployConfig.getSecurityContext().getMessageProviderId() : 
                appPolicy.getMessageProviderId());
                
            if ( endpoint.getRole() == javax.jbi.messaging.MessageExchange.Role.CONSUMER )
            {
                
                mLogger.finest("Registering a Server Auth Context for Endpoint "
                    + endpoint.getEndpointName() + " in Service " 
                    + endpoint.getServiceName().toString() + " with provider id "
                    + providerId);
                mServerAuthContexts.put(getKey(endpoint, opName),
                    factory.getServerAuthContext(mAuthType, providerId, 
                        appPolicy.getRequestPolicy(), appPolicy.getResponsePolicy(), 
                        handler));
            }
            else
            {
                mLogger.finest("Registering a Client Auth Context for Endpoint "
                    + endpoint.getEndpointName() + " in Service " 
                    + endpoint.getServiceName().toString() + " with provider id "
                    + providerId);
                
                // -- There seems to be  a bug in AS, AuthConfig does not return
                // -- the default ClientProvider even when it has been configured
                mClientAuthContexts.put(getKey(endpoint, opName),
                    factory.getClientAuthContext(mAuthType, providerId, 
                        appPolicy.getRequestPolicy(), appPolicy.getResponsePolicy(), 
                        handler));
            }
            
        }  
                
        // -- Add the default operation
        MessageSecPolicy appPolicy = 
                getMsgSecPolicy(endpoint, epDeployConfig, 
                Constants.DEFAULT_OPERATION_NAME);
        
        // -- Determine the Provider Id to be used, order of priority
        //    1. Operation Level Provider Id
        //    2. Endpoint Level Provider Id
        //    3. Default Container Provider Id.
        String providerId = ( appPolicy.getMessageProviderId() == null ? 
            epDeployConfig.getSecurityContext().getMessageProviderId() : 
            appPolicy.getMessageProviderId());
                
        
        if ( endpoint.getRole() == javax.jbi.messaging.MessageExchange.Role.CONSUMER )
        {
            mServerAuthContexts.put(getKey(endpoint, Constants.DEFAULT_OPERATION_NAME),
                factory.getServerAuthContext(mAuthType, providerId, 
                    appPolicy.getRequestPolicy(), appPolicy.getResponsePolicy(), 
                    handler));
        }
        else
        {
            mClientAuthContexts.put(getKey(endpoint, Constants.DEFAULT_OPERATION_NAME),
                factory.getClientAuthContext(mAuthType, providerId, 
                    appPolicy.getRequestPolicy(), appPolicy.getResponsePolicy(), 
                    handler));
        }
    }
    
    /**
     * Get a Key based on operation and endpoint
     * @return the Key generated from the Endpoint. The Key is of the
     * form TargetNamespace:Service:Endpoint:Operation
     * @param endpt is the Endpoint
     * @param operation is the operation
     */
    private String getKey(Endpoint endpt, String operation)
    {
        StringBuffer strBuffer = new StringBuffer();
        String separator = ":";
        strBuffer.append(endpt.getServiceName().getNamespaceURI());
        strBuffer.append(separator);
        strBuffer.append(endpt.getServiceName().getLocalPart().trim());
        strBuffer.append(separator);
        strBuffer.append(endpt.getEndpointName().trim());
        strBuffer.append(separator);
        strBuffer.append(operation);

        return strBuffer.toString();
    }
    
    
    /**
     * @param deplSecCtx is the deployment Security Context.
     * @return the CallbackHandler instance. If the SecurityService has not been
     * initialized this method returns null.
     */
    public javax.security.auth.callback.CallbackHandler createCallbackHandler(
        com.sun.jbi.internal.security.config.SecurityContext   deplSecCtx)
    {
        if ( com.sun.jbi.internal.security.SecurityService.
        getSecurityConfiguration () != null )
        {

            // -- KeyManagement Environment
            KeyStoreManager kmgr =
                com.sun.jbi.internal.security.SecurityService.getKeyStoreManager (
                    deplSecCtx.getKeyStoreManagerName ());
            kmgr = ( (kmgr == null) ?  com.sun.jbi.internal.security.SecurityService.
                getDefaultKeyStoreManager () : kmgr);          
            
            // -- UserDomain, Authenticator
            UserDomain ud = com.sun.jbi.internal.security.SecurityService.
                getUserDomain (deplSecCtx.getUserDomainName ());
            
            ud = ( (ud == null) ? com.sun.jbi.internal.security.SecurityService.
                getDefaultUserDomain () : ud );
            
            // -- create the Authenticator and pass it to the CallbackHandler
            com.sun.jbi.internal.security.auth.Authenticator ator =
                com.sun.jbi.internal.security.auth.AuthenticatorFactory.
                createAuthenticator (ud);
            
            return new com.sun.jbi.internal.security.callback.ProxyCallbackHandler (
                kmgr, ator);

        }
        return null;
    }
    
    /**
     * Determine Message Security Policy
     *
     * @param ep is the Endpoint whose security policy is to be determined.
     * @param epConfig is the Endpoint deployment Security Configuration.
     * @param opName is the operation name.
     * @return the MessageSecPolicy for the Endpoint / operation
     * @throws Exception on errors
     */
    private MessageSecPolicy getMsgSecPolicy(Endpoint ep,
        EndpointSecurityConfig epConfig, 
        String opName)
        throws Exception
    {
        EndpointInfo depEp = new EndpointInfoImpl(ep);
        
        if ( epConfig.getEndpointInfo() == null )
        {
            // -- This is a default configuration, not specific to any endpoint
            // -- the message security policy is that of the container.
            return epConfig.getDefaultMessagePolicy();
        }
        
        
        if ( !epConfig.getEndpointInfo().equals(depEp) )
        {
            throw new Exception(mTranslator.getString(
                LocalStringConstants.BC_ERR_INVALID_DEPL_CONFIG,
                
                "{" + epConfig.getEndpointInfo().getTargetNamespace() + "}" 
                + ":" + epConfig.getEndpointInfo().getServiceName()
                + ":" + epConfig.getEndpointInfo().getName(),
                 
                 "{" + depEp.getTargetNamespace() + "}" 
                + ":" + depEp.getServiceName()
                + ":" + depEp.getName()));
        }
        
        MessageSecPolicy resPolicy = epConfig.getMessagePolicy (opName);
        
        // -- Get the default Endpoint Policy if one is defined
        if ( resPolicy == null )
        {
            resPolicy = epConfig.getMessagePolicy(Constants.DEFAULT_OPERATION_NAME);
        }
        
        // -- The Container Level Policy applies.
        if ( resPolicy == null )
        {
            resPolicy = epConfig.getDefaultMessagePolicy();    
        }
        
        return resPolicy; 
    }
    
    /**
     * Create a request AuthParam from the MessageContext.
     *
     * @param msgCtx is the MessageContext.
     * @param isRequest indicates whether the auth param is for a request or response.
     * @return the AuthParam
     */
    private AuthParam getAuthParam(MessageContext msgCtx, boolean isRequest)
    {
        if ( mAuthType.equals("SOAP") )
        {
            if ( isRequest )
            {
                return new SOAPAuthParam(
                    (javax.xml.soap.SOAPMessage) msgCtx.getMessage(), 
                    null);
            }
            else
            {
                return new SOAPAuthParam(null, 
                    (javax.xml.soap.SOAPMessage) msgCtx.getMessage());
            }
        }
        
        // -- Shasta does not support any other Auth Type, so this statement is
        // -- never reached.
        return null;
        
    }
    
    /**
     *
     * @param authParam is the AuthParam 
     * @param msgCtx is the Message Context which is updated with the response message.
     */
    private void checkAuthParamForResponse(AuthParam authParam, MessageContext msgCtx )
    {
        if ( authParam instanceof SOAPAuthParam )
        {
            SOAPAuthParam soapParam = (SOAPAuthParam) authParam;
            
            if ( soapParam.getResponse() != null )
            {
                msgCtx.setResponseMessage( soapParam.getResponse() );
            }
        }
    }
    
}
