/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)DeploymentNotifier.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  DeploymentNotifier.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on November 11, 2004, 10:04 PM
 */

package com.sun.jbi.internal.security;

/**
 *
 * @author Sun Microsystems, Inc.
 */
public interface DeploymentNotifier
{
    /**
     * Add a DeploymentObserver.
     *
     * @param obs is an observer.
     */
    void addDeploymentObserver(DeploymentObserver obs);
    
    /**
     * Remove a DeploymentObserver.
     *
     * @param obs is an observer.
     */
    void removeDeploymentObserver(DeploymentObserver obs);
    
    
}
