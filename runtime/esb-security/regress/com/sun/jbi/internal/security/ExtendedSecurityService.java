/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ExtendedSecurityService.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  SampleSecurityService.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on December 19, 2004, 11:11 AM
 */

package com.sun.jbi.internal.security;

import com.sun.jbi.binding.security.SecurityHandler;
import com.sun.jbi.internal.security.config.SecurityConfiguration;

import java.io.File;

/**
 * Sacffolded Security Service to be used for testing purposes.
 *
 * @author Sun Microsystems, Inc.
 */
public class ExtendedSecurityService
    extends com.sun.jbi.internal.security.SecurityService
{
    
    /** 
     * Creates a new instance of SampleSecurityService. 
     * This constructor is used by the unit tests. Creating
     * a security Service this way does away with the need to init the
     * service.
     *
     * @param secConfig is the Security Service configuration. 
     */
    public ExtendedSecurityService (SecurityConfiguration secCfg)
        throws Exception
    {
        super();
        sSecInstCfg = secCfg;
        sTranslator = Util.getStringTranslator("com.sun.jbi.internal.security");
        sEnvCtx = new SampleEnvironmentContext();
        initKeyStoreManagers();
        initUserDomains();
        checkState();
    }
    
    /**
     * Get the Schema Dir
     *
     * @return the Schema Dir
     */
    public String getSchemaDir()
    {
        if ( System.getProperty("junit.srcroot")  != null )
        {
            String srcroot = System.getProperty("junit.srcroot") +
                File.separator + "runtime" +
                File.separator + "esb-security";              // open-esb build

            java.io.File f = new java.io.File(srcroot);
            if (! f.exists())
            {
                srcroot = System.getProperty("junit.srcroot") +
                    File.separator + "shasta" +
                    File.separator + "security";      // mainline/whitney build
            }

            return srcroot + File.separator + "schema" + File.separator;
        }
        else if (System.getProperty("com.sun.aas.installRoot") != null )
        {
            return System.getProperty("com.sun.aas.installRoot") 
                + File.separator + "jbi" 
                + File.separator + "schemas" ;
        }
        return null;
    }
    
    
}
