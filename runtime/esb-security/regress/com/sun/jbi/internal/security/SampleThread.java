/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)SampleThread.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  TestThread.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on February 6, 2005, 8:13 PM
 */

package com.sun.jbi.internal.security;

import javax.security.auth.Subject;
import javax.security.auth.x500.X500Principal;

public class SampleThread extends Thread
{

    public void run ()
    {
        try
        {
            modifySubject(getName());
            checkMySubject(getName());
            modifySubject(getName() + "-modified-" + getName());
            checkMySubject(getName() + "-modified-" + getName());
        }
        catch ( Exception ex)
        {
           ex.printStackTrace();
           throw new IllegalStateException(ex.getMessage());
        }
        
    }
    
    private void modifySubject(String tid)
        throws Exception
    {
            createSubjectInTLS (tid);
            sleep (3000);
    }
    
    /**
     * Update the X500 Principal in the Subject.
     */
    private void createSubjectInTLS (String tid)
        throws Exception
    {
            boolean subjectLinked = true;
            Subject subject = ThreadLocalContext.getLocalSubject();
            
            if ( subject == null )
            {
                subject = new Subject();
                subjectLinked = false;
            }

            java.util.Set pSet = subject.getPrincipals();
            java.util.Iterator itr = pSet.iterator();
            
            // -- Clean the Principal Set
            while ( itr.hasNext() )
            {
                Object obj = itr.next();
                pSet.remove(obj);
                obj = null;
            }
            
            pSet.add (new X500Principal ("CN=" + tid));
            
            if ( !subjectLinked )
            {
                ThreadLocalContext.setLocalSubject (subject);
            }
    }
    
    /**
     * Checks if the Principal name is as expected.
     *
     * @throws Exception if the Prinicipal name is not as expected.
     */
    private void checkMySubject(String expectedSubject)
        throws Exception
    {
        Subject mySubject = ThreadLocalContext.getLocalSubject();
        if ( mySubject != null )
        {
            java.util.Iterator itr = mySubject.getPrincipals ().iterator ();
            if ( itr.hasNext () )
            {
                if ( !expectedSubject.equals(((X500Principal) itr.next ()).getName()) )
                {
                    throw new Exception("Thread " + getName() + " expected Subject "
                        + expectedSubject + " got " 
                        + ((X500Principal) itr.next ()).getName());
                };
            }
        }
        else
        {
            throw new Exception( "Thread " + getName() + " Subject is null :( ");
        }
    }
    
}
