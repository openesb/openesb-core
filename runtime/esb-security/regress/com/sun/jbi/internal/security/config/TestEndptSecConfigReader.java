/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestEndptSecConfigReader.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  TestEndptSecConfigReader.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on March 21, 2005, 2:35 PM
 */

package com.sun.jbi.internal.security.config;

import com.sun.jbi.internal.security.Util;
import java.io.File;

/**
 *
 * @author Sun Microsystems, Inc.
 */
public class TestEndptSecConfigReader extends junit.framework.TestCase
{
    private String mConfig;
    private String mConfigSchemaDir;
        
    /** Creates a new instance of TestSecurityInstallConfigReader */
    public TestEndptSecConfigReader (String testname)
    {
        super(testname);
        String srcroot = System.getProperty("junit.srcroot");
        String security = "/runtime/esb-security";        // open-esb build

        java.io.File f = new java.io.File(srcroot + security);
        if (! f.exists())
        {
            security = "/shasta/security";       // mainline/whitney build
        }

        mConfig = srcroot + security +
            "/regress/deployconfig/EndptDeployConfig.xml";
        mConfigSchemaDir = srcroot + security + "/schema";
    }
    
    /**
     *
     */
    public void testRead()
        throws Exception
    {
        try
        {
            EndptSecConfigReader reader = new EndptSecConfigReader(Util.getStringTranslator(
                    "com.sun.jbi.internal.security"), mConfigSchemaDir );
            EndpointSecurityConfig epSecConfig = reader.read(new java.io.File(mConfig));

            // -- Is the Message Policy Read correctly ?
            MessageSecPolicy msgPolicy = epSecConfig.getMessagePolicy("getTicker");
            assertNotNull(msgPolicy);
            
            assertEquals("OperationMessageProvider", msgPolicy.getMessageProviderId());
            
            com.sun.enterprise.security.jauth.AuthPolicy reqPolicy = msgPolicy.getRequestPolicy();
            assertNotNull(reqPolicy);
            assertTrue ( reqPolicy.isContentAuthRequired());
            assertTrue ( reqPolicy.isRecipientAuthRequired());
            assertTrue ( reqPolicy.isRecipientAuthBeforeContent());
            
            com.sun.enterprise.security.jauth.AuthPolicy respPolicy = msgPolicy.getResponsePolicy();
            assertNotNull(respPolicy);
            assertTrue ( respPolicy.isSenderAuthRequired());
            assertTrue ( respPolicy.isRecipientAuthRequired());
            assertFalse ( respPolicy.isRecipientAuthBeforeContent());
            
            // -- Is the Security Environment Read Correctly
            SecurityContext secCtx = epSecConfig.getSecurityContext();
            
            assertNotNull(secCtx);
            assertEquals("file", secCtx.getUserDomainName());
            assertEquals("javaks", secCtx.getKeyStoreManagerName());
            assertEquals("EndpointMessageProvider", secCtx.getMessageProviderId());
            
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            fail();
        }
        
        
    }
    
    
    
}
