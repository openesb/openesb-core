package com.sun.esb.eventmanagement.impl;

import com.sun.esb.eventmanagement.api.InstanceIdentifier;
import com.sun.jbi.platform.PlatformContext;
import javax.management.MBeanServerConnection;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class StandaloneInstanceIdentifier implements InstanceIdentifier {

    private PlatformContext mPlatformContext;
    private MBeanServerConnection mMBeanServer;
    
    public StandaloneInstanceIdentifier(PlatformContext aPlatformContext) {
        mPlatformContext = aPlatformContext;
        mMBeanServer = mPlatformContext.getMBeanServer();
    }
    
    @Override
    public String getInstanceUniqueName() {
        return mPlatformContext.getInstanceName();
    }

    @Override
    public String getInstanceUniquePort() {
        return mPlatformContext.getJmxRmiPort();
    }

    @Override
    public String getHttpPort() {
        //TODO: How to handle HTTP Port ?
        return "4848";
    }

    @Override
    public String getDomainAdminPort() {
        return mPlatformContext.getJmxRmiPort();
    }

    @Override
    public Boolean isSecureAdminPort() {
        return (mPlatformContext.getSecurityProvider() != null);
    }

    @Override
    public String getInstanceHttpPort() {
        //TODO: How to handle HTTP Port ?
        return "4848";
    }
    
}
