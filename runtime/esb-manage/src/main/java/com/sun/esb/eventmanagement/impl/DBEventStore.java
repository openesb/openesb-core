/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)DBEventStore.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */

package com.sun.esb.eventmanagement.impl;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.sun.esb.eventmanagement.api.AlertPersistenceDBType;
import com.sun.esb.eventmanagement.api.NotificationEvent;
import com.sun.esb.management.common.ManagementRemoteException;
import com.sun.jbi.platform.PlatformContext;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.management.InstanceNotFoundException;
import javax.management.MBeanException;
import javax.management.MBeanServer;
import javax.management.MBeanServerConnection;
import javax.management.Notification;
import javax.management.NotificationFilterSupport;
import javax.management.NotificationListener;
import javax.management.ObjectName;
import javax.management.ReflectionException;
import javax.management.timer.Timer;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 */
public class DBEventStore implements NotificationListener,EventStoreConstants {

    
    private static final Logger mLogger;
    static {
        mLogger = Logger.getLogger(DBEventStore.class.getName());
     
     }
    private static boolean isDebugEnabled;
    private AlertPersistenceConfiguration mAlertPersistenceConfiguration;
    private String mJndiName;
    private DataSource mDataSource;

    private MBeanServerConnection mMBeanServer;
    Timer mTimer;
    private Boolean mShutdownInProgress;
    private Integer mTimerNotificationId;
    private ObjectName mTimerObjectName;   
    private NotificationFilterSupport mTimerFilter;
    private boolean isTimerRunning;
    private static DBEventStore mDBEventStore;
    private PlatformContext mPlatformContext;

    public static synchronized DBEventStore getInstance(AlertPersistenceConfiguration  aAlertPersistenceConfiguration ,
            MBeanServerConnection aMBeanServer,PlatformContext aPlatformContext) {
        if(mDBEventStore == null) {
            mDBEventStore =  
                new DBEventStore(aAlertPersistenceConfiguration,aMBeanServer,aPlatformContext); 
        }  
        return mDBEventStore;
        
    }
    
    /**
     * 
     */
    private DBEventStore(AlertPersistenceConfiguration  aAlertPersistenceConfiguration ,
            MBeanServerConnection aMBeanServer,PlatformContext aPlatformContext) {
        super();
        mMBeanServer = aMBeanServer;
        mPlatformContext = aPlatformContext;
        mAlertPersistenceConfiguration = aAlertPersistenceConfiguration;
        mJndiName = aAlertPersistenceConfiguration.getDataSourceJndiName();
        mTimerFilter = new NotificationFilterSupport();
        if(mPlatformContext.isAdminServer()) {
            registerPolicyExecutionTimer();
        }
        mShutdownInProgress = false;
    }

    private void registerPolicyExecutionTimer() {
        try {
            
            mTimer = new Timer();
            mTimerObjectName = new ObjectName(EventManagementConstants.EVENT_MANAGEMENT_REMOVAL_POLICY_TIMER_MBEAN_NAME);
            if(mPlatformContext.isAdminServer() && ((MBeanServer) mMBeanServer).isRegistered(mTimerObjectName)) {
                return; // registered by the controller mbean
            }
            ((MBeanServer) mMBeanServer).registerMBean(mTimer, mTimerObjectName);
            hookUpToTimerService(mAlertPersistenceConfiguration.getPersistenceAlertPolicyExecInterval());
            if(mAlertPersistenceConfiguration.getPersistedAlertRemovalPolicyExecEnabled()) {
                // add notification interval prior to starting
                isTimerRunning = true;
                mTimer.start();
            }
        } catch (Exception e) {
             String bundleKey = "caps.management.server.alert.persistence.removal.policy.setup.error.log.msg";
             String message = AlertUtil.constructLogMessage(bundleKey);
             mLogger.log(Level.WARNING, message, e);
        }
    }

    public void unRegisterTimerService(){
        if(!mPlatformContext.isAdminServer()) {
            return; // not init. on instances therefore not need to unregister.
        } else {
            if(((MBeanServer) mMBeanServer).isRegistered(mTimerObjectName)== false) {
                return; // was already unreg. on the DAS
            }
           
        }
        mTimer.stop();
        unHookFromTimerService();
        synchronized(mShutdownInProgress) {
            mShutdownInProgress = true;
        }

        try{
            ObjectName name = new ObjectName(EventManagementConstants.EVENT_MANAGEMENT_REMOVAL_POLICY_TIMER_MBEAN_NAME);
            if ( mMBeanServer.isRegistered(name) ) {
                 mMBeanServer.unregisterMBean(name);
            }
        }catch(Exception e){
            String bundleKey = "caps.management.server.alert.persistence.removal.policy.shutdown.error.log.msg";
            String message = AlertUtil.constructLogMessage(bundleKey);
            mLogger.log(Level.WARNING, message, e);
       }
    }


    public void setDataSourceJndiName(String aJndiName) {
        mJndiName = aJndiName;
    }

    public void deleteEvent(String aEventId) throws ManagementRemoteException {
       String deleteEventQuery = EVENT_STORE_DELETE_EVENT_BY_ID;
       String fixedQuery = fixQueryTableName(deleteEventQuery);
       deleteEvent(fixedQuery,aEventId);
    }
    
    public int getPersistedEventCount()  {
        int count = 0;
        try {
            count = executeCountQuery();
        } catch (ManagementRemoteException e) {
            return 0;
        }
        return count;
    }

    public void enablePolicyExecution(boolean aEnable) {
        if(!isTimerRunning && aEnable) {
            isTimerRunning = true;
            mTimer.start();
        } 
        if(isTimerRunning && !aEnable) {
            isTimerRunning = false;
            mTimer.stop();
        }
    }

    
    
    public void UpdatePolicyExecutionInterval(Long aNewInterval) {
            if(isTimerRunning) {
                mTimer.stop();
                unHookFromTimerService();
                hookUpToTimerService(aNewInterval);
                mTimer.start();
            } else {
                unHookFromTimerService();
                hookUpToTimerService(aNewInterval);
            }
    }

    public void initializeDataSource() throws ManagementRemoteException{
       
        if(mDataSource != null) {
            return;  // datasource was init. b4.
        }

        try {
            InitialContext ic = new InitialContext();
            mDataSource = (DataSource) ic.lookup(mJndiName);
        } catch (NamingException e) {
            Exception exception = AlertUtil.createManagementException(
                    "caps.management.server.alert.configuration.initialContext.error", 
                    null, e);
             throw new ManagementRemoteException(exception); 
        }
        if(mDataSource == null)  {
            String args[] = {mJndiName};
            Exception e= AlertUtil.createManagementException("caps.management.server.alert.configuration.invalidDataSource.error", args, null);
            throw new ManagementRemoteException(e);
        }
        
    }

    public void reInitializeDataSource(String aNewJndi) {
        

        try {
            InitialContext ic = new InitialContext();
            mDataSource = (DataSource) ic.lookup(aNewJndi);
        } catch (Exception e) {
            String bundleKey = "caps.management.server.alert.reconfiguration.initialContext.error";
            String message = AlertUtil.constructMessage(bundleKey)+aNewJndi;
            mLogger.log(Level.WARNING, message, e);
        }   
        
    }

    private void deleteEvent(String aSql,String aParamValue) throws ManagementRemoteException {
        Connection lConnection = null;
        PreparedStatement ps = null;
        try {
             lConnection = mDataSource.getConnection();
                if(lConnection == null) {
                    return; // non available
                }
     
                 ps = lConnection.prepareStatement(aSql);
                ps.setString(1, aParamValue);
                ps.execute();
                lConnection.commit();
         } catch (SQLException sqle) {
             String args[] = {aSql};
             Exception e= AlertUtil.createManagementException("caps.management.client.alert.runtime.delete.error", args, sqle);
             throw new ManagementRemoteException(e);
         } finally {
           try {
               if(ps != null) {
                   ps.close();
               }
               if(lConnection != null) {
                   lConnection.close();
               }
           }  catch(Exception e) {
               ps = null;
               lConnection = null;
           }             
         }
     }
     
      private int executeCountQuery () throws ManagementRemoteException {
      
          String lCountQuery = EVENT_STORE_EVENT_COUNT;
          String fixedQuery = fixQueryTableName(lCountQuery);

          if(mDataSource == null) {
              String args[] = {mJndiName};
              Exception e= AlertUtil.createManagementException("caps.management.server.alert.configuration.invalidDataSource.error", args, null);
              throw new ManagementRemoteException(e);
            }
          
          Connection lConnection = null;
          Statement stmt = null;
        
          try {
              lConnection = mDataSource.getConnection();
             if(lConnection == null) {
                 return 0;
             }
             stmt = lConnection.createStatement();
            
             ResultSet rset = stmt.executeQuery(fixedQuery);
             
             while (rset.next()) {               
                  return rset.getInt(1);               
             }
                        
          } catch (SQLException sqle) {
              String args[] = {fixedQuery};
              Exception e= AlertUtil.createManagementException("caps.management.client.alert.runtime.getcount.error", args, sqle);
              throw new ManagementRemoteException(e);
          } finally {
              try {
                 if(stmt != null) { 
                     stmt.close();
                 }
                 if(lConnection != null) 
                 {
                     lConnection.close();
                 }
              }  catch(Exception e) {
                  stmt = null;
                  lConnection = null;
             }                                 
          }            
      
          return 0;
      }
    

 
    public void save(NotificationEvent notificationEvent) throws ManagementRemoteException {

    if (mShutdownInProgress) {
        String bundleKey = "caps.management.server.alert.persistence.discard.event.msg";
        String message = AlertUtil.constructLogMessage(bundleKey);
        mLogger.log(Level.INFO,message);
       return;
    }
        
    String fixedQuery = null;
    Connection conn = null;
    PreparedStatement pStmt = null;
    Statement stmt = null;
    try {
       conn = mDataSource.getConnection();    
       if (conn==null) {
           Exception e= AlertUtil.createManagementException("caps.management.server.alert.runtime.database.connection.error", null,null);
           throw new ManagementRemoteException(e);
       }
       
       String query = INSERT_STATEMENT_OTHER;
       if(mAlertPersistenceConfiguration.getAlertPersistenceDBType() == AlertPersistenceDBType.ORACLE) {
           query = INSERT_STATEMENT_ORACLE;
       }

       
       
       fixedQuery = fixQueryTableName(query);
       pStmt = conn.prepareStatement(fixedQuery);
       
       pStmt.setLong(1, notificationEvent.getTimeStamp());           

       String phyHostName = notificationEvent.getPhysicalHostName() == null ? " ": notificationEvent.getPhysicalHostName();
       pStmt.setString(2, phyHostName);
       
       String envName = notificationEvent.getEnvironmentName() == null ? " ": notificationEvent.getEnvironmentName();
       pStmt.setString(3, envName);
       
       String lglHostName = notificationEvent.getLogicalHostName()== null ? " ": notificationEvent.getLogicalHostName();
       pStmt.setString(4, lglHostName);
       
       String serverType = notificationEvent.getServerType() == null ? " ": notificationEvent.getServerType();
       pStmt.setString(5, serverType);
       
       String serverName = notificationEvent.getServerName()== null ? " ": notificationEvent.getServerName();
       pStmt.setString(6, serverName);
       
       String componentType = notificationEvent.getComponentType() == null ? " ": notificationEvent.getComponentType();
       pStmt.setString(7, componentType);
       
       String compProjPathName = notificationEvent.getComponentProjectPathName() == null ? " ": notificationEvent.getComponentProjectPathName();
       pStmt.setString(8, compProjPathName);
       
       String componentName = notificationEvent.getComponentName() == null ? " ": notificationEvent.getComponentName();
       pStmt.setString(9, componentName);
       
       String type = notificationEvent.getType() == null ? " ": notificationEvent.getType();
       pStmt.setString(10, type);
       
       pStmt.setInt(11, notificationEvent.getSeverity());
       pStmt.setInt(12, notificationEvent.getOperationalState());
       
       String msgCode = notificationEvent.getMessageCode() == null ? " ": notificationEvent.getMessageCode();
       pStmt.setString(13, msgCode);
       
       String msgDetails = notificationEvent.getMessageDetails() == null ? " ": notificationEvent.getMessageDetails();
       pStmt.setString(14, msgDetails);
       
       pStmt.setInt(15, notificationEvent.getObservationalState());
       
       String depName = notificationEvent.getDeploymentName() == null ? " ": notificationEvent.getDeploymentName();
       pStmt.setString(16, depName);
       
       pStmt.executeUpdate();
       if(mLogger.isLoggable(Level.FINEST)) {
           mLogger.log(Level.FINEST, "Executed:{0}", fixedQuery);
       }
       
     
    } catch (Exception e) {
        String resourceKey = null;
        Object[] args = null;
        if(fixedQuery == null) {
            resourceKey = "caps.management.server.alert.runtime.connectionnotavaiableforinsert.error";
        } else {
            args = new String[] {fixedQuery};
            resourceKey = "caps.management.server.alert.runtime.insertalert.error";
        }
        Exception exception = AlertUtil.createManagementException( resourceKey, 
                args, e);
        throw new ManagementRemoteException(exception);
    } finally {
        try {
           if ( stmt!=null ) {
               stmt.close(); 
           }
           if ( pStmt!=null ) {
               pStmt.close();
           }
           if ( conn!=null ) {
               conn.close();
           }
        }  catch(Exception e) {
           Object[] args = new String[] {fixedQuery};
           Exception exception = AlertUtil.createManagementException(
                   "caps.management.server.alert.runtime.failtoclosestatementorconnection.error", 
                   args, e);
           throw new ManagementRemoteException(exception);
        }                                 
    }                                                   
}
    
   


    public void handleNotification(Notification notification, Object handback) {

        if(false == notification.getType().equals(EVENT_POLICY_FILTER_TYPE)) {
            // only handle policy removal timer notification
            return;
        }
        // handle policy execution here spawn thread to run the policy exec.
        synchronized(mAlertPersistenceConfiguration.getAlertRemovalPolicyTypeList()) {
            if(!mAlertPersistenceConfiguration.getPersistedAlertRemovalPolicyExecEnabled()) {
                return;
            }
        }
        
        new Thread(new Runnable() {
            public void run() {
              try {
                  List<AlertRemovalPolicyType> safeList = null;
                  synchronized(mAlertPersistenceConfiguration.getAlertRemovalPolicyTypeList()) {
                      if(mAlertPersistenceConfiguration.getAlertRemovalPolicyTypeList() != null && 
                              mAlertPersistenceConfiguration.getAlertRemovalPolicyTypeList().size() > 0) {
                          safeList = Collections.unmodifiableList(mAlertPersistenceConfiguration.getAlertRemovalPolicyTypeList());
                      } else {
                          return;
                      }
                  }
                  ExecutePolicy(safeList);
              }
              catch(Exception ex) {}
            }
          }).start();       
        
        
    }
    
    private void ExecutePolicy(List<AlertRemovalPolicyType> aPolicyList) throws ManagementRemoteException {
        synchronized(mShutdownInProgress) {
            if(mShutdownInProgress) {
                return;
            }
        }
        for (AlertRemovalPolicyType alertRemovalPolicyType : aPolicyList) {
            synchronized(mShutdownInProgress) {
                if(mShutdownInProgress) {
                    // abort policy execution if we get de-registered.
                    return;
                }
            }

            if(getPersistedEventCount() == 0) {
                return; // db is empty
            }
            
            String deleteSql = null;
            switch (alertRemovalPolicyType) {
                case ALERTS_LEVEL:
                    AlertLevelType alertLevel = 
                        mAlertPersistenceConfiguration.getPersistenceAlertLevelPolicyValue();
                    if(AlertLevelType.NONE == alertLevel) {
                        continue;
                    }
                    String alertLevelInt = alertLevel.getAlertLevelAsIntString();
                    deleteSql =  EVENT_STORE_DELETE_EVENT_BY_LEVEL;
                    String fixedQuery = fixQueryTableName(deleteSql);
                    deleteEvent(fixedQuery,alertLevelInt);
                    break;
                case ALERTS_COUNT:
                    long alertMaxCount = 
                            mAlertPersistenceConfiguration.getPersistenceAlertCountPolicyValue();
                    if(alertMaxCount > getPersistedEventCount()) {
                        continue; // no alerts over max count
                    }
                    //since derby does not support offset limit natively - we use portable
                    // logic to remove alert
                    removeExcessiveAlerts((int)alertMaxCount);
                    break;
                case ALERTS_AGE:
                    long alertAge = mAlertPersistenceConfiguration.getPersistenceAlertAgePolicyValue();
                    Long lastValidEventDate = (new Date()).getTime() - alertAge;
                    deleteSql = EVENT_STORE_DELETE_EVENT_BY_DATE;
                    fixedQuery = fixQueryTableName(deleteSql);
                    deleteEvent(fixedQuery,lastValidEventDate.toString());
                    break;
            }
        }
    }
    
    private void removeExcessiveAlerts(int aMaxRowCount) throws ManagementRemoteException{
        if(mDataSource == null) {
            return;
        }
        String fixedQuery = null;
        Connection conn = null;
        Statement stmt = null;
      
        try {
           conn = mDataSource.getConnection();
           if(conn == null) {
               return ; 
                       
           }
           stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
                   ResultSet.CONCUR_READ_ONLY);
           
           String query = EVENT_STORE_GET_EVENT_DATES;
           fixedQuery = fixQueryTableName(query);
           ResultSet rset = stmt.executeQuery(fixedQuery);
           
           rset.relative(aMaxRowCount);
           String currentRowDate = rset.getString(1);
           String deleteSql =  EVENT_STORE_DELETE_EVENT_BY_DATE;
           fixedQuery = fixQueryTableName(deleteSql);
           deleteEvent(fixedQuery,currentRowDate);

                      
        } catch (Exception e) {
            Object[] args = new String[] {fixedQuery};
            Exception lException = AlertUtil.createManagementException(
                    "caps.management.client.alert.runtime.delete.error", 
                    args, e);
            throw new ManagementRemoteException(lException);
        } finally {
            try {
               if(stmt != null) { 
                   stmt.close();
               }
               if(conn != null) 
               {
                   conn.close();
                   conn = null;
               }
            }  catch(Exception e) {
                stmt = null;
                conn = null;
            }                                 
        }            
    }
 
    private void hookUpToTimerService(Long aTimeIntervalForArchive){
        try {
            mTimerFilter.enableType(EVENT_POLICY_FILTER_TYPE);
            mMBeanServer.addNotificationListener(mTimerObjectName, this, mTimerFilter, null);
            // setup the parameters for the timer addNotification method
            Object[] paramsArray = {EVENT_POLICY_FILTER_TYPE,
                                    "alerts removal interval",
                                    null, new Date(), aTimeIntervalForArchive};
            String[] paramsSig = {"java.lang.String", "java.lang.String",
                                  "java.lang.Object", "java.util.Date",
                                  "long"};
            mTimerNotificationId =(Integer) mMBeanServer.invoke(mTimerObjectName,
                    "addNotification", paramsArray, paramsSig);
 
        }  catch (Exception ex) {
            String bundleKey = "caps.management.server.alert.runtime.timer.hook.error.msg";
            String message = AlertUtil.constructLogMessage(bundleKey);
            mLogger.log(Level.WARNING,message, ex);
        }
    }
    
    private void unHookFromTimerService(){
        try{
           if ( mMBeanServer.isRegistered(mTimerObjectName) ) {
               mMBeanServer.removeNotificationListener(mTimerObjectName, this);
               if(mTimerNotificationId != null) {
                   Object[] paramsArray = {mTimerNotificationId};
                   String[] paramsSig = {"java.lang.Integer"};
                   mMBeanServer.invoke(mTimerObjectName,"removeNotification", paramsArray, paramsSig);
               }
           }
       }catch(Exception e){
           String bundleKey = "caps.management.server.alert.runtime.timer.unhook.error.msg";
           String message = AlertUtil.constructLogMessage(bundleKey);
           mLogger.log(Level.WARNING,message, e.getMessage());
       }
    }    
    
     
    private String replaceQueryTokens(String src,String[] tokensReplacements) {
        String result = src;
        for (int index = 0; index < tokensReplacements.length; index++) {
            String constructToken = "?" + index;
            result = result.replaceAll(constructToken, tokensReplacements[index]);
        }
        return result;
    }
    
    
    
    private String fixQueryTableName(String originalQuery) {
        String fixedQuery = originalQuery.replace("NOTIFICATION_EVENT", 
                mAlertPersistenceConfiguration.getAlertTableName());
        return fixedQuery;
    }
    
}