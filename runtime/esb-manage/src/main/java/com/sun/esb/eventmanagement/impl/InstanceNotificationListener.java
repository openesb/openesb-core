package com.sun.esb.eventmanagement.impl;

import javax.management.Notification;
import javax.management.NotificationListener;
import javax.management.openmbean.CompositeData;

import com.sun.esb.eventmanagement.api.NotificationEvent;

public class InstanceNotificationListener implements NotificationListener {

    EventManagementControllerMBean mEventManagementControllerMBean;

    public InstanceNotificationListener(EventManagementControllerMBean aEventManagementControllerMBean) {
        mEventManagementControllerMBean = aEventManagementControllerMBean;
    }
    public void handleNotification(Notification notification, Object handback) {
        synchronized (mEventManagementControllerMBean) {
            mEventManagementControllerMBean.handleNotification(notification,handback);
        }
    }

}
