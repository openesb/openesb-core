/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ApplicationInterceptor.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.ee;

/**
 * ApplicationInterceptor represents a deployment interception SPI between the 
 * JBI runtime and the underlying platform.  By providing an implementation of 
 * this interface, the platform is able to perform one-time processing related
 * to application artifacts before operations on the service unit are carried out
 * in the target.
 *
 * @author Sun Microsystems, Inc.
 */
public interface ApplicationInterceptor
{
    /** Performs platform-specific application processing on a service unit
     *  for a given operation.
     *  @param operation name of the operation
     *  @serviceAssemblyName name of the service assembly targeted by the operation
     *  @serviceUnitName name of the service unit targeted by the operation
     *  @serviceUnitPath path to the deployable service unit archive
     *  @target the target (cluster/instance/domain) of the operation
     */
    String performAction(String operation,
        String serviceAssemblyName, 
        String serviceUnitName, 
        String serviceUnitPath,
        String target)
        throws Exception;
}
