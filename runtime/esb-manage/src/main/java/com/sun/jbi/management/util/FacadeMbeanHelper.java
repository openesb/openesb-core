/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)FacadeMbeanHelper.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.util;

import com.sun.jbi.ComponentType;
import com.sun.jbi.management.MBeanNames;

import javax.management.ObjectName;
import javax.management.MBeanServerConnection;

/**
 * A utility class for supporting Facade MBean Implementations
 *
 * @author Sun Microsystems, Inc.
 */
public class FacadeMbeanHelper
{

    public static final String JBI_DOMAIN = "com.sun.jbi";

    public static final String SERVICE_TYPE_KEY = "ServiceType";

    public static final String TARGET_KEY = "Target";

    public  static final String COMPONENT_NAME_KEY = "ComponentName";

    public static final String EQUAL = "=";

    public static final String COLON = ":";

    public static final String COMMA = ",";
    
    /**
     * Strip the JMX Exceptions from the exception chain and return the actual
     * embedded Exception thrown by the remote instance.
     *
     * @return the actual exception thrown by the remote instance
     */
    public static Throwable stripJmException(Throwable ex)
    {
        Throwable currEx = ex;
        
        while ( null != currEx.getCause() )
        {
            if ( !(currEx instanceof javax.management.JMException) 
                && !(currEx instanceof javax.management.JMRuntimeException))
            {
                return currEx;
            }
            currEx = currEx.getCause();
        }
        return currEx;
    }
    
    /**
     * @return the object name of the facade ComponentLifeCycle MBean for the Component
     */
    public static ObjectName 
    getComponentLifeCycleFacadeMBeanName(String componentName, ComponentType type, 
        String target, MBeanNames mbeanNames)
    {
        ObjectName compLCName = null;
        if ( ComponentType.BINDING.equals(type) )
        {
            compLCName = mbeanNames.getBindingMBeanName(componentName, 
                    MBeanNames.ComponentServiceType.ComponentLifeCycle, target);
        }
        else if ( ComponentType.ENGINE.equals(type) )
        {
            compLCName = mbeanNames.getEngineMBeanName(componentName, 
                    MBeanNames.ComponentServiceType.ComponentLifeCycle, target);
        }
        return compLCName;
    }
    
    /**
     * Get the ObjectNames of all facade ComponentLifeCycleMBeans registered in 
     * the MBean Server
     */
    public static ObjectName[] 
    getComponentLifeCycleObjectNames(MBeanServerConnection mbs, MBeanNames mbns, String target)
            throws javax.management.JMException, java.io.IOException
    {
        String objNamePattern = 
            JBI_DOMAIN + COLON + 
				TARGET_KEY         + EQUAL + target + COMMA + 
                SERVICE_TYPE_KEY   + EQUAL + MBeanNames.ComponentServiceType.ComponentLifeCycle + COMMA + 
                "*";
        ObjectName objName =  new ObjectName(objNamePattern);
   
        java.util.Set<ObjectName> nameSet = mbs.queryNames(objName, null);
        
        return nameSet.toArray(new ObjectName[nameSet.size()]);
        
    }
    
    /**
     * Get the ObjectName of the  facade ComponentLifeCycleMBean registered in 
     * the MBean Server for the specified component and target.
     */
    public static ObjectName
    getComponentLifeCycleObjectName(MBeanServerConnection mbs, MBeanNames mbns, 
        String target, String compName)
            throws javax.management.JMException, java.io.IOException
    {
        String objNamePattern = 
            JBI_DOMAIN + COLON + 
				TARGET_KEY         + EQUAL + target + COMMA + 
                SERVICE_TYPE_KEY   + EQUAL + MBeanNames.ComponentServiceType.ComponentLifeCycle + COMMA + 
                COMPONENT_NAME_KEY + EQUAL + compName;
        ObjectName objName =  new ObjectName(objNamePattern);
   
        java.util.Set<ObjectName> nameSet = mbs.queryNames(objName, null);
        
        return nameSet.iterator().next();
    }
}
