<?xml version="1.0"?>
<!--
 # BEGIN_HEADER - DO NOT EDIT
 #
 # The contents of this file are subject to the terms
 # of the Common Development and Distribution License
 # (the "License").  You may not use this file except
 # in compliance with the License.
 #
 # You can obtain a copy of the license at
 # https://open-esb.dev.java.net/public/CDDLv1.0.html.
 # See the License for the specific language governing
 # permissions and limitations under the License.
 #
 # When distributing Covered Code, include this CDDL
 # HEADER in each file and include the License file at
 # https://open-esb.dev.java.net/public/CDDLv1.0.html.
 # If applicable add the following below this CDDL HEADER,
 # with the fields enclosed by brackets "[]" replaced with
 # your own identifying information: Portions Copyright
 # [year] [name of copyright owner]
-->

<!--
 # @(#)jbiadmin00013.xml
 # Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 #
 # END_HEADER - DO NOT EDIT
-->
<!--
 This tests the state accessors of the ComponentLifeCycleMBean and 
 DeploymentServiceMBean for the instance and domain target.
 
 A Component is intstalled to the "server" and "instance1" instances. 
 The ComponentLifeCycleMBean.getCurrentState() is tested individually and for 
 the domain target when the state of the components on instance1 and server 
 differ. 
 
 A Service Assembly is then deployed to the component on all targets and the
 DeploymentServiceMBean.getState() operation is tested individually and for 
 the domain target when the state of the components on instance1 and server 
 differ. 
-->
<project name="StateTest" default="runtests" basedir=".">
   <import file="inc/jbitestsetup.ant"/>
   
   <property name="component-binding1.jar" value="component-binding1.jar"/>
   <property name="component-binding2.jar" value="component-binding2.jar"/>
   <property name="deploy-sa.jar" value="deploy-sa.jar"/>
   <property name="su1.jar" value="su1.jar"/>
   <property name="su2.jar" value="su2.jar"/>
   
   
   <target name="pkg.init" description="initializes the temp packaging directories if required">
      <mkdir dir="${regress.dist.dir}"/>
   </target>
   
   <target name="pkg.component.jars" depends="pkg.init" description="package component jars for deployment tests">
      <jar destfile="${regress.dist.dir}/${component-binding2.jar}">
         <zipfileset dir="${deploy.test.dir}" includes="binding2.xml" fullpath="META-INF/jbi.xml"/>
         <zipfileset dir="${deploy.test.dir}" includes="BindingComponent.class" fullpath="classes/deploytest/BindingComponent.class"/>
      </jar>
      <jar destfile="${regress.dist.dir}/${component-binding1.jar}">
         <zipfileset dir="${deploy.test.dir}" includes="binding1.xml" fullpath="META-INF/jbi.xml"/>
         <zipfileset dir="${deploy.test.dir}" includes="BindingComponent.class" fullpath="classes/deploytest/BindingComponent.class"/>
      </jar>
   </target>
   
   <target name="pkg.service.assembly.jars" depends="pkg.init" description="package service assembly jars for deployment tests">
      <delete file="${regress.dist.dir}/${deploy-sa.jar}"/>
      <jar destfile="${regress.dist.dir}/${su1.jar}">
        <zipfileset dir="${deploy.test.dir}" includes="service-unit.xml" fullpath="META-INF/jbi.xml"/>
      </jar>
      <jar destfile="${regress.dist.dir}/${su2.jar}">
        <zipfileset dir="${deploy.test.dir}" includes="service-unit.xml" fullpath="META-INF/jbi.xml"/>
      </jar>
      <jar destfile="${regress.dist.dir}/${deploy-sa.jar}">
         <zipfileset dir="${deploy.test.dir}" includes="service-assembly.xml" fullpath="META-INF/jbi.xml"/>
         <zipfileset dir="${regress.dist.dir}" includes="${su1.jar}" fullpath="${su1.jar}"/>
         <zipfileset dir="${regress.dist.dir}" includes="${su2.jar}" fullpath="${su2.jar}"/>
      </jar>
   </target>
   
   
   
   
   <target name="init" depends="manage_test_init">  
   </target>
   
   <target name="install.components" description="install the test components to
        server and instance1 targets.">
      
     <antcall target="pkg.component.jars"/> 
     
     <antcall target="install.component">
        <param name="component_zip" value="${regress.dist.dir}/${component-binding1.jar}"/>
        <param name="component_name" value="manage-binding-1"/>
        <param name="target" value="domain"/>
      </antcall>
      
      <antcall target="install.component">
        <param name="component_zip" value="${regress.dist.dir}/${component-binding1.jar}"/>
        <param name="component_name" value="manage-binding-1"/>
        <param name="target" value="server"/>
      </antcall> 
      
      
      <antcall target="install.component">
        <param name="component_zip" value="${regress.dist.dir}/${component-binding1.jar}"/>
        <param name="component_name" value="manage-binding-1"/>
        <param name="target" value="instance1"/>
      </antcall> 
    
      
      
      <antcall target="install.component">
        <param name="component_zip" value="${regress.dist.dir}/${component-binding2.jar}"/>
        <param name="component_name" value="manage-binding-2"/>
        <param name="target" value="domain"/>
      </antcall>
      
      <antcall target="install.component">
        <param name="component_zip" value="${regress.dist.dir}/${component-binding2.jar}"/>
        <param name="component_name" value="manage-binding-2"/>
        <param name="target" value="server"/>
      </antcall> 
      
      <antcall target="install.component">
        <param name="component_zip" value="${regress.dist.dir}/${component-binding2.jar}"/>
        <param name="component_name" value="manage-binding-2"/>
        <param name="target" value="instance1"/>
      </antcall>
      
   </target>
   
   <target name="uninstall.components" description="install the two test components to server and instance1 targets.">
   
      <antcall target="uninstall.component">
        <param name="component_name" value="manage-binding-1"/>
        <param name="delete" value="true"/>
        <param name="target" value="server"/>
      </antcall>

      <antcall target="uninstall.component">
        <param name="component_name" value="manage-binding-1"/>
        <param name="delete" value="true"/>
        <param name="target" value="instance1"/>
      </antcall>
            
      <antcall target="uninstall.component">
        <param name="component_name" value="manage-binding-2"/>
        <param name="delete" value="true"/>
        <param name="target" value="server"/>
      </antcall>
      
      
      <antcall target="uninstall.component">
        <param name="component_name" value="manage-binding-2"/>
        <param name="delete" value="true"/>
        <param name="target" value="instance1"/>
      </antcall>
      
   </target>
   
   <target name="get.component.states" description="Get the state of manage-binding-1 on 
        the server, instance1 and domain targets.">
   
      <antcall target="get.current.state">
        <param name="component_name" value="manage-binding-1"/>
        <param name="target" value="server"/>
      </antcall>
      
      
      <antcall target="get.current.state">
        <param name="component_name" value="manage-binding-1"/>
        <param name="target" value="instance1"/>
      </antcall>
      
      <echo message=" State of component manage-binding-1 in the domain :"/>
      <antcall target="get.current.state">
        <param name="component_name" value="manage-binding-1"/>
        <param name="target" value="domain"/>
      </antcall>
   </target>
   
   <!-- start the components for deploy state test -->
   <target name="start.components" description="start both the 
        test components on server and instance1">
   
      <antcall target="start.component">
        <param name="component_name" value="manage-binding-1"/>
        <param name="target" value="server"/>
      </antcall> 
      
      
      <antcall target="start.component">
        <param name="component_name" value="manage-binding-1"/>
        <param name="target" value="instance1"/>
      </antcall> 
      
      <antcall target="start.component">
        <param name="component_name" value="manage-binding-2"/>
        <param name="target" value="server"/>
      </antcall> 
      
      
      <antcall target="start.component">
        <param name="component_name" value="manage-binding-2"/>
        <param name="target" value="instance1"/>
      </antcall> 
      
   </target>
   

   <!-- stop the components for deploy state test -->
   <target name="stop.components" description="start both the test components 
        on server and instance1">
   
      <antcall target="stop.component">
        <param name="component_name" value="manage-binding-1"/>
        <param name="target" value="server"/>
      </antcall> 
      
      
      <antcall target="stop.component">
        <param name="component_name" value="manage-binding-1"/>
        <param name="target" value="instance1"/>
      </antcall> 
      
      <antcall target="stop.component">
        <param name="component_name" value="manage-binding-2"/>
        <param name="target" value="server"/>
      </antcall> 
      
      
      <antcall target="stop.component">
        <param name="component_name" value="manage-binding-2"/>
        <param name="target" value="instance1"/>
      </antcall> 
      
   </target>
   

   <!-- shutdown the components for deploy state test -->
   <target name="shut.down.components" description="shutdown both the test 
        components on server and instance1">
   
      <antcall target="shut.down.component">
        <param name="component_name" value="manage-binding-1"/>
        <param name="target" value="server"/>
      </antcall> 
      
      
      <antcall target="shut.down.component">
        <param name="component_name" value="manage-binding-1"/>
        <param name="target" value="instance1"/>
      </antcall> 
      
      <antcall target="shut.down.component">
        <param name="component_name" value="manage-binding-2"/>
        <param name="target" value="server"/>
      </antcall> 
      
      
      <antcall target="shut.down.component">
        <param name="component_name" value="manage-binding-2"/>
        <param name="target" value="instance1"/>
      </antcall> 
      
   </target>
   
   <target name="deploy.service.assemblies" description="deploy the sa to 
        all the targets.">
        
      <antcall target="pkg.service.assembly.jars"/> 
      
      <antcall target="deploy">
        <param name="service_assembly_zip" value="${regress.dist.dir}/${deploy-sa.jar}"/>
        <param name="target" value="domain"/>
      </antcall> 

      <antcall target="deploy">
        <param name="service_assembly_zip" value="${regress.dist.dir}/${deploy-sa.jar}"/>
        <param name="target" value="server"/>
      </antcall> 

      <antcall target="deploy">
        <param name="service_assembly_zip" value="${regress.dist.dir}/${deploy-sa.jar}"/>
        <param name="target" value="instance1"/>
      </antcall> 
                  
    </target>
    
    
   <target name="undeploy.service.assemblies" description="undeploy the sa from
        all the targets.">
        
      <antcall target="undeploy">
        <param name="service_assembly_name" value="esbadmin00089-sa"/>
        <param name="target" value="server"/>
      </antcall>
      
      
      <antcall target="undeploy">
        <param name="service_assembly_name" value="esbadmin00089-sa"/>
        <param name="target" value="instance1"/>
      </antcall>
      
    </target>
      
   
   <target name="get.service.assembly.states" description="Get the state of 
        the service assembly on all instances.">
        
      <antcall target="get.service.assembly.state">
        <param name="service_assembly_name" value="esbadmin00089-sa"/>
        <param name="target" value="server"/>
      </antcall>    
      
      <antcall target="get.service.assembly.state">
        <param name="service_assembly_name" value="esbadmin00089-sa"/>
        <param name="target" value="instance1"/>
      </antcall>    
      
      
      <echo message=" State of service assembly esbadmin00089-sa in the domain :"/>
      <antcall target="get.service.assembly.state">
        <param name="service_assembly_name" value="esbadmin00089-sa"/>
        <param name="target" value="domain"/>
      </antcall>    
   </target>
   
   <!---
     Test
   -->
   <target name="runtests" depends="init">
  
      <antcall target="install.components"/>
      
      <!-- Start the component on the server instance only  -->
      <antcall target="start.component">
        <param name="component_name" value="manage-binding-1"/>
        <param name="target" value="server"/>
      </antcall> 
      
      <antcall target="get.component.states"/>
      
      <antcall target="stop.component">
        <param name="component_name" value="manage-binding-1"/>
        <param name="target" value="server"/>
      </antcall>  
     
      <antcall target="get.component.states"/>
      
      <antcall target="shut.down.component">
        <param name="component_name" value="manage-binding-1"/>
        <param name="target" value="server"/>
      </antcall>  
      
      <antcall target="get.component.states"/>
      
      <!-- Test Deployment states -->
      <antcall target="start.components" />
            
      <!-- Deploy to the SA -->
      <antcall target="deploy.service.assemblies"/>
      
      <antcall target="start.service.assembly">
        <param name="service_assembly_name" value="esbadmin00089-sa"/>
        <param name="target" value="server"/>
      </antcall>    
      
      <antcall target="get.service.assembly.states" />
      
      <antcall target="stop.service.assembly">
        <param name="service_assembly_name" value="esbadmin00089-sa"/>
        <param name="target" value="server"/>
      </antcall>   
            
      <antcall target="get.service.assembly.states" />
      
      <antcall target="shut.down.service.assembly">
        <param name="service_assembly_name" value="esbadmin00089-sa"/>
        <param name="target" value="server"/>
      </antcall>   
      
      <antcall target="get.service.assembly.states" />
      
      <antcall target="undeploy.service.assemblies"/>

      <antcall target="stop.components" />
      <antcall target="shut.down.components" />
      <antcall target="uninstall.components"/>

   </target>
   
</project>
