<?xml version="1.0"?>
<!--
 # BEGIN_HEADER - DO NOT EDIT
 #
 # The contents of this file are subject to the terms
 # of the Common Development and Distribution License
 # (the "License").  You may not use this file except
 # in compliance with the License.
 #
 # You can obtain a copy of the license at
 # https://open-esb.dev.java.net/public/CDDLv1.0.html.
 # See the License for the specific language governing
 # permissions and limitations under the License.
 #
 # When distributing Covered Code, include this CDDL
 # HEADER in each file and include the License file at
 # https://open-esb.dev.java.net/public/CDDLv1.0.html.
 # If applicable add the following below this CDDL HEADER,
 # with the fields enclosed by brackets "[]" replaced with
 # your own identifying information: Portions Copyright
 # [year] [name of copyright owner]
-->

<!--
 # @(#)jbitestsetup.ant
 # Copyright 2004-2008 Sun Microsystems, Inc. All Rights Reserved.
 #
 # END_HEADER - DO NOT EDIT
-->
<project name="taskdefs">
   <!--
    # We assume that you have passed in the <JBI_DOMAIN_ROOT>/jbi/config/jbienv.properties file:
    #    ant -propertyfile $JBI_DOMAIN_ROOT/jbi/config/jbienv.properties ...
    -->
   <!--
    # DEFINE JMX4ANT typdefs and tasks:
    -->
   <typedef name="mbean"          classname="org.apache.tools.ant.types.optional.MBeanType"/>
   <typedef name="context"        classname="org.apache.tools.ant.types.optional.ContextType"/>
   <taskdef name="configureMBean" classname="org.apache.tools.ant.taskdefs.optional.jmx.ConfigureMBeanTask"/>
   <taskdef name="copyMBean"      classname="org.apache.tools.ant.taskdefs.optional.jmx.CopyMBeanTask"/>
   <taskdef name="removeMBean"    classname="org.apache.tools.ant.taskdefs.optional.jmx.RemoveMBeanTask"/>
   <taskdef name="createMBean"    classname="org.apache.tools.ant.taskdefs.optional.jmx.CreateMBeanTask"/>
   <taskdef name="invokeMBean"    classname="jmx4ant.InvokeMBeanTask"/> 
   <taskdef name="showMBean"      classname="org.apache.tools.ant.taskdefs.optional.jmx.ShowMBeanTask"/>
   <!--
    # Use ant-contrib tasks (includes "foreach" task):
    -->
   <taskdef resource="net/sf/antcontrib/antlib.xml"/>
   <!--
   # Include our XML canonicalizer task
   -->
   <taskdef name="canonicalizeXml" classname="com.sun.jbi.internal.ant.tools.CanonicalizerTask" />

   <target name="manage_test_init" depends="set-global,set-defaults,set-mbeanrefs">
      <echo message="JBI_HOME is ${JBI_HOME}"/>
      <echo message="JBI_DOMAIN_ROOT is ${JBI_DOMAIN_ROOT}"/>
      <echo message="jbi_instance_name is ${jbi_instance_name}"/>
      <echo message="jmx.provider is ${jmx.provider}"/>
   </target>
   <target name="set-defaults">
      <property name="JBI_HOME" value="${env.JBI_HOME}"/>
      <property name="JBI_DOMAIN_ROOT" value="${env.JBI_DOMAIN_ROOT}"/>
      <property name="jbi_jmx_domain" value="com.sun.jbi"/>
      <!-- these come from JBI_DOMAIN_ROOT property file: -->
      <property name="jbi_instance_name" value="${env.JBI_INSTANCE_NAME}"/>
      <property name="jmx.provider" value="${env.AS_JMX_REMOTE_URL}"/>
      <!-- these must be defined in the test environment: -->
      <property name="jmx.user" value="${env.AS_ADMIN_USER}"/>
      <property name="jmx.password" value="${env.AS_ADMIN_PASSWD}"/>
      <!-- build products folder: -->
      <property name="manage_bld_dir" value="${env.JV_SVC_BLD}"/>
      <property name="manage_bld_regress" value="${env.JV_SVC_TEST_CLASSES}"/>
      <property name="src_schemas_dir" value="${env.JV_SVC_ROOT}/src/schemas"/>
      <property name="manage_src_schemas_dir" value="${env.JV_SVC_ROOT}/../manage/src/schemas"/>
      <property name="regress.dist.dir" location="${manage_bld_regress}/dist"/>
      <property name="deploy.test.dir" location="${manage_bld_regress}/deploytest"/>
      <property name="test.component.dir" location="${manage_bld_regress}/testdata"/>
      <property name="cconfigmbeaninstartstop.dir" location="${manage_bld_regress}/componentconfiguration/cconfigmbeaninstartstop"/>
      <property name="cconfigmbeannoattrs.dir" location="${manage_bld_regress}/componentconfiguration/cconfigmbeannoattrs"/>

      <!-- Sample Component Zip to use for testing. -->
      <property name="transformationengine.zip" value="${env.JV_JBI_HOME}/components/engines/transformationengine/installer/transformationengine.jar"/>
      <property name="configurable-component.jar" value="${manage_bld_regress}/testdata/configurable-component.jar"/>
      <property name="empty-service-assembly.zip" value="${manage_bld_regress}/testdata/empty-service-assembly.zip"/>
      
   </target>
   <target name="set-global">
      <property environment="env"/>
   </target>

   <target name="set-mbeanrefs">
      <mbean id="InstallationService" providerUrl="${jmx.provider}" name="com.sun.jbi:Target=${target},ServiceName=InstallationService,ServiceType=Installation" serverType="rjmx" user="${jmx.user}" password="${jmx.password}"/>
      <mbean id="AdminService" providerUrl="${jmx.provider}" name="com.sun.jbi:Target=${target},ServiceName=AdminService,ServiceType=Admin" serverType="rjmx" user="${jmx.user}" password="${jmx.password}"/>
      <mbean id="DeploymentService" providerUrl="${jmx.provider}" name="com.sun.jbi:Target=${target},ServiceName=DeploymentService,ServiceType=Deployment" serverType="rjmx" user="${jmx.user}" password="${jmx.password}"/>
      <mbean id="ConfigurationService" providerUrl="${jmx.provider}" name="com.sun.jbi:Target=${target},ServiceName=ConfigurationService,ServiceType=${category}" serverType="rjmx" user="${jmx.user}" password="${jmx.password}"/>
      
      <!-- Component Specific MBeans -->
      <mbean id="Installer"              providerUrl="${jmx.provider}" name="com.sun.jbi:Target=${target},ServiceType=Installer,ComponentName=${component_name}" serverType="rjmx" user="${jmx.user}" password="${jmx.password}"/>
      <mbean id="ComponentLifeCycle"     providerUrl="${jmx.provider}" name="com.sun.jbi:Target=${target},ServiceType=ComponentLifeCycle,ComponentName=${component_name}" serverType="rjmx" user="${jmx.user}" password="${jmx.password}"/>
      <mbean id="ComponentConfiguration" providerUrl="${jmx.provider}" name="com.sun.jbi:Target=${target},ServiceType=Configuration,ComponentName=${component_name}" serverType="rjmx" user="${jmx.user}" password="${jmx.password}"/>
   </target>
   
   <!-- Validate the Result XML from the Core Admin MBeans against the managementMessage schema. -->
   <target name="validate-result">
      <xmlvalidate file="${input_xml_file}" failonerror="yes" lenient="no" warn="yes">
         <attribute name="http://xml.org/sax/features/validation" value="true"/>
         <attribute name="http://apache.org/xml/features/validation/schema" value="true"/>
         <attribute name="http://xml.org/sax/features/namespaces" value="true"/>
         <property name="http://java.sun.com/xml/jaxp/properties/schemaSource" value="${schema}"/>
      </xmlvalidate>
   </target>
   
   <!-- Process the result of an MBean operation through the following steps:
            (1) schema validate
            (2) prune environment-sensitive values (timestamp, host, port, etc.)
            (3) canonicalize
            (4) output XML doc
   -->
   <target name="process-result">
      <echo message="${result_data}" file="${manage_bld_dir}/${operation}Result.xml" append="false"/>
      <antcall target="validate-result">
         <param name="input_xml_file" value="${manage_bld_dir}/${operation}Result.xml"/>
      </antcall>
      <style force="true" basedir="${manage_bld_dir}" destdir="${manage_bld_dir}" extension=".out" style="${env.JV_SVC_REGRESS}/com/sun/jbi/management/facade/prune.xsl">
         <include name="${operation}Result.xml"/>
      </style>
      <loadfile property="${operation}Data" srcFile="${manage_bld_dir}/${operation}Result.out"/>
      <canonicalizeXml XmlIn="${operation}Data" XmlOut="operationResult"/>
      <echo message="##### Result of ${operation} = ${operationResult}"/>
   </target>

   <target name="process-text-result">
      <echo message="##### Result of ${operation} = ${result_data}"/>
   </target>
      
   <!--
     InstallationService operations
   -->
   
   
    <target name="load.new.installer" description="Load a new Installer">
      <invokeMBean operation="loadNewInstaller" mbeanref="InstallationService">
         <parameter value="${component_zip}"/>
      </invokeMBean>
      <antcall target="process-text-result">
         <param name="result_data" value="${loadNewInstaller.result}"/>
         <param name="operation" value="loadNewInstaller"/>
      </antcall>
   </target>
   
   
    <target name="load.installer" description="Load the Installer for an existing component">
      <invokeMBean operation="loadInstaller" mbeanref="InstallationService">
         <parameter value="${component_name}"/>
      </invokeMBean>
      <antcall target="process-text-result">
         <param name="result_data" value="${loadInstaller.result}"/>
         <param name="operation" value="loadInstaller"/>
      </antcall>
   </target>
   
   
    <target name="load.installer.force" description="Load the Installer for an existing component with the force option">
      <invokeMBean operation="loadInstaller" mbeanref="InstallationService">
         <parameter value="${component_name}"/>
         <parameter value="${force}"/>
      </invokeMBean>
      <antcall target="process-text-result">
         <param name="result_data" value="${loadInstaller.result}"/>
         <param name="operation" value="loadInstaller"/>
      </antcall>
   </target>
   
   
    <target name="load.installer.from.repos" description="Load the installer for
            a pre-registered component">
      <invokeMBean operation="loadInstallerFromRepository" mbeanref="InstallationService">
         <parameter value="${component_name}"/>
      </invokeMBean>
      <antcall target="process-text-result">
         <param name="result_data" value="${loadInstallerFromRepository.result}"/>
         <param name="operation" value="loadInstallerFromRepository"/>
      </antcall>
    </target>
    
    <target name="unload.installer" description="unload the Installer for a Component">
      <invokeMBean operation="unloadInstaller" mbeanref="InstallationService">
         <parameter value="${component_name}"/>
         <parameter value="${delete}"/>
      </invokeMBean>
      <antcall target="process-text-result">
         <param name="result_data" value="${unloadInstaller.result}"/>
         <param name="operation" value="unloadInstaller"/>
      </antcall>
   </target>
   
    <target name="install.shared.library" description="Install a Shared Library">
      <invokeMBean operation="installSharedLibrary" mbeanref="InstallationService">
         <parameter value="${library_zip}"/>
      </invokeMBean>
      <antcall target="process-text-result">
         <param name="result_data" value="${installSharedLibrary.result}"/>
         <param name="operation" value="installSharedLibrary"/>
      </antcall>
   </target>
   
   <target name="install.shared.library.from.repos" description="Install a pre-registered Shared Library">
      <invokeMBean operation="installSharedLibraryFromRepository" mbeanref="InstallationService">
         <parameter value="${library_name}"/>
      </invokeMBean>
      <antcall target="process-text-result">
         <param name="result_data" value="${installSharedLibraryFromRepository.result}"/>
         <param name="operation" value="installSharedLibraryFromRepository"/>
      </antcall>
   </target>
   
   <target name="uninstall.shared.library" description="Uninstall a Shared Library">
      <invokeMBean operation="uninstallSharedLibrary" mbeanref="InstallationService">
         <parameter value="${library_name}"/>
      </invokeMBean>
      <antcall target="process-text-result">
         <param name="result_data" value="${uninstallSharedLibrary.result}"/>
         <param name="operation" value="uninstallSharedLibrary"/>
      </antcall>
   </target>
   
   <target name="uninstall.shared.library.with.keep" description="Uninstall a Shared Library">
      <invokeMBean operation="uninstallSharedLibrary" mbeanref="InstallationService">
         <parameter value="${library_name}"/>
         <parameter value="${keep}"/>
      </invokeMBean>
      <antcall target="process-text-result">
         <param name="result_data" value="${uninstallSharedLibrary.result}"/>
         <param name="operation" value="uninstallSharedLibrary"/>
      </antcall>
   </target>
   
    <target name="update.component" description="Update a Component">
      <invokeMBean operation="upgradeComponent" mbeanref="InstallationService">
         <parameter value="${component_name}"/>
         <parameter value="${component_zip}"/>
      </invokeMBean>
   </target>
   
   <!--
     DeploymentService operations
   -->
   
    <target name="deploy" description="Deploy a Service Assembly">
      <invokeMBean operation="deploy" mbeanref="DeploymentService">
         <parameter value="${service_assembly_zip}"/>
      </invokeMBean>
      <antcall target="process-result">
         <param name="result_data" value="${deploy.result}"/>
         <param name="operation" value="deploy"/>
         <param name="schema" value="${src_schemas_dir}/managementMessage.xsd"/>
      </antcall>
   </target>
   
   <target name="deploy.from.repos" description="Deploy a pre-registered Service Assembly">
      <invokeMBean operation="deployFromRepository" mbeanref="DeploymentService">
         <parameter value="${service_assembly_name}"/>
      </invokeMBean>
      <antcall target="process-result">
         <param name="result_data" value="${deployFromRepository.result}"/>
         <param name="operation" value="deployFromRepository"/>
         <param name="schema" value="${src_schemas_dir}/managementMessage.xsd"/>
      </antcall>
   </target>
   
    <target name="undeploy" description="Undeploy a Service Assembly">
      <invokeMBean operation="undeploy" mbeanref="DeploymentService">
         <parameter value="${service_assembly_name}"/>
      </invokeMBean>
      <antcall target="process-result">
         <param name="result_data" value="${undeploy.result}"/>
         <param name="operation" value="undeploy"/>
         <param name="schema" value="${src_schemas_dir}/managementMessage.xsd"/>
      </antcall>
   </target> 
   
   <target name="undeploy.with.force" description="Undeploy a Service Assembly with the force option">
      <invokeMBean operation="undeploy" mbeanref="DeploymentService">
         <parameter value="${service_assembly_name}"/>
         <parameter value="${force}"/>
      </invokeMBean>
      <antcall target="process-result">
         <param name="result_data" value="${undeploy.result}"/>
         <param name="operation" value="undeploy"/>
         <param name="schema" value="${src_schemas_dir}/managementMessage.xsd"/>
      </antcall>
   </target> 
   
   <target name="undeploy.with.force.and.keep" description="Undeploy a Service Assembly with the force option">
      <invokeMBean operation="undeploy" mbeanref="DeploymentService">
         <parameter value="${service_assembly_name}"/>
         <parameter value="${force}"/>
         <parameter value="${keep}"/>
      </invokeMBean>
      <antcall target="process-result">
         <param name="result_data" value="${undeploy.result}"/>
         <param name="operation" value="undeploy"/>
         <param name="schema" value="${src_schemas_dir}/managementMessage.xsd"/>
      </antcall>
   </target>
   
   <target name="start.service.assembly" description="Start a Service Assembly">
      <invokeMBean operation="start" mbeanref="DeploymentService">
         <parameter value="${service_assembly_name}"/>
      </invokeMBean>
      <antcall target="process-result">
         <param name="result_data" value="${start.result}"/>
         <param name="operation" value="start"/>
         <param name="schema" value="${src_schemas_dir}/managementMessage.xsd"/>
      </antcall>
   </target>   
   
   
   <target name="stop.service.assembly" description="Stop a Service Assembly">
      <invokeMBean operation="stop" mbeanref="DeploymentService">
         <parameter value="${service_assembly_name}"/>
      </invokeMBean>
      <antcall target="process-result">
         <param name="result_data" value="${stop.result}"/>
         <param name="operation" value="stop"/>
         <param name="schema" value="${src_schemas_dir}/managementMessage.xsd"/>
      </antcall>
   </target>   
   
   
   <target name="shut.down.service.assembly" description="Shutdown a Service Assembly">
      <invokeMBean operation="shutDown" mbeanref="DeploymentService">
         <parameter value="${service_assembly_name}"/>
      </invokeMBean>
      <antcall target="process-result">
         <param name="result_data" value="${shutDown.result}"/>
         <param name="operation" value="shutDown"/>
         <param name="schema" value="${src_schemas_dir}/managementMessage.xsd"/>
      </antcall>
   </target>   
   

   <target name="shut.down.service.assembly.with.force" description="Shutdown a Service Assembly">
      <invokeMBean operation="shutDown" mbeanref="DeploymentService">
         <parameter value="${service_assembly_name}"/>
         <parameter value="${force}"/>
      </invokeMBean>
      <antcall target="process-result">
         <param name="result_data" value="${shutDown.result}"/>
         <param name="operation" value="shutDown"/>
         <param name="schema" value="${src_schemas_dir}/managementMessage.xsd"/>
      </antcall>
   </target>   
   
    <target name="get.service.assembly.descriptor" description="Get the Service Assembly Descriptor">
      <invokeMBean operation="getServiceAssemblyDescriptor" mbeanref="DeploymentService">
         <parameter value="${service_assembly_name}"/>
      </invokeMBean>
      <antcall target="process-result">
         <param name="result_data" value="${getServiceAssemblyDescriptor.result}"/>
         <param name="operation" value="getServiceAssemblyDescriptor"/>
         <param name="schema" value="${manage_src_schemas_dir}/jbi.xsd"/>
      </antcall>
   </target> 
   
   <target name="get.service.assembly.state" description="Get the Service Assembly state">
      <invokeMBean operation="getState" mbeanref="DeploymentService">
         <parameter value="${service_assembly_name}"/>
      </invokeMBean>
      <antcall target="process-text-result">
         <param name="result_data" value="${getState.result}"/>
         <param name="operation" value="getState"/>
      </antcall>
   </target>       
   
   <target name="get.service.unit.state" description="Get the Service Unit state">
      <invokeMBean operation="getServiceUnitState" mbeanref="DeploymentService">
         <parameter value="${component_name}"/>
         <parameter value="${service_unit_name}"/>
      </invokeMBean>
      <antcall target="process-text-result">
         <param name="result_data" value="${getServiceUnitState.result}"/>
         <param name="operation" value="getServiceUnitState"/>
      </antcall>
   </target>
   
   <target name="get.components.for.service.assembly" description="Service Assembly query">
      <invokeMBean operation="getComponentsForDeployedServiceAssembly" mbeanref="DeploymentService">
         <parameter value="${service_assembly_name}"/>
      </invokeMBean>
      <antcall target="process-text-result">
         <param name="result_data" value="${getComponentsForDeployedServiceAssembly.result}"/>
         <param name="operation" value="getComponentsForDeployedServiceAssembly"/>
      </antcall>
   </target>       
   
   
   <target name="is.deployed.service.unit" description="Service Assembly query">
      <invokeMBean operation="isDeployedServiceUnit" mbeanref="DeploymentService">
         <parameter value="${component_name}"/>
         <parameter value="${service_unit_name}"/>
      </invokeMBean>
      <antcall target="process-text-result">
         <param name="result_data" value="${isDeployedServiceUnit.result}"/>
         <param name="operation" value="isDeployedServiceUnit"/>
      </antcall>
   </target> 
   
   <target name="get.deployed.service.assemblies" description="Get a string array of deployed service assemblies">
      <invokeMBean operation="getDeployedServiceAssemblies" mbeanref="DeploymentService">
      </invokeMBean>
      <antcall target="process-text-result">
         <param name="result_data" value="${getDeployedServiceAssemblies.result}"/>
         <param name="operation" value="getDeployedServiceAssemblies"/>
      </antcall>
   </target>
   
   <target name="get.deployed.service.assemblies.for.component" description="Get service assemblies deployed to a component">
      <invokeMBean operation="getDeployedServiceAssembliesForComponent" mbeanref="DeploymentService">   
            <parameter value="${component_name}"/>
      </invokeMBean>
      <antcall target="process-text-result">
         <param name="result_data" value="${getDeployedServiceAssembliesForComponent.result}"/>
         <param name="operation" value="getDeployedServiceAssembliesForComponent"/>
      </antcall>
   </target>
   
   <target name="get.deployed.service.unit.list" description="Get service units deployed to a component">
      <invokeMBean operation="getDeployedServiceUnitList" mbeanref="DeploymentService">   
            <parameter value="${component_name}"/>
      </invokeMBean>
      <antcall target="process-text-result">
         <param name="result_data" value="${getDeployedServiceUnitList.result}"/>
         <param name="operation" value="getDeployedServiceUnitList"/>
      </antcall>
   </target>
   
   <target name="can.deploy.to.component" description="Determine if can deploy to component">
      <invokeMBean operation="canDeployToComponent" mbeanref="DeploymentService">   
            <parameter value="${component_name}"/>
      </invokeMBean>
      <antcall target="process-text-result">
         <param name="result_data" value="${canDeployToComponent.result}"/>
         <param name="operation" value="canDeployToComponent"/>
      </antcall>
   </target>
   
   <!-- Installer operations -->
   <target name="install" description="Invoke Installer.install">
      <invokeMBean operation="install" mbeanref="Installer">
      </invokeMBean>
      <antcall target="process-text-result">
         <param name="result_data" value="${install.result}"/>
         <param name="operation" value="install"/>
      </antcall>
   </target>
   
   <target name="is.installed" description="Invoke Installer.isInstalled">
      <configureMBean mbeanref="Installer">
        <getAttribute name="Installed" property="isInstalled.result"/>
      </configureMBean>
      <antcall target="process-text-result">
         <param name="result_data" value="${isInstalled.result}"/>
         <param name="operation" value="isInstalled"/>
      </antcall>
   </target>
   
   <target name="get.install.root" description="Invoke Installer.getInstallRoot">
      <configureMBean  mbeanref="Installer">
        <getAttribute name="InstallRoot" property="getInstallRoot.result"/>
      </configureMBean>
      <antcall target="process-text-result">
         <param name="result_data" value="${getInstallRoot.result}"/>
         <param name="operation" value="getInstallRoot"/>
      </antcall>
   </target>
   
   <target name="get.installer.config.mbean" description="Invoke Installer.getInstallerConfigurationMBean">
      <configureMBean  mbeanref="Installer">
        <getAttribute name="InstallerConfigurationMBean" property="getInstallerConfigurationMBean.result"/>
      </configureMBean>
      <antcall target="process-text-result">
         <param name="result_data" value="${getInstallerConfigurationMBean.result}"/>
         <param name="operation" value="getInstallerConfigurationMBean"/>
      </antcall>
   </target>

   
   <target name="uninstall" description="Invoke Installer.uninstall">
      <invokeMBean operation="uninstall" mbeanref="Installer">
      </invokeMBean>
   </target>
   
    <target name="uninstall.force" description="Invoke Installer.uninstall(boolean force)">
      <invokeMBean operation="uninstall" mbeanref="Installer">
        <parameter value="${force}"/>
      </invokeMBean>
      <antcall target="process-text-result">
         <param name="result_data" value="${uninstall.result}"/>
         <param name="operation" value="uninstall"/>
      </antcall>
   </target>
   
   <!-- ComponentLifeCycle Operations -->
   <target name="start.component" description="Invoke ComponentLifeCycle.start">
      <invokeMBean operation="start" mbeanref="ComponentLifeCycle">
      </invokeMBean>
      
      <antcall target="process-text-result">
         <param name="result_data" value="${start.result}"/>
         <param name="operation" value="start"/>
      </antcall>
   </target>
   
   <target name="stop.component" description="Invoke ComponentLifeCycle.stop">
      <invokeMBean operation="stop" mbeanref="ComponentLifeCycle">
      </invokeMBean>
      
      <antcall target="process-text-result">
         <param name="result_data" value="${stop.result}"/>
         <param name="operation" value="stop"/>
      </antcall>
   </target>
   
   <target name="shut.down.component" description="Invoke ComponentLifeCycle.shutDown">
      <invokeMBean operation="shutDown" mbeanref="ComponentLifeCycle">
      </invokeMBean>
      
      <antcall target="process-text-result">
         <param name="result_data" value="${shutDown.result}"/>
         <param name="operation" value="shutDown"/>
      </antcall>
   </target>
   
   <target name="shut.down.component.force" 
           description="Invoke ComponentLifeCycle.shutDown(boolean force) with force=true">
      <invokeMBean operation="shutDown" mbeanref="ComponentLifeCycle">
        <parameter value="${force}"/>
      </invokeMBean>
      
      <antcall target="process-text-result">
         <param name="result_data" value="${shutDown.result}"/>
         <param name="operation" value="shutDown"/>
      </antcall>
   </target>
   
   <target name="get.current.state" description="Invoke ComponentLifeCycle.getCurrentState">
      <configureMBean  mbeanref="ComponentLifeCycle">
        <getAttribute name="CurrentState" property="getCurrentState.result"/>
      </configureMBean>
      
      <antcall target="process-text-result">
         <param name="result_data" value="${getCurrentState.result}"/>
         <param name="operation" value="getCurrentState"/>
      </antcall>
   </target>
   
   <!-- AdminService operations
   -->
   <target name="is.binding" description="Is a Component a Binding">
      <invokeMBean operation="isBinding" mbeanref="AdminService">
         <parameter value="${component_name}"/>
      </invokeMBean>
      <antcall target="process-text-result">
         <param name="result_data" value="${isBinding.result}"/>
         <param name="operation" value="isBinding"/>
      </antcall>
   </target>
   
   <target name="is.engine" description="Is a Component a Engine">
      <invokeMBean operation="isEngine" mbeanref="AdminService">
         <parameter value="${component_name}"/>
      </invokeMBean>
      <antcall target="process-text-result">
         <param name="result_data" value="${isEngine.result}"/>
         <param name="operation" value="isEngine"/>
      </antcall>
   </target>
   
   <target name="get.component.by.name" description="Get specific Component Life Cycle ObjectName">
      <invokeMBean operation="getComponentByName" mbeanref="AdminService">
        <parameter value="${component_name}"/>
      </invokeMBean>
      <antcall target="process-text-result">
         <param name="result_data" value="${getComponentByName.result}"/>
         <param name="operation" value="getComponentByName"/>
      </antcall>
   </target>

   <!-- Component Configuration Operations -->
   <target name="is.app.config.supported" description="Invoke ComponentConfiguration.isAppConfigSupported">

      <invokeMBean operation="isAppConfigSupported" mbeanref="ComponentConfiguration"/>
      
      <antcall target="process-text-result">
         <param name="result_data" value="${isAppConfigSupported.result}"/>
         <param name="operation" value="isAppConfigSupported"/>
      </antcall>
   </target>

   <target name="is.app.vars.supported" description="Invoke ComponentConfiguration.isAppVarsSupported">

      <invokeMBean operation="isAppVarsSupported" mbeanref="ComponentConfiguration"/>
      
      <antcall target="process-text-result">
         <param name="result_data" value="${isAppVarsSupported.result}"/>
         <param name="operation" value="isAppVarsSupported"/>
      </antcall>
   </target>
   
   <!-- Configuration service targets -->
   <target name="set.config.attribute" description="Set an attribute on a configuration MBean">
      <configureMBean mbeanref="ConfigurationService">
        <setAttribute name="${attribute_name}" value="${attribute_value}"/>
      </configureMBean>
      <echo message="##### Value of attribute ${attribute_name} has been set to ${attribute_value}"/>
   </target>
   
   <target name="get.config.attribute" description="Set an attribute on a configuration MBean">
      <configureMBean mbeanref="ConfigurationService">
        <getAttribute name="${attribute_name}" property="retrieved_value"/>
      </configureMBean>
      <echo message="##### Value of attribute ${attribute_name} = ${retrieved_value}"/>
   </target>
   
   <!-- Java Test Classes -->
   <target name="test.admin.service" description="Run a java test class to test Admin Service">
      <java classname="java4ant.TestAdminService" outputproperty="TestAdminService.result">
        <sysproperty key="jmx.provider" value="${jmx.provider}"/>
        <sysproperty key="jmx.user" value="${jmx.user}"/>
        <sysproperty key="jmx.password" value="${jmx.password}"/>
        <sysproperty key="target" value="${target}"/>
      </java>
      <echo message="${TestAdminService.result}"/>
   </target>
   
   
   <target name="test.component.extension" description="Run a java test class to test getting component Logger MBeans">
      <java classname="java4ant.TestComponentExtension" outputproperty="TestComponentExtension.result">
        <sysproperty key="jmx.provider" value="${jmx.provider}"/>
        <sysproperty key="jmx.user" value="${jmx.user}"/>
        <sysproperty key="jmx.password" value="${jmx.password}"/>
        <sysproperty key="target" value="${target}"/>
        <sysproperty key="custom.control.name" value="${custom_control_name}"/>
        <sysproperty key="component.name" value="${component_name}"/>
	<sysproperty key="logger.name" value="${logger_name}"/>
      </java>
      <echo message="${TestComponentExtension.result}"/>
   </target>
   
      
   <target name="test.component.configuration" description="Run a java test class to test set/get Component Configuration attributes">
      <java classname="java4ant.TestComponentConfiguration" outputproperty="TestComponentConfiguration.result">
        <sysproperty key="jmx.provider" value="${jmx.provider}"/>
        <sysproperty key="jmx.user" value="${jmx.user}"/>
        <sysproperty key="jmx.password" value="${jmx.password}"/>
        <sysproperty key="target" value="${target}"/>
        <sysproperty key="component.name" value="${component_name}"/>
        <sysproperty key="CLASSPATH" value="${JBI_HOME}/lib/jbi.jar"/>
      </java>
      <echo message="${TestComponentConfiguration.result}"/>
   </target>

   <target name="test.application.configuration.support" description="Run a java test class to test Application Configuration attributes">
      <java classname="java4ant.TestApplicationConfigurationSupport" outputproperty="TestApplicationConfigurationSupport.result">
        <sysproperty key="jmx.provider" value="${jmx.provider}"/>
        <sysproperty key="jmx.user" value="${jmx.user}"/>
        <sysproperty key="jmx.password" value="${jmx.password}"/>
        <sysproperty key="target" value="${target}"/>
        <sysproperty key="component.name" value="${component_name}"/>
        <classpath>
            <pathelement path="${java.class.path}"/>
            <pathelement location="${admin_common_bld_dir}/regress/"/>
            <dirset dir="${AS_INSTALL}/lib/">
                <include name="*.jar"/>
            </dirset>
            <pathelement location="${JBI_HOME}/lib/jbi_rt.jar"/>
            <pathelement location="${JBI_HOME}/lib/jbi.jar"/>
         </classpath>
      </java>
      <echo message="${TestApplicationConfigurationSupport.result}"/>
   </target>
   
   <target name="test.runtime.config" description="Run a java test class to test Runtime Configuration MBean">
      <java classname="java4ant.TestRuntimeConfiguration" outputproperty="TestRuntimeConfiguration.result">
        <sysproperty key="jmx.provider" value="${jmx.provider}"/>
        <sysproperty key="jmx.user" value="${jmx.user}"/>
        <sysproperty key="jmx.password" value="${jmx.password}"/>
        <sysproperty key="target" value="${target}"/>
        <sysproperty key="category" value="${category}"/>
      </java>
      <echo message="${TestRuntimeConfiguration.result}"/>
   </target>
   
   <!-- Composite helper tasks -->
   <target name="install.component" description="Load Installer, install, unload installer">
      <antcall target="load.new.installer"/>
      <antcall target="install"/>
      <antcall target="unload.installer"/>
   </target>
   
   
   <target name="uninstall.component" description="Load Installer, uninstall, unload installer">
      <antcall target="load.installer"/>
      <antcall target="uninstall"/>
      <antcall target="unload.installer"/>
   </target>
   
   <target name="uninstall.component.force" description="Load Installer with force, uninstall with force, unload installer">
      <antcall target="load.installer.force"/>
      <antcall target="uninstall.force"/>
      <antcall target="unload.installer"/>
   </target>
   
   <target name="install.component.from.repos" description="Load Installer from repository, install, unload installer">
      <antcall target="load.installer.from.repos"/>
      <antcall target="install"/>
      <antcall target="unload.installer"/>
   </target>
   
   <!--================================= info ==================================-->
   <target name="info" depends="init" description="- display informational message">
      <echo><![CDATA[
Test properties:

    ==== java properties ====

    java.home        ${java.home}
    user.name        ${user.name}
    os.name          ${os.name}
    os.arch          ${os.arch}
    os.version       ${os.version}
    java.vm.version  ${java.vm.version}
    path.separator   ${path.separator}
    classpath        ${java.class.path}
]]></echo>
   </target>
</project>
