#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)jbiadmin01401.ksh
# Copyright 2004-2008 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT


echo jbiadmin01401 - Test persistence in the Runtime Configuration MBeans

. ./regress_defs.ksh

# start the framework
start_jbise -Dcom.sun.jbi.registry.readonly=true &
startInstanceDelay

# set some configuration properties ( reuse 01400 )
ant -q -emacs $JBISE_PROPS -DTARGET="server" -lib "$REGRESS_CLASSPATH" -f jbiadmin01402.xml

# stop the JBI framework
shutdown_jbise
stopInstanceDelay

# Now restart the JBI Runtime, the values set by jbiadmin 01400 run above should be persisted and read on startup
start_jbise -Dcom.sun.jbi.registry.readonly=true &
startInstanceDelay

# query for persisted attributes
ant -q -emacs $JBISE_PROPS -DTARGET="server" -lib "$REGRESS_CLASSPATH" -f jbiadmin01401.xml

# stop the JBI framework
shutdown_jbise
stopInstanceDelay
