#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)jbiadmin01300.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#
#jbiadmin01300 - Setup a cluster ( cluster1 ) with two instances ( cluster1-instance1 and cluster2-instance2 ) 

echo testname is jbiadmin01300
. ./regress_defs.ksh

. ./cluster-setup.ksh

echo Creating instance : cluster1-instance1
asadmin create-instance --port $ASADMIN_PORT $ASADMIN_PW_OPTS --cluster cluster1 --nodeagent agent1 cluster1-instance1 1>&2

echo Starting instance : cluster1-instance1
asadmin start-instance --port $ASADMIN_PORT $ASADMIN_PW_OPTS cluster1-instance1

echo Creating instance : cluster1-instance2
asadmin create-instance --port $ASADMIN_PORT $ASADMIN_PW_OPTS --cluster cluster1 --nodeagent agent1 cluster1-instance2 1>&2

echo Starting instance : cluster1-instance2
asadmin start-instance --port $ASADMIN_PORT $ASADMIN_PW_OPTS cluster1-instance2
