#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)jbiadmin01600.ksh
# Copyright 2004-2008 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

#############################################################################################
#                     Tests fix for Issue 100                                               #
#############################################################################################

echo "jbiadmin01608 : Test listing component configuration when target is down and component is shutdown for all targets."

. ./regress_defs.ksh
ant -q -emacs -DESBMEMBER_ADMIN_PORT="$JBI_ADMIN_PORT" -lib "$REGRESS_CLASSPATH" -f jbiadmin01600.xml pkg.new.test.component

COMPONENT_ARCHIVE=$JV_SVC_TEST_CLASSES/dist/component-with-new-config-mbean.jar
COMPONENT_NAME=manage-binding-1

# Cluster and Member Instance Setup
asadmin create-cluster  --port $ASADMIN_PORT $ASADMIN_PW_OPTS ccfg-cluster
createClusterDelay
asadmin create-instance --port $ASADMIN_PORT $ASADMIN_PW_OPTS --cluster ccfg-cluster --nodeagent agent1 ccfg-cluster-instance1
createInstanceDelay
asadmin create-instance --port $ASADMIN_PORT $ASADMIN_PW_OPTS --cluster ccfg-cluster --nodeagent agent1 ccfg-cluster-instance2
createInstanceDelay
asadmin create-instance --port $ASADMIN_PORT $ASADMIN_PW_OPTS                        --nodeagent agent1 ccfg-instance1
createInstanceDelay

# Component Setup
$JBI_ANT_NEG -Djbi.install.file=$COMPONENT_ARCHIVE  -Djbi.target="ccfg-cluster" install-component
installComponentDelay
$JBI_ANT_NEG -Djbi.install.file=$COMPONENT_ARCHIVE  -Djbi.target="ccfg-instance1" install-component
installComponentDelay
$JBI_ANT_NEG -Djbi.install.file=$COMPONENT_ARCHIVE  -Djbi.target="server" install-component
installComponentDelay

#  Test (a) getting component configuration when cluster is down 
$JBI_ANT_NEG  -Djbi.target="ccfg-cluster" -Djbi.component.name=$COMPONENT_NAME list-component-configuration

#  Test (b) getting component configuration when standalone instance is down 
$JBI_ANT_NEG  -Djbi.target="ccfg-instance1" -Djbi.component.name=$COMPONENT_NAME list-component-configuration

# Start targets
asadmin start-instance  --port $ASADMIN_PORT $ASADMIN_PW_OPTS ccfg-cluster-instance1
startInstanceDelay
asadmin start-instance  --port $ASADMIN_PORT $ASADMIN_PW_OPTS ccfg-cluster-instance2
startInstanceDelay
asadmin start-instance  --port $ASADMIN_PORT $ASADMIN_PW_OPTS ccfg-instance1
startInstanceDelay

#  Test (c) getting component configuration for target=cluster when component is shutdown 
$JBI_ANT_NEG  -Djbi.target="ccfg-cluster" -Djbi.component.name=$COMPONENT_NAME list-component-configuration

#  Test (d) getting component configuration for target=standalone instance when component is shutdown
$JBI_ANT_NEG  -Djbi.target="ccfg-instance1" -Djbi.component.name=$COMPONENT_NAME list-component-configuration

#  Test (e) getting component configuration for target=server when component is shutdown
$JBI_ANT_NEG  -Djbi.target="server" -Djbi.component.name=$COMPONENT_NAME list-component-configuration

# Component Cleanup
$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME  -Djbi.target="ccfg-cluster" uninstall-component
uninstallComponentDelay
$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME  -Djbi.target="ccfg-instance1" uninstall-component
uninstallComponentDelay
$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME  -Djbi.target="server" uninstall-component
uninstallComponentDelay

# Cluster and Member Instance Cleanup
asadmin stop-instance $ASADMIN_PW_OPTS --port $ASADMIN_PORT ccfg-cluster-instance1
stopInstanceDelay
asadmin stop-instance $ASADMIN_PW_OPTS --port $ASADMIN_PORT ccfg-cluster-instance2
stopInstanceDelay
asadmin stop-instance $ASADMIN_PW_OPTS --port $ASADMIN_PORT ccfg-instance1
stopInstanceDelay
asadmin delete-instance --port $ASADMIN_PORT $ASADMIN_PW_OPTS ccfg-cluster-instance1
deleteInstanceDelay
asadmin delete-instance --port $ASADMIN_PORT $ASADMIN_PW_OPTS ccfg-cluster-instance2
deleteInstanceDelay
asadmin delete-instance --port $ASADMIN_PORT $ASADMIN_PW_OPTS ccfg-instance1
deleteInstanceDelay
asadmin delete-cluster $ASADMIN_PW_OPTS --port $ASADMIN_PORT ccfg-cluster
deleteClusterDelay
