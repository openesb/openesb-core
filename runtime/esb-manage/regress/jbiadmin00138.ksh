#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)jbiadmin00138.ksh
# Copyright 2004-2008 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

echo "jbiadmin00138 : Test for CR 6546022 - undeploy after partial deployment in cluster"

####
# This test installs a binding component that allows only one successful deployment.
# (if it is able to create a shared file). Subsequent deployment to this binding component 
# on the same machine would fail because this shared file exists. Using this SA deployment is
# made to go through in one instance of a cluster and fail in another instance. 
# after the partial deployment, undeploy is attempted and verified that it is successful
####
. ./regress_defs.ksh

#package component and sa files
ant -q  -lib "$REGRESS_CLASSPATH" -f jbiadmin00138.xml 1>&2

#create cluster, create instances and start the instances

asadmin create-cluster --port $ASADMIN_PORT --user $AS_ADMIN_USER $ASADMIN_PW_OPTS testcluster >&1
createClusterDelay
asadmin create-instance  --port $ASADMIN_PORT --user $AS_ADMIN_USER $ASADMIN_PW_OPTS --cluster testcluster --nodeagent agent1 testinstance1 >&1
createInstanceDelay
asadmin create-instance  --port $ASADMIN_PORT --user $AS_ADMIN_USER $ASADMIN_PW_OPTS --cluster testcluster --nodeagent agent1 testinstance2 >&1
createInstanceDelay
asadmin start-instance  --port $ASADMIN_PORT -u $AS_ADMIN_USER $ASADMIN_PW_OPTS testinstance1 >&1
startInstanceDelay
asadmin start-instance  --port $ASADMIN_PORT -u $AS_ADMIN_USER $ASADMIN_PW_OPTS testinstance2 >&1
startInstanceDelay

#remove the shared file if it exists
if [ -f "$SVC_TEST_CLASSES/dist/shared-file.txt" ]; then
    rm $SVC_TEST_CLASSES/dist/shared-file.txt
fi

#install the component, deploy the SA in cluster target
asadmin install-jbi-component  -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --target=testcluster --port $ASADMIN_PORT $JV_SVC_TEST_CLASSES/dist/DeployOnceBindingComponent.jar  2>&1
installComponentDelay
asadmin deploy-jbi-service-assembly -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT --target=testcluster $JV_SVC_TEST_CLASSES/dist/cluster-partial-deploy-sa.jar >&1 
deploySaDelay

# now verify that undeploy works
asadmin undeploy-jbi-service-assembly -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT --target=testcluster cluster-partial-deploy-sa 2>&1
undeploySaDelay

# cleanup
asadmin shut-down-jbi-component -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT --target=testcluster DeployOnceBindingComponent 2>&1
stopComponentDelay
asadmin uninstall-jbi-component -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT --target=testcluster DeployOnceBindingComponent 2>&1
uninstallComponentDelay

asadmin stop-instance --port $ASADMIN_PORT -u $AS_ADMIN_USER $ASADMIN_PW_OPTS testinstance1 >&1
stopInstanceDelay
asadmin stop-instance --port $ASADMIN_PORT -u $AS_ADMIN_USER $ASADMIN_PW_OPTS testinstance2 >&1
stopInstanceDelay

asadmin delete-instance --port $ASADMIN_PORT -u $AS_ADMIN_USER $ASADMIN_PW_OPTS testinstance1 >&1
deleteInstanceDelay
asadmin delete-instance --port $ASADMIN_PORT -u $AS_ADMIN_USER $ASADMIN_PW_OPTS testinstance2 >&1
deleteInstanceDelay
asadmin delete-cluster --port $ASADMIN_PORT -u $AS_ADMIN_USER $ASADMIN_PW_OPTS testcluster >&1
deleteClusterDelay

rm -rf $AS8BASE/nodeagents/agent1/testinstance* 
