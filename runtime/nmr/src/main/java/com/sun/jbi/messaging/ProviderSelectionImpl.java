package com.sun.jbi.messaging;

import com.sun.jbi.ComponentManager;
import java.util.ArrayList;
import javax.jbi.component.Component;

/**
 * @author David
 */
public class ProviderSelectionImpl implements ProviderSelection {
    
    public ProviderSelectionImpl(){
        
    }
    
    @Override
    public RegisteredEndpoint selectProvider(MessageExchangeProxy exchange,
                        ArrayList<RegisteredEndpoint> providers,
                        String channelID,
                        ComponentManager componentManager){

        /*             
         *JBI 1.0 mentions that we must ask the consumer and the provider
         *if they are okay with this exchange before processing it
         *However, the main components always return true for these two methods
         *except the BPEL engine, which returns false when it should not.
         *So we bypass this verification
         * (Uncomment to restore the verification)
         */
        //providers = removeNotMatchingProvider(exchange, providers, channelID, componentManager);
        
        if(providers.size()>0){
            return providers.get(0);
        }       
        
        //no match
        return null;
    }
    
    protected ArrayList<RegisteredEndpoint> removeNotMatchingProvider(MessageExchangeProxy exchange,
                        ArrayList<RegisteredEndpoint> providers,
                        String channelID,
                        ComponentManager componentManager){
        
        ArrayList<RegisteredEndpoint> matching = new ArrayList<RegisteredEndpoint>();
        
        Component sourceComponent = componentManager.getComponentInstance(channelID);
        Component destComponent = null;
        for(RegisteredEndpoint provider : providers){
            destComponent = componentManager.getComponentInstance(provider.getOwnerId());
            //exchange.beforeCapabilityCheck(provider);
            if(sourceComponent.isExchangeWithProviderOkay(provider, exchange)
                    && destComponent.isExchangeWithConsumerOkay(provider, exchange)){
                //exchange.afterCapabilityCheck();
                matching.add(provider);
            }
            //exchange.afterCapabilityCheck();            
        }
        return matching;
    }
    
}
