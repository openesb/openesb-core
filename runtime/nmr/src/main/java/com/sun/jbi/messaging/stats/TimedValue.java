package com.sun.jbi.messaging.stats;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class TimedValue extends Value {
    
    private long lastTime = System.currentTimeMillis();

    public long getLastTime() {
        return lastTime;
    }
    
    @Override
    public void zero()
    {
        super.zero();
        lastTime = System.currentTimeMillis();
    }
}
