package com.sun.jbi.messaging;

import java.util.Iterator;
import java.util.ServiceLoader;
import java.util.logging.Logger;

/**
 * @author theyoz
 * @since 30/09/18
 */
public class MessageServiceFactory {
    private static final Logger LOGGER = Logger.getLogger(MessageServiceFactory.class.getName());
    private static MessageServiceSpi factory;

    public static MessageService newInstance() {
        if (factory == null) {
            ServiceLoader<MessageServiceSpi> serviceLoader = ServiceLoader.load(MessageServiceSpi.class);

            Iterator<MessageServiceSpi> ite = serviceLoader.iterator();

            if (ite.hasNext()) {
                factory = ite.next();

                if (ite.hasNext()) {
                    LOGGER.warning(
                            String.format("Multiple MessageServiceSpi found, using %s", factory.getClass().getName()));
                }
            } else {
                factory = new MessageServiceSpi();
            }
        }

        return factory.create();
    }
}
