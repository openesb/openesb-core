package com.sun.jbi.messaging;

/**
 * @author theyoz
 * @since 30/09/18
 */
public class MessageServiceSpi {
    public MessageService create() {
        return new MessageService();
    }
}
