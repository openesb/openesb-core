/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)FakeTransactionManager.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.messaging;

import java.util.HashMap;
import java.util.Random;

import javax.transaction.Transaction;
import javax.transaction.TransactionManager;

/** Simple utility class that implements a TransactionManager for testing.
 *  binding tests in independent threads.
 * @author Sun Microsystems, Inc.
 */
public class FakeTransactionManager
    implements javax.transaction.TransactionManager
{
    private static TransactionManager  mTM = new FakeTransactionManager();
    private HashMap     mThread;
    private Random      mRandom;

    public static TransactionManager getTM()
    {
        return (mTM);
    }
    
    private FakeTransactionManager()
    {
        mThread = new HashMap();
        mRandom = new Random();
      
    }
    
    public void begin() 
        throws javax.transaction.NotSupportedException,
                javax.transaction.SystemException 
    {
        Transaction     x;
        byte[]          id = new byte[8];
        
        mRandom.nextBytes(id);
        x = new FakeTransaction(this, id);
        mThread.put(Thread.currentThread(), x);
    }
    
    public void commit() 
        throws javax.transaction.RollbackException, 
                javax.transaction.HeuristicMixedException, 
                javax.transaction.HeuristicRollbackException, 
                java.lang.SecurityException, 
                java.lang.IllegalStateException, 
                javax.transaction.SystemException 
    {
        Transaction x;
        x = (Transaction)mThread.get(Thread.currentThread());
        if (x == null)
        {
            throw new javax.transaction.SystemException();
        }
        x.commit();
     }
    
    public int getStatus() 
        throws javax.transaction.SystemException 
    {
        Transaction x;
        x = (Transaction)mThread.get(Thread.currentThread());
        if (x == null)
        {
            throw new javax.transaction.SystemException();
        }
        
        return (x.getStatus());
    }
    
    public javax.transaction.Transaction getTransaction() 
        throws javax.transaction.SystemException 
    {
        Transaction x;
        
        x = (Transaction)mThread.get(Thread.currentThread());
        return (x);
    }
    
    public void resume(javax.transaction.Transaction transaction) 
        throws javax.transaction.InvalidTransactionException, 
                java.lang.IllegalStateException, 
                javax.transaction.SystemException 
    {
        Transaction x;
        x = (Transaction)mThread.get(Thread.currentThread());
        if (x != null)
        {
            throw new javax.transaction.SystemException();
        }
        mThread.put(Thread.currentThread(), transaction);
    }
    
    public void rollback() 
        throws java.lang.IllegalStateException, 
                java.lang.SecurityException, 
                javax.transaction.SystemException 
    {
        Transaction x;
        x = (Transaction)mThread.get(Thread.currentThread());
        if (x == null)
        {
            throw new javax.transaction.SystemException();
        }
        x.rollback();
    }
    
    public void setRollbackOnly() 
        throws java.lang.IllegalStateException, 
                javax.transaction.SystemException 
    {
        Transaction x;
        x = (Transaction)mThread.get(Thread.currentThread());
        if (x == null)
        {
            throw new javax.transaction.SystemException();
        }
        x.setRollbackOnly();
    }
    
    public void setTransactionTimeout(int param) 
        throws javax.transaction.SystemException 
    {
    }
    
    public javax.transaction.Transaction suspend() 
        throws javax.transaction.SystemException 
    {
        Transaction x;
        x = (Transaction)mThread.get(Thread.currentThread());
        if (x == null)
        {
            throw new javax.transaction.SystemException();
        }
        mThread.remove(Thread.currentThread());
        return (x);
    }
}
