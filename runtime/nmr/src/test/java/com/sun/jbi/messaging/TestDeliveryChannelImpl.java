/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestDeliveryChannelImpl.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.messaging;

import javax.jbi.component.Component;
import javax.jbi.component.ComponentContext;

import javax.jbi.messaging.ExchangeStatus;
import javax.jbi.messaging.InOnly;
import javax.jbi.messaging.MessageExchange;
import javax.jbi.messaging.MessageExchangeFactory;

import javax.jbi.servicedesc.ServiceEndpoint;

import javax.xml.namespace.QName;

import javax.management.openmbean.CompositeData;

/**
 * Tests for the DeliveryChannelImpl class
 *
 * @author Sun Microsystems, Inc.
 * @author theyoz
 */
public class TestDeliveryChannelImpl extends junit.framework.TestCase
{
    /** Component ID for test channel. */
    private static final String ID_A = "ChannelA";
    /** Component ID for test channel. */
    private static final String ID_B = "ChannelB";
    /** Component ID for test channel. */
    private static final String ID_C = "Observer";
    
    /** Service and endpoint constants. */
    private static final QName  OPERATION       = new QName("operation");
    private static final QName  SERVICE_A       = new QName("service-a");
    private static final String ENDPOINT_A      = "endpoint-a";    
    private static final QName  SERVICE_B       = new QName("service-b");
    private static final String ENDPOINT_B      = "endpoint-b";
    private static final QName  SERVICE_B2      = new QName("service-b2");
    private static final String ENDPOINT_B2     = "endpoint-b2";
    private static final QName  SERVICE_FOO     = new QName("foo");
    private static final String ENDPOINT_BAR    = "bar";
    
    /** NMS impl */
    private MessageService mMsgSvc;
     /** NMR Environment Context */
    private NMRContext mContext;
    /** Test channel which is created/destroyed for each test. */
    private DeliveryChannelImpl mChannelA;
    /** Test channel which is created/destroyed for each test. */
    private DeliveryChannelImpl mChannelB;
    /** Test channel which is created/destroyed for each test. */
    private DeliveryChannelImpl mChannelC;
    /** Exchange factory for channel A. */
    private MessageExchangeFactory mFactoryA;
    /** Endpoint Reference on Channel A */    
    private RegisteredEndpoint   mEndpointA;
    /** Endpoint Reference on Channel B */    
    private RegisteredEndpoint   mEndpointB;

    /**
     * The constructor for this testcase, forwards the test name to
     * the jUnit TestCase base class.
     * @param aTestName String with the name of this test.
     */
    public TestDeliveryChannelImpl(String aTestName)
    {
        super(aTestName);
        
        mMsgSvc  = MessageServiceFactory.newInstance();
        mContext = new NMRContext(mMsgSvc);
    }
    

    /**
     * Setup for the test. This creates the ComponentRegistry instance
     * and other objects needed for the tests.
     * @throws Exception when set up fails for any reason.
     */
    public void setUp()
        throws Exception
    {
        super.setUp();
        
        mMsgSvc.initService(mContext);
        mMsgSvc.startService();
        
        // create test channels and add them to the NMS routing table
        mChannelA  = new DeliveryChannelImpl(ID_A, null, mMsgSvc, null);
        mChannelB  = new DeliveryChannelImpl(ID_B, null, mMsgSvc, null);
        mChannelC  = new DeliveryChannelImpl(ID_C, null, mMsgSvc, null);
        mMsgSvc.addChannel(mChannelA);
        mMsgSvc.addChannel(mChannelB);
        mMsgSvc.addChannel(mChannelC);
        EndpointRegistry.getInstance().setMessageService(mMsgSvc);

        mFactoryA = mChannelA.createExchangeFactory();
        
        mEndpointA = mChannelA.activateEndpoint(SERVICE_A, ENDPOINT_A);
        mEndpointB = mChannelB.activateEndpoint(SERVICE_B, ENDPOINT_B);        
    }

    /**
     * Cleanup for the test.
     * @throws Exception when tearDown fails for any reason.
     */
    public void tearDown()
        throws Exception
    {
        super.tearDown();
        
        mChannelA.close();
        mChannelB.close();
        mChannelC.close();
        
        mMsgSvc.stopService();
        mContext.reset();
    }

// =============================  test methods ================================

    /**
     * Test accept on a channel with a pending exchange.
     * @throws Exception if an unexpected error occurs
     */
    public void testAccept()
           throws Exception
    {
        InOnly me1;
        InOnly me2;
        
        me1 = mFactoryA.createInOnlyExchange();
        me1.setProperty("test", "keith");
        me1.setEndpoint(mEndpointB);
        me1.setOperation(OPERATION);
        mChannelA.send(me1);
        
        me2 = (InOnly)mChannelB.accept();
        
        // check to make sure we got something back
        assertTrue(me2 != null);
        
        // simple test to see if we got the same thing back
        assertTrue(me2.getProperty("test") != null);
    }
    
    /**
     * Test accept on a channel when closed.
     * @throws Exception if an unexpected error occurs
     */
    public void testAcceptClosed()
           throws Exception
    {
        mChannelB.close();
        try
        {
            mChannelB.accept();
	    fail("No exception on accept of a closed channel.");
        }
	catch (javax.jbi.messaging.MessagingException mEx)
        {
            //
            // A exception for a closed channel should not return a cause.
            // This allows the caller to determine if the channel is closed or 
            // some other error has occured.
            //
	    assertTrue(mEx.getCause() == null);
        }
    }
 
    /**
     * Make sure accept returns when the channel is closed.
     * @throws Exception if an unexpected error occurs
     */
    public void testAcceptOnClose()
           throws Exception
    {
        DeliveryChannelImpl channel;       
        Acceptor            acceptor;
        Exception           error = null;
        
        channel = new DeliveryChannelImpl("foo", null, mMsgSvc, null);
        acceptor = new Acceptor(channel);
        
        // start the accept and then close the channel
        acceptor.start();
        
        // give the thread a chance to run
        Thread.sleep(200);
        
        // close the channel to release the blocking accept
        channel.close();
        
        // give the close() method a chance to acquire the lock
        Thread.sleep(200);
        
        // make sure the thread didn't abend
        if (acceptor.hasError())
        {
            fail(acceptor.getError().toString());            
        }
        
        // see if our accept thread is still running
        assertTrue(!acceptor.isAlive());
    }
    
 
    /**
     * testDeactivateEndpoint
     * @throws Exception if an unexpected error occurs
     */
    public void testDeactivateEndpointGood()
           throws Exception
    {
        RegisteredEndpoint a;
        RegisteredEndpoint b;
        
        // deactivate inbound and outbound endpoints
        mChannelA.deactivateEndpoint(mEndpointA);
        mChannelB.deactivateEndpoint(mEndpointB);
        
        a  = (RegisteredEndpoint)mEndpointA;
        b = (RegisteredEndpoint)mEndpointB;
        
        // make sure the deactivation took place
        assertTrue(!a.isActive());
        assertTrue(!b.isActive());

        // try again: deactivate inbound and outbound endpoints
        mChannelA.deactivateEndpoint(mEndpointA);
        mChannelB.deactivateEndpoint(mEndpointB);
    }
    
    public void testDeactivateEndpointBad()
           throws Exception
    {
        // try to deactivate channel B's reference using channel A
        try
        {
            mChannelA.deactivateEndpoint(mEndpointB);
            fail("able to deactivate endpoint that I didn't activate");
        }
        catch (javax.jbi.messaging.MessagingException msgEx) {}
        
        // make sure the ref wasn't deactivated, even though an exception was thrown
        if (!((RegisteredEndpoint)mEndpointB).isActive())
        {
            fail("able to deactivate endpoint that I didn't activate");
        }
    }
        
    /**
     * testActivateEndpointGood
     * @throws Exception if an unexpected error occurs
     */
    public void testActivateEndpointGood()
           throws Exception
    {
        RegisteredEndpoint re;
        
        re = (RegisteredEndpoint)mChannelA.activateEndpoint(SERVICE_FOO, ENDPOINT_BAR);
        
        assertTrue(re.isActive());
    }    
    
    /**
     * testActivateEndpointBad
     * @throws Exception if an unexpected error occurs
     */
    public void testActivateEndpointBad()
           throws Exception
    {
        ServiceEndpoint er;
             
        try
        {
            er = mChannelA.activateEndpoint(SERVICE_B, ENDPOINT_B);
            fail("able to activate a duplicate endpoint");
        }
        catch (javax.jbi.messaging.MessagingException msgEx) {}
    }
    
    /**
     * Happy path for send.
     * @throws Exception if an unexpected error occurs
     */
    public void testSendGood()
           throws Exception
    {
        InOnly me1;
        
        me1 = mFactoryA.createInOnlyExchange();
        me1.setEndpoint(mEndpointB);
        me1.setOperation(OPERATION);
        mChannelA.send(me1);
        
        // Attempt to receive the exchange on channel B
        assertTrue(mChannelB.accept() != null);
    }
    
    /**
     * Failure path for send.
     * @throws Exception if an unexpected error occurs
     */
    public void testSendBad()
           throws Exception
    {
        InOnly me1;
        
        // close channel A
        mChannelA.close();
        
        me1 = mFactoryA.createInOnlyExchange();
        me1.setEndpoint(mEndpointB);
        me1.setOperation(OPERATION);        
        
        // try to send on a closed channel
        try
        {
            mChannelA.send(me1);
            fail("Able to send exchange on a closed channel!");
        }
        catch (javax.jbi.messaging.MessagingException msgEx) {}
    }
    
    public void testSendServiceGood()
        throws Exception
    {
        InOnly me1;
        
        me1 = mFactoryA.createInOnlyExchange();
        me1.setService(SERVICE_B);
        me1.setOperation(OPERATION);
        mChannelA.send(me1);
        
        // Attempt to receive the exchange on channel B
        assertTrue(mChannelB.accept() != null);
    }
    
    
    public void testSendServiceBad()
        throws Exception
    {        
        InOnly me1;
                
        me1 = mFactoryA.createInOnlyExchange();
        me1.setService(new QName("BOGUS"));
        me1.setOperation(OPERATION);        
        
        // try to send on a closed channel
        try
        {
            mChannelA.send(me1);
            fail("Able to send exchange with bogus service!");
        }
        catch (javax.jbi.messaging.MessagingException msgEx) 
	{
	}
    }
    
    
    public void testSendInterfaceGood()
        throws Exception
    {
        final QName INTERFACE_NAME = new QName("myinterface");
        ServiceEndpoint	ie;

        InOnly me1;
        
        // add interface QName to service endpoint
        ie = mChannelB.activateEndpoint(SERVICE_B2, ENDPOINT_B2);        
        ((RegisteredEndpoint)ie).setInterfaces(new QName[] {INTERFACE_NAME});

        me1 = mFactoryA.createInOnlyExchange();
        me1.setInterfaceName(INTERFACE_NAME);
        me1.setOperation(OPERATION);
        mChannelA.send(me1);
        mChannelB.deactivateEndpoint(ie);        

        // Attempt to receive the exchange on channel B
        assertTrue(mChannelB.accept() != null);
    }
    
    
    public void testSendInterfaceBad()
        throws Exception
    {           
        InOnly me1;
        
        me1 = mFactoryA.createInOnlyExchange();
        me1.setService(new QName("RUBBISH"));
        me1.setOperation(OPERATION);        
        
        // try to send on a closed channel
        try
        {
            mChannelA.send(me1);
            fail("Able to send exchange with rubbish interface!");
        }
        catch (javax.jbi.messaging.MessagingException msgEx) {}
    }
    
    public void testSendExternalEndpointBad()
        throws Exception
    {           
        InOnly me1;
        ServiceEndpoint	se = new Endpoint(SERVICE_A, ENDPOINT_B);
	ServiceEndpoint	ep[];

        me1 = mFactoryA.createInOnlyExchange();
        mChannelA.registerExternalEndpoint(se);
	ep = mChannelA.getExternalEndpoints(null);
        me1.setEndpoint(ep[0]);
        
        // try to send using an external endpoint
        try
        {
            mChannelA.send(me1);
            fail("Able to send exchange with external endpoint!");
        }
        catch (javax.jbi.messaging.MessagingException msgEx) {}
    }
    
    public void testCloseBeforePendingAccept()
        throws Exception
    {           
        InOnly me1;

        me1 = mFactoryA.createInOnlyExchange();
        me1.setEndpoint(mEndpointB);
        
	mChannelA.send(me1);
	mChannelB.close();
        assertTrue(mChannelA.accept() != null);
    }

    /**
     * Check that the transactional properties are supported.
     * @throws Exception if unexcepted error occurs.
     */
    public void testTransactional()
        throws Exception
    {
        assertTrue(mChannelA.isTransactional());
        mChannelA.setTransactional(false);
        assertTrue(!mChannelA.isTransactional());
        mChannelA.setTransactional(true);
        assertTrue(mChannelA.isTransactional());
    }
    
    public void testSendInterfaceWsdl20()
        throws Exception
    {
        String              id = "testcomp";
        Component           com;
        ComponentContext    ctx;
        MessageExchange     me;
        
        // create a component, add it to env, and get it's context object
        com = new NMRComponent();
        mContext.addComponentInstance(id, com);
        ctx = mContext.getComponentContext(id);
        
        // activate an endpoint and create an exchange for one of its operations
        ctx.activateEndpoint(
            WsdlDocument.STOCK_SERVICE_Q, WsdlDocument.STOCK_ENDPOINT);
        me = mChannelA.createExchangeFactory().createInOutExchange();
        
        me.setInterfaceName(WsdlDocument.STOCK_INTERFACE_Q);
        me.setOperation(WsdlDocument.STOCK_OPERATION_Q);
        
        mChannelA.send(me);
        
        assertTrue(ctx.getDeliveryChannel().accept() != null);
    }
    
    public void testSendInterfaceWsdl11()
        throws Exception
    {
        String              id = "testcomp";
        Component           com;
        ComponentContext    ctx;
        MessageExchange     me;
        
        // create a component, add it to env, and get it's context object
        com = new NMRComponent(NMRComponent.WSDL_11);
        mContext.addComponentInstance(id, com);
        ctx = mContext.getComponentContext(id);
        
        // activate an endpoint and create an exchange for one of its operations
        ctx.activateEndpoint(
            WsdlDocument.STOCK_SERVICE_Q, WsdlDocument.STOCK_ENDPOINT);
        me = mChannelA.createExchangeFactory().createInOutExchange();
        
        me.setInterfaceName(WsdlDocument.STOCK_INTERFACE_Q);
        me.setOperation(WsdlDocument.STOCK_OPERATION_Q);
        
        mChannelA.send(me);
        
        assertTrue(ctx.getDeliveryChannel().accept() != null);
    }
    
    /** Make sure that all aspects of a service endpoint (including interface
     *  information) are cleaned up when the endpoint is deactivated).
     */
    public void testRemoveEndpointWSDL()
        throws Exception
    {
        String              id = "testcomp";
        Component           com;
        ComponentContext    ctx;
        ServiceEndpoint     se;
        
        // create a component, add it to env, and get it's context object
        com = new NMRComponent(NMRComponent.WSDL_11);
        mContext.addComponentInstance(id, com);
        ctx = mContext.getComponentContext(id);
        
        // activate an endpoint and verify it exists
        se = ctx.activateEndpoint(
            WsdlDocument.STOCK_SERVICE_Q, WsdlDocument.STOCK_ENDPOINT);
        assertTrue(ctx.getEndpoints(WsdlDocument.STOCK_INTERFACE_Q).length == 1);
        
        // deactivate and verify that the endpoint no longer exists
        ctx.deactivateEndpoint(se);
        assertTrue(ctx.getEndpointsForService(WsdlDocument.STOCK_SERVICE_Q).length == 0);
        assertTrue(ctx.getEndpoints(WsdlDocument.STOCK_INTERFACE_Q).length == 0);        
    }
    
    /**
     * Test Observer
     * @throws Exception if an unexpected error occurs
     */
    public void testObserver()
           throws Exception
    {
        InOnly          me1;
        MessageExchange me;
        
        // Setup an Observer.
        
        mMsgSvc.addObserver(mChannelC);
        
        // Perform a simple exchange.
        
        me1 = mFactoryA.createInOnlyExchange();
        me1.setEndpoint(mEndpointB);
        me1.setOperation(OPERATION);
        mChannelA.send(me1);
        me1 = (InOnly)mChannelB.accept();
        me1.setStatus(ExchangeStatus.DONE);
        mChannelB.send(me1);
        mChannelA.accept();
        
        // Now check the Observer results.
        me = mChannelC.accept();
        assertTrue(me instanceof Observer);
        assertTrue(me.getRole().equals(MessageExchange.Role.CONSUMER));
        assertTrue(me.getStatus().equals(ExchangeStatus.ACTIVE));
        assertTrue(me.getOperation().equals(OPERATION));
        assertTrue(me.getProperty("com.sun.jbi.observer.sender").equals("ChannelA"));
        assertTrue(me.getProperty("com.sun.jbi.observer.receiver").equals("ChannelB"));
        assertTrue(me.getPattern().equals(ExchangePattern.IN_ONLY.getURI()));
        
        me = mChannelC.accept();
        assertTrue(me instanceof Observer);
        assertTrue(me.getRole().equals(MessageExchange.Role.PROVIDER));
        assertTrue(me.getStatus().equals(ExchangeStatus.DONE));
        assertTrue(me.getOperation().equals(OPERATION));
        assertTrue(me.getProperty("com.sun.jbi.observer.sender").equals("ChannelB"));
        assertTrue(me.getProperty("com.sun.jbi.observer.receiver").equals("ChannelA"));
        assertTrue(me.getPattern().equals(ExchangePattern.IN_ONLY.getURI()));
        assertTrue(mChannelC.accept(1) == null);
        
        mMsgSvc.removeObserver(mChannelC);

        // Perform a simple exchange.
        
        me1 = mFactoryA.createInOnlyExchange();
        me1.setEndpoint(mEndpointB);
        me1.setOperation(OPERATION);
        mChannelA.send(me1);
        me1 = (InOnly)mChannelB.accept();
        me1.setStatus(ExchangeStatus.DONE);
        mChannelB.send(me1);
        mChannelA.accept();
        
        // Make sure that be don't observe this.
        assertTrue(mChannelC.accept(1) == null);
    }

    public void testStatistics()
        throws Exception
    {
        InOnly me1;
        
        me1 = mFactoryA.createInOnlyExchange();
        me1.setService(SERVICE_B);
        me1.setOperation(OPERATION);
        mChannelA.send(me1);
        assertTrue((me1 = (InOnly)mChannelB.accept()) != null);
        me1.setStatus(ExchangeStatus.DONE);
        mChannelB.send(me1);
        assertTrue(mChannelA.accept() != null);
                
        CompositeData   cd = mChannelA.getStatistics();
        assertTrue(cd != null);
        assertEquals(20, cd.values().size());
        mMsgSvc.enableStatistics();
        me1 = mFactoryA.createInOnlyExchange();
        me1.setService(SERVICE_B);
        me1.setOperation(OPERATION);
        mChannelA.send(me1);
        assertTrue((me1 = (InOnly)mChannelB.accept()) != null);
        me1.setStatus(ExchangeStatus.DONE);
        mChannelB.send(me1);
        assertTrue(mChannelA.accept() != null);
        cd = mChannelA.getStatistics();
        mMsgSvc.disableStatistics();
        assertTrue(cd != null);
        assertEquals(40, cd.values().size());
    }
 
    public void testEndpointStatistics()
        throws Exception
    {
        InOnly me1;
        
        me1 = mFactoryA.createInOnlyExchange();
        me1.setService(SERVICE_B);
        me1.setOperation(OPERATION);
        mChannelA.send(me1);
        assertTrue((me1 = (InOnly)mChannelB.accept()) != null);
        me1.setStatus(ExchangeStatus.DONE);
        mChannelB.send(me1);
        assertTrue(mChannelA.accept() != null);
                
        String[] c = mChannelB.getEndpointNames();
        for (String c1 : c) {
            EndpointStatistics es = mChannelB.getEndpointStatistics(c1);
            assertTrue(es != null);
            CompositeData   cd = es.getStatistics();
            assertTrue(cd != null);
            assertTrue(cd.values().size() == 14);
            mMsgSvc.enableStatistics();
            me1 = mFactoryA.createInOnlyExchange();
            me1.setService(SERVICE_B);
            me1.setOperation(OPERATION);
            mChannelA.send(me1);
            assertTrue((me1 = (InOnly)mChannelB.accept()) != null);
            me1.setStatus(ExchangeStatus.DONE);
            mChannelB.send(me1);
            assertTrue(mChannelA.accept() != null);
            cd = es.getStatistics();
            mMsgSvc.disableStatistics();
            assertTrue(cd != null);
            assertTrue(cd.values().size() == 30);
        }
    }
    
    /** Tests thread-safety of DeliveryChannelImpl with concurrent use.
     */
    public void testConcurrency()
        throws Exception
    {
        MessageExchangeFactory mcf;
        
        InOnlyInitiator     ioi1;
        InOnlyInitiator     ioi2;
        InOnlyInitiator     ioi3;
        InOnlyInitiator     ioi4;
        InOnlyServicer      ios1;
        InOnlyServicer      ios2;
        InOnlyServicer      ios3;
        InOnlyServicer      ios4;
        
        mcf = mChannelA.createExchangeFactory(mEndpointB);
        
        ioi1 = new InOnlyInitiator("A", mChannelA, mcf.createInOnlyExchange());
        ios1 = new InOnlyServicer(mChannelB);
        
        ioi2 = new InOnlyInitiator("B", mChannelA, mcf.createInOnlyExchange());
        ioi3 = new InOnlyInitiator("C", mChannelA, mcf.createInOnlyExchange());
        ioi4 = new InOnlyInitiator("D", mChannelA, mcf.createInOnlyExchange());
        
        ios2 = new InOnlyServicer(mChannelB);
        ios3 = new InOnlyServicer(mChannelB);
        ios4 = new InOnlyServicer(mChannelB);
        
        ioi1.verifySuccess();
        ioi2.verifySuccess();
        ioi3.verifySuccess();
        ioi4.verifySuccess();
        
    }
    
    
    //------------------------  Supporting Classes -------------------------
      
    class Acceptor extends Thread
    {
        DeliveryChannelImpl mChannel;
        Exception           mError;
        
        Acceptor(DeliveryChannelImpl channel)
        {
            mChannel = channel;
        }
        
        public void run()
        {
            try
            {
                mChannel.accept();
            }
            catch (Exception ex)
            {
                mError = ex;
            }
        }
        
        public Exception getError()
        {
            return mError;
        }
        
        public boolean hasError()
        {
            return mError != null;
        }
    }
    
    /** Initiating party for concurrency test. */
    class InOnlyInitiator extends Thread
    {
        String              mId;
        DeliveryChannelImpl mChannel;
        MessageExchange     mExchange;
        Exception           mError;
        
        InOnlyInitiator(String id,
                        DeliveryChannelImpl channel, 
                        MessageExchange exchange)
        {
            mId         = id;
            mChannel    = channel;
            mExchange   = exchange;
            
            mExchange.setOperation(OPERATION);
            start();
        }
        
        public void run()
        {
            try
            {
                // send exchange
                mChannel.send(mExchange);
                // accept it back with status
                mExchange = mChannel.accept();
            }
            catch (Exception ex)
            {
                mError = ex;
            }
        }
        
        public void verifySuccess()
            throws Exception
        {
            // make sure we're done
            join(5000);
            
            if (mError != null)
            {
                throw mError;
            }
            else if (mExchange == null)
            {
                throw new Exception("Initiator " + mId + 
                    " received null exchange from accept");
            }
            else
            {
                System.out.println("Iniator " + mId + " completed successfully.");
            }
        }        
    }
    
    /** Servicing party for concurrency test. */
    class InOnlyServicer extends Thread
    {        
        DeliveryChannelImpl mChannel;
        Exception           mError;
        
        InOnlyServicer(DeliveryChannelImpl channel)
        {
            mChannel = channel;
            start();
        }
        
        public void run()
        {
            try
            {
                MessageExchange me = mChannel.accept();
                me.setStatus(ExchangeStatus.DONE);
                mChannel.send(me);
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
            }
        }
        
        public void verifySuccess()
            throws Exception
        {
            if (mError != null)
            {
                throw mError;
            }
        }       
    }

    class Endpoint implements ServiceEndpoint
    {
        private QName mService;
        private String mEndpoint;
        
        Endpoint(QName service, String endpoint)
        {
            mService    = service;
            mEndpoint   = endpoint;
        }
        
        public org.w3c.dom.DocumentFragment getAsReference(QName operationName)
        {
            return null;
        }
        
        public String getEndpointName()
        {
            return mEndpoint;
        }
        
        public QName[] getInterfaces()
        {
            return null;
        }
        
        public javax.xml.namespace.QName getServiceName()
        {
            return mService;
        }
        
    }
}
