package net.openesb.security;

import javax.security.auth.Subject;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public interface SecurityHandler {
    
    Subject authenticate(String realmName, AuthenticationToken authenticationToken) 
            throws AuthenticationException;
}
