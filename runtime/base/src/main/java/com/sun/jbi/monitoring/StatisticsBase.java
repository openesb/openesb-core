/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)StatisticsBase.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.monitoring;

import java.util.List;

/**
 * This interface provides the common base for all of the statistics 
 * monitoring interfaces. The methods here provide the infrastucture for
 * creating an object tree that represents the statistics for the entire JBI
 * instance. Each instance of a statistics class has a parent and optionally
 * one or more children. Note that the top object in the tree has no parent.
 *
 * @author Sun Microsystems, Inc.
 */
public interface StatisticsBase extends StatisticsMBean
{
    /**
     * Get the key of this statistics object.
     * @return the key of this object.
     */
    String getKey();

    /**
     * Get the parent of this statistics object. The parent of the top object
     * in the tree is always null.
     * @return the parent object of this object.
     */
    StatisticsBase getParent();

    /**
     * Set the parent of this statistics object.
     * @param parent the parent object of this object.
     */
    void setParent(StatisticsBase parent);

    /**
     * Add a child statistics object.
     * @param child the statistics object that is a child of this object.
     */
    void addChild(StatisticsBase child);

    /**
     * Get a particular child of this statistics object based on a key.
     * @param key the string containing the key for the child object.
     * @return the statistics object with the specified key, or null if none
     * is found.
     */
    StatisticsBase getChild(String key);

    /**
     * Get a list of all children of this statistics object.
     * @return a List containing all the children objects. Returns an empty
     * list if no children exist.
     */
    List getChildren();

    /**
     * Register an MBean to represent this set of statistics.
     * @param interfaceClass the interface implemented by the MBean instance.
     * @param instance the MBean instance to register.
     * @param mbeanName the JMX ObjectName to use for this MBean.
     * @throws javax.jbi.JBIException If the MBean creation or registration
     * fails.
     */
    void registerMBean(
        Class interfaceClass,
        Object instance,
        javax.management.ObjectName mbeanName)
        throws javax.jbi.JBIException;

    /**
     * Unregister the MBean for this set of statistics.
     * @throws javax.jbi.JBIException If the MBean unregistration fails.
     */
    void unregisterMBean()
        throws javax.jbi.JBIException;
}
