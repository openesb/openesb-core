/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)MBeanSet.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management;

import javax.management.ObjectName;

/**
 * MBeanSet is used to manage a set of Standard MBean's. We provide methods
 * that to operate on the set as a whole; for example register and
 * deregister the set.
 *
 * All operations are keyed by the Object Name of the MBean.
 *
 * @author Sun Microsystems, Inc.
 */
public interface MBeanSet
{
    /**
     * Add an Standard MBean to the Set that can potentially emit JMX notifications.
     *
     * @param theMBeanName - the object name of the mbean to add.
     * @param theMBeanDefinition - the interface that defines the MBean.
     * @param theMBeanImplementation - an instance of the MBean implementation class.
     * @param isEmitter - true if this MBean wants to emit jmx notifications.
     *
     * @return true if mbean is added to the set, otherwise false.
     */
    boolean add(ObjectName theMBeanName, Class theMBeanDefinition,
        Object theMBeanImplementation, boolean isEmitter);

    /**
     * Add an Standard MBean to the Set that will not emit notifications.
     *
     * @param theMBeanName - the object name of the mbean to add.
     * @param theMBeanDefinition - the interface that defines the MBean.
     * @param theMBeanImplementation - an instance of the MBean implementation class.
     *
     * @return true if mbean is added to the set, otherwise false.
     */
    boolean add(ObjectName theMBeanName, Class theMBeanDefinition,
        Object theMBeanImplementation);

    /**
     * Replace a Standard MBean in the Set that can potentially emit JMX notifications.
     *
     * @param theMBeanName - the object name of the mbean to replace.
     * @param theMBeanDefinition - the interface that defines the MBean.
     * @param theMBeanImplementation - an instance of the MBean implementation class.
     * @param isEmitter - true if this MBean wants to emit jmx notifications.
     *
     * @return true if mbean is replaced in the set, otherwise false.
     */
    boolean replace(ObjectName theMBeanName, Class theMBeanDefinition,
        Object theMBeanImplementation, boolean isEmitter);

    /**
     * Replace a Standard MBean in the Set.
     *
     * @param theMBeanName - the object name of the mbean to replace.
     * @param theMBeanDefinition - the interface that defines the MBean.
     * @param theMBeanImplementation - an instance of the MBean implementation class.
     *
     * @return true if mbean is replaced in the set, otherwise false.
     */
    boolean replace(ObjectName theMBeanName, Class theMBeanDefinition,
        Object theMBeanImplementation);

    /**
     * Delete an MBean from an existing MBean Set.
     *
     * @param theMBeanName - the object name of the mbean to delete.
     *
     * @return true if the MBean is deleted from the set, otherwise false.
     */
    boolean delete(ObjectName theMBeanName);

    /**
     * True if an MBean exists in the named MBean Set.
     *
     * @param theMBeanName - the object name of the mbean to check.
     *
     * @return true if the MBean exists in the set, otherwise false.
     */
    boolean exists(ObjectName theMBeanName);

    /**
     * True if an MBean is part of this set and is registered.
     *
     * @param theMBeanName - the object name of the mbean to check.
     *
     * @return true if the MBean exists in the set, otherwise false.
     */
    boolean isRegistered(ObjectName theMBeanName);

    /**
     * Register all of the MBean's in an MBean Set.
     *
     * Okay if some are already registered.
     * Ignores null entries.
     *
     * To re-register, you must unregister the named object before
     * registering the set.
     *
     * @return true iff all mbeans are now registered.
     */
    boolean register();

    /**
     * Unregister all of the MBean's in an MBean Set.
     *
     * It is not an error to unregister an MBean that is not registered.
     *
     * @return true iff all mbeans are unregistered.
     */
    boolean unregister();

    /**
     * dump contents of an MBeanSet to the log.
     * @param aTitle - the title of the dump.
     */
    void dump(String aTitle);
}
