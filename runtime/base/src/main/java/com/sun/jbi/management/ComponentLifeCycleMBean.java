/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ComponentLifeCycleMBean.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management;

/**
 * This ComponentLifeCycleMBean extends the public interface to add a
 * getter for the DeployerMBean name.
 *
 * @author Sun Microsystems, Inc.
 */
public interface ComponentLifeCycleMBean
    extends javax.jbi.management.ComponentLifeCycleMBean
{
    /**
     * Shut down this component. 
     *
     * @param force set to true indicates that the component should be shutdown 
     * forcefully; that is, place the component in the "Shutdown" state regardless of the 
     * result of calling its LifeCycle shutDown method.
     * @exception javax.jbi.JBIException if some other failure occurs.
     */
    void shutDown(boolean force) throws javax.jbi.JBIException;
}
