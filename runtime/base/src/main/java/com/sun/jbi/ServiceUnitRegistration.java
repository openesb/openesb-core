/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ServiceUnitRegistration.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi;

/**
 * This interface provides services for registration/unregistration/query of
 * Service Units.
 * 
 * @author Sun Microsystems, Inc.
 */
public interface ServiceUnitRegistration
{
    /**
     * Check to see if a Service Unit with the specified name is registered
     * to the Component with the specified name.
     * @param componentName The unique Component name.
     * @param serviceUnitName The unique Service Unit name.
     * @return true if the name is registered, false if not.
     * @throws javax.jbi.JBIException if no Component exists with the specified
     * component name.
     */
    boolean isServiceUnitRegistered(String componentName,
                                    String serviceUnitName)
        throws javax.jbi.JBIException;

    /**
     * Register a Service Unit that has been deployed to a particular BC or
     * SE. This is done after a successful deployment operation.
     * @param componentName The unique name of the BC or SE.
     * @param serviceAssemblyName The unique name of the Service Assembly.
     * @param serviceUnitName The unique name of the Service Unit.
     * @param serviceUnitFilePath The fully-qualified path to the Service Unit
     * deployment descriptor.
     * @throws javax.jbi.JBIException if no Component exists with the specified
     * component name or if there is already a Service Unit with the specified
     * service unit name registered to the component.
     */
    void registerServiceUnit(
        String componentName,
        String serviceAssemblyName,
        String serviceUnitName,
        String serviceUnitFilePath)
        throws javax.jbi.JBIException;

    /**
     * Unregister a Service Unit that has been undeployed from a particular BC
     * or SE. This is done after a successful undeployment operation.
     * @param componentName The unique name of the BC or SE.
     * @param serviceUnitName The unique name of the Service Unit.
     * @throws javax.jbi.JBIException if no Component exists with the specified
     * component name or if there is no Service Unit with the specified
     * service unit name registered to the component.
     */
    void unregisterServiceUnit(
        String componentName,
        String serviceUnitName)
        throws javax.jbi.JBIException;
}
