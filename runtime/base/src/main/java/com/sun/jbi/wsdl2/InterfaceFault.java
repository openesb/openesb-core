/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)InterfaceFault.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.wsdl2;

import javax.xml.namespace.QName;

/**
 * API for  WSDL 2.0 Interface Fault component.
 *
 * @author ApiGen AX.00
 */
public interface InterfaceFault extends ExtensibleDocumentedComponent
{
    /**
     * Get the name of this component.
     *
     * @return The name of this component
     */
    String getName();

    /**
     * Set the name of this component.
     *
     * @param theName The name of this component
     */
    void setName(String theName);

    /**
     * Get qualified name referring to the element declaration of the payload
     * of the fault.
     *
     * @return Qualified name referring to the element declaration of the
     * payload of the fault
     */
    QName getElement();

    /**
     * Set qualified name referring to the element declaration of the payload
     * of the fault.
     *
     * @param theElement Qualified name referring to the element declaration
     * of the payload of the fault
     */
    void setElement(QName theElement);

    /**
     * Get qualified name of this component.
     *
     * @return Qualified name of this component
     */
    QName getQualifiedName();

}

// End-of-file: InterfaceFault.java
