/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)HttpErrorResponseException.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  HttpErrorResponseException.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on November 11, 2004, 1:18 PM
 */

package com.sun.jbi.binding.security;

/**
 *
 * @author Sun Microsystems, Inc.
 */
public class HttpErrorResponseException
     extends javax.jbi.JBIException 
{
    /** The HTTP Error Message. */
    private String mErrorMessage;
    
     /** The HTTP Error Code. */
    private int mErrorCode;
    
    /** 
     * Creates a new instance of HttpErrorResponseException.
     * @param message is a String describing the exception
     */
    public HttpErrorResponseException(String message) 
    {
        super(message);
    }
    
    /** 
     * Creates a new instance of HttpErrorResponseException.
     * @param message is a String describing the exception
     * @param cause is the cause of the exception. 
     */
    public HttpErrorResponseException(String message, Throwable cause)
    {
        super(message, cause);
    };

    /**
     * @param cause is the Cause of the Exception
     */
    public HttpErrorResponseException(Throwable cause) 
    {
        super(cause);
    }

    /**
     * Getter for property errorMessage.
     * @return Value of property errorMessage.
     */
    public String getErrorMessage ()
    {
        return mErrorMessage;
    }
    
    /**
     * Setter for property errorMessage.
     * @param errorMessage New value of property errorMessage.
     */
    public void setErrorMessage (String errorMessage)
    {
        mErrorMessage = errorMessage;
    }
    
    /**
     * Getter for property errorCode.
     * @return Value of property errorCode.
     */
    public int getErrorCode ()
    {
        return mErrorCode;
    }
    
    /**
     * Setter for property errorCode.
     * @param errorCode New value of property errorCode.
     */
    public void setErrorCode (int errorCode)
    {
        mErrorCode = errorCode;
    }
    
}
