/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ComponentExtensionMBean.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management;

import java.util.Map;
import javax.management.ObjectName;

/**
 * This ComponentExtensionMBean is a facade MBean which provides component specific 
 * information such as the ObjectNames of the custom configuration MBeans registered
 * by the component.
 *
 * @author Sun Microsystems, Inc.
 */
public interface ComponentExtensionMBean
{

    /**
     * Get the ObjectName of the Component Configuration facade MBean for a target.
     *
     * @param target - target name 
     * @return the Component Configuration MBean name.
     */
    public ObjectName getComponentConfigurationFacadeMBeanName(String target)
        throws javax.jbi.JBIException;
        
    /**
     * Get the ObjectNames of all the custom MBeans registered by the component on the
     * target. If the target is :
     * <ul>
     *  <li>domain     - the return map has all the instances the component is installed on,
     *                   the corresponding value is the ObjectName(s) of the cutom MBeans
     *                   registered on the instance. </li>
     *  <li>server     - the return map has a single instance "server",
     *                   the corresponding value is the ObjectName(s) of the cutom MBeans 
     *                   registered on the instance. </li>
     *  <li>"instance" - the return map has a single instance "instance",
     *                   the corresponding value is the ObjectName(s) of the cutom MBeans 
     *                   registered on the instance. </li>
     *  <li>"cluster"  - the return map has all the member instances in the cluster,
     *                   the corresponding value is the ObjectName(s) of the cutom MBeans 
     *                   registered on the instance.  </li>
     *  </ul>
     *
     * @param  customName - the custom control name which will filter the MBeans
     * @return a Map of the MBeanNames, each map entry is keyed by the instance
     *         name and the value is an array of ObjectNames of custom MBeans registered
     *         by the component on the instance. Only those MBeans whose ObjectNames 
     *         have been generated based on MBeanNames.createCustomComponentMBeanName()
     *         will be present in the array. If the component does not have any registered
     *         MBeans the ObjectName array is empty.
     * @exception javax.jbi.JBIException if some other failure occurs.
     */
    Map<String, ObjectName[]> getCustomMBeanNames(String customName) throws javax.jbi.JBIException;
    
    /**
     * Get the ObjectNames of all the Logger MBeans registered by the component on the
     * target. If the target is :
     * <ul>
     *  <li>domain     - the return map has all the instances the component is installed on,
     *                   the corresponding value is the ObjectName(s) of the Logger MBeans
     *                   registered on the instance. </li>
     *  <li>server     - the return map has a single instance "server",
     *                   the corresponding value is the ObjectName(s) of the Logger MBeans 
     *                   registered on the instance. </li>
     *  <li>"instance" - the return map has a single instance "instance",
     *                   the corresponding value is the ObjectName(s) of the Logger MBeans 
     *                   registered on the instance. </li>
     *  <li>"cluster"  - the return map has all the member instances in the cluster,
     *                   the corresponding value is the ObjectName(s) of the Logger MBeans 
     *                   registered on the instance.  </li>
     *  </ul>
     *
     * @return a Map of the MBeanNames, each map entry is keyed by the instance
     *         name and the value is an array of ObjectNames of Logger MBeans registered
     *         by the component on the instance. Only those MBeans whose ObjectNames 
     *         have been generated based on MBeanNames.createCustomComponentMBeanName()
     *         will be present in the array. If the component does not have any registered
     *         MBeans the ObjectName array is empty.
     * @exception javax.jbi.JBIException if some other failure occurs.
     */
    Map<String, ObjectName[]> getLoggerMBeanNames() throws javax.jbi.JBIException;
    
}
