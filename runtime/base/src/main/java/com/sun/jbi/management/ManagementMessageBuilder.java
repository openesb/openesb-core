/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ManagementMessageBuilder.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management;



/**
 * BuildManagementMessage defines the interface that must be invoked by
 * bindings and engines in order to construct exception messages before
 * throwing them back to the JBI Framework.
 *
 * @author Sun Microsystems, Inc.
 */

public interface ManagementMessageBuilder
{
    
    
    /** return an XML string of the component message(either status
     *   or exception message).
     * @param msgObject component message holder object
     * @return Message as XML string
     * @throws Exception if unable to build message
     *
     */
    String buildComponentMessage (ComponentMessageHolder msgObject)
        throws Exception;
    
}
