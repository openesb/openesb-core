/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestFrameworkStatistics.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.framework;

/**
 * Tests for FrameworkStatistics.
 *
 * @author Sun Microsystems, Inc.
 */
public class TestFrameworkStatistics
    extends junit.framework.TestCase
{
    /**
     * Instance of FrameworkStatistics.
     */
    private FrameworkStatistics mFrameworkStatistics;

    /**
     * The constructor for this testcase, forwards the test name to
     * the jUnit TestCase base class.
     * @param aTestName String with the name of this test.
     */
    public TestFrameworkStatistics(String aTestName)
    {
        super(aTestName);
    }

    /**
     * Setup for the test. This creates the FrameworkStatistics instance
     * and other objects needed for the tests.
     * @throws Exception when set up fails for any reason.
     */
    public void setUp()
        throws Exception
    {
        super.setUp();

        mFrameworkStatistics = new FrameworkStatistics();
    }

    /**
     * Cleanup for the test.
     * @throws Exception when tearDown fails for any reason.
     */
    public void tearDown()
        throws Exception
    {
        super.tearDown();
    }

// =============================  test methods ================================

    /**
     * Tests get/setLastRestartTime.
     * @throws Exception if an unexpected error occurs.
     */
    public void testLastRestartTime()
    {
        java.util.Date d = new java.util.Date();
        mFrameworkStatistics.setLastRestartTime(d);
        assertEquals("Failure on set/getLastRestartTime: ",
            d, mFrameworkStatistics.getLastRestartTime());
    }

    /**
     * Tests get/setStartupTime.
     * @throws Exception if an unexpected error occurs.
     */
    public void testStartupTime()
    {
        long t = Long.MAX_VALUE - 1;
        mFrameworkStatistics.setStartupTime(t);
        assertEquals("Failure on set/getStartupTime: ",
            t, mFrameworkStatistics.getStartupTime());
    }

    /**
     * Tests get/incrementRegistryAdds.
     * @throws Exception if an unexpected error occurs.
     */
    public void testRegistryAdds()
        throws Exception
    {
        long n = 0;
        assertEquals("Failure on getRegistryAdds: ",
            new Long(n),
            new Long(mFrameworkStatistics.getRegistryAdds()));
        mFrameworkStatistics.incrementRegistryAdds();
        ++n;
        assertEquals("Failure on incrementRegistryAdds: ",
            new Long(n),
            new Long(mFrameworkStatistics.getRegistryAdds()));
    }

    /**
     * Tests get/incrementRegistryDeletes.
     * @throws Exception if an unexpected error occurs.
     */
    public void testRegistryDeletes()
        throws Exception
    {
        long n = 0;
        assertEquals("Failure on getRegistryDeletes: ",
            new Long(n),
            new Long(mFrameworkStatistics.getRegistryDeletes()));
        mFrameworkStatistics.incrementRegistryDeletes();
        ++n;
        assertEquals("Failure on incrementRegistryDeletes: ",
            new Long(n),
            new Long(mFrameworkStatistics.getRegistryDeletes()));
    }

    /**
     * Test resetRegistryStatistics.
     * @throws Exception if an unexpected error occurs.
     */
    public void testResetRegistryStatistics()
    {
        mFrameworkStatistics.incrementRegistryAdds();
        mFrameworkStatistics.incrementRegistryDeletes();
        mFrameworkStatistics.resetRegistryStatistics();
        long n = 0;
        assertEquals("Failure on resetRegistryStatistics: ",
            new Long(n),
            new Long(mFrameworkStatistics.getRegistryAdds()));
        assertEquals("Failure on resetRegistryStatistics: ",
            new Long(n),
            new Long(mFrameworkStatistics.getRegistryDeletes()));
    }
}
