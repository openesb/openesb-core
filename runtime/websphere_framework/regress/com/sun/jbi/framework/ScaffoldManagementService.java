/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ScaffoldManagementService.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.framework;

import java.util.logging.Logger;

/**
 * This is the Management Service of the JBI Framework, which provides
 * the JMX MBean server and related items.
 *
 * @author Sun Microsystems, Inc.
 */
public class ScaffoldManagementService
    extends com.sun.jbi.management.system.ManagementService
{
   /**
    * Handle to the logger
    */
    private Logger mLog = Logger.getLogger(this.getClass().getPackage().getName());
    
   /**
    * Initialize the Management Service.
    * @param envContext the JBI environment context created
    * by the JBI framework
    * @throws javax.jbi.JBIException if an error occurs
    */
    public void initService(EnvironmentContext envContext)
        throws javax.jbi.JBIException
    {
        mLog.info("ScaffoldManagementService: initialization complete");
    }

    /**
     * Start the Management service.
     * @throws javax.jbi.JBIException if an error occurs.
     */
    public void startService()
        throws javax.jbi.JBIException
    {
        mLog.info("ScaffoldManagementService: startup complete");
    }

    /**
     * Stop the Management service.
     * @throws javax.jbi.JBIException if an error occurs.
     */
    public void stopService()
        throws javax.jbi.JBIException
    {
        mLog.info("ScaffoldManagementService: shutdown complete");
    }
    
    /** 
     *  Inform the management service of a component that has changed state.
     *  @param componentName name of a component that has changed state
     */
    public void updateComponentState(String componentName)
    {
        mLog.info("ScaffoldManagementService: updateComponentState for " +
            componentName);
    }
}
