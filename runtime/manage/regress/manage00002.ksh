#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)manage00002.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#
echo manage00002.ksh

. ./regress_defs.ksh

testcp="$JV_SVC_CLASSES\
${JBI_PS}$JV_SVC_TEST_CLASSES\
${JBI_PS}$JV_JBI_HOME/appserver/jbi.jar\
${JBI_PS}$JV_AS8BASE/lib/xalan.jar\
${JBI_PS}$JV_AS8BASE/jbi/lib/jbi_rt.jar\
"

java -Dcom.sun.jbi.home=$JV_SVC_ROOT/src -classpath "$testcp" com/sun/jbi/management/internal/support/XmlReaderMain         $JV_SVC_REGRESS/support/*.xml
