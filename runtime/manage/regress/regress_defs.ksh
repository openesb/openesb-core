#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)regress_defs.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#
#common definitions for management regression tests.

my_test_domain=domain1
. $SRCROOT/antbld/regress/common_defs.ksh

#this suite only uses DOS version of props (passed to ant):
JBI_DOMAIN_PROPS="$JV_JBI_DOMAIN_PROPS"

#this shoud by dynmaically assigned for ESB use cases
export JBI_TARGET_NAME
JBI_TARGET_NAME=server

export REGRESS_CLASSPATH
REGRESS_CLASSPATH="$JV_JBI_DOMAIN_ROOT/lib/jbi.jar\
${JBI_PS}$AS_INSTALL/lib/jmxremote.jar\
${JBI_PS}$AS_INSTALL/lib/jmxremote_optional.jar\
${JBI_PS}$AS_INSTALL/lib/appserv-rt.jar\
${JBI_PS}$AS_INSTALL/lib/j2ee.jar\
"

export IGNORED_EXCEPTIONS
IGNORED_EXCEPTIONS='java:comp/env|hello|WARNING\|j2ee-appserver1.4'
