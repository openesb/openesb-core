#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)manage00501.ksh
# Copyright 2004-2008 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#
#manage00501 - test the autoinstall and autodeploy functionality.
#the stand-alone JBI container version of this test is manage00511.ksh.

echo testname is manage00501
. ./regress_defs.ksh

echo "Before autoinstall ..."
$JBI_ANT list-service-engines
$JBI_ANT -Djbi.shared.library.name=b3c97e99-bcb2-4ee0-95e4-60e1cf1408aa list-shared-libraries

autoInstallDir="$JBI_DOMAIN_ROOT/jbi/autoinstall"
statusDir="$autoInstallDir/.autoinstallstatus"

rm -rf $autoInstallDir
mkdir -p $autoInstallDir

echo "Copying component and shared library installers into autoinstall directory."
cp $JBI_HOME/components/jbicalc/installers/calc.jar $autoInstallDir
cp $JBI_HOME/components/sharedlibraries/sns1.jar $autoInstallDir
autoInstallDelay

echo "After autoinstall ..."
echo "File count: $autoInstallDir"
ls -l $autoInstallDir | wc -l
echo "File count: $statusDir"
ls -l $statusDir | wc -l
echo "autoinstalled area success files"
ls -l $autoInstallDir | grep _installed | wc -l
ls -l $autoInstallDir | grep _uninstalled | wc -l
echo "autoinstalled area failed files"
ls -l $autoInstallDir | grep _notInstalled | wc -l
ls -l $autoInstallDir | grep _notUninstalled | wc -l
echo "recursive listing"
cd $autoInstallDir
find . -type f -print | sort

echo "After autoinstall ..."
$JBI_ANT list-service-engines
$JBI_ANT -Djbi.shared.library.name=b3c97e99-bcb2-4ee0-95e4-60e1cf1408aa list-shared-libraries

# reinstall
chmod +w $autoInstallDir/*.jar
# replace the calc.jar with a different one!
cp $JBI_HOME/components/jbicalc/installers/mathlib1.jar $autoInstallDir/calc.jar
cp $JBI_HOME/components/sharedlibraries/sns1.jar $autoInstallDir
autoInstallDelay

echo "After reinstall ..."
echo "File count: $autoInstallDir"
ls -l $autoInstallDir | wc -l
echo "File count: $statusDir"
ls -l $statusDir | wc -l
echo "autoinstalled area success files"
ls -l $autoInstallDir | grep _installed | wc -l
ls -l $autoInstallDir | grep _uninstalled | wc -l
echo "autoinstalled area failed files"
ls -l $autoInstallDir | grep _notInstalled | wc -l
ls -l $autoInstallDir | grep _notUninstalled | wc -l
echo "recursive listing"
find . -type f -print | sort

echo "After reinstall ..."
$JBI_ANT list-service-engines
$JBI_ANT -Djbi.shared.library.name=b3c97e99-bcb2-4ee0-95e4-60e1cf1408aa list-shared-libraries

# clean up
cd $autoInstallDir
rm calc.jar
rm sns1.jar
autoInstallDelay

echo "After autouninstall ..."
echo "File count: $autoInstallDir"
ls -l $autoInstallDir | wc -l
echo "File count: $statusDir"
ls -l $statusDir | wc -l
echo "autoinstalled area success files"
ls -l $autoInstallDir | grep _installed | wc -l
ls -l $autoInstallDir | grep _uninstalled | wc -l
echo "autoinstalled area failed files"
ls -l $autoInstallDir | grep _notInstalled | wc -l
ls -l $autoInstallDir | grep _notunInstalled | wc -l
echo "recursive listing"
find . -type f -print | sort

echo "After uninstall ..."
$JBI_ANT list-service-engines
$JBI_ANT -Djbi.shared.library.name=b3c97e99-bcb2-4ee0-95e4-60e1cf1408aa list-shared-libraries
