/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)StringHelper.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.util;

import java.util.List;

/**
 * Helper class for string operations
 *
 * @author Sun Microsystems, Inc.
 */
public class StringHelper
{
    /**
     * Trim the whitespaces from all the string elements in the list. If the string
     * is empty then it is removed from the list.
     */
    public static List<String> trim(List<String> list)
    {
        List<String> trimmedList = new java.util.ArrayList<String>(list.size());
        for ( String str : list )
        {
            String trimmedStr = trim(str);
            if ( !"".equals(trimmedStr) )
            {
                trimmedList.add(trim(str));
            }
        }
        return trimmedList;
    }
    
    /**
     * Trim the white spaces from the source string. If the string is a null
     * value, an empty string is returned.
     */
    public static String trim(String srcStr)
    {
        if ( srcStr == null )
        {
            return "";
        }
        
        return srcStr.trim();
    }
    
    /**
     * Convert a String Value to an Object of a Specific Type.
     *
     * @param type - the fully qualified name of the desired type
     * @param arg - String value to be converted
     * @return Object Instance of the type specified, constructed with the String value
     */
    public static Object convertStringToType(String type, String arg)
        throws Exception
    {
        Class objClass = null;
        
        //  -- If it's a primitive type
        if ( type.equals("boolean"))
        {
            objClass = Boolean.class;
        }
        else if (type.equals("byte"))
        {
            objClass = Byte.class;
        }
        else if (type.equals("char"))
        {
            objClass = Character.class;
        }
        else if (type.equals("short"))
        {
            objClass = Short.class;
        }
        else if (type.equals("int"))
        {
            objClass = Integer.class;
        }
        else if (type.equals("long"))
        {
            objClass = Long.class;
        }
        else if (type.equals("float"))
        {
            objClass = Float.class;
        }
        else if (type.equals("double"))
        {
            objClass = Double.class;
        }
        else if (type.equals("void"))
        {
            objClass = Void.class;
        }
        else
        {
            objClass = Class.forName(type);
        }
        
        Class[] argClass = new Class[] {String.class};
        java.lang.reflect.Constructor objConstructor = objClass.getConstructor(argClass);
        String[] argValue = { arg };
        return objConstructor.newInstance(argValue);
    }    
}
