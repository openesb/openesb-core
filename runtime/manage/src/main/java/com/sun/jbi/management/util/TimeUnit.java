/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TimeUnit.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.util;

/**
/**
 * This is a implementation of a tailored subset of java.util.concurrent.TimeUNitk for
 * pre J2SE 5.0 systems. The implemented class/interfaces/methods are compatible at compile time.
 *
 *
 * @author Sun Microsystems, Inc
 */
public class TimeUnit
{
    long        mMilliseconds;
    
    /** Creates a new instance of TimeUnit */
    protected TimeUnit(long milliseconds)
    {
        mMilliseconds = milliseconds;
    }
    
    static public final TimeUnit UNIT_SECONDS = new TimeUnit(1000);
    
    public long getMilliseconds()
    {
        return (mMilliseconds);
    }
}
