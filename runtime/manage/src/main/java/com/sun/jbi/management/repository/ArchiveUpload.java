/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ArchiveUpload.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.repository;

import com.sun.jbi.management.system.ManagementContext;

import com.sun.jbi.StringTranslator;
import com.sun.jbi.management.LocalStringKeys;
import com.sun.jbi.management.util.FileHelper;

import java.io.File;
import java.io.OutputStream;
import java.io.FileOutputStream;
import java.net.URI;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Logger;
import java.rmi.server.UID;

/**
 *  Uploads archive bytes to a temporary store in the ESB repository.
 */
public class ArchiveUpload implements ArchiveUploadMBean
{
    private StringTranslator        mStrings;
    private Logger                  mLog;
    private File                    mUploadDir;
    private HashMap                 mSessions;
    private HashMap<String,File>    mFiles;
    
    public ArchiveUpload(ManagementContext ctx)
    {
        mSessions   = new HashMap();
        mFiles      = new HashMap();
        mLog        = ctx.getLogger();
        mUploadDir  = ctx.getRepository().getTempStore();
        mStrings    = ctx.getEnvironmentContext().
                getStringTranslator("com.sun.jbi.management");
    }
    
    /** Initiates an upload session.  The Object returned from this method
     *  is used as an identifier for uploading bytes with the 
     *  <code>uploadBytes()</code> method.
     *  @return a unique id used for uploading bytes
     */
    public Object initiateUpload(String archiveName)
        throws java.io.IOException
    {
        String              id;
        File                dir;
        File                file;
        FileOutputStream    fos;
        
        id = createUID();
        dir = new File(mUploadDir, id);
        dir.mkdir();
        file = new File(dir, archiveName);
        fos = new FileOutputStream(file);
        
        mSessions.put(id, fos);
        mFiles.put(id, file);
        return id;
    }
    
    /** Uploads a chunk of bytes to an existing temporary archive file.
     *  @param id the upload identifier
     *  @param bytes the content to upload
     */
    public void uploadBytes(Object id, byte[] bytes)
        throws java.io.IOException
    {
        OutputStream out;
        
        out = getUploadStream(id);
        out.write(bytes);
        out.flush();
    }
    
    /** Used to indicate that the upload session with the specified id is
     *  complete.
     */
    public void terminateUpload(Object id, Long timestamp)
        throws java.io.IOException
    {   
        File        file = mFiles.get(id);
        
        getUploadStream(id).close();
        mSessions.remove(id);
        mFiles.remove(id);
        if (file != null && timestamp != null && timestamp != 0)
        {
            file.setLastModified(timestamp);
        }
    }
    
    public void terminateAllUploads()
    {
        Iterator ids;
        
        try
        {
            ids = mSessions.keySet().iterator();
            while (ids.hasNext())
            {
                terminateUpload(ids.next(), null);
            }
        }
        catch (java.io.IOException ioEx)
        {
            mLog.warning(ioEx.getMessage());
        }
    }
    
    /** Returns the location of the uploaded archive.
     *  @param id the upload identifier
     *  @return the location of the archive as a String URL, or null
     *  if the specified archive id does not exist in the repository.
     */
    public String getArchiveURL(Object id)
    {
        String arUrl = null;
        File dir     = new File(mUploadDir, (String) id);
       
        if ( dir.exists() )
        {
            File [] files = dir.listFiles();
            
            if ( files.length > 0 )
            {
                File archive   = files[0];
                URI archiveURI = archive.toURI();
                try
                {
                    arUrl = archiveURI.toURL().toString();
                }
                catch ( java.net.MalformedURLException muex)
                {
                    mLog.warning(muex.getMessage());
                }
            }
        }
        
        return arUrl;
    }
    
    /** Returns the absolute path to the location of the uploaded archive.
     *  @param id the upload identifier
     *  @return the absolute path to the location of the archive as a String , or null
     *  if the specified archive id does not exist in the repository.
     */
    public String getArchiveFilePath(Object id) 
    {
        String archivePath = null;
        File dir     = new File(mUploadDir, (String) id);
       
        if ( dir.exists() )
        {
            File [] files = dir.listFiles();
            
            if ( files.length > 0 )
            {
                File archive   = files[0];
                if ( archive != null ) {
                    archivePath = archive.getAbsolutePath();
                }
            }
        }
        
        return archivePath;        
    }
    
    /** Delete the uploaded archive.
     *
     * @return true if the archive is deleted succcessfully, false otherwise
     */
    public boolean removeArchive(Object id)
    {
        File dir        = new File(mUploadDir, (String) id);
        boolean removed = false;
        
        removed =  FileHelper.cleanDirectory(dir);
        if ( removed )
        {
            dir.delete();
        }
        return removed;
    }    
    
    /** Retrieve the output stream for a session. */
    private OutputStream getUploadStream(Object id)
        throws java.io.IOException
    {
        FileOutputStream fos;
        
        fos = (FileOutputStream)mSessions.get(id);
        if (fos == null)
        {
            throw new java.io.IOException(mStrings.getString(
                    LocalStringKeys.JBI_ADMIN_UPLOAD_ID_NOT_FOUND));
        }
        
        return fos;
    }
    
    /** Create a unique id for the upload session. */
    private String createUID()
    {
        String uid;
        
        uid = new UID().toString();
        uid = uid.replaceAll("-", "");
        uid = uid.replaceAll(":", "");
        
        return uid;
    }
}
