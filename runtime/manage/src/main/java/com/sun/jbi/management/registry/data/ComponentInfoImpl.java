/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ComponentInfoImpl.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.registry.data;

import com.sun.jbi.ComponentState;
import com.sun.jbi.ComponentType;
import com.sun.jbi.ServiceUnitInfo;
import com.sun.jbi.management.ComponentInfo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

/**
 * This interface provides information on components (Binding Components,
 * Service Engines, and Shared Libraries.
 *
 * @author Sun Microsystems, Inc.
 */
public class ComponentInfoImpl
    implements ComponentInfo
{
    private List<String>   mClassPathElements;
    private String         mComponentClassName;
    private ComponentType  mComponentType;
    private ComponentState mComponentState;
    private String         mDescription;
    private String         mInstallRoot;
    private String         mWorkspaceRoot;
    private String         mName;
    private String         mJbiXmlString;
    private List<String>   mSharedLibraryNames;
    private boolean        mIsClassLoaderSelfFirst;
    private boolean        mIsBootstrapClassLoaderSelfFirst;
    private Map<String, String> mProps;  
    private List<ServiceUnitInfo>   mServiceUnitList;
    private String         mBootstrapClassName;
    private List<String>   mBootstrapClassPathElements;
    private long           mTimestamp;
    private int            mUpgradeNumber;

    // Component Configuration
    private Variable[]               mEnvVars;
    private Properties               mConfigs;
    private  Map<String, Properties> mApplicationConfigs;

    /**
     * Determine if the supplied Object is equal to this one.
     * @param object - the Object to be compared with this one.
     * @return true if the supplied Object is equal to this one.
     */
    public boolean equals(Object object)
    {
        return ( object.hashCode() == this.hashCode());
    }

    /**
     * Get the class path elements. For BCs and SEs this is the list of elements
     * that comprise the runtime class path; for Shared Libraries it is the list
     * of elements that comprise the shared class path. Each element in the list
     * is a String representing a single jar file or directory.
     * @return List of jar files and directories that comprise the class path
     * for the component or shared library.
     */ 
    public List getClassPathElements()
    {
        if ( mClassPathElements == null )
        {
            mClassPathElements = new ArrayList();
        }
        return mClassPathElements;
    }
    
    public void setClassPathElements(List<String> elements)
    {
        mClassPathElements = elements;
    }
    
    public void addClassPathElement(String element)
    {
        getClassPathElements().add(element);
    }
    

    /**
     * Get the component class name. For BCs and SEs this is the name of
     * the class that implements the
     * <CODE>javax.jbi.component.Component</CODE>
     * interface; for Shared Libraries it is null.
     * @return the name of the Component class.
     */
    public String getComponentClassName()
    {
        return mComponentClassName;
    }
    
    public void setComponentClassName(String className)
    {
        mComponentClassName = className;
    }

    /**
     * Get the component type.
     * @return the component type: ComponentInfo.BINDING, ComponentInfo.ENGINE,
     * or ComponentInfo.SHARED_LIBRARY.
     */
    public ComponentType getComponentType()
    {
        return mComponentType;
    }
    
    public void setComponentType(ComponentType type)
    {
        mComponentType = type;
    }

    /**
     * Get the description of the component.
     * @return the String describing the component.
     */
    public String getDescription()
    {
        return mDescription;
    }
    
    public void setDescription(String descr)
    {
        mDescription = descr;
    }

    /**
     * Get the directory into which this component is installed.
     * @return the directory into which this component is installed.
     */
    public String getInstallRoot()
    {
        return mInstallRoot;
    }
    
    public void setInstallRoot(String installRoot)
    {
        mInstallRoot = installRoot;
    }
    
    /**
     * Get the workspace for the component
     * @return this components workspace directory.
     */
    public String getWorkspaceRoot()
    {
        return mWorkspaceRoot;
    }
    
    public void setWorkspaceRoot(String workspaceRoot)
    {
        if (workspaceRoot != null )
        {
            mWorkspaceRoot = workspaceRoot.replace('\\', '/');
        }
        else
        	mWorkspaceRoot = workspaceRoot;
    }

    /**
     * Get the name of the component.
     * @return the name of the component.
     */
    public String getName()
    {
        return mName;
    }

    public void setName(String name)
    {
        mName = name;
    }
    
    /**
     * Get the timestamp associated with archive.
     * @return the timestamp of the archive.
     */
    public long getTimestamp()
    {
        return (mTimestamp);
    }
    
    public void setTimestamp(long timestamp)
    {
        mTimestamp = timestamp;
    }
    
    /**
     * Get the grade associated with a component.
     * @return the upgrade number of the component.
     */
    public long getUpgradeNumber()
    {
        return (mUpgradeNumber);
    }
    
    public void setUpgradeNumber(int upgradeNumber)
    {
        mUpgradeNumber = upgradeNumber;
    }
    
    /**
     * Get the list of ServiceUnitInfo objects representing all the SUs that
     * are currently deployed to this component; for Shared Libraries this is
     * null.
     * @return List of ServiceUnitInfo objects.
     */
    public List getServiceUnitList()
    {
        if ( mServiceUnitList == null )
        {
            mServiceUnitList = new ArrayList();
        }
        return mServiceUnitList;
    }
    
    public void setServiceUnitList(List<ServiceUnitInfo> list)
    {
        mServiceUnitList = list;
    }
    
    public void addServiceUnitInfo(ServiceUnitInfo suInfo)
    {
        getServiceUnitList().add(suInfo);
    }

    /**
     * Get the list of Shared Libraries required by this component. For BCs
     * and SEs, this is the list of Shared Library names in the order that they
     * appear in the class loader chain; for Shared Libraries this is null.
     * @return List of Strings containing Shared Library names.
     */
    public List getSharedLibraryNames()
    {
       if ( mSharedLibraryNames == null )
       {
           mSharedLibraryNames = new ArrayList();
       }
       return mSharedLibraryNames;
           
    }

    public void setSharedLibraryNames(List<String> list)
    {
        mSharedLibraryNames = list;
    }
    
    public void addSharedLibrary(String sharedLibraryName)
    {
        getSharedLibraryNames().add(sharedLibraryName);
    }
    
    /**
     * Get the Status of the Component. For Shared Libraries this is always
     * SHUTDOWN.
     * @return should be of the values: LOADED, SHUTDOWN, STOPPED and STARTED
     */
    public ComponentState getStatus()
    {
        return mComponentState;
    }
    
    public void setStatus(ComponentState state)
    {
        mComponentState = state;
    }

    /**
     * Get a hash code for this ComponentInfo.
     * @return the hash code for the object.
     */
    public int hashCode()
    {
        // --- See what all Mark W. uses 
        return 0;
    }
    
    /**
     * Get the installation specific properties for the component.
     *
     * @return a Map of the properties
     */
    public Map getProperties()
    {
        if ( mProps == null )
        {
            mProps = new HashMap();
        }
        return mProps;
    }
    
    public void setProperty(String key, String value)
    {
        getProperties().put(key, value);
    }
    
    
    
    /**
     * @return true/false depending on the value of the bootstrapClassLoaderSelfFirst 
     * attribute. This method returns false for shared libraries.
     */
    public boolean isBootstrapClassLoaderSelfFirst()
    {
        return mIsBootstrapClassLoaderSelfFirst;
    }
    
    public void setBootstrapClassLoaderSelfFirst(boolean flag)
    {
        mIsBootstrapClassLoaderSelfFirst = flag;
    }
    /**
     * @return true/false depending on the value of the {Component}ClassLoaderSelfFirst 
     * attribute.
     */
    public boolean isClassLoaderSelfFirst()
    {
        return mIsClassLoaderSelfFirst;
    }
    
    public void setClassLoaderSelfFirst(boolean flag)
    {
        mIsClassLoaderSelfFirst = flag;
    }

       /**
    * Get the class path elements that this BC or SE needs in its bootstrap
    * runtime environment.
    * @return A list of the elements of the bootstrap class path as strings.
    */
    public java.util.List getBootstrapClassPathElements()
    {
        if ( mBootstrapClassPathElements == null )
        {
            mBootstrapClassPathElements = new ArrayList();
        }
        return mBootstrapClassPathElements;
    }
    
    public void setBootstrapClassPathElements(List elements)
    {
        mBootstrapClassPathElements = elements;
    }
    
   /**
    * Get the class name of the bootstrap implementation for this BC or SE.
    * @return The bootstrap class name.
    */
    public String getBootstrapClassName()
    {
        return mBootstrapClassName;
    }
    
    public void setBootstrapClassName(String name)
    {
        mBootstrapClassName = name;
    }
    
    /**
     * @return the Installation Descriptor [jbi.xml] for the
     * Component / Shared Library.
     */
    public String getInstallationDescriptor()
    {
        return mJbiXmlString;
    }
    
    /**
     * @param jbiXml - the string representation of the installation descriptor
     * for this Component / Shared Library
     */
    public void setInstallationDescriptor(String jbiXml)
    {
        mJbiXmlString = jbiXml;
    }
    
    /**
     * Return an XML string representing this component.
     * @return The XML that represents this component's attributes.
     */
    public String toXmlString()
    {
        return "<?xml>";
    }
    
    /**
     * Get the Environment Variables
     *
     * @return an array of Variables set on the component. If there are zero
     *         environment variables et on the component an empty array is returned.
     */ 
    public Variable[] getVariables()
    {
        if ( mEnvVars == null )
        {
            mEnvVars = new Variable[0];
        }
        return mEnvVars;
    }
    
    /**
     * Get the static component configuration.
     * 
     * @return the components constant configuration.
     */
    public Properties getConfiguration()
    {
        if ( mConfigs == null )
        {
            mConfigs = new Properties();
        }
        return mConfigs;
    }
    
    /**
     * Get a specific application configuration.
     *
     * @return the application configuration whose name = configName. If one does not exist
     *         an empty properties object is returned.
     */
    public Properties getApplicationConfiguration(String name)
    {
        if ( getApplicationConfigurations().containsKey(name))
        {
            return getApplicationConfigurations().get(name);
        }
        else
            return new Properties();
    }
    
    /**
     * Get the names of all application configurations set on the component.
     *
     * @return a list of names of all application configurations. If there are zero
     *         application configurations set on the component an empty array is returned.
     */
    public String[] getApplicationConfigurationNames()
    {
        Set configNames = getApplicationConfigurations().keySet();
        
        String[] result = new String[configNames.size()];
        configNames.toArray(result);
        return result;
    }
    
    /**
     * Set the Environment Variables
     *
     * @param envVars - an array of Variables set on the component.
     */ 
    public void setVariables(Variable[] envVars)
    {
        mEnvVars = envVars;
    }
    
    /**
     * Set the static component configuration.
     * 
     * @param configs the components constant configuration.
     */
    public void setConfiguration(Properties configs)
    {
        mConfigs = configs;
    }
    
    /**
     * Get a specific application configuration.
     *
     * @return the application configuration whose name = configName. If one does not exist
     *         an empty properties object is returned.
     */
    public void setApplicationConfiguration(String name, Properties applicationConfig)
    {
        getApplicationConfigurations().put(name, applicationConfig);
    }
    
    /**
     * @return the application Configuration Map.
     */
    private Map<String, Properties> getApplicationConfigurations()
    {
        if ( mApplicationConfigs == null )
        {
            mApplicationConfigs = new HashMap();
        }
        
        return mApplicationConfigs;
    }
}
