/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)SecurityHndlr.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  SecurityHndlr.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on October 8, 2004, 6:28 PM
 */

package com.sun.jbi.internal.security;

import com.sun.jbi.component.ComponentContext;

/**
 * No-op implementation of SecurityHandler.
 *
 * @author Sun Microsystems, Inc.
 */
public class SecurityHndlr implements com.sun.jbi.binding.security.SecurityHandler
{ 
    /** the ComponentContext. */
    private ComponentContext mCtx;
    
    /**
     * Constructor.
     *
     * @param compCtx is the ComponentContext
     * @throws IllegalStateException if initialization causes the handler 
     * to be in a invalid state.
     */
    public SecurityHndlr (ComponentContext compCtx) throws IllegalStateException
    {
        initialize(compCtx);
    }
    /**
     * This method initializes the SecurityHndlr with the ComponentContext.
     *
     * @param compCtx is the ComponentContext
     * @throws IllegalStateException if initialization causes the handler 
     * to be in a invalid state.
     */
    public void initialize(ComponentContext compCtx)
        throws IllegalStateException
    {
        mCtx = compCtx;
    }
    
    /**
     * Get an Instance of the Deployment Listener.
     *
     * @return an Instance of the Deployment Listener
     */
    public com.sun.jbi.binding.security.DeploymentListener getDeploymentListener()
    {
        return new DeploymentListener();
    }

    /**
     * Get an Instance of the Interceptor.
     *
     * @return an Instance of the Deployment Listener
     */
    public com.sun.jbi.binding.security.Interceptor getInterceptor()
    {
        return new Interceptor();
    }

    /**
     *
     * Get the HttpSecurityHandler.
     *
     * @return a new Instance of the HttpSecurityHandler
     */
    public com.sun.jbi.binding.security.HttpSecurityHandler getHttpSecurityHandler()
    {
        return new HttpSecurityHandler();
    }

    /** 
     * Get the Name of the Component associated with the handler.
     *
     * @return the String Component Name.
     */
    public String getComponentName()
    {
        if ( mCtx != null )
        {
            return mCtx.getComponentName();
        }
        else
        {
            return "";
        }   
    }
}
