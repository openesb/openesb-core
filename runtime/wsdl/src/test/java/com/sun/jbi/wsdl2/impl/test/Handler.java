/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Handler.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.wsdl2.impl.test;

import java.net.URL;
import java.net.URLConnection;

/**
 * This class implements a "test:" protocol URL Stream handler.
 * 
 * @author Sun Microsystems Inc.
 */
public class Handler extends java.net.URLStreamHandler
{
    /**
     * Open a connection for the test: protocol to the given url, which
     * implicitly must use the test: protocol/schema.
     * 
     * @param url the URL of the resource to open a connection to.
     * @return the connection to the test resource described in 
     *         <code>url</code>.
     */
    public URLConnection openConnection(URL url)
    {
        return new TestUrlConnection(url);
    }
}
