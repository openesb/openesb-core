/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestWsdlFactory.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.wsdl2.impl;

/**
 * Unit tests for the WSDL factory.
 * 
 * @author Sun Microsystems Inc.
 */
public class TestWsdlFactory extends junit.framework.TestCase
{
    /**
     * Set up for each test. 
     * 
     * @exception Exception when set up fails for any reason. This tells JUnit 
     *                      that it cannot run the test.
     */
    public void setUp() throws Exception
    {
        super.setUp();
    }

    /**
     * Clean up after each test case.
     * 
     * @exception Exception when clean up fails for any reason.
     */
    public void tearDown() throws Exception
    { 
    }

    /**
     * Test creation of a WsldFactory instance.
     * 
     * @exception Exception when any unexpected error occurs.
     */
    public void testFactoryCreation() throws Exception
    {
        com.sun.jbi.wsdl2.WsdlFactory factory;

        factory = WsdlFactory.newInstance();

        assertTrue( factory != null );
    }

    /**
     * Test creation of Wsdl reader using factory.
     *
     * @exception Exception when any unexpected error occurs.
     */
    public void testReaderCreation() throws Exception
    {
        com.sun.jbi.wsdl2.WsdlFactory factory = WsdlFactory.newInstance();
        com.sun.jbi.wsdl2.WsdlReader  reader  = factory.newWsdlReader();

        assertTrue( reader != null );
    }

    /**
     * Test creation of Wsdl writer using factory.
     *
     * @exception Exception when any unexpected error occurs.
     */
    public void testWriterCreation() throws Exception
    {
        com.sun.jbi.wsdl2.WsdlFactory factory = WsdlFactory.newInstance();
        com.sun.jbi.wsdl2.WsdlWriter  writer  = factory.newWsdlWriter();

        assertTrue( writer != null );
    }
}
