/*
 * XML Type:  DocumentedType
 * Namespace: http://www.w3.org/ns/wsdl
 * Java type: org.w3.ns.wsdl.DocumentedType
 *
 * Automatically generated - do not modify.
 */
package org.w3.ns.wsdl.impl;
/**
 * An XML DocumentedType(@http://www.w3.org/ns/wsdl).
 *
 * This is a complex type.
 */
public class DocumentedTypeImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements org.w3.ns.wsdl.DocumentedType
{
    
    public DocumentedTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName DOCUMENTATION$0 = 
        new javax.xml.namespace.QName("http://www.w3.org/ns/wsdl", "documentation");
    
    
    /**
     * Gets array of all "documentation" elements
     */
    public org.w3.ns.wsdl.DocumentationType[] getDocumentationArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(DOCUMENTATION$0, targetList);
            org.w3.ns.wsdl.DocumentationType[] result = new org.w3.ns.wsdl.DocumentationType[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "documentation" element
     */
    public org.w3.ns.wsdl.DocumentationType getDocumentationArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.DocumentationType target = null;
            target = (org.w3.ns.wsdl.DocumentationType)get_store().find_element_user(DOCUMENTATION$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "documentation" element
     */
    public int sizeOfDocumentationArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(DOCUMENTATION$0);
        }
    }
    
    /**
     * Sets array of all "documentation" element
     */
    public void setDocumentationArray(org.w3.ns.wsdl.DocumentationType[] documentationArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(documentationArray, DOCUMENTATION$0);
        }
    }
    
    /**
     * Sets ith "documentation" element
     */
    public void setDocumentationArray(int i, org.w3.ns.wsdl.DocumentationType documentation)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.DocumentationType target = null;
            target = (org.w3.ns.wsdl.DocumentationType)get_store().find_element_user(DOCUMENTATION$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(documentation);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "documentation" element
     */
    public org.w3.ns.wsdl.DocumentationType insertNewDocumentation(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.DocumentationType target = null;
            target = (org.w3.ns.wsdl.DocumentationType)get_store().insert_element_user(DOCUMENTATION$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "documentation" element
     */
    public org.w3.ns.wsdl.DocumentationType addNewDocumentation()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.DocumentationType target = null;
            target = (org.w3.ns.wsdl.DocumentationType)get_store().add_element_user(DOCUMENTATION$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "documentation" element
     */
    public void removeDocumentation(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(DOCUMENTATION$0, i);
        }
    }
}
