/*
 * XML Type:  InterfaceType
 * Namespace: http://www.w3.org/ns/wsdl
 * Java type: org.w3.ns.wsdl.InterfaceType
 *
 * Automatically generated - do not modify.
 */
package org.w3.ns.wsdl.impl;
/**
 * An XML InterfaceType(@http://www.w3.org/ns/wsdl).
 *
 * This is a complex type.
 */
public class InterfaceTypeImpl extends org.w3.ns.wsdl.impl.ExtensibleDocumentedTypeImpl implements org.w3.ns.wsdl.InterfaceType
{
    
    public InterfaceTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName OPERATION$0 = 
        new javax.xml.namespace.QName("http://www.w3.org/ns/wsdl", "operation");
    private static final javax.xml.namespace.QName FAULT$2 = 
        new javax.xml.namespace.QName("http://www.w3.org/ns/wsdl", "fault");
    private static final javax.xml.namespace.QName NAME$4 = 
        new javax.xml.namespace.QName("", "name");
    private static final javax.xml.namespace.QName EXTENDS$6 = 
        new javax.xml.namespace.QName("", "extends");
    private static final javax.xml.namespace.QName STYLEDEFAULT$8 = 
        new javax.xml.namespace.QName("", "styleDefault");
    
    
    /**
     * Gets array of all "operation" elements
     */
    public org.w3.ns.wsdl.InterfaceOperationType[] getOperationArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(OPERATION$0, targetList);
            org.w3.ns.wsdl.InterfaceOperationType[] result = new org.w3.ns.wsdl.InterfaceOperationType[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "operation" element
     */
    public org.w3.ns.wsdl.InterfaceOperationType getOperationArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.InterfaceOperationType target = null;
            target = (org.w3.ns.wsdl.InterfaceOperationType)get_store().find_element_user(OPERATION$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "operation" element
     */
    public int sizeOfOperationArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(OPERATION$0);
        }
    }
    
    /**
     * Sets array of all "operation" element
     */
    public void setOperationArray(org.w3.ns.wsdl.InterfaceOperationType[] operationArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(operationArray, OPERATION$0);
        }
    }
    
    /**
     * Sets ith "operation" element
     */
    public void setOperationArray(int i, org.w3.ns.wsdl.InterfaceOperationType operation)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.InterfaceOperationType target = null;
            target = (org.w3.ns.wsdl.InterfaceOperationType)get_store().find_element_user(OPERATION$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(operation);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "operation" element
     */
    public org.w3.ns.wsdl.InterfaceOperationType insertNewOperation(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.InterfaceOperationType target = null;
            target = (org.w3.ns.wsdl.InterfaceOperationType)get_store().insert_element_user(OPERATION$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "operation" element
     */
    public org.w3.ns.wsdl.InterfaceOperationType addNewOperation()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.InterfaceOperationType target = null;
            target = (org.w3.ns.wsdl.InterfaceOperationType)get_store().add_element_user(OPERATION$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "operation" element
     */
    public void removeOperation(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(OPERATION$0, i);
        }
    }
    
    /**
     * Gets array of all "fault" elements
     */
    public org.w3.ns.wsdl.InterfaceFaultType[] getFaultArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(FAULT$2, targetList);
            org.w3.ns.wsdl.InterfaceFaultType[] result = new org.w3.ns.wsdl.InterfaceFaultType[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "fault" element
     */
    public org.w3.ns.wsdl.InterfaceFaultType getFaultArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.InterfaceFaultType target = null;
            target = (org.w3.ns.wsdl.InterfaceFaultType)get_store().find_element_user(FAULT$2, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "fault" element
     */
    public int sizeOfFaultArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(FAULT$2);
        }
    }
    
    /**
     * Sets array of all "fault" element
     */
    public void setFaultArray(org.w3.ns.wsdl.InterfaceFaultType[] faultArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(faultArray, FAULT$2);
        }
    }
    
    /**
     * Sets ith "fault" element
     */
    public void setFaultArray(int i, org.w3.ns.wsdl.InterfaceFaultType fault)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.InterfaceFaultType target = null;
            target = (org.w3.ns.wsdl.InterfaceFaultType)get_store().find_element_user(FAULT$2, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(fault);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "fault" element
     */
    public org.w3.ns.wsdl.InterfaceFaultType insertNewFault(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.InterfaceFaultType target = null;
            target = (org.w3.ns.wsdl.InterfaceFaultType)get_store().insert_element_user(FAULT$2, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "fault" element
     */
    public org.w3.ns.wsdl.InterfaceFaultType addNewFault()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.InterfaceFaultType target = null;
            target = (org.w3.ns.wsdl.InterfaceFaultType)get_store().add_element_user(FAULT$2);
            return target;
        }
    }
    
    /**
     * Removes the ith "fault" element
     */
    public void removeFault(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(FAULT$2, i);
        }
    }
    
    /**
     * Gets the "name" attribute
     */
    public java.lang.String getName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(NAME$4);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "name" attribute
     */
    public org.apache.xmlbeans.XmlNCName xgetName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlNCName target = null;
            target = (org.apache.xmlbeans.XmlNCName)get_store().find_attribute_user(NAME$4);
            return target;
        }
    }
    
    /**
     * Sets the "name" attribute
     */
    public void setName(java.lang.String name)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(NAME$4);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_attribute_user(NAME$4);
            }
            target.setStringValue(name);
        }
    }
    
    /**
     * Sets (as xml) the "name" attribute
     */
    public void xsetName(org.apache.xmlbeans.XmlNCName name)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlNCName target = null;
            target = (org.apache.xmlbeans.XmlNCName)get_store().find_attribute_user(NAME$4);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlNCName)get_store().add_attribute_user(NAME$4);
            }
            target.set(name);
        }
    }
    
    /**
     * Gets the "extends" attribute
     */
    public java.util.List getExtends()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(EXTENDS$6);
            if (target == null)
            {
                return null;
            }
            return target.getListValue();
        }
    }
    
    /**
     * Gets (as xml) the "extends" attribute
     */
    public org.w3.ns.wsdl.InterfaceType.Extends xgetExtends()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.InterfaceType.Extends target = null;
            target = (org.w3.ns.wsdl.InterfaceType.Extends)get_store().find_attribute_user(EXTENDS$6);
            return target;
        }
    }
    
    /**
     * True if has "extends" attribute
     */
    public boolean isSetExtends()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().find_attribute_user(EXTENDS$6) != null;
        }
    }
    
    /**
     * Sets the "extends" attribute
     */
    public void setExtends(java.util.List xextends)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(EXTENDS$6);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_attribute_user(EXTENDS$6);
            }
            target.setListValue(xextends);
        }
    }
    
    /**
     * Sets (as xml) the "extends" attribute
     */
    public void xsetExtends(org.w3.ns.wsdl.InterfaceType.Extends xextends)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.InterfaceType.Extends target = null;
            target = (org.w3.ns.wsdl.InterfaceType.Extends)get_store().find_attribute_user(EXTENDS$6);
            if (target == null)
            {
                target = (org.w3.ns.wsdl.InterfaceType.Extends)get_store().add_attribute_user(EXTENDS$6);
            }
            target.set(xextends);
        }
    }
    
    /**
     * Unsets the "extends" attribute
     */
    public void unsetExtends()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_attribute(EXTENDS$6);
        }
    }
    
    /**
     * Gets the "styleDefault" attribute
     */
    public java.util.List getStyleDefault()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(STYLEDEFAULT$8);
            if (target == null)
            {
                return null;
            }
            return target.getListValue();
        }
    }
    
    /**
     * Gets (as xml) the "styleDefault" attribute
     */
    public org.w3.ns.wsdl.InterfaceType.StyleDefault xgetStyleDefault()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.InterfaceType.StyleDefault target = null;
            target = (org.w3.ns.wsdl.InterfaceType.StyleDefault)get_store().find_attribute_user(STYLEDEFAULT$8);
            return target;
        }
    }
    
    /**
     * True if has "styleDefault" attribute
     */
    public boolean isSetStyleDefault()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().find_attribute_user(STYLEDEFAULT$8) != null;
        }
    }
    
    /**
     * Sets the "styleDefault" attribute
     */
    public void setStyleDefault(java.util.List styleDefault)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(STYLEDEFAULT$8);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_attribute_user(STYLEDEFAULT$8);
            }
            target.setListValue(styleDefault);
        }
    }
    
    /**
     * Sets (as xml) the "styleDefault" attribute
     */
    public void xsetStyleDefault(org.w3.ns.wsdl.InterfaceType.StyleDefault styleDefault)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.InterfaceType.StyleDefault target = null;
            target = (org.w3.ns.wsdl.InterfaceType.StyleDefault)get_store().find_attribute_user(STYLEDEFAULT$8);
            if (target == null)
            {
                target = (org.w3.ns.wsdl.InterfaceType.StyleDefault)get_store().add_attribute_user(STYLEDEFAULT$8);
            }
            target.set(styleDefault);
        }
    }
    
    /**
     * Unsets the "styleDefault" attribute
     */
    public void unsetStyleDefault()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_attribute(STYLEDEFAULT$8);
        }
    }
    /**
     * An XML extends(@).
     *
     * This is a list type whose items are org.apache.xmlbeans.XmlQName.
     */
    public static class ExtendsImpl extends org.apache.xmlbeans.impl.values.XmlListImpl implements org.w3.ns.wsdl.InterfaceType.Extends
    {
        
        public ExtendsImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected ExtendsImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
    /**
     * An XML styleDefault(@).
     *
     * This is a list type whose items are org.apache.xmlbeans.XmlAnyURI.
     */
    public static class StyleDefaultImpl extends org.apache.xmlbeans.impl.values.XmlListImpl implements org.w3.ns.wsdl.InterfaceType.StyleDefault
    {
        
        public StyleDefaultImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, false);
        }
        
        protected StyleDefaultImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
    }
}
