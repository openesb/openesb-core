/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ImportImpl.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.wsdl2.impl;

import com.sun.jbi.wsdl2.WsdlException;

import java.io.IOException;

import java.util.Map;

import org.w3.ns.wsdl.ImportType;

/**
 * Implementation of WSDL 2.0 Import component.
 * 
 * @author Sun Microsystems, Inc.
 */
final class ImportImpl extends Import
{
    /** The container for this import component */
    private DescriptionImpl   mContainer;

    /** The result of importing this component */
    private DescriptionImpl   mImportResult = null;

    /**
     * Get the container for this component.
     * 
     * @return The component for this component
     */
    protected DescriptionImpl getContainer()
    {
        return this.mContainer;
    }
  
    /**
     * Construct this Import component implementation from the given XML bean.
     * @param bean The import XML bean to use to construct this component.
     * @param defs The container for this component.
     */
    private ImportImpl(ImportType bean, DescriptionImpl defs)
    {
        super(bean);
        this.mContainer = defs;
    }
  
    /** Map of WSDL-defined attribute QNames. Keyed by QName.toString value */
    private static java.util.Map sWsdlAttributeQNames = null;

    /** 
     * Worker class method for {@link #getWsdlAttributeNameMap()}.
     * 
     * @return Map of WSDL-defined attribute QNames for this component, 
     *         indexed by QName.toString()
     */
    static synchronized java.util.Map getAttributeNameMap()
    {
        if (sWsdlAttributeQNames == null)
        {
            sWsdlAttributeQNames = XmlBeansUtil.getAttributesMap(
                ImportType.type);
        }

        return sWsdlAttributeQNames;
    }
  
    /**
     * Get map of WSDL-defined attribute QNames for this component, indexed by 
     * canonical QName string (see {@link javax.xml.namespace.QName#toString()}.
     *
     * @return Map of WSDL-defined attribute QNames for this component, 
     *         indexed by QName.toString()
     */
    public java.util.Map getWsdlAttributeNameMap()
    {
        return getAttributeNameMap();
    }
  
    /**
     * Get target namespace of import (must be different that this Description TNS).
     *
     * @return Target namespace of import (must be different that this Description TNS)
     */
    public String getNamespace()
    {
        return getBean().getNamespace();
    }

    /**
     * Set target namespace of import (must be different that this Description TNS).
     *
     * @param theNamespace Target namespace of import (must be different that this 
     *                     Description TNS)
     */
    public void setNamespace(String theNamespace)
    {
        getBean().setNamespace(theNamespace);
    }

    /**
     * Get location hint, if any.
     *
     * @return Location hint, if any
     */
    public String getLocation()
    {
        return getBean().getLocation();
    }

    /**
     * Set location hint, if any.
     *
     * @param theLocation Location hint, if any
     */
    public void setLocation(String theLocation)
    {
        getBean().setLocation(theLocation);
    }

    /**
     * Get the description from this import component
     *
     * @return Description from this import component, if any
     */
    public synchronized com.sun.jbi.wsdl2.Description getDescription()
    {
        if (this.mImportResult == null)
        {
            mImportResult = importTarget();
        }
    
        return this.mImportResult;
    }

    /**
     * Get the definitions from this import component.
     *
     * @deprecated - replaced by getDescription
     * @return Definitions n from this import component, if any
     */
     public com.sun.jbi.wsdl2.Definitions getDefinitions()
     {
	 return (com.sun.jbi.wsdl2.Definitions) getDescription();
     }

    /**
     * Import the target of this import component, if possible.
     * 
     * @return The definitions component imported by this component, if any.
     */
    private DescriptionImpl importTarget()
    {
        WsdlReader                    reader = new WsdlReader();
        com.sun.jbi.wsdl2.Description defs   = null;

        try
        {
            defs = reader.readDescription(mContainer.getDocumentBaseUri(), getLocation());
        }
        catch (IOException ex)
        {
            System.err.println( "WSDL import IO error reading " + getLocation() + 
                " (relative to) " + mContainer.getDocumentBaseUri() + ":" );
                
            if (ex.getMessage() != null)
            {
                System.err.println( ex.getMessage() );
            }
            
            ex.printStackTrace(System.err);
        }
        catch (WsdlException ex)
        {
            System.err.println( "WSDL import error reading " + getLocation() + 
                " (relative to) " + mContainer.getDocumentBaseUri() + ":" );

            if (ex.getMessage() != null)
            {
                System.err.println( ex.getMessage() );
            }

            ex.printStackTrace(System.err);
        }

        return defs != null ? (DescriptionImpl) defs : null;
    }

    /**
     * A factory class for creating / finding components for given XML beans.
     * <p>
     * This factory guarantees that there will only be one component for each
     * XML bean instance.
     */
    static class Factory
    {
        /**
         * Find the WSDL import component associated with the given XML
         * bean, creating a new component if necessary.
         * <p>
         * This is thread-safe.<p>
         * 
         * @param bean The XML bean to find the component for.
         * @param defs The container for the component.
         * @return The WSDL import component for the given <code>bean</code>
         *         (null if the <code>bean</code> is null).
         */
        static ImportImpl getInstance(ImportType bean, DescriptionImpl defs)
        {
            ImportImpl    result;

            if (bean != null)
            {
                Map        map = defs.getImportMap();
                
                synchronized (map)
                {
                    result = (ImportImpl) map.get(bean);

                    if (result == null)
                    {
                        result = new ImportImpl(bean, defs);
                        map.put(bean, result);
                    }
                }
            }
            else
            {
                result = null;
            }

            return result;
        }
    }
}

// End-of-file: ImportImpl.java
