/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)EndpointImpl.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.wsdl2.impl;

import java.io.StringWriter;

import java.util.Map;

import javax.xml.namespace.QName;

import org.apache.xmlbeans.XmlOptions;

import org.w3.ns.wsdl.EndpointType;

import org.w3c.dom.DocumentFragment;

/**
 * Implementation of WSDL 2.0 Endpoint component.
 * 
 * @author Sun Microsystems, Inc.
 */
final class EndpointImpl extends Endpoint
{
    /** Container for this component. */
    private DescriptionImpl mContainer;
  
    /**
     * Get the container for this component.
     * 
     * @return The component for this component
     */
    protected DescriptionImpl getContainer()
    {
        return this.mContainer;
    }
  
    /**
     * Construct an Endpoint component implementation, based on the given Xml Bean.
     * 
     * @param bean The Xml bean to construct this endpoint from.
     * @param defs The container for this endpoint component.
     */
    private EndpointImpl(EndpointType bean, DescriptionImpl defs)
    {
        super(bean);

        this.mContainer = defs;
    }
  
    /** Map of WSDL-defined attribute QNames. Keyed by QName.toString value */
    private static java.util.Map sWsdlAttributeQNames = null;

    /** 
     * Worker class method for {@link #getWsdlAttributeNameMap()}.
     * 
     * @return Map of WSDL-defined attribute QNames for this component, 
     *         indexed by QName.toString()
     */
    static synchronized java.util.Map getAttributeNameMap()
    {
        if (sWsdlAttributeQNames == null)
        {
            sWsdlAttributeQNames = XmlBeansUtil.getAttributesMap(
                EndpointType.type);
        }

        return sWsdlAttributeQNames;
    }
  
    /**
     * Get map of WSDL-defined attribute QNames for this component, indexed by 
     * canonical QName string (see {@link javax.xml.namespace.QName#toString()}.
     * <p>
     * Implementation note: since this method is declared in the public API
     * <code>interface</code>, it has to be a member, not static. We delegate
     * to a class method to do the actual work.
     *
     * @return Map of WSDL-defined attribute QNames for this component, 
     *         indexed by QName.toString()
     */
    public java.util.Map getWsdlAttributeNameMap()
    {
        return getAttributeNameMap();
    }
  
    /**
     * Get name of this endpoint component.
     *
     * @return Name of this endpoint component
     */
    public String getName()
    {
        return getBean().getName();
    }

    /**
     * Set name of this endpoint component.
     *
     * @param theName Name of this endpoint component
     */
    public void setName(String theName)
    {
        getBean().setName(theName);
    }

    /**
     * Get binding for this endpoint.
     *
     * @return Binding for this endpoint
     */
    public com.sun.jbi.wsdl2.Binding getBinding()
    {
        return this.mContainer.findBinding(getBean().getBinding());
    }

    /**
     * Set binding for this endpoint.
     *
     * @param theBinding Binding for this endpoint
     */
    public void setBinding(com.sun.jbi.wsdl2.Binding theBinding)
    {
        QName     name;
    
        if (theBinding != null)
        {
            name = new QName(
                theBinding.getTargetNamespace(), 
                theBinding.getName());
        }
        else
        {
            name = null;
        }
      
        getBean().setBinding(name);
    }
    
    /**
    * Return this WSDL endpoint as an XML string.
    *
    * @return This endpoint, serialized as an XML string.
    */
    public String toXmlString()
    {
        String          result; 
        StringWriter    sw      = new StringWriter();
        XmlOptions      options = new XmlOptions();

        options.setSavePrettyPrint();
        options.setSavePrettyPrintIndent(Constants.XML_PRETTY_PRINT_INDENT);
        options.setSaveOuter();

        try
        {
            getBean().save(sw, options);
            sw.close();
        }
        catch (java.io.IOException ex)
        {
            sw.write("\n<!-- IO error: ");
            sw.write(ex.getMessage());
            sw.write("\n     Document fragment truncated. -->\n");
            // $TODO: log error
        }
        
        return sw.getBuffer().toString();
    }

    /**
    * Return this endpoint as a DOM document fragment.
    *
    * @return This endpoint, as a DOM document fragment.
    */
    public DocumentFragment toXmlDocumentFragment()
    {
        XmlOptions  options = new XmlOptions();

        options.setSaveOuter();
        return (DocumentFragment) getBean().newDomNode(options);
    }

    /**
     * A factory class for creating / finding components for given XML beans.
     * <p>
     * This factory guarantees that there will only be one component for each
     * XML bean instance.
     */
    static class Factory
    {
        /**
         * Find the WSDL endpoint component associated with the given XML
         * bean, creating a new component if necessary.
         * <p>
         * This is thread-safe.<p>
         * 
         * @param bean The XML bean to find the component for.
         * @param defs The container for the component.
         * @return The WSDL endpoint component for the given <code>bean</code>
         *         (null if the <code>bean</code> is null).
         */
        static EndpointImpl getInstance(EndpointType bean, DescriptionImpl defs)
        {
            EndpointImpl  result;

            if (bean != null)
            {
                Map         map = defs.getEndpointMap();
                
                synchronized (map)
                {
                    result = (EndpointImpl) map.get(bean);

                    if (result == null)
                    {
                        result = new EndpointImpl(bean, defs);
                        map.put(bean, result);
                    }
                }
            }
            else
            {
                result = null;
            }

            return result;
        }
    }
}

// End-of-file: EndpointImpl.java
