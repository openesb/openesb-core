#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)framework00001.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#
echo framework00001.ksh

#use global regress setup:
my_test_domain=domain1
. ./regress_defs.ksh
PS="$JBI_PS"

rm -f $JBI_DOMAIN_ROOT/logs/server.log
sed -e "s#JBI_DOMAIN_ROOT#$JV_JBI_DOMAIN_ROOT#g" ComponentList.dat  > $JBI_DOMAIN_ROOT/config/ComponentList.dat
sed -e "s#FRAMEWORK_BLD_DIR#$JV_FRAMEWORK_BLD_DIR#g" engine1/engine1.xml > $JBI_DOMAIN_ROOT/config/engine1.xml
sed -e "s#FRAMEWORK_BLD_DIR#$JV_FRAMEWORK_BLD_DIR#g" binding1/binding1.xml > $JBI_DOMAIN_ROOT/config/binding1.xml

#
# This step creates a persisted component registry containing the test binding
# and engine definitions.
#
testcp="$JV_FRAMEWORK_BLD_DIR\
${PS}$JV_JBI_DOMAIN_ROOT/lib/jbi.jar\
${PS}$JV_JBI_DOMAIN_ROOT/lib/jbi-ext.jar\
${PS}$JV_JBI_HOME/lib/jbi_tests.jar\
${PS}$JV_JBI_HOME/lib/jbi_rt.jar\
${PS}$AS_INSTALL/lib/appserv-rt.jar\
${PS}$AS_INSTALL/lib/j2ee.jar\
${PS}$JV_SRCROOT/vendor-libs/jars/xalan.jar\
"

1>&2 echo testcp is $testcp
1>&2 echo AS_INSTALL is $AS_INSTALL

java -cp "$testcp" -Djunit.srcroot=$JV_SRCROOT -Djunit.as8base=$AS_INSTALL com.sun.jbi.framework.ComponentRegistryBuilder $JV_JBI_DOMAIN_ROOT/config

cmd="asadmin start-domain -t -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --domaindir '$JV_JBI_DOMAIN_DIR' $JBI_DOMAIN_NAME"
1>&2 eval $cmd
status=$?
if [ $status -ne 0 ]; then
    bldmsg -status $status -p $0 -error "COMMAND '$cmd' FAILED"
    exit 1
fi

#wait for the admin started file to appear:
bldwait -nomsg -max 600 $JBI_DOMAIN_STARTED
if [ $? -ne 0 ]; then
    bldmsg -error -p $0 error starting JBI - admin service did not start
    exit 1
else
    echo Domain $JBI_DOMAIN_NAME started.
fi

#wait a bit for the components to be processed:
sleep 30

1>&2 asadmin stop-domain --domaindir "$JV_JBI_DOMAIN_DIR" "$JBI_DOMAIN_NAME"
status=$?
if [ $status -ne 0 ]; then
    bldmsg -status $status -p $0 -error "COMMAND 'asadmin stop-domain --domaindir $JV_JBI_DOMAIN_DIR $JBI_DOMAIN_NAME' FAILED"
    exit 1
fi

#wait for the admin stopped file to appear:
bldwait -nomsg -max 600 "$JBI_DOMAIN_STOPPED"
if [ $? -ne 0 ]; then
    bldmsg -error -p $0 error stopping JBI - admin service did not stop
    exit 1
else
    echo Domain $JBI_DOMAIN_NAME stopped.
fi

echo initialization complete
grep JBIFW0002 $JBI_DOMAIN_ROOT/logs/server.log | wc -l
echo startup complete
grep JBIFW0012 $JBI_DOMAIN_ROOT/logs/server.log | wc -l
echo Engine Pings
grep "Engine ping exchange successful" $JBI_DOMAIN_ROOT/logs/server.log | wc -l
echo Binding Pings
grep "Binding ping exchange successful" $JBI_DOMAIN_ROOT/logs/server.log | wc -l
echo Bulk Engine Test
grep "successfully initiated 100 InOnly exchanges to engine" $JBI_DOMAIN_ROOT/logs/server.log | wc -l
echo Bulk Binding Test
grep "successfully initiated 100 InOnly exchanges to binding" $JBI_DOMAIN_ROOT/logs/server.log | wc -l
