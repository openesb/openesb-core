/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)EngineBootstrap.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.framework;

import java.io.File;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.jbi.component.InstallationContext;

import javax.management.ObjectName;

/**
 * This is an implementation of the bootstrap class for a Service Engine
 * that is purely for unit testing. It does nothing but log messages when
 * its methods are called.
 *
 * @author Sun Microsystems, Inc.
 */
public class EngineBootstrap implements javax.jbi.component.Bootstrap
{
    /**
     * InstallationContext instance
     */
    private InstallationContext mCtx;

    /**
     * Logger instance
     */
    private Logger mLog = Logger.getLogger("com.sun.jbi.framework.EngineBootstrap");


    /**
     * Cleans up any resources allocated by the bootstrap implementation,
     * including deregistration of the extension MBean, if applicable.
     * This method will be called after the onInstall() or onUninstall() method
     * is called, whether it succeeds or fails.
     * @throws javax.jbi.JBIException when cleanup processing fails to complete
     * successfully.
     */
    public void cleanUp()
        throws javax.jbi.JBIException
    {
        String name = mCtx.getComponentName();
        mLog.log(Level.INFO, "{0}: cleanUp() called", name);
        if ( mCtx.isInstall() )
        {
            if ( name.equals(Constants.SE_NAME_BAD_BOOTSTRAP_INSTALL_CLEANUP)
                || name.equals(Constants.SE_NAME_BAD_BOOTSTRAP_ONINSTALL_CLEANUP) 
                || name.equals(Constants.SE_NAME_BAD_BOOTSTRAP_INIT_CLEANUP) )
            {
                throw new javax.jbi.JBIException("bootstrap cleanup failed");
            }
        }
        else
        {
            if ( name.equals(Constants.SE_NAME_BAD_BOOTSTRAP_UNINSTALL_CLEANUP)
                || name.equals(Constants.SE_NAME_BAD_BOOTSTRAP_ONUNINSTALL_CLEANUP)
                || name.equals(Constants.SE_NAME_BAD_BOOTSTRAP_INIT_CLEANUP) )
            {
                throw new javax.jbi.JBIException("bootstrap cleanup failed");
            }
        }
    }

    /**
     * Initialize for installation.
     * @param installContext is the InstallationContext
     * @throws javax.jbi.JBIException if any error occurs.
     */
    public void init(InstallationContext installContext)
        throws javax.jbi.JBIException
    {
        mCtx = installContext;
        mLog.log(Level.INFO, "{0}: init() called", mCtx.getComponentName());
        String name = mCtx.getComponentName();
        if ( name.equals(Constants.SE_NAME_BAD_BOOTSTRAP_INIT)
            || name.equals(Constants.SE_NAME_BAD_BOOTSTRAP_INIT_CLEANUP) )
        {
            throw new javax.jbi.JBIException("bootstrap init failed");
        }
        return;
    }

    /**
     * Return an optional installation configuration MBean ObjectName.
     * @return The MBean ObjectName for the installation configuration MBean.
     */
    public ObjectName getExtensionMBeanName()
    {
        mLog.log(Level.INFO, "{0}: getExtensionMBeanName() called", mCtx.getComponentName());
        javax.jbi.component.ComponentContext env = mCtx.getContext();
        javax.jbi.management.MBeanNames mbn = env.getMBeanNames();
        ObjectName on = mbn.createCustomComponentMBeanName(
            "InstallerConfigurationMBean");
        return on;
    }

    /**
     * Install the Service Engine into the JBI framework.
     * @throws javax.jbi.JBIException if any error occurs.
     */
    public void onInstall()
        throws javax.jbi.JBIException
    {
        mLog.log(Level.INFO, "{0}: onInstall() called", mCtx.getComponentName());
        String compId = mCtx.getComponentName();
        if ( compId.equals(Constants.SE_NAME_BAD_BOOTSTRAP_ONINSTALL)
            || compId.equals(Constants.SE_NAME_BAD_BOOTSTRAP_ONINSTALL_CLEANUP) )
        {
            throw new javax.jbi.JBIException("unable to install engine");
        }
        else if ( compId.equals(Constants.SE_NAME_BOOTSTRAP_MODIFY_CLASS_PATH) )
        {
            mLog.log(Level.INFO, "Life cycle class path was {0}", mCtx.getClassPathElements());
            List cp = new ArrayList();
            cp.add("framework" + File.separator + "target" + File.separator +
                   "regress" + File.separator + "Engine");
            mCtx.setClassPathElements(cp);
            mLog.log(Level.INFO, "Life cycle class path now {0}", mCtx.getClassPathElements());
            mLog.log(Level.INFO, "Service Engine {0} now installed", compId);
        }
        else
        {
            mLog.log(Level.INFO, "Service Engine {0} now installed", compId);
        }
        return;
    }

   /**
    * Uninstall a Service Engine from the JBI framework.
    * @throws javax.jbi.JBIException if any error occurs.
    */
    public void onUninstall()
        throws javax.jbi.JBIException
    {
        mLog.log(Level.INFO, "{0}: onUninstall() called", mCtx.getComponentName());
        String compId = mCtx.getComponentName();
        if ( compId.equals(Constants.SE_NAME_BAD_BOOTSTRAP_ONUNINSTALL)
            || compId.equals(Constants.SE_NAME_BAD_BOOTSTRAP_ONUNINSTALL_CLEANUP)
            || compId.equals(Constants.SE_NAME_BAD_SHUTDOWN_BOOTSTRAP_ONUNINSTALL) )
        {
            throw new javax.jbi.JBIException("unable to uninstall engine");
        }
        else
        {
            mLog.log(Level.INFO, "Service Engine {0} now uninstalled", compId);
        }
        return;
    }
 
}
