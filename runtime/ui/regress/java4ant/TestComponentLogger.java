/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestComponentLogger.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package java4ant;

import java.util.Iterator;
import java.util.Set;

import javax.management.MBeanServerConnection;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;
import javax.management.ObjectName;



/**
 * This class is used to test Application Verifier.
 */
public class TestComponentLogger
{
    /**  MBean Server Connection */
    private MBeanServerConnection mbns;
    
    /** Result Prefix */
    private static final String RESULT_PREFIX = "##### Result of ";
    
    /** schemaorg_apache_xmlbeans.system properties used by this class */
    private static final String USER     = "jmx.user";
    private static final String PASSWORD = "jmx.password";
    private static final String PROVIDER = "jmx.provider";
    
    private static final String APPLICATION_URL = "application_url";
    private static final String TARGET   = "target";
  

    /**
     * Initialize connection to MBean Server
     */
    public void initMBeanServerConnection()
        throws Exception
    {
        java.util.Map<String, String[]> env = new java.util.HashMap();
        String user = System.getProperty(USER);
        String pass = System.getProperty(PASSWORD);
        String[] credentials = new String[] { user, pass};
        env.put("jmx.remote.credentials", credentials);   
        
        String jmxProvider = System.getProperty(PROVIDER);
        
        JMXConnector connector = JMXConnectorFactory.connect(new JMXServiceURL(jmxProvider), env);

        mbns = connector.getMBeanServerConnection();
    }
    
    /**
     * Verify Application
     */
            
    public void setComponentLoggerLevel()
        throws Exception
    {
        String info = "com.sun.jbi:ServiceName=JbiReferenceAdminUiService,ComponentType=System";
        mbns.invoke( 
            new ObjectName(info),
            "setComponentLoggerLevel",
            new Object[] {
                null, 
                "sun-http-binding", 
                java.util.logging.Level.FINE, 
                "server", 
                "null"},
            new String[] {
                 "java.lang.String",
                 "java.lang.String",
                 "java.util.logging.Level",
                 "java.lang.String",
                 "java.lang.String"
            }
       );

    }
    
 
    
    /**
     * The main method
     */
    public static void main (String[] params)
        throws Exception 
    {
       
        TestComponentLogger compLogger = new TestComponentLogger();
        compLogger.initMBeanServerConnection();
        try
        {
            compLogger.setComponentLoggerLevel();
        }
        catch ( Throwable ex)
        {
            ex = ex.getCause();
            
            if ( ex != null )
            {
                String exMsg = ex.getMessage().replaceAll("en_US","en");
                System.out.println("setComponentLoggerLevel failed with exception message : "
                        + exMsg);
            }
        }
    
    }
    
}
