/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestVerifier.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package java4ant;

import java.util.Iterator;
import java.util.Set;

import javax.management.MBeanServerConnection;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;
import javax.management.ObjectName;

import javax.management.openmbean.ArrayType;
import javax.management.openmbean.CompositeData;
import javax.management.openmbean.CompositeDataSupport;
import javax.management.openmbean.TabularData;
import javax.management.openmbean.CompositeType;
import javax.management.openmbean.OpenType;
import javax.management.openmbean.OpenDataException;
import javax.management.openmbean.SimpleType;

/**
 * This class is used to test Application Verifier.
 */
public class TestStats
{
    /**  MBean Server Connection */
    private MBeanServerConnection mbns;
    
    /** Result Prefix */
    private static final String RESULT_PREFIX = "##### Result of ";
    
    /** schemaorg_apache_xmlbeans.system properties used by this class */
    private static final String USER     = "jmx.user";
    private static final String PASSWORD = "jmx.password";
    private static final String PROVIDER = "jmx.provider";
    
    private static final String TARGET   = "target";
 
        

    /**
     * Initialize connection to MBean Server
     */
    public void initMBeanServerConnection()
        throws Exception
    {
        java.util.Map<String, String[]> env = new java.util.HashMap();
        String user = System.getProperty(USER);
        String pass = System.getProperty(PASSWORD);
        String[] credentials = new String[] { user, pass};
        env.put("jmx.remote.credentials", credentials);   
        
        String jmxProvider = System.getProperty(PROVIDER);
        
        JMXConnector connector = JMXConnectorFactory.connect(new JMXServiceURL(jmxProvider), env);

        mbns = connector.getMBeanServerConnection();
    }
    
    /**
     * get stats
     */
    public TabularData getFrameworkStats(String target)
        throws Exception
    {
        String info = "com.sun.jbi:ServiceName=JbiReferenceAdminUiService,ComponentType=System";
        return (TabularData)
                mbns.invoke( 
                    new ObjectName(info),
                    "getFrameworkStats",
                    new Object[] { target },
                    new String[] {"java.lang.String"});
    }
    /**
     * get stats
     */
    public TabularData getNMRStats(String target)
        throws Exception
    {
        String info = "com.sun.jbi:ServiceName=JbiReferenceAdminUiService,ComponentType=System";
        return (TabularData)
                mbns.invoke( 
                    new ObjectName(info),
                    "getNMRStats",
                    new Object[] { target },
                    new String[] {"java.lang.String"});
    }    
    
    /**
     * get stats
     */
    public TabularData getComponentStats(String target, String componentName)
        throws Exception
    {
        String info = "com.sun.jbi:ServiceName=JbiReferenceAdminUiService,ComponentType=System";
        return (TabularData)
                mbns.invoke( 
                    new ObjectName(info),
                    "getComponentStats",
                    new Object[] { componentName, target },
                    new String[] {"java.lang.String", "java.lang.String"});
    }      
    
    /**
     * get stats
     */
    public TabularData getEndpointStats(String target, String endpointName)
        throws Exception
    {
        String info = "com.sun.jbi:ServiceName=JbiReferenceAdminUiService,ComponentType=System";
        return (TabularData)
                mbns.invoke( 
                    new ObjectName(info),
                    "getEndpointStats",
                    new Object[] { endpointName, target },
                    new String[] {"java.lang.String", "java.lang.String"});
    }       

    /**
     * get stats
     */
    public TabularData getSAStats(String target, String saName)
        throws Exception
    {
        String info = "com.sun.jbi:ServiceName=JbiReferenceAdminUiService,ComponentType=System";
        return (TabularData)
                mbns.invoke( 
                    new ObjectName(info),
                    "getServiceAssemblyStats",
                    new Object[] { saName, target },
                    new String[] {"java.lang.String", "java.lang.String"});
    }     
    
    /**
     * parse and print the report
     */
    public void printCompositeData(TabularData statsReport)
    {
        Set names = statsReport.keySet();
        for ( Object name : names )
        {
            Object[] key = ( (java.util.List) name).toArray();
            CompositeData compData = statsReport.get(key);
            System.out.println();
            printCompositeData(compData,1);
        }
    }    
    
 
  protected void printCompositeData(CompositeData compData, int recursiveLevel)
    {

	String tabT = "";
	for(int i=0; i<recursiveLevel; ++i)
	{
	    tabT = tabT + "\t";
	}
	recursiveLevel++;

	CompositeType compType = compData.getCompositeType();
        Iterator itr = compType.keySet().iterator();
        while (itr.hasNext())
        {
	    String keyName = (String) itr.next();
            OpenType openType = compType.getType(keyName);
            Object itemValue = compData.get(keyName);
            String className = null;
            if (itemValue != null)
            {
                className = itemValue.getClass().getName();
            }
            
            if (openType.isArray())
            {
		System.out.println(tabT + keyName + ":");
                if ((openType.getTypeName() != null) &&
			(openType.getTypeName().compareTo("[Ljava.lang.String;") == 0))
		{
		    String [] strArray = (String []) compData.get(keyName);
		    for (int i=0; i < strArray.length; ++i)
		    {
			System.out.println(tabT+"\t"+strArray[i]);
		    }
		}
		else if ((openType.getTypeName() != null) &&       
                        (openType.getTypeName().compareTo("[Ljavax.management.openmbean.CompositeData;") == 0))
		{
		    CompositeData [] compDataArray = (CompositeData []) compData.get(keyName);
		    for (int i=0; i < compDataArray.length; ++i)
                    {
                        printCompositeData(compDataArray[i], recursiveLevel);
                    }
                }
		    
            }
            else
            {

		if (className != null &&
			(className.equals("javax.management.openmbean.CompositeDataSupport") || 
                        className.equals("javax.management.openmbean.CompositeData")))
		{
		    printCompositeData((CompositeData) compData.get(keyName), recursiveLevel);
		}
                else if (className != null &&
			(className.equals("javax.management.openmbean.TabularDataSupport") || 
                        className.equals("javax.management.openmbean.TabularData")))
                {
                    printCompositeData((TabularData)compData.get(keyName));
                }
                else
                {
                    System.out.println(tabT + keyName + "=" + compData.get(keyName));
                }
            }
        }


    }    
  

    /**
     * get stats
     */
    public TabularData getConsumingEndpointsForComponent(String target, String componentName)
        throws Exception
    {
        String info = "com.sun.jbi:ServiceName=JbiReferenceAdminUiService,ComponentType=System";
        return (TabularData)
                mbns.invoke( 
                    new ObjectName(info),
                    "getConsumingEndpointsForComponent",
                    new Object[] { componentName, target },
                    new String[] {"java.lang.String", "java.lang.String"});
    }     
      
    /**
     * get stats
     */
    public TabularData getProvidingEndpointsForComponent(String target, String componentName)
        throws Exception
    {
        String info = "com.sun.jbi:ServiceName=JbiReferenceAdminUiService,ComponentType=System";
        return (TabularData)
                mbns.invoke( 
                    new ObjectName(info),
                    "getProvidingEndpointsForComponent",
                    new Object[] { componentName, target },
                    new String[] {"java.lang.String", "java.lang.String"});
    }         
   
    /**
     * The main method
     */
    public static void main (String[] params)
        throws Exception 
    {
        if (params.length == 0)
        {
            throw new Exception("Usage: TestVerifier Framework|NMR|SA|Component|Endpoint");
        }
        try
        {
        
            String target = System.getProperty(TARGET);

            TestStats testStats = new TestStats();
            testStats.initMBeanServerConnection();

            if (params[0].equals("Framework"))
            {
                TabularData frameworkStats = testStats.getFrameworkStats(target);
                testStats.printCompositeData(frameworkStats);
            }
            else if (params[0].equals("NMR"))
            {
                TabularData nmrStats = testStats.getNMRStats(target);
                testStats.printCompositeData(nmrStats);
            }
            else if (params[0].equals("Component"))
            {
                TabularData componentStats = testStats.getComponentStats(target, params[1]);
                testStats.printCompositeData(componentStats);
            }

            else if (params[0].equals("Endpoint"))
            {
                TabularData epStats = testStats.getEndpointStats(target, params[1]);
                testStats.printCompositeData(epStats);
            }          
            else if (params[0].equals("SA"))
            {
                TabularData saStats = testStats.getSAStats(target, params[1]);
                testStats.printCompositeData(saStats);
            } 
            else if (params[0].equals("listconsumingendpoints"))
            {
                TabularData endpointsList = testStats.getConsumingEndpointsForComponent(target, params[1]);
                testStats.printCompositeData(endpointsList);
            }          
            else if (params[0].equals("listprovidingendpoints"))
            {
                TabularData endpointsList = testStats.getProvidingEndpointsForComponent(target, params[1]);
                testStats.printCompositeData(endpointsList);
            }                      
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    
    }
    
}
