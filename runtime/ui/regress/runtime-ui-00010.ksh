#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)runtime-ui-00010.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

####
# This test is used to add a simple lifecycle module which would register a
# dummy JavaEEVerifier MBean. This tests the contract between JavaEEVerifier 
# and JBIVerifier. 
####

echo "runtime-ui-00010 : Verifier integration testing."
export my_test_domain
my_test_domain=domain1
. ./regress_defs.ksh

ant -q  -emacs -lib "$REGRESS_CLASSPATH" -f runtime-ui-00010.xml package

start_domain

echo deleting lifecycle module
cmd="asadmin delete-lifecycle-module -t --port $ASADMIN_PORT $ASADMIN_PW_OPTS JavaEEVerifier"
1>&2 eval $cmd

echo creating lifecycle module
cmd="asadmin create-lifecycle-module -t --port $ASADMIN_PORT $ASADMIN_PW_OPTS --classpath  $JV_SRCROOT/runtime/ui/bld/test-classes/dist/javaee-verifier-test-module.jar --classname test.JavaEEVerifierMBeanInstaller JavaEEVerifier"
1>&2 eval $cmd
status=$?
if [ $status -ne 0 ]; then
    bldmsg -status $status -p $0 -error "COMMAND '$cmd' FAILED"
    exit 1
fi

shutdown_domain
start_domain
   
echo verifying application
ant -q -emacs -lib "$REGRESS_CLASSPATH" -DAS_JMX_REMOTE_URL=$AS_JMX_REMOTE_URL -f runtime-ui-00010.xml verify-sa

shutdown_domain
exit 0