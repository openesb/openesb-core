#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)jbi-admin-cli00001.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

#----------------------------------------------------------------------------------
# Perform any necessary cleanup to restore the repository back to its initial state.
#----------------------------------------------------------------------------------
test_cleanup()
{
  cleanup server
}


#----------------------------------------------------------------------------------
# Main function called that will run the test
#----------------------------------------------------------------------------------
run_test()
{
  initilize_test 
  build_test_application_artifacts
  #test_cleanup
  
  #--------------------------------------------------------------------------------
  # Make sure cli-config-binding componet is installed and started
  #--------------------------------------------------------------------------------
  install_component server ${JV_SVC_BLD}/regress/dist/cli-config-binding.jar
  start_component server cli-config-binding
      
  echo ""
  echo "-------------------------------------------------------------------"
  echo " List the Application Variables for the cli-config-binding component"
  echo "-------------------------------------------------------------------"
  echo "list-jbi-application-variables --component=cli-config-binding"
  $AS8BASE/bin/asadmin list-jbi-application-variables --component=cli-config-binding --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS
    
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Create the Application Variables for the cli-config-binding component using the command line"
  echo "-------------------------------------------------------------------"
  echo "create-jbi-application-variable --component=cli-config-binding FirstName=[String]Fred,LastName=Smith,t=[Boolean]true,f=[Boolean]false"
  $AS8BASE/bin/asadmin create-jbi-application-variable --component=cli-config-binding --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS FirstName=[String]Fred,LastName=Smith,t=[BOOLEAN]true,f=[BOOLEAN]false
    
  echo ""
  echo "-------------------------------------------------------------------"
  echo " List the Application Variables for the cli-config-binding component"
  echo "-------------------------------------------------------------------"
  echo "list-jbi-application-variables --component=cli-config-binding"
  $AS8BASE/bin/asadmin list-jbi-application-variables --component=cli-config-binding --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS
    
  echo ""
  echo "-------------------------------------------------------------------"
  echo " List again and save the output in the property file appVariables.property"
  echo "-------------------------------------------------------------------"
  echo "list-jbi-application-variables --terse --component=cli-config-binding"
  $AS8BASE/bin/asadmin list-jbi-application-variables --component=cli-config-binding --terse --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS > $JV_SVC_BLD/tmp/appVariables.property
  $AS8BASE/bin/asadmin list-jbi-application-variables --component=cli-config-binding --terse --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS
    
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Try to create the same application variable that was already created ** Should generate and Error **"
  echo "-------------------------------------------------------------------"
  echo "create-jbi-application-variable --component=cli-config-binding FirstName=[String]Tom"
  $AS8BASE/bin/asadmin create-jbi-application-variable --component=cli-config-binding --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS FirstName=[String]Tom
    
  echo ""
  echo "-------------------------------------------------------------------"
  echo " List the Application Variables for the cli-config-binding component"
  echo "-------------------------------------------------------------------"
  echo "list-jbi-application-variables --component=cli-config-binding"
  $AS8BASE/bin/asadmin list-jbi-application-variables --component=cli-config-binding --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS
     
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Update some existing variables from the command line"
  echo "-------------------------------------------------------------------"
  echo "update-jbi-application-variable --component=cli-config-binding FirstName=[String]Steve,LastName=Jones"
  $AS8BASE/bin/asadmin update-jbi-application-variable --component=cli-config-binding --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS FirstName=[String]Steve,LastName=Jones
    
  echo ""
  echo "-------------------------------------------------------------------"
  echo " List the Application Variables for the cli-config-binding component"
  echo "-------------------------------------------------------------------"
  echo "list-jbi-application-variables --component=cli-config-binding"
  $AS8BASE/bin/asadmin list-jbi-application-variables --component=cli-config-binding --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS
       
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Update/restore some existing variables using the saved properties file"
  echo "-------------------------------------------------------------------"
  echo "update-jbi-application-variable --component=cli-config-binding appVariables.property"
  $AS8BASE/bin/asadmin update-jbi-application-variable --component=cli-config-binding --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS $JV_SVC_BLD/tmp/appVariables.property
    
  echo ""
  echo "-------------------------------------------------------------------"
  echo " List the Application Variables for the cli-config-binding component"
  echo "-------------------------------------------------------------------"
  echo "list-jbi-application-variables --component=cli-config-binding"
  $AS8BASE/bin/asadmin list-jbi-application-variables --component=cli-config-binding --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS
    
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Delete the application variables"
  echo "-------------------------------------------------------------------"
  echo "delete-jbi-application-variable --component=cli-config-binding FirstName,LastName"
  $AS8BASE/bin/asadmin delete-jbi-application-variable --component=cli-config-binding --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS FirstName,LastName
    
  echo ""
  echo "-------------------------------------------------------------------"
  echo " List the Application Variables for the cli-config-binding component"
  echo "-------------------------------------------------------------------"
  echo "list-jbi-application-variables --component=cli-config-binding"
  $AS8BASE/bin/asadmin list-jbi-application-variables --component=cli-config-binding --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS
            
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Create the Application Variables using a property file"
  echo "-------------------------------------------------------------------"
  echo "create-jbi-application-variable --component=cli-config-binding appVariables.property"
  $AS8BASE/bin/asadmin create-jbi-application-variable --component=cli-config-binding --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS $JV_SVC_BLD/tmp/appVariables.property
      
  echo ""
  echo "-------------------------------------------------------------------"
  echo " List the Application Variables for the cli-config-binding component"
  echo "-------------------------------------------------------------------"
  echo "list-jbi-application-variables --component=cli-config-binding"
  $AS8BASE/bin/asadmin list-jbi-application-variables --component=cli-config-binding --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS
     
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Create an application variable that contains an embedded common and colon"
  echo "-------------------------------------------------------------------"
  echo "create-jbi-application-variable --component=cli-config-binding URL=http\://www.youraretheman.com,AMOUNT=$1\,000"
  $AS8BASE/bin/asadmin create-jbi-application-variable --component=cli-config-binding --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS "URL=http\://www.youraretheman.com,AMOUNT=1\,000"
      
  echo ""
  echo "-------------------------------------------------------------------"
  echo " List the Application Variables for the cli-config-binding component"
  echo "-------------------------------------------------------------------"
  echo "list-jbi-application-variables --component=cli-config-binding"
  $AS8BASE/bin/asadmin list-jbi-application-variables --component=cli-config-binding --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Delete the application variables"
  echo "-------------------------------------------------------------------"
  echo "delete-jbi-application-variable --component=cli-config-binding URL,AMOUNT"
  $AS8BASE/bin/asadmin delete-jbi-application-variable --component=cli-config-binding --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS URL,AMOUNT,FirstName,LastName,f,t
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " List the Application Variables for the cli-config-binding component"
  echo "-------------------------------------------------------------------"
  echo "list-jbi-application-variables --component=cli-config-binding"
  $AS8BASE/bin/asadmin list-jbi-application-variables --component=cli-config-binding --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS

}


#----------------------------------------------------------------------------------
# Perform the regression setup and call the function that will run the test
#----------------------------------------------------------------------------------
TEST_NAME="jbi-admin-cli00010"
TEST_DESCRIPTION="Test Application Variable Commands"
. ./regress_defs.ksh
run_test

exit 0


