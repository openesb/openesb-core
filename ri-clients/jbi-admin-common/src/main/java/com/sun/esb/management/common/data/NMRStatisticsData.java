/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)NMRStatisticsData.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.esb.management.common.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.management.openmbean.CompositeData;
import javax.management.openmbean.CompositeType;
import javax.management.openmbean.TabularData;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import com.sun.esb.management.common.ManagementRemoteException;
import com.sun.esb.management.common.data.helper.NMRStatisticsDataWriter;

/**
 * @author graj
 *
 */
public class NMRStatisticsData implements Serializable {
    static final long                  serialVersionUID    = -1L;
    
    public static final String         INSTANCENAME_KEY    = "InstanceName";
    
    public static final String         ACTIVECHANNELS_KEY  = "ListActiveChannels";
    
    public static final String         ACTIVEENDPOINTS_KEY = "ListActiveEndpoints";
    
    String                             instanceName;
    
    List<String /*active channels*/>  activeChannelsList  = new ArrayList<String /*active channels*/>();
    
    List<String /*active endpoints*/> activeEndpointsList = new ArrayList<String /*active endpoints*/>();
    
    /**
     * Generate Tabular Data for this object
     * 
     * @param map
     * @return tabular data of this object
     */
    static public TabularData generateTabularData(
            Map<String /* instanceName */, NMRStatisticsData> map) {
        TabularData tabularData = null;
        try {
            tabularData = NMRStatisticsDataCreator.createTabularData(map);
        } catch (ManagementRemoteException e) {
        }
        return tabularData;
    }
    
    /**
     * Retrieves the NMR Statistics Data
     * 
     * @param tabularData
     * @return NMR Statistics Data
     */
    @SuppressWarnings("unchecked")
    static public Map<String /* instanceName */, NMRStatisticsData> retrieveDataMap(
            TabularData tabularData) {
        NMRStatisticsData data = null;
        Map<String /* instanceName */, NMRStatisticsData> map = null;
        map = new HashMap<String /* instanceName */, NMRStatisticsData>();
        for (Iterator dataIterator = tabularData.values().iterator(); dataIterator
                .hasNext();) {
            CompositeData compositeData = (CompositeData) dataIterator.next();
            CompositeType compositeType = compositeData.getCompositeType();
            data = new NMRStatisticsData();
            for (String item : compositeType.keySet()) {
                if (true == item.equals(NMRStatisticsData.INSTANCENAME_KEY)) {
                    String value = (String) compositeData.get(item);
                    data.setInstanceName(value);
                }
                if (true == item.equals(NMRStatisticsData.ACTIVECHANNELS_KEY)) {
                    String[] values = (String[]) compositeData.get(item);
                    data.setActiveChannelsList(values);
                }
                if (true == item.equals(NMRStatisticsData.ACTIVEENDPOINTS_KEY)) {
                    String[] values = (String[]) compositeData.get(item);
                    data.setActiveEndpointsList(values);
                }
            }
            map.put(data.getInstanceName(), data);
        }
        return map;
    }
    
    /**
     * Converts a NMR Statistics Data to an XML String
     * 
     * @param map
     * @return XML string representing a NMR Statistics Datum
     * @throws ManagementRemoteException
     */
    static public String convertDataMapToXML(
            Map<String /* instanceName */, NMRStatisticsData> map)
            throws ManagementRemoteException {
        String xmlText = null;
        try {
            xmlText = NMRStatisticsDataWriter.serialize(map);
        } catch (ParserConfigurationException e) {
            throw new ManagementRemoteException(e);
        } catch (TransformerException e) {
            throw new ManagementRemoteException(e);
        }
        
        return xmlText;
        
    }
    
    
    
    /**
     * @return the instanceName
     */
    public String getInstanceName() {
        return this.instanceName;
    }
    
    /**
     * @param instanceName the instanceName to set
     */
    public void setInstanceName(String instanceName) {
        this.instanceName = instanceName;
    }
    
    /**
     * @return the activeChannelList
     */
    public List<String> getActiveChannelsList() {
        return this.activeChannelsList;
    }
    
    /**
     * 
     * @return the activeChannel array
     */
    public String[] getActiveChannelsArray() {
        String[] result = toArray(this.activeChannelsList, String.class);
        return result;
    }
    
    /**
     * @param activeChannelList the activeChannelList to set
     */
    public void setActiveChannelsList(List<String> activeChannelList) {
        this.activeChannelsList = activeChannelList;
    }
    
    /**
     * @param activeChannelsArray
     *            the activeChannelsArray to set
     */
    public void setActiveChannelsList(String[] activeChannelsArray) {
        for (String activeChannel : activeChannelsArray) {
            this.activeChannelsList.add(activeChannel);
        }
    }
    
    /**
     * @return the activeEndpoints
     */
    public List<String> getActiveEndpointsList() {
        return this.activeEndpointsList;
    }
    
    /**
     * @param activeEndpoints the activeEndpoints to set
     */
    public void setActiveEndpointsList(List<String> activeEndpoints) {
        this.activeEndpointsList = activeEndpoints;
    }
    
    /**
     * 
     * @return the activeChannel array
     */
    public String[] getActiveEndpointsArray() {
        String[] result = toArray(this.activeEndpointsList, String.class);
        return result;
    }
    
    /**
     * @param activeEndpointsArray
     *            the activeEndpointsArray to set
     */
    public void setActiveEndpointsList(String[] activeEndpointsArray) {
        for (String activeEndpoint : activeEndpointsArray) {
            this.activeEndpointsList.add(activeEndpoint);
        }
    }
    
    /**
     * 
     * @param collection
     * @param componentType
     * @return
     */
    @SuppressWarnings("unchecked")
    static protected <Type> Type[] toArray(Collection<Type> collection,
            Class<Type> componentType) {
        // unchecked cast
        Type[] array = (Type[]) java.lang.reflect.Array.newInstance(
                componentType, collection.size());
        int index = 0;
        for (Type value : collection) {
            array[index++] = value;
        }
        return array;
    }
    
    /**
     * 
     * @return
     */
    public String getDisplayString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append("\n  Instance Name" + "=" + this.getInstanceName());
        if ((this.getActiveChannelsList() != null)
                && (this.getActiveChannelsList().size() > 0)) {
            buffer.append("\n  Active Channels List:");
            for (String channel : this.getActiveChannelsList()) {
                buffer.append("\n    Active Channel" + "=" + channel);
            }
        }
        if ((this.getActiveEndpointsList() != null)
                && (this.getActiveEndpointsList().size() > 0)) {
            buffer.append("\n  Active Endpoints List:");
            for (String endpoint : this.getActiveEndpointsList()) {
                buffer.append("\n    Active Endpoint" + "=" + endpoint);
            }
        }
        buffer.append("\n  ========================================\n");
        return buffer.toString();
        
    }
    
    /**
     * @param args
     */
    public static void main(String[] args) {
    }
    
}
