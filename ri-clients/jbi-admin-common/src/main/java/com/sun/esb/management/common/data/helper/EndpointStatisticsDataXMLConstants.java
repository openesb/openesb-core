/**
 * 
 */
package com.sun.esb.management.common.data.helper;

/**
 * @author graj
 * 
 */
public interface EndpointStatisticsDataXMLConstants {
    // XML TAGS
    public static final String ENDPOINT_VERSION_KEY                                        = "version";
    
    public static final String ENDPOINT_VERSION_VALUE                                      = "1.0";
    
    public static final String ENDPOINT_NAMESPACE_KEY                                      = "xmlns";
    
    public static final String ENDPOINT_NAMESPACE_VALUE                                    = "http://java.sun.com/xml/ns/esb/management/EndpointStatisticsDataList";
    
    public static final String ENDPOINT_STATISTICS_DATA_LIST_KEY                  = "EndpointStatisticsDataList";
    
    public static final String PROVISIONING_ENDPOINT_STATISTICS_DATA_KEY          = "ProvisioningEndpointStatisticsData";
    
    public static final String CONSUMING_ENDPOINT_STATISTICS_DATA_KEY          = "ConsumingEndpointStatisticsData";
    
    // Base Endpoint Statistics Data XML Tags
    public static final String INSTANCE_NAME_KEY                                  = "InstanceName";
    
    public static final String IS_PROVISIONING_ENDPOINT_KEY                       = "IsProvisioningEndpoint";
    
    public static final String NUMBER_OF_RECEIVED_DONES_KEY                       = "NumberOfReceivedDones";
    
    public static final String NUMBER_OF_SENT_DONES_KEY                           = "NumberOfSentDones";
    
    public static final String NUMBER_OF_RECEIVED_FAULTS_KEY                      = "NumberOfReceivedFaults";
    
    public static final String NUMBER_OF_RECEIVED_ERRORS_KEY                      = "NumberOfReceivedErrors";
    
    public static final String NUMBER_OF_SENT_FAULTS_KEY                          = "NumberOfSentFaults";
    
    public static final String NUMBER_OF_SENT_ERRORS_KEY                          = "NumberOfSentErrors";
    
    public static final String MESSAGE_EXCHANGE_COMPONENT_TIME_AVERAGE_KEY        = "MessageExchangeComponentTimeAverage";
    
    public static final String MESSAGE_EXCHANGE_DELIVERY_CHANNEL_TIME_AVERAGE_KEY = "MessageExchangeDeliveryChannelTimeAverage";
    
    public static final String MESSAGE_EXCHANGE_SERVICE_TIME_AVERAGE_KEY          = "MessageExchangeServiceTimeAverage";
    
    public static final String EXTENDED_TIMING_STATISTICS_FLAG_ENABLED_KEY        = "ExtendedTimingStatisticsFlagEnabled";
    
    // Provisioning Endpoint Statistics Data XML Tags
    public static final String ACTIVATION_TIME_KEY                                = "ActivationTime";
    
    public static final String UPTIME_KEY                                         = "Uptime";
    
    public static final String NUMBER_OF_ACTIVE_EXCHANGES_KEY                     = "NumberOfActiveExchanges";
    
    public static final String NUMBER_OF_RECEIVED_REQUESTS_KEY                    = "NumberOfReceivedRequests";
    
    public static final String NUMBER_OF_SENT_REPLIES_KEY                         = "NumberOfSentReplies";
    
    public static final String COMPONENT_NAME_KEY                                 = "ComponentName";
    
    public static final String MESSAGE_EXCHANGE_RESPONSE_TIME_AVERAGE_KEY         = "MessageExchangeResponseTimeAverage";
    
    // Consuming Endpoint Statistics Data XML Tags
    public static final String NUMBER_OF_SENT_REQUESTS_KEY                        = "NumberOfSentRequests";
    
    public static final String NUMBER_OF_RECEIVED_REPLIES_KEY                     = "NumberOfReceivedReplies";
    
    public static final String MESSAGE_EXCHANGE_STATUS_TIME_AVERAGE_KEY           = "MessageExchangeStatusTimeAverage";
}
