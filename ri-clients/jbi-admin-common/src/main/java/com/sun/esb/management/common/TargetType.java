/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TargetType.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.esb.management.common;

/**
 * Defines the different types of targets that we support
 * 
 * @author graj
 */
public enum TargetType {
    
    STANDALONE_SERVER("StandaloneServer"), 
    CLUSTER("Cluster"), 
    CLUSTERED_SERVER("ClusteredServer"), 
    DOMAIN("domain"), 
    INVALID_TARGET("Invalid_Target");
    
    String targetType;
    
    /** @param typeString */
    private TargetType(String typeString) {
        this.targetType = typeString;
    }
    
    /** @return the targetType description */
    public String getDescription() {
        switch (this) {
            case STANDALONE_SERVER:
                return "Standalone Server Target";
            case CLUSTER:
                return "Cluster Target";
            case CLUSTERED_SERVER:
                return "Clustered Server Target";
            case DOMAIN:
                return "domain Target";
            case INVALID_TARGET:
                return "Invalid Target";
            default:
                return "Invalid Target";
        }
    }
    
    /** @return the targetType */
    public String getTargetType() {
        return targetType;
    }
    
}
