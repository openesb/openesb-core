/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)EndpointStatisticsDataReader.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.esb.management.common.data.helper;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.Serializable;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.sun.esb.management.common.data.ConsumingEndpointStatisticsData;
import com.sun.esb.management.common.data.IEndpointStatisticsData;
import com.sun.esb.management.common.data.PerformanceData;
import com.sun.esb.management.common.data.ProvisioningEndpointStatisticsData;

/**
 * @author graj
 *
 */
public class EndpointStatisticsDataReader extends DefaultHandler implements
EndpointStatisticsDataXMLConstants, Serializable {

    static final long                       serialVersionUID = -1L;
    
    // Private members needed to parse the XML document
    
    // keep track of parsing
    private boolean                         parsingInProgress;
    
    // keep track of QName
    private Stack<String>                   qNameStack       = new Stack<String>();
    
    private String                          endpointStatisticsDataListVersion;
    
    private IEndpointStatisticsData         data;
    
    private ProvisioningEndpointStatisticsData provisioningData;
    
    private ConsumingEndpointStatisticsData consumingData;
    
    private Map<String /* instanceName */, IEndpointStatisticsData> dataMap;
    
    private PerformanceData                              performanceData; 
    
    private Map<String /* category */, PerformanceData> performanceDataMap;
    
    private String performanceDataListVersion;

    /**
     * Constructor - creates a new instance of
     * EndpointStatisticsDataReader
     */
    public EndpointStatisticsDataReader() {
    }
    
    /**
     * @return the dataMap
     */
    public Map<String /* instanceName */, IEndpointStatisticsData> getEndpointStatisticsDataMap() {
        return this.dataMap;
    }  
    
    /**
     * Start of document processing.
     * 
     * @throws org.xml.sax.SAXException
     *             is any SAX exception, possibly wrapping another exception.
     */
    public void startDocument() throws SAXException {
        parsingInProgress = true;
        qNameStack.removeAllElements();
    }
    
    /**
     * End of document processing.
     * 
     * @throws org.xml.sax.SAXException
     *             is any SAX exception, possibly wrapping another exception.
     */
    public void endDocument() throws SAXException {
        parsingInProgress = false;
        // We have encountered the end of the document. Do any processing that
        // is desired, for example dump all collected element2 values.
        
    }
    
    /**
     * Process the new element.
     * 
     * @param uri
     *            is the Namespace URI, or the empty string if the element has
     *            no Namespace URI or if Namespace processing is not being
     *            performed.
     * @param localName
     *            is the The local name (without prefix), or the empty string if
     *            Namespace processing is not being performed.
     * @param qName
     *            is the qualified name (with prefix), or the empty string if
     *            qualified names are not available.
     * @param attributes
     *            is the attributes attached to the element. If there are no
     *            attributes, it shall be an empty Attributes object.
     * @throws org.xml.sax.SAXException
     *             is any SAX exception, possibly wrapping another exception.
     */
    public void startElement(String uri, String localName, String qName,
            Attributes attributes) throws SAXException {
        if (qName != null) {
            if (qName.endsWith(ENDPOINT_STATISTICS_DATA_LIST_KEY)) {
                // ELEMENT1 has an attribute, get it by name
                // Do something with the attribute
                if ((attributes != null) && (attributes.getLength() > 0)) {
                    String namespace = attributes.getValue(ENDPOINT_NAMESPACE_KEY);
                    // //////////////////////////////////////////////////////
                    // Read performanceDataListVersion attribute and ensure you
                    // store the right
                    // performanceDataListVersion of the report map list
                    // //////////////////////////////////////////////////////
                    this.endpointStatisticsDataListVersion = attributes
                    .getValue(ENDPOINT_VERSION_KEY);
                    if ((endpointStatisticsDataListVersion != null)
                            && (ENDPOINT_VERSION_VALUE
                                    .equals(endpointStatisticsDataListVersion))) {
                        this.dataMap = new HashMap<String /* instanceName */, IEndpointStatisticsData>();
                    } else {
                        // Invalid endpointStatisticsDataListVersion.
                        // Not storing it
                    }
                }
            } else if (qName.endsWith(PROVISIONING_ENDPOINT_STATISTICS_DATA_KEY)) {
                // ELEMENT1 has an attribute, get it by name
                // Do something with the attribute
                if (this.dataMap != null) {
                    this.data = new ProvisioningEndpointStatisticsData();
                    this.provisioningData = (ProvisioningEndpointStatisticsData) this.data;
                }
            } else if (qName.endsWith(CONSUMING_ENDPOINT_STATISTICS_DATA_KEY)) {
                // ELEMENT1 has an attribute, get it by name
                // Do something with the attribute
                if (this.dataMap != null) {
                    this.data = new ConsumingEndpointStatisticsData();
                    this.consumingData = (ConsumingEndpointStatisticsData) this.data;
                }
            } else if (qName.endsWith(PerformanceDataXMLConstants.PERFORMANCE_MEASUREMENT_DATA_LIST_KEY)) {
                // ELEMENT1 has an attribute, get it by name
                // Do something with the attribute
                if ((attributes != null) && (attributes.getLength() > 0)) {
                    String namespace = attributes.getValue(PerformanceDataXMLConstants.NAMESPACE_KEY);
                    ////////////////////////////////////////////////////////
                    // Read performanceDataListVersion attribute and ensure you store the right
                    // performanceDataListVersion of the data map list
                    ////////////////////////////////////////////////////////
                    this.performanceDataListVersion = attributes.getValue(PerformanceDataXMLConstants.VERSION_KEY);
                    if ((performanceDataListVersion != null) && (PerformanceDataXMLConstants.VERSION_VALUE.equals(performanceDataListVersion))) {
                        this.performanceDataMap = new HashMap<String /* category */, PerformanceData>();
                    } else {
                        // Invalid performanceDataListVersion. Not storing it
                    }
                }
                
            } else if (qName.endsWith(PerformanceDataXMLConstants.PERFORMANCE_MEASUREMENT_DATA_KEY)) {
                // ELEMENT1 has an attribute, get it by name
                // Do something with the attribute
                if (this.performanceDataMap != null) {
                    this.performanceData = new PerformanceData();
                }
            }

            // Keep track of QNames
            qNameStack.push(qName);
        }
    }    

    /**
     * Process the character report for current tag.
     * 
     * @param ch
     *            are the element's characters.
     * @param start
     *            is the start position in the character array.
     * @param length
     *            is the number of characters to use from the character array.
     * @throws org.xml.sax.SAXException
     *             is any SAX exception, possibly wrapping another exception.
     */
    public void characters(char[] ch, int start, int length)
    throws SAXException {
        String qName;
        String chars = new String(ch, start, length);
        // Get current QName
        qName = (String) qNameStack.peek();
        ///////
        // 1. Read all the Provisioning Endpoints first
        if (true == qName.endsWith(EndpointStatisticsDataXMLConstants.ACTIVATION_TIME_KEY)) {
            if(data == null) {
                data = new ProvisioningEndpointStatisticsData();
                provisioningData = (ProvisioningEndpointStatisticsData) data;
            }
            Date value = null;
            // Mon Dec 03 02:19:09 PST 2007
            // Make a SimpleDateFormat for toString()'s output. This
            // has short (text) date, a space, short (text) month, a space,
            // 2-digit date, a space, hour (0-23), minute, second, a space,
            // short timezone, a final space, and a long year.
            SimpleDateFormat format = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy");            
            try {
                value = format.parse(chars);
            } catch (ParseException e) {
            }
            provisioningData.setActivationTime(value);
        }
        if (true == qName.endsWith(EndpointStatisticsDataXMLConstants.NUMBER_OF_ACTIVE_EXCHANGES_KEY)) {
            if(data == null) {
                data = new ProvisioningEndpointStatisticsData();
                provisioningData = (ProvisioningEndpointStatisticsData) data;
            }
            Long value = new Long(chars);
            provisioningData.setNumberOfActiveExchanges(value);
        }
        if (true == qName.endsWith(EndpointStatisticsDataXMLConstants.NUMBER_OF_RECEIVED_REQUESTS_KEY)) {
            if(data == null) {
                data = new ProvisioningEndpointStatisticsData();
                provisioningData = (ProvisioningEndpointStatisticsData) data;
            }
            Long value = new Long(chars);
            provisioningData.setNumberOfReceivedRequests(value);
        }
        if (true == qName.endsWith(EndpointStatisticsDataXMLConstants.NUMBER_OF_SENT_REPLIES_KEY)) {
            if(data == null) {
                data = new ProvisioningEndpointStatisticsData();
                provisioningData = (ProvisioningEndpointStatisticsData) data;
            }
            Long value = new Long(chars);
            provisioningData.setNumberOfSentReplies(value);
        }
        if (true == qName.endsWith(EndpointStatisticsDataXMLConstants.UPTIME_KEY)) {
            if(data == null) {
                data = new ProvisioningEndpointStatisticsData();
                provisioningData = (ProvisioningEndpointStatisticsData) data;
            }
            Long value = new Long(chars);
            provisioningData.setUptime(value);
        }
        if (true == qName.endsWith(EndpointStatisticsDataXMLConstants.MESSAGE_EXCHANGE_RESPONSE_TIME_AVERAGE_KEY)) {
            if(data == null) {
                data = new ProvisioningEndpointStatisticsData();
                provisioningData = (ProvisioningEndpointStatisticsData) data;
            }
            Long value = new Long(chars);
            provisioningData.setMessageExchangeResponseTimeAverage(value);
        }
        // 2. Read all the Consuming Endpoints.
        if (true == qName.endsWith(EndpointStatisticsDataXMLConstants.NUMBER_OF_RECEIVED_REPLIES_KEY)) {
            if(data == null) {
                data = new ConsumingEndpointStatisticsData();
                consumingData = (ConsumingEndpointStatisticsData) data;
            }
            Long value = new Long(chars);
            consumingData.setNumberOfReceivedReplies(value);
        }
        if (true == qName.endsWith(EndpointStatisticsDataXMLConstants.NUMBER_OF_SENT_REQUESTS_KEY)) {
            if(data == null) {
                data = new ConsumingEndpointStatisticsData();
                consumingData = (ConsumingEndpointStatisticsData) data;
            }
            Long value = new Long(chars);
            consumingData.setNumberOfSentRequests(value);
        }
        if (true == qName.endsWith(EndpointStatisticsDataXMLConstants.MESSAGE_EXCHANGE_STATUS_TIME_AVERAGE_KEY)) {
            if(data == null) {
                data = new ConsumingEndpointStatisticsData();
                consumingData = (ConsumingEndpointStatisticsData) data;
            }
            Long value = new Long(chars);
            consumingData.setMessageExchangeStatusTimeAverage(value);
        }
        // 3. Read all the base Endpoint Statistics Data.
        if (true == qName.endsWith(EndpointStatisticsDataXMLConstants.INSTANCE_NAME_KEY)) {
            if(data != null) {
                this.data.setInstanceName(chars);
            }
        }
        if (true == qName.endsWith(EndpointStatisticsDataXMLConstants.COMPONENT_NAME_KEY)) {
            if(data != null) {
                this.data.setComponentName(chars);
            }
        }
        if (true == qName.endsWith(EndpointStatisticsDataXMLConstants.EXTENDED_TIMING_STATISTICS_FLAG_ENABLED_KEY)) {
            Boolean value = new Boolean(chars);
            if(data != null) {
                this.data.setExtendedTimingStatisticsFlagEnabled(value);
            }
        }
        if (true == qName.endsWith(EndpointStatisticsDataXMLConstants.MESSAGE_EXCHANGE_COMPONENT_TIME_AVERAGE_KEY)) {
            Long value = new Long(chars);
            if(data != null) {
                this.data.setMessageExchangeComponentTimeAverage(value);
            }
        }
        if (true == qName.endsWith(EndpointStatisticsDataXMLConstants.MESSAGE_EXCHANGE_DELIVERY_CHANNEL_TIME_AVERAGE_KEY)) {
            Long value = new Long(chars);
            if(data != null) {
                this.data.setMessageExchangeDeliveryChannelTimeAverage(value);
            }
        }
        if (true == qName.endsWith(EndpointStatisticsDataXMLConstants.MESSAGE_EXCHANGE_SERVICE_TIME_AVERAGE_KEY)) {
            Long value = new Long(chars);
            if(data != null) {
                this.data.setMessageExchangeServiceTimeAverage(value);
            }
        }
        if (true == qName.endsWith(EndpointStatisticsDataXMLConstants.NUMBER_OF_RECEIVED_DONES_KEY)) {
            Long value = new Long(chars);
            if(data != null) {
                this.data.setNumberOfReceivedDones(value);
            }
        }
        if (true == qName.endsWith(EndpointStatisticsDataXMLConstants.NUMBER_OF_RECEIVED_ERRORS_KEY)) {
            Long value = new Long(chars);
            if(data != null) {
                this.data.setNumberOfReceivedErrors(value);
            }
        }
        if (true == qName.endsWith(EndpointStatisticsDataXMLConstants.NUMBER_OF_RECEIVED_FAULTS_KEY)) {
            Long value = new Long(chars);
            if(data != null) {
                this.data.setNumberOfReceivedFaults(value);
            }
        }
        if (true == qName.endsWith(EndpointStatisticsDataXMLConstants.NUMBER_OF_SENT_DONES_KEY)) {
            Long value = new Long(chars);
            if(data != null) {
                this.data.setNumberOfSentDones(value);
            }
        }
        if (true == qName.endsWith(EndpointStatisticsDataXMLConstants.NUMBER_OF_SENT_ERRORS_KEY)) {
            Long value = new Long(chars);
            if(data != null) {
                this.data.setNumberOfSentErrors(value);
            }
        }
        if (true == qName.endsWith(EndpointStatisticsDataXMLConstants.NUMBER_OF_SENT_FAULTS_KEY)) {
            Long value = new Long(chars);
            if(data != null) {
                this.data.setNumberOfSentFaults(value);
            }
        }
        ////////
        if (qName.endsWith(PerformanceDataXMLConstants.CATEGORY_KEY)) {
            if (this.performanceData != null) {
                this.performanceData.setCategory(chars);
            }
        } else if (qName.endsWith(PerformanceDataXMLConstants.ENDPOINT_KEY)) {
            if (this.performanceData != null) {
                this.performanceData.setEndpoint(chars);
            }
        } else if (qName.endsWith(PerformanceDataXMLConstants.SOURCE_CLASS_NAME_KEY)) {
            if (this.performanceData != null) {
                this.performanceData.setSourceClassName(chars);
            }
        } else if (qName.endsWith(PerformanceDataXMLConstants.NUMBER_OF_MEASUREMENT_OBJECTS_KEY)) {
            if (this.performanceData != null) {
                int numberOfMeasurementObjects = Integer.valueOf(chars);
                this.performanceData
                        .setNumberOfMeasurementObjects(numberOfMeasurementObjects);
            }
        } else if (qName.endsWith(PerformanceDataXMLConstants.NUMBER_OF_MEASUREMENTS_KEY)) {
            if (this.performanceData != null) {
                int numberOfMeasurements = Integer.valueOf(chars);
                this.performanceData.setNumberOfMeasurements(numberOfMeasurements);
            }
        } else if (qName.endsWith(PerformanceDataXMLConstants.AVERAGE_KEY)) {
            if (this.performanceData != null) {
                double average = Double.valueOf(chars);
                this.performanceData.setAverage(average);
            }
        } else if (qName.endsWith(PerformanceDataXMLConstants.AVERAGE_WITHOUT_FIRST_MEASUREMENT_KEY)) {
            if (this.performanceData != null) {
                double averageWithoutFirstMeasurement = Double.valueOf(chars);
                this.performanceData
                        .setAverageWithoutFirstMeasurement(averageWithoutFirstMeasurement);
            }
        } else if (qName.endsWith(PerformanceDataXMLConstants.FIRST_MEASUREMENT_TIME_KEY)) {
            if (this.performanceData != null) {
                double firstMeasurementTime = Double.valueOf(chars);
                this.performanceData.setFirstMeasurementTime(firstMeasurementTime);
            }
        } else if (qName.endsWith(PerformanceDataXMLConstants.LOAD_KEY)) {
            if (this.performanceData != null) {
                double load = Double.valueOf(chars);
                this.performanceData.setLoad(load);
            }
        } else if (qName.endsWith(PerformanceDataXMLConstants.MEDIAN_KEY)) {
            if (this.performanceData != null) {
                double median = Double.valueOf(chars);
                this.performanceData.setMedian(median);
            }
        } else if (qName.endsWith(PerformanceDataXMLConstants.THROUGHPUT_KEY)) {
            if (this.performanceData != null) {
                double throughput = Double.valueOf(chars);
                this.performanceData.setThroughput(throughput);
            }
        } else if (qName.endsWith(PerformanceDataXMLConstants.TIME_TAKEN_KEY)) {
            if (this.performanceData != null) {
                double timeTaken = Double.valueOf(chars);
                this.performanceData.setTimeTaken(timeTaken);
            }
        } else if (qName.endsWith(PerformanceDataXMLConstants.TOTAL_TIME_KEY)) {
            if (this.performanceData != null) {
                double totalTime = Double.valueOf(chars);
                this.performanceData.setTotalTime(totalTime);
            }
        }
    }
    
    /**
     * Process the end element tag.
     * 
     * @param uri
     *            is the Namespace URI, or the empty string if the element has
     *            no Namespace URI or if Namespace processing is not being
     *            performed.
     * @param localName
     *            is the The local name (without prefix), or the empty string if
     *            Namespace processing is not being performed.
     * @param qName
     *            is the qualified name (with prefix), or the empty string if
     *            qualified names are not available.
     * @throws org.xml.sax.SAXException
     *             is any SAX exception, possibly wrapping another exception.
     */
    public void endElement(String uri, String localName, String qName)
    throws SAXException {
        // Pop QName, since we are done with it
        qNameStack.pop();
        if (qName != null) {
            if (qName.endsWith(PROVISIONING_ENDPOINT_STATISTICS_DATA_KEY)) {
                // We have encountered the end of ELEMENT1
                // ...
                if ((this.dataMap != null) && (this.data != null)) {
                    this.dataMap.put(this.data.getInstanceName(), this.provisioningData);
                    this.data = null;
                    this.provisioningData = null;
                }
            }
            if (qName.endsWith(CONSUMING_ENDPOINT_STATISTICS_DATA_KEY)) {
                // We have encountered the end of ELEMENT1
                // ...
                if ((this.dataMap != null) && (this.data != null)) {
                    this.dataMap.put(this.data.getInstanceName(), this.consumingData);
                    this.data = null;
                    this.consumingData = null;
                }
            }
            if (qName.endsWith(PerformanceDataXMLConstants.PERFORMANCE_MEASUREMENT_DATA_KEY)) {
                // We have encountered the end of ELEMENT1
                // ...
                if ((this.dataMap != null) && (this.data != null) &&
                        (this.performanceDataMap != null) && (this.performanceData != null)) {
                    this.performanceDataMap.put(this.performanceData.getCategory(), this.performanceData);
                    this.performanceData = null;
                }
            }
            if (qName.endsWith(PerformanceDataXMLConstants.PERFORMANCE_MEASUREMENT_DATA_LIST_KEY)) {
                // We have encountered the end of ELEMENT1
                // ...
                if ((this.dataMap != null) && (this.data != null) &&
                        (this.performanceDataMap != null)) {
                    this.data.setCategoryToPerformanceDataMap(this.performanceDataMap);
                    this.performanceDataMap = null;
                    this.performanceData = null;
                    
                }
            }
        }
    }    
    
    /**
     * 
     * @param rawXMLData
     * @return
     * @throws MalformedURLException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws URISyntaxException
     * @throws IOException
     */
    public static Map<String /* instanceName */, IEndpointStatisticsData> parseFromXMLData(
            String rawXMLData) throws MalformedURLException,
            ParserConfigurationException, SAXException, URISyntaxException,
            IOException {
        // System.out.println("Parsing file: "+uriString);
        // Get an instance of the SAX parser factory
        SAXParserFactory factory = SAXParserFactory.newInstance();
        
        // Get an instance of the SAX parser
        SAXParser saxParser = factory.newSAXParser();
        
        // Initialize the XML Document InputStream
        Reader reader = new StringReader(rawXMLData);
        
        // Create an InputSource from the InputStream
        InputSource inputSource = new InputSource(reader);
        
        // Parse the aspectInput XML document stream, using my event handler
        EndpointStatisticsDataReader parser = new EndpointStatisticsDataReader();
        saxParser.parse(inputSource, parser);
        
        return parser.getEndpointStatisticsDataMap();
        
    }
    
    /**
     * 
     * @param fileName
     * @return
     * @throws MalformedURLException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws URISyntaxException
     * @throws IOException
     */
    public static Map<String /* instanceName */, IEndpointStatisticsData> parseFromFile(
            String fileName) throws MalformedURLException,
            ParserConfigurationException, SAXException, URISyntaxException,
            IOException {
        File file = new File(fileName);
        return parseFromFile(file);
    }
    
    /**
     * 
     * @param fileName
     * @return
     * @throws MalformedURLException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws URISyntaxException
     * @throws IOException
     */
    public static Map<String /* instanceName */, IEndpointStatisticsData> parseFromFile(
            File file) throws MalformedURLException,
            ParserConfigurationException, SAXException, URISyntaxException,
            IOException {
        
        // Get an instance of the SAX parser factory
        SAXParserFactory factory = SAXParserFactory.newInstance();
        
        // Get an instance of the SAX parser
        SAXParser saxParser = factory.newSAXParser();
        
        // Initialize the URI and XML Document InputStream
        InputStream inputStream = new FileInputStream(file);
        
        // Create an InputSource from the InputStream
        InputSource inputSource = new InputSource(inputStream);
        
        // Parse the aspectInput XML document stream, using my event handler
        EndpointStatisticsDataReader parser = new EndpointStatisticsDataReader();
        saxParser.parse(inputSource, parser);
        
        return parser.getEndpointStatisticsDataMap();
    }
    
    /**
     * 
     * @param uriString
     * @return
     * @throws MalformedURLException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws URISyntaxException
     * @throws IOException
     */
    public static Map<String /* instanceName */, IEndpointStatisticsData> parseFromURI(
            String uriString) throws MalformedURLException,
            ParserConfigurationException, SAXException, URISyntaxException,
            IOException {
        URI uri = new URI(uriString);
        return parseFromURI(uri);
    }
    
    /**
     * 
     * @param uri
     * @return
     * @throws MalformedURLException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws URISyntaxException
     * @throws IOException
     */
    public static Map<String /* instanceName */, IEndpointStatisticsData> parseFromURI(
            URI uri) throws MalformedURLException,
            ParserConfigurationException, SAXException, URISyntaxException,
            IOException {
        
        // Get an instance of the SAX parser factory
        SAXParserFactory factory = SAXParserFactory.newInstance();
        
        // Get an instance of the SAX parser
        SAXParser saxParser = factory.newSAXParser();
        
        // Initialize the URI and XML Document InputStream
        InputStream inputStream = uri.toURL().openStream();
        
        // Create an InputSource from the InputStream
        InputSource inputSource = new InputSource(inputStream);
        
        // Parse the aspectInput XML document stream, using my event handler
        EndpointStatisticsDataReader parser = new EndpointStatisticsDataReader();
        saxParser.parse(inputSource, parser);
        
        return parser.getEndpointStatisticsDataMap();
    }
    
    
    
    /**
     * @param args
     */
    public static void main(String[] args) {
        String uri = "C:/test/schema/endpointstatistics/EndpointStatisticsData.xml";
        try {
            Map<String /* instanceName */, IEndpointStatisticsData> map = null;
            map = EndpointStatisticsDataReader.parseFromFile(uri);
            for (String instanceName : map.keySet()) {
                System.out.println("////////////////////////");
                System.out.println("//"+instanceName);
                System.out.println("////////////////////////");
                System.out.println(map.get(instanceName).getDisplayString());
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
}
