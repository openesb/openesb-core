/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ConfigurationServiceImpl.java
 * Copyright 2004-2008 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.esb.management.impl.configuration;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.zip.ZipFile;

import javax.management.MBeanServerConnection;
import javax.management.ObjectName;
import javax.management.openmbean.CompositeData;
import javax.management.openmbean.CompositeType;
import javax.management.openmbean.TabularData;
import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import com.sun.esb.management.api.configuration.ConfigurationService;
import com.sun.esb.management.base.services.BaseServiceImpl;
import com.sun.esb.management.common.ManagementRemoteException;
import com.sun.esb.management.common.data.ApplicationVerificationReport;
import com.sun.esb.management.common.data.helper.ApplicationVerificationReportReader;
import com.sun.jbi.ui.common.JBIResultXmlBuilder;
import com.sun.jbi.ui.common.JBIRemoteException;
import com.sun.jbi.ui.common.JarFactory;

/**
 * Defines client operations for common configuration services across the JBI
 * Runtime, component containers, configuring logger levels, etc. for clients.
 *
 * @author graj
 */
public class ConfigurationServiceImpl extends BaseServiceImpl implements
        Serializable, ConfigurationService {

    static final long serialVersionUID = -1L;

    /** Constructor - Constructs a new instance of ConfigurationServiceImpl */
    public ConfigurationServiceImpl() {
        super(null, false);
    }

    /**
     * Constructor - Constructs a new instance of ConfigurationServiceImpl
     *
     * @param serverConnection
     */
    public ConfigurationServiceImpl(MBeanServerConnection serverConnection) {
        super(serverConnection, false);
    }

    /**
     * Constructor - Constructs a new instance of ConfigurationServiceImpl
     *
     * @param serverConnection
     * @param isRemoteConnection
     */
    public ConfigurationServiceImpl(MBeanServerConnection serverConnection,
            boolean isRemoteConnection) {
        super(serverConnection, isRemoteConnection);
    }

    /**
     * Detect the components support for component configuration. This method
     * returns true if the component has a configuration MBean with configurable
     * attributes
     *
     * @param componentName 
     *            component identification
     * @param targetName 
     *              identification of the target. Can be a standalone server,
     *              cluster or clustered instance.
     * @return true if the components configuration MBean has configuration 
     *              attributes
     * @throws ManagementRemoteException if the component is not installed or is not 
     *         in the Started state.
     * 
     */
     public boolean isComponentConfigSupported(String componentName, String targetName)  
        throws ManagementRemoteException
     {
        ObjectName mbeanName = this.getConfigurationServiceMBeanObjectName();
        Boolean result = null;

        Object[] params = new Object[2];
        params[0] = componentName;
        params[1] = targetName;

        String[] signature = new String[2];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";

        result = (Boolean) this.invokeMBeanOperation(mbeanName,
                "isComponentConfigSupported", params, signature);

        return result;   
     }
     
    /**
     * Detect the components support for application configuration. This method
     * returns true if the component has a configuration MBean and implements 
     * all the operations for application configuration management.
     *
     * @param componentName 
     *            component identification
     * @param targetName 
     *              identification of the target. Can be a standalone server,
     *              cluster or clustered instance.
     * @return true if the components configuration MBean implements all the
     *         operations for application configuration.
     * @throws ManagementRemoteException if the component is not installed or is not 
     *         in the Started state.
     * 
     */
     public boolean isAppConfigSupported(String componentName, String targetName)  
        throws ManagementRemoteException
     {
        ObjectName mbeanName = this.getConfigurationServiceMBeanObjectName();
        Boolean result = null;

        Object[] params = new Object[2];
        params[0] = componentName;
        params[1] = targetName;

        String[] signature = new String[2];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";

        result = (Boolean) this.invokeMBeanOperation(mbeanName,
                "isAppConfigSupported", params, signature);

        return result;         
     }
     
    /**
     * Detect the components support for application variables. This method
     * returns true if the component has a configuration MBean and implements 
     * all the operations for application variable management.
     *
     * @param componentName 
     *            component identification
     * @param targetName 
     *              identification of the target. Can be a standalone server,
     *              cluster or clustered instance.
     * @return true if the components configuration MBean implements all the
     *         operations for application variables.
     * @throws ManagementRemoteException if the component is not installed or is not 
     *         in the Started state.
     * 
     */
     public boolean isAppVarsSupported(String componentName, String targetName)  
        throws ManagementRemoteException
     {
        ObjectName mbeanName = this.getConfigurationServiceMBeanObjectName();
        Boolean result = null;

        Object[] params = new Object[2];
        params[0] = componentName;
        params[1] = targetName;

        String[] signature = new String[2];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";

        result = (Boolean) this.invokeMBeanOperation(mbeanName,
                "isAppVarsSupported", params, signature);

        return result;         
     }
             
    /**
     * Add a named application configuration to a component installed on a given
     * target.
     *
     * @param componentName
     *            component identification
     * @param targetName
     *            identification of the target. Can be a standalone server,
     *            cluster or clustered instance.
     * @param name
     *            application configuration name
     * @param config
     *            application configuration represented as a set of properties.
     * @return a JBI Management message indicating the status of the operation.
     * @throws ManagementRemoteException
     *             if there is a jmx error accessing the instance
     *
     * @see com.sun.esb.management.api.configuration.ConfigurationService#addApplicationConfiguration(java.lang.String,
     *      java.lang.String, java.lang.String, java.util.Properties)
     */
    public String addApplicationConfiguration(String componentName,
            String targetName, String name, Properties config)
            throws ManagementRemoteException {
        ObjectName mbeanName = this.getConfigurationServiceMBeanObjectName();
        String jbiMgtMsgResponse = null;

        Object[] params = new Object[4];
        params[0] = componentName;
        params[1] = targetName;
        params[2] = name;
        params[3] = config;

        String[] signature = new String[4];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";
        signature[2] = "java.lang.String";
        signature[3] = "java.util.Properties";

        jbiMgtMsgResponse = (String) this.invokeMBeanOperation(mbeanName,
                "addApplicationConfiguration", params, signature);

        return jbiMgtMsgResponse;
    }

    /**
     * Add application variables to a component installed on a given target. If
     * even a variable from the set is already set on the component, this
     * operation fails.
     *
     * @param componentName
     *            component identification
     * @param targetName
     *            identification of the target. Can be a standalone server,
     *            cluster or clustered instance.
     * @param appVariables -
     *            set of application variables to add. The values of the
     *            application variables have the application variable type and
     *            value and the format is "[type]value"
     * @return a JBI Management message indicating the status of the operation.
     *         In case a variable is not added the management message has a
     *         ERROR task status message giving the details of the failure.
     * @throws ManagementRemoteException
     *
     * @see com.sun.esb.management.api.configuration.ConfigurationService#addApplicationVariables(java.lang.String,
     *      java.lang.String, java.util.Properties)
     */
    public String addApplicationVariables(String componentName,
            String targetName, Properties appVariables)
            throws ManagementRemoteException {
        ObjectName mbeanName = this.getConfigurationServiceMBeanObjectName();
        String result = null;

        Object[] params = new Object[3];
        params[0] = componentName;
        params[1] = targetName;
        params[2] = appVariables;

        String[] signature = new String[3];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";
        signature[2] = "java.util.Properties";

        result = (String) this.invokeMBeanOperation(mbeanName,
                "addApplicationVariables", params, signature);

        return result;
    }

    /**
     * Delete a named application configuration in a component installed on a
     * given target.
     *
     * @param componentName
     *            component identification
     * @param targetName
     *            identification of the target. Can be a standalone server,
     *            cluster or clustered instance.
     * @param name
     *            name of application configuration to be deleted
     * @return a JBI Management message indicating the status of the operation.
     * @throws ManagementRemoteException
     *             if there is a jmx error accessing the instance
     *
     * @see com.sun.esb.management.api.configuration.ConfigurationService#deleteApplicationConfiguration(java.lang.String,
     *      java.lang.String, java.lang.String)
     */
    public String deleteApplicationConfiguration(String componentName,
            String targetName, String name) throws ManagementRemoteException {
        ObjectName mbeanName = this.getConfigurationServiceMBeanObjectName();
        String jbiMgtMsgResponse = null;

        Object[] params = new Object[3];
        params[0] = componentName;
        params[1] = targetName;
        params[2] = name;

        String[] signature = new String[3];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";
        signature[2] = "java.lang.String";

        jbiMgtMsgResponse = (String) this.invokeMBeanOperation(mbeanName,
                "deleteApplicationConfiguration", params, signature);

        return jbiMgtMsgResponse;
    }

    /**
     * Delete application variables from a component installed on a given
     * target. If even a variable from the set has not been added to the
     * component, this operation fails.
     *
     * @param componentName
     *            component identification
     * @param targetName
     *            identification of the target. Can be a standalone server,
     *            cluster or clustered instance.
     * @param appVariableNames -
     *            names of application variables to delete.
     * @return a JBI Management message indicating the status of the operation.
     *         In case a variable is not deleted the management message has a
     *         ERROR task status message giving the details of the failure.
     * @throws ManagementRemoteException
     *             if there is a jmx error accessing the instance
     *
     * @see com.sun.esb.management.api.configuration.ConfigurationService#deleteApplicationVariables(java.lang.String,
     *      java.lang.String, java.lang.String[])
     */
    public String deleteApplicationVariables(String componentName,
            String targetName, String[] appVariableNames)
            throws ManagementRemoteException {
        ObjectName mbeanName = this.getConfigurationServiceMBeanObjectName();
        String result = null;

        Object[] params = new Object[3];
        params[0] = componentName;
        params[1] = targetName;
        params[2] = appVariableNames;

        String[] signature = new String[3];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";
        signature[2] = "[Ljava.lang.String;";

        result = (String) this.invokeMBeanOperation(mbeanName,
                "deleteApplicationVariables", params, signature);

        return result;
    }

    /**
     * This method is used to export the application variables and application
     * configuration objects used by the given application in the specified
     * target.
     *
     * @param applicationName
     *            the name of the application
     * @param targetName
     *            the target whose configuration has to be exported
     * @param configDir
     *            the dir to store the configurations
     * @returns String the id for the zip file with exported configurations
     *
     * @throws ManagementRemoteException
     *             if the application configuration could not be exported
     *
     * Note: param configDir is used between ant/cli and common client client.
     * The return value is used between common client server and common client
     * client.
     *
     * @see com.sun.esb.management.api.configuration.ConfigurationService#exportApplicationConfiguration(java.lang.String,
     *      java.lang.String, java.lang.String)
     */
    public String exportApplicationConfiguration(String applicationName,
            String targetName, String configDir)
            throws ManagementRemoteException {
        ObjectName mbeanName = this.getConfigurationServiceMBeanObjectName();
        String result = null;

        Object[] params = new Object[3];
        params[0] = applicationName;
        params[1] = targetName;
        params[2] = configDir;

        String[] signature = new String[3];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";
        signature[2] = "java.lang.String";

        result = (String) this.invokeMBeanOperation(mbeanName,
                "exportApplicationConfiguration", params, signature);

        downloadAndExtractZip(result, configDir);
        
        return result;
    }

    /**
     * Get a specific named configuration. If the named configuration does not
     * exist in the component the returned properties is an empty set.
     *
     * @param componentName
     *            component identification
     * @param targetName
     *            identification of the target. Can be a standalone server,
     *            cluster or clustered instance.
     * @return the application configuration represented as a set of properties.
     * @throws ManagementRemoteException
     *             if there is a jmx error accessing the instance
     *
     * @see com.sun.esb.management.api.configuration.ConfigurationService#getApplicationConfiguration(java.lang.String,
     *      java.lang.String, java.lang.String)
     */
    public Properties getApplicationConfiguration(String componentName,
            String name, String targetName) throws ManagementRemoteException {
        ObjectName mbeanName = this.getConfigurationServiceMBeanObjectName();
        Properties appConfig = new Properties();

        Object[] params = new Object[3];
        params[0] = componentName;
        params[1] = name;
        params[2] = targetName;

        String[] signature = new String[3];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";
        signature[2] = "java.lang.String";

        appConfig = (Properties) this.invokeMBeanOperation(mbeanName,
                "getApplicationConfiguration", params, signature);

        return appConfig;
    }

    /**
     * Get all the application configurations set on a component.
     *
     * @param componentName
     *            component identification
     * @param targetName
     *            identification of the target. Can be a standalone server,
     *            cluster or clustered instance.
     * @return a map of all the application configurations keyed by the
     *         configuration name.
     *
     * @see com.sun.esb.management.api.configuration.ConfigurationService#getApplicationConfigurations(java.lang.String,
     *      java.lang.String)
     */
    @SuppressWarnings("unchecked")
    public Map<String, Properties> getApplicationConfigurations(
            String componentName, String targetName)
            throws ManagementRemoteException {
        ObjectName mbeanName = this.getConfigurationServiceMBeanObjectName();
        Map<String, Properties> appConfigMap = new HashMap<String, Properties>();

        Object[] params = new Object[2];
        params[0] = componentName;
        params[1] = targetName;

        String[] signature = new String[2];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";

        appConfigMap = (Map<String, Properties>) this.invokeMBeanOperation(
                mbeanName, "getApplicationConfigurations", params, signature);

        return appConfigMap;
    }

    /**
     * Get all the application variables set on a component.
     *
     * @return all the application variables et on the component. The return
     *         proerties set has the name="[type]value" pairs for the
     *         application variables.
     * @throws a
     *             JBI Management message indicating the status of the
     *             operation.
     * @see com.sun.esb.management.api.configuration.ConfigurationService#getApplicationVariables(java.lang.String,
     *      java.lang.String)
     */
    public Properties getApplicationVariables(String componentName,
            String targetName) throws ManagementRemoteException {
        ObjectName mbeanName = this.getConfigurationServiceMBeanObjectName();
        Properties appVarProps = new Properties();

        Object[] params = new Object[2];
        params[0] = componentName;
        params[1] = targetName;

        String[] signature = new String[2];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";

        appVarProps = (Properties) this.invokeMBeanOperation(mbeanName,
                "getApplicationVariables", params, signature);

        return appVarProps;
    }

    /**
     * Retrieve component configuration
     *
     * @param componentName
     * @param targetName
     * @return the targetName as key and the name/value pairs as properties
     *
     * @see com.sun.esb.management.api.configuration.ConfigurationService#getComponentConfiguration(java.lang.String,
     *      java.lang.String)
     */
    public Properties getComponentConfiguration(String componentName,
            String targetName) throws ManagementRemoteException {
        ObjectName mbeanName = this.getConfigurationServiceMBeanObjectName();
        Properties resultObject = null;

        Object[] params = new Object[2];
        params[0] = componentName;
        params[1] = targetName;

        String[] signature = new String[2];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";

        resultObject = (Properties) this.invokeMBeanOperation(mbeanName,
                "getComponentConfiguration", params, signature);

        return resultObject;
    }

    /**
     * Gets the extension MBean object names
     *
     * @param componentName
     *            name of the component
     * @param extensionName
     *            the name of the extension (e.g., Configuration, Logger, etc.)
     * @param targetName
     *            name of the target (e.g., cluster1, server, etc.)
     * @return
     * @throws ManagementRemoteException
     *             on error
     * @see com.sun.esb.management.api.configuration.ConfigurationService#getComponentExtensionMBeanObjectNames(java.lang.String,
     *      java.lang.String, java.lang.String)
     */
    @SuppressWarnings("unchecked")
    public Map<String, ObjectName[]> getComponentExtensionMBeanObjectNames(
            String componentName, String extensionName, String targetName)
            throws ManagementRemoteException {
        Map<String, ObjectName[]> result = new HashMap<String, ObjectName[]>();
        
        ObjectName mbeanName = this.getConfigurationServiceMBeanObjectName();
        Map<String, ObjectName[]> resultObject = null;

        // Retrieve list of entension MBeans for all targets
        Object[] params = new Object[3];
        params[0] = componentName;
        params[1] = extensionName;
        params[2] = targetName;

        String[] signature = new String[3];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";
        signature[2] = "java.lang.String";

        resultObject = (Map<String, ObjectName[]>) this.invokeMBeanOperation(
                mbeanName, "getComponentExtensionMBeanObjectNames", params,
                signature);

        // Retrieve list of target to instances
        Map<String /*targetName*/, String[] /*targetInstanceNames*/> targetToInstanceMap = null;
        try {
            targetToInstanceMap = super.listTargetNames();
            if(targetToInstanceMap != null) {
                String[] targetInstanceNames = targetToInstanceMap.get(targetName);
                if((targetInstanceNames != null) &&
                        (resultObject != null)) {
                    for(String instanceName : targetInstanceNames) {
                        if(instanceName != null) {
                            ObjectName[] values = resultObject.get(instanceName);
                            if(values != null) {
                                result.put(instanceName, values);
                            }
                        }
                    }
                    resultObject = result;
                } else if(resultObject != null) {
                    ObjectName[] values = resultObject.get(targetName);
                    if(values != null) {
                        result.put(targetName, values);
                    }
                    resultObject = result;
                }
            }
        } catch(ManagementRemoteException e) {
            // Ignore the exception
        }
        
        return resultObject;
    }

    /**
     * Gets the extension MBean object names
     *
     * @param componentName
     *            name of the component
     * @param extensionName
     *            the name of the extension (e.g., Configuration, Logger, etc.)
     * @param targetName
     *            name of the target (e.g., cluster1, server, etc.)
     * @param targetInstanceName
     *            name of the target instance (e.g., cluster1-instance1, etc.)
     * @return an array of ObjectName(s)
     * @throws ManagementRemoteException
     *             on error
     *
     * @see com.sun.esb.management.api.configuration.ConfigurationService#getComponentExtensionMBeanObjectNames(java.lang.String,
     *      java.lang.String, java.lang.String, java.lang.String)
     */
    public ObjectName[] getComponentExtensionMBeanObjectNames(
            String componentName, String extensionName, String targetName,
            String targetInstanceName) throws ManagementRemoteException {
        ObjectName mbeanName = this.getConfigurationServiceMBeanObjectName();
        ObjectName[] resultObject = null;

        Object[] params = new Object[4];
        params[0] = componentName;
        params[1] = extensionName;
        params[2] = targetName;
        params[3] = targetInstanceName;

        String[] signature = new String[4];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";
        signature[2] = "java.lang.String";
        signature[3] = "java.lang.String";

        resultObject = (ObjectName[]) this.invokeMBeanOperation(mbeanName,
                "getComponentExtensionMBeanObjectNames", params, signature);

        return resultObject;
    }

    /**
     * Gets the component custom loggers and their levels
     *
     * @param componentName
     *            name of the component
     * @param targetName
     *            name of the target (e.g., cluster1, server, etc.)
     * @param targetInstanceName
     *            name of the target instance (e.g., cluster1-instance1, etc.)
     * @return a Map of loggerCustomName to their log levels
     * @throws ManagementRemoteException
     *             on error
     *
     * @see com.sun.esb.management.api.configuration.ConfigurationService#getComponentLoggerLevels(java.lang.String,
     *      java.lang.String, java.lang.String)
     */
    @SuppressWarnings("unchecked")
    public Map<String, Level> getComponentLoggerLevels(String componentName,
            String targetName, String targetInstanceName)
            throws ManagementRemoteException {
        ObjectName mbeanName = this.getConfigurationServiceMBeanObjectName();
        Map<String, Level> resultObject = null;

        Object[] params = new Object[3];
        params[0] = componentName;
        params[1] = targetName;
        params[2] = targetInstanceName;

        String[] signature = new String[3];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";
        signature[2] = "java.lang.String";

        resultObject = (Map<String, Level>) this.invokeMBeanOperation(
                mbeanName, "getComponentLoggerLevels", params, signature);

        return resultObject;
    }

    /**
     * Gets the component custom loggers and their display names.
     *
     * @param componentName
     *            name of the component
     * @param targetName
     *            name of the target (e.g., cluster1, server, etc.)
     * @param targetInstanceName
     *            name of the target instance (e.g., cluster1-instance1, etc.)
     * @return a Map of loggerCustomName to their display names
     * @throws ManagementRemoteException
     *             on error
     *
     * @see com.sun.esb.management.api.configuration.ConfigurationService#getComponentLoggerDisplayNames(java.lang.String,
     *      java.lang.String, java.lang.String)
     */
    @SuppressWarnings("unchecked")
    public Map<String, String> getComponentLoggerDisplayNames(String componentName,
            String targetName, String targetInstanceName)
            throws ManagementRemoteException {
        ObjectName mbeanName = this.getConfigurationServiceMBeanObjectName();
        Map<String, String> resultObject = null;

        Object[] params = new Object[3];
        params[0] = componentName;
        params[1] = targetName;
        params[2] = targetInstanceName;

        String[] signature = new String[3];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";
        signature[2] = "java.lang.String";

        resultObject = (Map<String, String>) this.invokeMBeanOperation(
                mbeanName, "getComponentLoggerDisplayNames", params, signature);

        return resultObject;
    }

    /**
     * This method returns a tabular data of a complex open data objects that
     * represent the runtime configuration parameter descriptor. The parameter
     * descriptor should contain the following data that represents the
     * parameter.
     *
     * name : name of the parameter value : value of the parameter as a String
     * type. type : type of the parameter. Basic data types only. description:
     * (optional) description of the parameter. displayName: (optional) display
     * name of the parameter readOnly : true/false validValues : (optional) list
     * of string values with ',' as delimiter. or a range value with - with a
     * '-' as delimiter. min and max strings will be converted to the parameter
     * type and then used to validate the value of the parameter.
     *
     * @param targetName
     *            cluster or instance name ( e.g. cluster1, instance1 ) on which
     *            configuration parameters will be set. null to represent the
     *            default instance which is admin server
     *
     * @return Map<String, Object> that represents the list of configuration
     *         parameter descriptors.
     *
     * @throws ManagementRemoteException
     *             if there is a jmx error accessing the instance
     *
     * @see com.sun.esb.management.api.configuration.ConfigurationService#getDefaultRuntimeConfiguration()
     */
    public Properties getDefaultRuntimeConfiguration()
            throws ManagementRemoteException {
        ObjectName mbeanName = this.getConfigurationServiceMBeanObjectName();
        Properties resultObject = null;

        Object[] params = null;
        String[] signature = null;

        resultObject = (Properties) this.invokeMBeanOperation(mbeanName,
                "getDefaultRuntimeConfiguration", params, signature);

        return resultObject;
    }

    /**
     * This method returns a tabular data of a complex open data objects that
     * represent the runtime configuration parameter descriptor. The parameter
     * descriptor should contain the following data that represents the
     * parameter.
     *
     * name : name of the parameter value : value of the parameter as a String
     * type. type : type of the parameter. Basic data types only. description:
     * (optional) description of the parameter. displayName: (optional) display
     * name of the parameter readOnly : true/false validValues : (optional) list
     * of string values with ',' as delimiter. or a range value with - with a
     * '-' as delimiter. min and max strings will be converted to the parameter
     * type and then used to validate the value of the parameter.
     *
     * @param targetName
     *            cluster or instance name ( e.g. cluster1, instance1 ) on which
     *            configuration parameters will be set. null to represent the
     *            default instance which is admin server
     *
     * @return Map<String, Object> that represents the list of configuration
     *         parameter descriptors.
     *
     * @throws ManagementRemoteException
     *             if there is a jmx error accessing the instance
     * @see com.sun.esb.management.api.configuration.ConfigurationService#getRuntimeConfiguration(java.lang.String)
     */
    public Properties getRuntimeConfiguration(String targetName)
            throws ManagementRemoteException {
        ObjectName mbeanName = this.getConfigurationServiceMBeanObjectName();
        Properties resultObject = null;

        Object[] params = new Object[1];
        params[0] = targetName;

        String[] signature = new String[1];
        signature[0] = "java.lang.String";

        resultObject = (Properties) this.invokeMBeanOperation(mbeanName,
                "getRuntimeConfiguration", params, signature);

        return resultObject;
    }

    /**
     * This method returns the runtime configuration metadata associated with
     * the specified property. The metadata contain name-value pairs like:
     * default, descriptionID, descriptorType, displayName, displayNameId,
     * isStatic, name, resourceBundleName, tooltip, tooltipId, etc.
     *
     * @param propertyKeyName
     * @return Properties that represent runtime configuration metadata
     * @throws ManagementRemoteException
     *
     * @see com.sun.esb.management.api.configuration.ConfigurationService#getRuntimeConfigurationMetaData(java.lang.String)
     */
    public Properties getRuntimeConfigurationMetaData(String propertyKeyName)
            throws ManagementRemoteException {
        ObjectName mbeanName = this.getConfigurationServiceMBeanObjectName();
        Properties resultObject = null;

        Object[] params = new Object[1];
        params[0] = propertyKeyName;

        String[] signature = new String[1];
        signature[0] = "java.lang.String";

        resultObject = (Properties) this.invokeMBeanOperation(mbeanName,
                "getRuntimeConfigurationMetaData", params, signature);

        return resultObject;
    }

    /**
     * Lookup the level of one runtime logger
     *
     * @param runtimeLoggerName
     *            name of the runtime logger (e.g. com.sun.jbi.framework
     * @param targetName
     *            name of the target (e.g., cluster1, server, etc.)
     * @param targetInstanceName
     *            name of the target instance (e.g., cluster1-instance1, etc.)
     * @return a Map of loggerCustomName to their log levels
     * @throws ManagementRemoteException
     *             on error
     *
     * @see com.sun.esb.management.api.configuration.ConfigurationService#getRuntimeLoggerLevel(java.lang.String,
     *      java.lang.String, java.lang.String)
     * @deprecated
     */
    public Level getRuntimeLoggerLevel(String runtimeLoggerName,
            String targetName, String targetInstanceName)
            throws ManagementRemoteException {
        ObjectName mbeanName = this.getConfigurationServiceMBeanObjectName();
        Level resultObject = null;

        Object[] params = new Object[3];
        params[0] = runtimeLoggerName;
        params[1] = targetName;
        params[2] = targetInstanceName;

        String[] signature = new String[3];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";
        signature[2] = "java.lang.String";

        resultObject = (Level) this.invokeMBeanOperation(mbeanName,
                "getRuntimeLoggerLevel", params, signature);

        return resultObject;
    }

    /**
     * Lookup the level of one runtime logger
     *
     * @param runtimeLoggerName
     *            name of the runtime logger (e.g. com.sun.jbi.framework
     * @param targetName
     *            name of the target (e.g., cluster1, server, etc.)
     * @return a Map of loggerCustomName to their log levels
     * @throws ManagementRemoteException
     *             on error
     *
     * @see com.sun.esb.management.api.configuration.ConfigurationService#getRuntimeLoggerLevel(java.lang.String,
     *      java.lang.String)
     */
    public Level getRuntimeLoggerLevel(String runtimeLoggerName,
            String targetName) throws ManagementRemoteException {
        ObjectName mbeanName = this.getConfigurationServiceMBeanObjectName();
        Level resultObject = null;

        Object[] params = new Object[2];
        params[0] = runtimeLoggerName;
        params[1] = targetName;

        String[] signature = new String[2];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";

        resultObject = (Level) this.invokeMBeanOperation(mbeanName,
                "getRuntimeLoggerLevel", params, signature);

        return resultObject;
    }

    /**
     * Gets all the runtime loggers and their levels
     *
     * @param targetName
     *            name of the target (e.g., cluster1, server, etc.)
     * @param targetInstanceName
     *            name of the target instance (e.g., cluster1-instance1, etc.)
     * @return a Map of loggerCustomName to their log levels
     * @throws ManagementRemoteException
     *             on error
     *
     * @see com.sun.esb.management.api.configuration.ConfigurationService#getRuntimeLoggerLevels(java.lang.String,
     *      java.lang.String)
     */
    @SuppressWarnings("unchecked")
    public Map<String, Level> getRuntimeLoggerLevels(String targetName,
            String targetInstanceName) throws ManagementRemoteException {
        ObjectName mbeanName = this.getConfigurationServiceMBeanObjectName();
        Map<String, Level> resultObject = null;

        Object[] params = new Object[2];
        params[0] = targetName;
        params[1] = targetInstanceName;

        String[] signature = new String[2];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";

        resultObject = (Map<String, Level>) this.invokeMBeanOperation(
                mbeanName, "getRuntimeLoggerLevels", params, signature);

        return resultObject;
    }

    /**
     * Gets all the runtime loggers and their levels
     *
     * @param targetName
     *            name of the target (e.g., cluster1, server, etc.)
     * @return a Map of loggerCustomName to their log levels
     * @throws ManagementRemoteException
     *             on error
     *
     * @see com.sun.esb.management.api.configuration.ConfigurationService#getRuntimeLoggerLevels(java.lang.String)
     */
    @SuppressWarnings("unchecked")
    public Map<String, Level> getRuntimeLoggerLevels(String targetName)
            throws ManagementRemoteException {
        ObjectName mbeanName = this.getConfigurationServiceMBeanObjectName();
        Map<String, Level> resultObject = null;

        Object[] params = new Object[1];
        params[0] = targetName;

        String[] signature = new String[1];
        signature[0] = "java.lang.String";

        resultObject = (Map<String, Level>) this.invokeMBeanOperation(
                mbeanName, "getRuntimeLoggerLevels", params, signature);

        return resultObject;
    }


    /**
     * checks if the server need to be restarted to apply the changes made to
     * some of the configuration parameters.
     *
     * @return true if server need to be restarted for updated configuration to
     *         take effect. false if no server restart is needed.
     *
     * @see com.sun.esb.management.api.configuration.ConfigurationService#isServerRestartRequired()
     */
    public boolean isServerRestartRequired() throws ManagementRemoteException {
        ObjectName mbeanName = this.getConfigurationServiceMBeanObjectName();
        Boolean resultObject = null;

        Object[] params = null;
        String[] signature = null;
        resultObject = (Boolean) this.invokeMBeanOperation(mbeanName,
                "isServerRestartRequired", params, signature);

        return resultObject;
    }

    /**
     * List all the application configurations in a component.
     *
     * @param componentName
     *            component identification
     * @param targetName
     *            identification of the target. Can be a standalone server,
     *            cluster or clustered instance.
     * @return an array of names of all the application configurations.
     * @throws ManagementRemoteException
     *             if there is a jmx error accessing the instance
     *
     * @see com.sun.esb.management.api.configuration.ConfigurationService#listApplicationConfigurationNames(java.lang.String,
     *      java.lang.String)
     */
    public String[] listApplicationConfigurationNames(String componentName,
            String targetName) throws ManagementRemoteException {
        ObjectName mbeanName = this.getConfigurationServiceMBeanObjectName();
        String[] appConfigNames = new String[] {};

        Object[] params = new Object[2];
        params[0] = componentName;
        params[1] = targetName;

        String[] signature = new String[2];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";

        appConfigNames = (String[]) this.invokeMBeanOperation(mbeanName,
                "listApplicationConfigurationNames", params, signature);

        return appConfigNames;
    }

    /**
     * Update a named application configuration in a component installed on a
     * given target.
     *
     * @param componentName
     *            component identification
     * @param name
     *            application configuration name
     * @param config
     *            application configuration represented as a set of properties.
     * @param targetName
     *            identification of the target. Can be a standalone server,
     *            cluster or clustered instance.
     * @return a JBI Management message indicating the status of the operation.
     * @throws ManagementRemoteException
     *             if there is a jmx error accessing the instance
     *
     * @see com.sun.esb.management.api.configuration.ConfigurationService#setApplicationConfiguration(java.lang.String,
     *      java.lang.String, java.util.Properties, java.lang.String)
     */
    public String setApplicationConfiguration(String componentName,
            String name, Properties config, String targetName)
            throws ManagementRemoteException {
        ObjectName mbeanName = this.getConfigurationServiceMBeanObjectName();
        String jbiMgtMsgResponse = null;

        Object[] params = new Object[4];
        params[0] = componentName;
        params[1] = name;
        params[2] = config;
        params[3] = targetName;

        String[] signature = new String[4];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";
        signature[2] = "java.util.Properties";
        signature[3] = "java.lang.String";

        jbiMgtMsgResponse = (String) this.invokeMBeanOperation(mbeanName,
                "setApplicationConfiguration", params, signature);

        return jbiMgtMsgResponse;
    }

    /**
     * Set application variables on a component installed on a given target. If
     * even a variable from the set has not been added to the component, this
     * operation fails.
     *
     * @param componentName
     *            component identification
     * @param appVariables -
     *            set of application variables to update. The values of the
     *            application variables have the application variable type and
     *            value and the format is "[type]value"
     * @param targetName
     *            identification of the target. Can be a standalone server,
     *            cluster or clustered instance.
     * @return a JBI Management message indicating the status of the operation.
     *         In case a variable is not set the management message has a ERROR
     *         task status message giving the details of the failure.
     * @throws ManagementRemoteException
     *             if there is a jmx error accessing the instance
     *
     * @see com.sun.esb.management.api.configuration.ConfigurationService#setApplicationVariables(java.lang.String,
     *      java.util.Properties, java.lang.String)
     */
    public String setApplicationVariables(String componentName,
            Properties appVariables, String targetName)
            throws ManagementRemoteException {
        ObjectName mbeanName = this.getConfigurationServiceMBeanObjectName();
        String result = null;

        Object[] params = new Object[3];
        params[0] = componentName;
        params[1] = appVariables;
        params[2] = targetName;

        String[] signature = new String[3];
        signature[0] = "java.lang.String";
        signature[1] = "java.util.Properties";
        signature[2] = "java.lang.String";

        result = (String) this.invokeMBeanOperation(mbeanName,
                "setApplicationVariables", params, signature);

        return result;
    }

    /**
     * Will return jbi mgmt message with success, failure, or partial success
     * per instance. The entry per instance will have value as part of the
     * management message (XML) String.
     *
     * @param targetName
     *            the name of the JBI target
     * @param componentName
     *            the component name
     * @param configurationValues
     *            the configuration properties
     * @return
     *
     * @see com.sun.esb.management.api.configuration.ConfigurationService#setComponentConfiguration(java.lang.String,
     *      java.util.Properties, java.lang.String)
     */
    public String setComponentConfiguration(String componentName,
            Properties configurationValues, String targetName)
            throws ManagementRemoteException {
        ObjectName mbeanName = this.getConfigurationServiceMBeanObjectName();
        String resultObject = null;

        Object[] params = new Object[3];
        params[0] = componentName;
        params[1] = configurationValues;
        params[2] = targetName;

        String[] signature = new String[3];
        signature[0] = "java.lang.String";
        signature[1] = "java.util.Properties";
        signature[2] = "java.lang.String";

        resultObject = (String) this.invokeMBeanOperation(mbeanName,
                "setComponentConfiguration", params, signature);
        return resultObject;
    }

    /**
     * Sets the component log level for a given custom logger
     *
     * @param componentName
     *            name of the component
     * @param loggerCustomName
     *            the custom logger name
     * @param logLevel
     *            the level to set the logger
     * @param targetName
     *            name of the target (e.g., cluster1, server, etc.)
     * @param targetInstanceName
     *            name of the target instance (e.g., cluster1-instance1, etc.)
     * @throws ManagementRemoteException
     *             on error
     *
     * @see com.sun.esb.management.api.configuration.ConfigurationService#setComponentLoggerLevel(java.lang.String,
     *      java.lang.String, java.util.logging.Level, java.lang.String,
     *      java.lang.String)
     */
    public void setComponentLoggerLevel(String componentName,
            String loggerCustomName, Level logLevel, String targetName,
            String targetInstanceName) throws ManagementRemoteException {
        ObjectName mbeanName = this.getConfigurationServiceMBeanObjectName();

        Object[] params = new Object[5];
        params[0] = componentName;
        params[1] = loggerCustomName;
        params[2] = logLevel;
        params[3] = targetName;
        params[4] = targetInstanceName;

        String[] signature = new String[5];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";
        signature[2] = "java.util.logging.Level";
        signature[3] = "java.lang.String";
        signature[4] = "java.lang.String";

        this.invokeMBeanOperation(mbeanName, "setComponentLoggerLevel", params,
                signature);
    }

    /**
     * This method sets one or more configuration parameters on the runtime with
     * a list of name/value pairs passed as a properties object. The property
     * name in the properties object should be an existing configuration
     * parameter name. If user try to set the parameter that is not in the
     * configuration parameters list, this method will throw an exception.
     *
     * The value of the property can be any object. If the value is non string
     * object, its string value (Object.toString()) will be used as a value that
     * will be set on the configuration.
     *
     * This method first validates whether all the paramters passed in
     * properties object exist in the runtime configuration or not. If any one
     * the parameters passed is not existing, it will return an error without
     * settings the parameters that are passed in the properties including a
     * valid parameters.
     *
     * If there is an error in setting a paramter, this method throws an
     * exception with the list of parameters that were not set.
     *
     * @param Map
     *            <String, Object> params Properties object that contains
     *            name/value pairs corresponding to the configuration parameters
     *            to be set on the runtime.
     *
     * @param targetName
     *            cluster or instance name ( e.g. cluster1, instance1 ) on which
     *            configuration parameters will be set. null to represent the
     *            default instance which is admin server
     *
     * @return true if server restart is required, false if not
     *
     * @throws ManagementRemoteException
     *             if there is a jmx error or a invalid parameter is passed in
     *             the params properties object. In case of an error setting the
     *             a particular parameter, the error message should list the
     *             invalid parameters.
     * @see com.sun.esb.management.api.configuration.ConfigurationService#setRuntimeConfiguration(java.util.Properties,
     *      java.lang.String)
     */
    public boolean setRuntimeConfiguration(Properties parameters,
            String targetName) throws ManagementRemoteException {
        ObjectName mbeanName = this.getConfigurationServiceMBeanObjectName();
        Boolean resultObject = null;

        Object[] params = new Object[2];
        params[0] = parameters;
        params[1] = targetName;

        String[] signature = new String[2];
        signature[0] = "java.util.Properties";
        signature[1] = "java.lang.String";

        resultObject = (Boolean) this.invokeMBeanOperation(mbeanName,
                "setRuntimeConfiguration", params, signature);
        return resultObject;
    }

    /**
     * Sets the log level for a given runtime logger
     *
     * @param runtimeLoggerName
     *            name of the runtime logger
     * @param logLevel
     *            the level to set the logger
     * @param targetName
     *            name of the target (e.g., cluster1, server, etc.)
     * @param targetInstanceName
     *            name of the target instance (e.g., cluster1-instance1, etc.)
     * @throws ManagementRemoteException
     *             on error
     *
     * @see com.sun.esb.management.api.configuration.ConfigurationService#setRuntimeLoggerLevel(java.lang.String,
     *      java.util.logging.Level, java.lang.String, java.lang.String)
     * @deprecated
     */
    public void setRuntimeLoggerLevel(String runtimeLoggerName, Level logLevel,
            String targetName, String targetInstanceName)
            throws ManagementRemoteException {
        ObjectName mbeanName = this.getConfigurationServiceMBeanObjectName();

        Object[] params = new Object[4];
        params[0] = runtimeLoggerName;
        params[1] = logLevel;
        params[2] = targetName;
        params[3] = targetInstanceName;

        String[] signature = new String[4];
        signature[0] = "java.lang.String";
        signature[1] = "java.util.logging.Level";
        signature[2] = "java.lang.String";
        signature[3] = "java.lang.String";

        this.invokeMBeanOperation(mbeanName, "setRuntimeLoggerLevel", params,
                signature);
    }

    /**
     * Sets the log level for a given runtime logger
     *
     * @param runtimeLoggerName
     *            name of the runtime logger
     * @param logLevel
     *            the level to set the logger
     * @param targetName
     *            name of the target (e.g., cluster1, server, etc.)
     * @throws ManagementRemoteException
     *             on error
     *
     * @see com.sun.esb.management.api.configuration.ConfigurationService#setRuntimeLoggerLevel(java.lang.String,
     *      java.util.logging.Level, java.lang.String)
     */
    public void setRuntimeLoggerLevel(String runtimeLoggerName, Level logLevel,
            String targetName) throws ManagementRemoteException {
        ObjectName mbeanName = this.getConfigurationServiceMBeanObjectName();

        Object[] params = new Object[3];
        params[0] = runtimeLoggerName;
        params[1] = logLevel;
        params[2] = targetName;

        String[] signature = new String[3];
        signature[0] = "java.lang.String";
        signature[1] = "java.util.logging.Level";
        signature[2] = "java.lang.String";

        this.invokeMBeanOperation(mbeanName, "setRuntimeLoggerLevel", params,
                signature);
    }

    /**
     * Gets all the runtime loggers and their display names
     *
     * @param targetName
     *            name of the target (e.g., cluster1, server, etc.)
     * @param targetInstanceName
     *            name of the target instance (e.g., cluster1-instance1, etc.)
     * @return a Map of display names to their logger names
     * @throws ManagementRemoteException
     *             on error
 * @deprecated
     */
    @SuppressWarnings("unchecked")
    public Map<String /* display name */, String /* logger name */> getRuntimeLoggerNames(
            String targetName, String targetInstanceName)
            throws ManagementRemoteException {
        ObjectName mbeanName = this.getConfigurationServiceMBeanObjectName();

        Map<String, String> resultObject = null;

        Object[] params = new Object[2];
        params[0] = targetName;
        params[1] = targetInstanceName;

        String[] signature = new String[2];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";

        resultObject = (Map<String, String>) this.invokeMBeanOperation(
                mbeanName, "getRuntimeLoggerNames", params, signature);

        return resultObject;
    }

    /**
     * Gets all the runtime loggers and their display names
     *
     * @param targetName
     *            name of the target (e.g., cluster1, server, etc.)
     * @return a Map of display names to their logger names
     * @throws ManagementRemoteException
     *             on error
     */
    @SuppressWarnings("unchecked")
    public Map<String /* display name */, String /* logger name */> getRuntimeLoggerNames(
            String targetName) throws ManagementRemoteException {
        ObjectName mbeanName = this.getConfigurationServiceMBeanObjectName();

        Map<String, String> resultObject = null;

        Object[] params = new Object[1];
        params[0] = targetName;

        String[] signature = new String[1];
        signature[0] = "java.lang.String";

        resultObject = (Map<String, String>) this.invokeMBeanOperation(
                mbeanName, "getRuntimeLoggerNames", params, signature);

        return resultObject;
    }

    /**
     * Return the display name for a runtime logger
     *
     * @param runtimeLoggerName
     *            name of the logger (e.g. com.sun.jbi.framework)
     * @param targetName
     *            name of the target (e.g., cluster1, server, etc.)
     * @param targetInstanceName
     *            name of the target instance (e.g., cluster1-instance1, etc.)
     * @return the display name for the given logger
     * @throws JBIRemoteException
     *             on error
     * @deprecated
     */
    public String getRuntimeLoggerDisplayName(String runtimeLoggerName,
            String targetName, String targetInstanceName)
            throws ManagementRemoteException {
        ObjectName mbeanName = this.getConfigurationServiceMBeanObjectName();

        String resultString = null;

        Object[] params = new Object[3];
        params[0] = runtimeLoggerName;
        params[1] = targetName;
        params[2] = targetInstanceName;

        String[] signature = new String[3];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";
        signature[2] = "java.lang.String";

        resultString = (String) this.invokeMBeanOperation(mbeanName,
                "getRuntimeLoggerDisplayName", params, signature);

        return resultString;
    }


    /**
     * Return the display name for a runtime logger
     *
     * @param runtimeLoggerName
     *            name of the logger (e.g. com.sun.jbi.framework)
     * @param targetName
     *            name of the target (e.g., cluster1, server, etc.)
     * @return the display name for the given logger
     * @throws JBIRemoteException
     *             on error
     */
    public String getRuntimeLoggerDisplayName(String runtimeLoggerName,
            String targetName) throws ManagementRemoteException {
        ObjectName mbeanName = this.getConfigurationServiceMBeanObjectName();

        String resultString = null;

        Object[] params = new Object[2];
        params[0] = runtimeLoggerName;
        params[1] = targetName;

        String[] signature = new String[2];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";

        resultString = (String) this.invokeMBeanOperation(mbeanName,
                "getRuntimeLoggerDisplayName", params, signature);

        return resultString;
    }

    /**
     * This method is used to verify if the application variables and
     * application configuration objects used in the given application are
     * available in JBI runtime in the specified target. Also this method
     * verifies if all necessary components are installed. If generateTemplates
     * is true templates for missing application variables and application
     * configurations are generated. A command script that uses the template
     * files to set configuration objects is generated.
     *
     * @param applicationURL
     *            the URL for the application zip file
     * @param generateTemplates
     *            true if templates have to be generated
     * @param templateDir
     *            the dir to store the generated templates
     * @param includeDeployCommand
     *            true if the generated script should include deploy command
     * @param targetName
     *            the target on which the application has to be verified
     * @param clientSAFilePath
     *            the path to the SA in the client file schemaorg_apache_xmlbeans.system
     *
     * @returns XML string corresponding to the verification report
     *
     * CompositeType of verification report String - "ServiceAssemblyName",
     * String - "ServiceAssemblyDescription", Integer - "NumServiceUnits",
     * Boolean - "AllComponentsInstalled", String[] - "MissingComponentsList",
     * CompositeData[] - "EndpointInfo", String - "TemplateZIPID"
     *
     * CompositeType of each EndpointInfo String - "EndpointName", String -
     * "ServiceUnitName", String - "ComponentName", String - "Status"
     *
     * @throws ManagementRemoteException
     *             if the application could not be verified
     *
     * Note: param templateDir is used between ant/cli and common client client
     * TemplateZIPID is used between common client server and common client
     * client
     *
     * @see com.sun.esb.management.api.configuration.ConfigurationService#verifyApplication(java.lang.String,
     *      boolean, java.lang.String, boolean, java.lang.String)
     */
    public String verifyApplication(String applicationURL,
            boolean generateTemplates, String templateDir,
            boolean includeDeployCommand, String targetName,
            String clientSAFilePath)
            throws ManagementRemoteException {
        ObjectName mbeanName = this.getConfigurationServiceMBeanObjectName();
        String appFilePath = applicationURL;
        if (this.isRemoteConnection() == true) {
            appFilePath = uploadFile(applicationURL);
        }

        Object[] params = new Object[6];
        params[0] = appFilePath;
        params[1] = generateTemplates;
        params[2] = templateDir;
        params[3] = includeDeployCommand;
        params[4] = targetName;
        params[5] = applicationURL;

        String[] signature = new String[6];
        signature[0] = "java.lang.String";
        signature[1] = "boolean";
        signature[2] = "java.lang.String";
        signature[3] = "boolean";
        signature[4] = "java.lang.String";
        signature[5] = "java.lang.String";

        String rawXMLData = (String) this.invokeMBeanOperation(mbeanName,
                "verifyApplication", params, signature);
        ApplicationVerificationReport report = null;
        try {
            report = ApplicationVerificationReportReader
                    .parseFromXMLData(rawXMLData);
        } catch (MalformedURLException e) {
            throw new ManagementRemoteException(e);
        } catch (ParserConfigurationException e) {
            throw new ManagementRemoteException(e);
        } catch (SAXException e) {
            throw new ManagementRemoteException(e);
        } catch (URISyntaxException e) {
            throw new ManagementRemoteException(e);
        } catch (IOException e) {
            throw new ManagementRemoteException(e);
        }

        if (generateTemplates) 
        {
            downloadAndExtractZip(report.getTemplateZipId(), templateDir);
        }
        return rawXMLData;
    }

    /**
     * This method is used to download the template zip from the server and
     * extract it in the specified dir
     *
     * @param filePath
     *            the path to the template zip in the server
     * @param templateDir
     *            the dir to extract the zip into
     */
    protected void downloadAndExtractZip(String filePath, String templateDir)
            throws ManagementRemoteException {
        try {

            boolean dirExist = true;
            File directory = new File(templateDir);

            if (false == directory.exists()) {
                dirExist = directory.mkdirs();
            }

            // Let the user know if the template directory was not created.
            if (!dirExist)
            {
                String jbiMgmtMsg = JBIResultXmlBuilder.createJbiResultXml (
                        getI18NBundle(), "jbi.ui.client.invalid.directory.name",
                        new Object[] { templateDir }, null);
                throw new ManagementRemoteException(new Exception(jbiMgmtMsg));
            }

            String fileName = downloadFile(templateDir, filePath);
            JarFactory jarFactory = new JarFactory(templateDir);
            File templateZip = new File(templateDir, fileName);
            ZipFile zipFile = new ZipFile(templateZip);
            jarFactory.unJar(zipFile);
            
            //after extracting the files, delete the zip file
            if (templateZip.isFile())
            {
                zipFile = null;
                if (!templateZip.delete())
                {
                    templateZip.deleteOnExit();
                }
            }
        } catch (Exception ex) {
            throw new ManagementRemoteException(ex);
        }
    }

    /*---------------------------------------------------------------------------------*\
     *            Operations Component Configuration meta-data Management              *
    \*---------------------------------------------------------------------------------*/
    /**
     * Get the CompositeType definition for the components application
     * configuration
     *
     * @param componentName
     *            component identification
     * @param targetName
     *            identification of the target. Can be a standalone server,
     *            cluster or clustered instance.
     * @return the CompositeType for the components application configuration.
     */
    public CompositeType queryApplicationConfigurationType(
            String componentName, String targetName)
            throws ManagementRemoteException {
        ObjectName mbeanName = this.getConfigurationServiceMBeanObjectName();
        javax.management.openmbean.CompositeType appCfgType = null;

        Object[] params = new Object[2];
        params[0] = componentName;
        params[1] = targetName;

        String[] signature = new String[2];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";

        appCfgType = (CompositeType) this.invokeMBeanOperation(mbeanName,
                "queryApplicationConfigurationType", params, signature);

        return appCfgType;
    }

    /**
     * Retrieves the component specific configuration schema.
     *
     * @param componentName
     *            component identification
     * @param targetName
     *            identification of the target. Can be a standalone server,
     *            cluster or clustered instance.
     * @return a String containing the configuration schema.
     * @throws ManagementRemoteException
     *             on errors.
     */
    public String retrieveConfigurationDisplaySchema(String componentName,
            String targetName) throws ManagementRemoteException {
        ObjectName mbeanName = this.getConfigurationServiceMBeanObjectName();
        String schema = "";

        Object[] params = new Object[2];
        params[0] = componentName;
        params[1] = targetName;

        String[] signature = new String[2];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";

        schema = (String) this.invokeMBeanOperation(mbeanName,
                "retrieveConfigurationDisplaySchema", params, signature);

        return schema;
    }

    /**
     * Retrieves the component configuration metadata. The XML data conforms to
     * the component configuration schema.
     *
     * @param componentName
     *            component identification
     * @param targetName
     *            identification of the target. Can be a standalone server,
     *            cluster or clustered instance.
     * @return a String containing the configuration metadata.
     * @throws ManagementRemoteException
     *             on errors
     */
    public String retrieveConfigurationDisplayData(String componentName,
            String targetName) throws ManagementRemoteException {
        ObjectName mbeanName = this.getConfigurationServiceMBeanObjectName();

        String data = "";

        Object[] params = new Object[2];
        params[0] = componentName;
        params[1] = targetName;

        String[] signature = new String[2];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";

        data = (String) this.invokeMBeanOperation(mbeanName,
                "retrieveConfigurationDisplayData", params, signature);

        return data;
    }

    /**
     * Add an application configuration. The configuration name is a part of the
     * CompositeData. The itemName for the configuration name is
     * "configurationName" and the type is SimpleType.STRING
     *
     * @param componentName
     * @param targetName
     * @param name -
     *            configuration name, must match the value of the field "name"
     *            in the namedConfig
     * @param appConfig -
     *            application configuration composite
     * @return management message string which gives the status of the
     *         operation. For target=cluster, instance specific details are
     *         included.
     * @throws ManagementRemoteException
     *             if the application configuration cannot be added.
     */
    public String addApplicationConfiguration(String componentName,
            String targetName, String name, CompositeData appConfig)
            throws ManagementRemoteException {
        ObjectName mbeanName = this.getConfigurationServiceMBeanObjectName();

        String data = "";

        Object[] params = new Object[4];
        params[0] = componentName;
        params[1] = targetName;
        params[2] = name;
        params[3] = appConfig;

        String[] signature = new String[4];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";
        signature[2] = "java.lang.String";
        signature[3] = "javax.management.openmbean.CompositeData";

        data = (String) this.invokeMBeanOperation(mbeanName,
                "addApplicationConfiguration", params, signature);

        return data;
    }

    /**
     * Update a application configuration. The configuration name is a part of
     * the CompositeData. The itemName for the configuration name is
     * "configurationName" and the type is SimpleType.STRING
     *
     * @param componentName
     * @param targetName
     * @param name -
     *            configuration name, must match the value of the field
     *            "configurationName" in the appConfig
     * @param appConfig -
     *            application configuration composite
     * @return management message string which gives the status of the
     *         operation. For target=cluster, instance specific details are
     *         included.
     * @throws ManagementRemoteException
     *             if there are errors encountered when updating the
     *             configuration.
     */
    public String setApplicationConfiguration(String componentName,
            String targetName, String name, CompositeData appConfig)
            throws ManagementRemoteException {
        ObjectName mbeanName = this.getConfigurationServiceMBeanObjectName();

        String data = "";

        Object[] params = new Object[4];
        params[0] = componentName;
        params[1] = targetName;
        params[2] = name;
        params[3] = appConfig;

        String[] signature = new String[4];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";
        signature[2] = "java.lang.String";
        signature[3] = "javax.management.openmbean.CompositeData";

        data = (String) this.invokeMBeanOperation(mbeanName,
                "setApplicationConfiguration", params, signature);

        return data;
    }

    /**
     * Get a Map of all application configurations for the component.
     *
     * @param componentName
     * @param targetName
     * @return a TabularData of all the application configurations for a
     *         component keyed by the configuration name.
     * @throws ManagementRemoteException
     *             if there are errors encountered when updating the
     *             configuration.
     */
    public TabularData getApplicationConfigurationsAsTabularData(
            String componentName, String targetName)
            throws ManagementRemoteException {
        ObjectName mbeanName = this.getConfigurationServiceMBeanObjectName();

        TabularData data = null;

        Object[] params = new Object[2];
        params[0] = componentName;
        params[1] = targetName;

        String[] signature = new String[2];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";

        data = (TabularData) this.invokeMBeanOperation(mbeanName,
                "getApplicationConfigurationsAsTabularData", params, signature);

        return data;
    }


    /**
     * Get the Application Variable set for a component.
     *
     * @param componentName
     * @param targetName
     * @return a TabularData which has all the application variables set on the
     *         component.
     * @throws ManagementRemoteException
     */
    public TabularData getApplicationVariablesAsTabularData(
            String componentName, String targetName)
            throws ManagementRemoteException {
        ObjectName mbeanName = this.getConfigurationServiceMBeanObjectName();

        TabularData data = null;

        Object[] params = new Object[2];
        params[0] = componentName;
        params[1] = targetName;

        String[] signature = new String[2];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";

        data = (TabularData) this.invokeMBeanOperation(mbeanName,
                "getApplicationVariablesAsTabularData", params, signature);

        return data;
    }

    /**
     * This operation adds a new application variable. If a variable already
     * exists with the same name as that specified then the operation fails.
     *
     * @param componentName
     * @param targetName
     * @param name -
     *            name of the application variable
     * @param appVar -
     *            this is the application variable compoiste
     * @return management message string which gives the status of the
     *         operation. For target=cluster, instance specific details are
     *         included.
     * @throws ManagementRemoteException
     *             if an error occurs in adding the application variables to the
     *         component.
     */
    public String addApplicationVariable(String componentName,
            String targetName, String name, CompositeData appVar)
            throws ManagementRemoteException {
        ObjectName mbeanName = this.getConfigurationServiceMBeanObjectName();

        String data = "";

        Object[] params = new Object[4];
        params[0] = componentName;
        params[1] = targetName;
        params[2] = name;
        params[3] = appVar;

        String[] signature = new String[4];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";
        signature[2] = "java.lang.String";
        signature[3] = "javax.management.openmbean.CompositeData";

        data = (String) this.invokeMBeanOperation(mbeanName,
                "addApplicationVariable", params, signature);

        return data;
    }

    /**
     * This operation sets an application variable. If a variable does not exist
     * with the same name, its an error.
     *
     * @param componentName
     * @param targetName
     * @param name -
     *            name of the application variable
     * @param appVar -
     *            this is the application variable compoiste to be updated.
     * @return management message string which gives the status of the
     *         operation. For target=cluster, instance specific details are
     *         included.
     * @throws ManagementRemoteException
     *             if one or more application variables cannot be deleted
     */
    public String setApplicationVariable(String componentName,
            String targetName, String name, CompositeData appVar)
            throws ManagementRemoteException {
        ObjectName mbeanName = this.getConfigurationServiceMBeanObjectName();

        String data = "";

        Object[] params = new Object[4];
        params[0] = componentName;
        params[1] = targetName;
        params[2] = name;
        params[3] = appVar;

        String[] signature = new String[4];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";
        signature[2] = "java.lang.String";
        signature[3] = "javax.management.openmbean.CompositeData";

        data = (String) this.invokeMBeanOperation(mbeanName,
                "setApplicationVariable", params, signature);

        return data;
    }

    /////////////////

    /**
     * This method returns a tabular data of a complex open data objects that
     * represent the runtime configuration parameter descriptor. The parameter
     * descriptor should contain the following data that represents the
     * parameter.
     *
     * name : name of the parameter value : value of the parameter as a String
     * type. type : type of the parameter. Basic data types only. description:
     * (optional) description of the parameter. displayName: (optional) display
     * name of the parameter readOnly : true/false validValues : (optional) list
     * of string values with ',' as delimiter. or a range value with - with a
     * '-' as delimiter. min and max strings will be converted to the parameter
     * type and then used to validate the value of the parameter.
     *
     * @param targetName
     *            cluster or instance name ( e.g. cluster1, instance1 ) on which
     *            configuration parameters will be set. null to represent the
     *            default instance which is admin server
     *
     * @return Map that represents the list of configuration parameter
     *         descriptors.
     *
     * @throws ManagementRemoteException
     *             if there is a jmx error accessing the instance
     */
    @SuppressWarnings("unchecked")
    public Map<String /*attributeName*/, Object /*attributeValue*/> getDefaultRuntimeConfigurationAsMap()
            throws ManagementRemoteException {
        ObjectName mbeanName = this.getConfigurationServiceMBeanObjectName();
        
        Map<String /* attributeName */, Object /* attributeValue */> data = null;
        
        Object[] params = null;
        
        String[] signature = null;
        
        data = (Map<String /* attributeName */, Object /* attributeValue */>) this
                .invokeMBeanOperation(mbeanName,
                        "getDefaultRuntimeConfigurationAsMap", params,
                        signature);
        
        return data;
    }

    /**
     * This method returns a tabular data of a complex open data objects that
     * represent the runtime configuration parameter descriptor. The parameter
     * descriptor should contain the following data that represents the
     * parameter.
     *
     * name : name of the parameter value : value of the parameter as a String
     * type. type : type of the parameter. Basic data types only. description:
     * (optional) description of the parameter. displayName: (optional) display
     * name of the parameter readOnly : true/false validValues : (optional) list
     * of string values with ',' as delimiter. or a range value with - with a
     * '-' as delimiter. min and max strings will be converted to the parameter
     * type and then used to validate the value of the parameter.
     *
     * @param targetName
     *            cluster or instance name ( e.g. cluster1, instance1 ) on which
     *            configuration parameters will be set. null to represent the
     *            default instance which is admin server
     *
     * @return Map that represents the list of configuration parameter
     *         descriptors.
     *
     * @throws ManagementRemoteException
     *             if there is a jmx error accessing the instance
     */
    @SuppressWarnings("unchecked")
    public Map<String /* attributeName */, Object /* attributeValue */> getRuntimeConfigurationAsMap(
            String targetName) throws ManagementRemoteException {
        ObjectName mbeanName = this.getConfigurationServiceMBeanObjectName();

        Map<String /* attributeName */, Object /* attributeValue */> data = null;
        
        Object[] params = new Object[1];
        params[0] = targetName;
        
        String[] signature = new String[1];
        signature[0] = "java.lang.String";
        
        data = (Map<String /* attributeName */, Object /* attributeValue */>) this
                .invokeMBeanOperation(mbeanName,
                        "getRuntimeConfigurationAsMap", params, signature);
        
        return data;
    }

   /**
     * This method sets one or more configuration parameters on the runtime with
     * a list of name/value pairs passed as a properties object. The property
     * name in the properties object should be an existing configuration
     * parameter name. If user try to set the parameter that is not in the
     * configuration parameters list, this method will throw an exception.
     *
     * The value of the property can be any object. If the value is non string
     * object, its string value (Object.toString()) will be used as a value that
     * will be set on the configuration.
     *
     * This method first validates whether all the paramters passed in
     * properties object exist in the runtime configuration or not. If any one
     * the parameters passed is not existing, it will return an error without
     * settings the parameters that are passed in the properties including a
     * valid parameters.
     *
     * If there is an error in setting a paramter, this method throws an
     * exception with the list of parameters that were not set.
     *
     * @param params
     *            Map object that contains name/value pairs corresponding to the
     *            configuration parameters to be set on the runtime.
     *
     * @param targetName
     *            cluster or instance name ( e.g. cluster1, instance1 ) on which
     *            configuration parameters will be set. null to represent the
     *            default instance which is admin server
     *
     * @return true if server restart is required, false if not
     *
     * @throws ManagementRemoteException
     *             if there is a jmx error or a invalid parameter is passed in
     *             the params properties object. In case of an error setting the
     *             a particular parameter, the error message should list the
     *             invalid parameters.
     */
    public boolean setRuntimeConfiguration(
            Map<String /* attributeName */, Object /* attributeValue */> parameters,
            String targetName) throws ManagementRemoteException {
        ObjectName mbeanName = this.getConfigurationServiceMBeanObjectName();

        Boolean data = null;

        Object[] params = new Object[2];
        params[0] = parameters;
        params[1] = targetName;
        
        String[] signature = new String[2];
        signature[0] = "java.util.Map";
        signature[1] = "java.lang.String";
        
        data = (Boolean) this.invokeMBeanOperation(mbeanName,
                "setRuntimeConfiguration", params, signature);
        
        return data;
    }

    /**
     * Retrieve component configuration
     *
     * @param componentName
     * @param targetName
     * @return the targetName as key and the name/value pairs as Map
     * @throws ManagementRemoteException
     *             on error
     */
    @SuppressWarnings("unchecked")
    public Map<String /* attributeName */, Object /* attributeValue */> getComponentConfigurationAsMap(
            String componentName, String targetName)
            throws ManagementRemoteException {
        ObjectName mbeanName = this.getConfigurationServiceMBeanObjectName();
        
        Map<String /* attributeName */, Object /* attributeValue */> data = null;
        
        Object[] params = new Object[2];
        params[0] = componentName;
        params[1] = targetName;
        
        String[] signature = new String[2];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";
        
        data = (Map<String /* attributeName */, Object /* attributeValue */>) this
                .invokeMBeanOperation(mbeanName,
                        "getComponentConfigurationAsMap", params, signature);

        return data;
    }

    /**
     * Will return jbi mgmt message with success, failure, or partial success
     * per instance. The entry per instance will have value as part of the
     * management message (XML) String.
     *
     * @param componentName
     * @param configurationValues
     * @param targetName
     * @return value as part of the management message (XML) String.
     * @throws ManagementRemoteException
     *             on error
     */
    public String setComponentConfiguration(
            String componentName,
            Map<String /* attributeName */, Object /* attributeValue */> configurationValues,
            String targetName) throws ManagementRemoteException {
        ObjectName mbeanName = this.getConfigurationServiceMBeanObjectName();

        String data = null;

        Object[] params = new Object[3];
        params[0] = componentName;
        params[1] = configurationValues;
        params[2] = targetName;

        String[] signature = new String[3];
        signature[0] = "java.lang.String";
        signature[1] = "java.util.Map";
        signature[2] = "java.lang.String";
        
        data = (String) this.invokeMBeanOperation(mbeanName,
                "setComponentConfiguration", params, signature);
        
        return data;
    }

    /////////////////

    /**
     * Invokes an operation on an Extension MBean.
     * @param componentName
     * @param extensionName the name of the extension (e.g., Configuration, Logger, etc.)
     * @param operationName The name of the operation to be invoked.
     * @param parameters An array containing the parameters to be set when the operation is invoked
     * @param signature An array containing the signature of the operation. The class objects will be loaded using the same class loader as the one used for loading the MBean on which the operation was invoked.
     * @param targetName name of the target (e.g., server, Cluster1, StandloneServer2, etc.)
     * @param targetInstanceName name of the target instance (e.g., Cluster1-Instance1, Cluster2-Instance10, etc.)
     * @return The object returned by the operation, which represents the result of invoking the operation on the Extension MBean specified.
     * @throws ManagementRemoteException
     */
    public Object invokeExtensionMBeanOperation(String componentName, String extensionName, String operationName, Object[] parameters, String[] signature, String targetName, String targetInstanceName) throws ManagementRemoteException {
        Object result = null;
        ObjectName mbeanName = this.getConfigurationServiceMBeanObjectName();
        
        Object[] params = new Object[7];
        params[0] = componentName;
        params[1] = extensionName;
        params[2] = operationName;
        params[3] = parameters;
        params[4] = signature;
        params[5] = targetName;
        params[6] = targetInstanceName;
        
        String[] signatureArray = new String[7];
        signatureArray[0] = "java.lang.String";
        signatureArray[1] = "java.lang.String";
        signatureArray[2] = "java.lang.String";
        signatureArray[3] = "[Ljava.lang.Object;";
        signatureArray[4] = "[Ljava.lang.String;";
        signatureArray[5] = "java.lang.String";
        signatureArray[6] = "java.lang.String";
        
        result = this.invokeMBeanOperation(mbeanName,
                "invokeExtensionMBeanOperation", params, signatureArray);
        return result;
    }
    
    
    /**
     * @param args
     */
    public static void main(String[] args) {

    }

}
