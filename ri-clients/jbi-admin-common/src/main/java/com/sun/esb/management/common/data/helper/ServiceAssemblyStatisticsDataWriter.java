/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ServiceAssemblyStatisticsDataWriter.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.esb.management.common.data.helper;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.io.Writer;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import com.sun.esb.management.common.data.ServiceAssemblyStatisticsData;
import com.sun.esb.management.common.data.ServiceUnitStatisticsData;

/**
 * @author graj
 *
 */
public class ServiceAssemblyStatisticsDataWriter implements
        ServiceAssemblyStatisticsDataXMLConstants, Serializable {
    static final long                       serialVersionUID = -1L;

    static final String       FILE_NAME_KEY    = "ServiceAssemblyStatisticsData.xml";
    
    /** Constructor - Creates an ServiceAssemblyStatisticsDataWriter */
    public ServiceAssemblyStatisticsDataWriter() {
    }
    
    /**
     * 
     * @param document
     * @param directoryPath
     * @throws TransformerConfigurationException
     * @throws TransformerException
     * @throws Exception
     */
    public static void writeToFile(Document document, String directoryPath)
            throws TransformerConfigurationException, TransformerException,
            Exception {
        File file = new File(directoryPath);
        if ((file.isDirectory() == false) || (file.exists() == false)) {
            throw new Exception("Directory Path: " + directoryPath
                    + " is invalid.");
        }
        String fileLocation = file.getAbsolutePath() + File.separator
                + FILE_NAME_KEY;
        System.out.println("Writing out to file: " + fileLocation);
        File outputFile = new File(fileLocation);
        // Use a Transformer for aspectOutput
        TransformerFactory tFactory = TransformerFactory.newInstance();
        Transformer transformer = tFactory.newTransformer();
        DOMSource source = new DOMSource(document);
        StreamResult result = new StreamResult(outputFile);
        
        // indent the Output to make it more legible...
        transformer.setOutputProperty(OutputKeys.METHOD, "xml");
        transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        transformer.setOutputProperty(OutputKeys.MEDIA_TYPE, "text/xml");
        transformer.setOutputProperty(OutputKeys.STANDALONE, "yes");
        transformer.setOutputProperty(
                "{http://xml.apache.org/xslt}indent-amount", "4");
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        
        transformer.transform(source, result);
    }
    
    /**
     * Change the contents of text file in its entirety, overwriting any
     * existing text. This style of implementation throws all exceptions to the
     * caller.
     * 
     * @param aFile
     *            is an existing file which can be written to.
     * @throws IllegalArgumentException
     *             if param does not comply.
     * @throws FileNotFoundException
     *             if the file does not exist.
     * @throws IOException
     *             if problem encountered during write.
     */
    public static void setContents(File aFile, String aContents)
            throws FileNotFoundException, IOException {
        if (aFile == null) {
            throw new IllegalArgumentException("File should not be null.");
        }
        if (!aFile.exists()) {
            aFile.createNewFile();
        }
        if (!aFile.isFile()) {
            throw new IllegalArgumentException("Should not be a directory: "
                    + aFile);
        }
        if (!aFile.canWrite()) {
            throw new IllegalArgumentException("File cannot be written: "
                    + aFile);
        }
        
        // declared here only to make visible to finally clause; generic
        // reference
        Writer output = null;
        try {
            // use buffering
            // FileWriter always assumes default encoding is OK!
            output = new BufferedWriter(new FileWriter(aFile));
            output.write(aContents);
        } finally {
            // flush and close both "aspectOutput" and its underlying FileWriter
            if (output != null) {
                output.close();
            }
        }
    }
    
    /**
     * 
     * @param ServiceAssemblyStatisticsData
     *            data 
     * @return XML string
     * @throws ParserConfigurationException
     * @throws TransformerException
     */
    public static String serialize(Map<String /* instanceName */, ServiceAssemblyStatisticsData> dataMap)
            throws ParserConfigurationException, TransformerException {
        Document document = null;
        ServiceAssemblyStatisticsDataWriter writer = new ServiceAssemblyStatisticsDataWriter();
        if (dataMap != null) {
            DocumentBuilderFactory factory = DocumentBuilderFactory
                    .newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            document = builder.newDocument(); // Create from whole cloth
            
            // ////////////////////////////////
            // <ServiceAssemblyStatisticsDataList>
            Element root = (Element) document
                    .createElement(ServiceAssemblyStatisticsDataXMLConstants.ASSEMBLY_STATISTICS_DATA_LIST_KEY);
            // xmlns="http://java.sun.com/xml/ns/esb/management/ServiceAssemblyStatisticsDataList"
            root.setAttribute(NAMESPACE_KEY, 
                    ServiceAssemblyStatisticsDataXMLConstants.ASSEMBLY_NAMESPACE_VALUE);
            // version = "1.0"
            root.setAttribute(VERSION_KEY, 
                    ServiceAssemblyStatisticsDataXMLConstants.ASSEMBLY_VERSION_VALUE);
            
            for (String instanceName : dataMap.keySet()) {
                ServiceAssemblyStatisticsData data = dataMap.get(instanceName);
                // ////////////////////////////////
                // <ServiceAssemblyStatisticsData>
                Element serviceAssemblyStatisticsDataElementChild = writer.createServiceAssemblyStatisticsDataElement(document, data);
                // </ServiceAssemblyStatisticsData>
                root.appendChild(serviceAssemblyStatisticsDataElementChild);
                // ////////////////////////////////
            }
            // ////////////////////////////////
            // </ServiceAssemblyStatisticsDataList>
            document.appendChild(root);
            // ////////////////////////////////
            
        }
        return writer.writeToString(document);
    }
    
    /**
     * 
     * @param document
     * @param data
     * @return
     */
    protected Element createServiceAssemblyStatisticsDataElement(Document document,
            ServiceAssemblyStatisticsData data) {
        Element serviceAssemblyStatisticsDataElement = null;
        if ((document != null) && (data != null)) {
            
            // <ServiceAssemblyStatisticsData>
            serviceAssemblyStatisticsDataElement = document.createElement(ServiceAssemblyStatisticsDataXMLConstants.ASSEMBLY_STATISTICS_DATA_KEY);
            
            // <InstanceName>
            Element instanceNameElementChild = document.createElement(ServiceAssemblyStatisticsDataXMLConstants.INSTANCE_NAME_KEY);
            if (instanceNameElementChild != null) {
                instanceNameElementChild.setTextContent(data.getInstanceName());
            }
            // </InstanceName>
            serviceAssemblyStatisticsDataElement.appendChild(instanceNameElementChild);
            
            // <ServiceAssemblyName>
            Element serviceAssemblyElementChild = document.createElement(ServiceAssemblyStatisticsDataXMLConstants.SERVICE_ASSEMBLY_NAME_KEY);
            if (serviceAssemblyElementChild != null) {
                serviceAssemblyElementChild.setTextContent(data.getName());
            }
            // </ServiceAssemblyName>
            serviceAssemblyStatisticsDataElement.appendChild(serviceAssemblyElementChild);

            // <ServiceAssemblyLastStartupTime>
            Element serviceAssemblyLastStartupElementChild = document.createElement(ServiceAssemblyStatisticsDataXMLConstants.SERVICE_ASSEMBLY_LAST_STARTUP_TIME_KEY);
            if (serviceAssemblyLastStartupElementChild != null) {
                serviceAssemblyLastStartupElementChild.setTextContent(data.getLastStartupTime()+"");
            }
            // </ServiceAssemblyLastStartupTime>
            serviceAssemblyStatisticsDataElement.appendChild(serviceAssemblyLastStartupElementChild);
            
            // <ServiceAssemblyStartupTimeAverage>
            Element serviceAssemblyStartupTimeAverageElementChild = document.createElement(ServiceAssemblyStatisticsDataXMLConstants.SERVICE_ASSEMBLY_STARTUP_TIME_AVERAGE_KEY);
            if (serviceAssemblyStartupTimeAverageElementChild != null) {
                serviceAssemblyStartupTimeAverageElementChild.setTextContent(data.getStartupTimeAverage()+"");
            }
            // </ServiceAssemblyStartupTimeAverage>
            serviceAssemblyStatisticsDataElement.appendChild(serviceAssemblyStartupTimeAverageElementChild);
            
            // <ServiceAssemblyShutdownTimeAverage>
            Element serviceAssemblyShutdownTimeAverageElementChild = document.createElement(ServiceAssemblyStatisticsDataXMLConstants.SERVICE_ASSEMBLY_SHUTDOWN_TIME_AVERAGE_KEY);
            if (serviceAssemblyShutdownTimeAverageElementChild != null) {
                serviceAssemblyShutdownTimeAverageElementChild.setTextContent(data.getShutdownTimeAverage()+"");
            }
            // </ServiceAssemblyShutdownTimeAverage>
            serviceAssemblyStatisticsDataElement.appendChild(serviceAssemblyShutdownTimeAverageElementChild);
            
            // <ServiceAssemblyStopTimeAverage>
            Element serviceAssemblyStopTimeAverageElementChild = document.createElement(ServiceAssemblyStatisticsDataXMLConstants.SERVICE_ASSEMBLY_STOP_TIME_AVERAGE_KEY);
            if (serviceAssemblyStopTimeAverageElementChild != null) {
                serviceAssemblyStopTimeAverageElementChild.setTextContent(data.getStopTimeAverage()+"");
            }
            // </ServiceAssemblyStopTimeAverage>
            serviceAssemblyStatisticsDataElement.appendChild(serviceAssemblyStopTimeAverageElementChild);
            
            // <ServiceAssemblyUpTime>
            Element serviceAssemblyUpTimeElementChild = document.createElement(ServiceAssemblyStatisticsDataXMLConstants.SERVICE_ASSEMBLY_UP_TIME_KEY);
            if (serviceAssemblyUpTimeElementChild != null) {
                serviceAssemblyUpTimeElementChild.setTextContent(data.getUpTime()+"");
            }
            // </ServiceAssemblyUpTime>
            serviceAssemblyStatisticsDataElement.appendChild(serviceAssemblyUpTimeElementChild);

            // ////////////////////////////////
            // <ServiceUnitStatisticsDataList>
            Element serviceUnitStatisticsDataListElementChild = this.createServiceUnitStatisticsDataListElement(document, data.getServiceUnitStatisticsList());
            // </ServiceAssemblyStatisticsData>
            serviceAssemblyStatisticsDataElement.appendChild(serviceUnitStatisticsDataListElementChild);
            // ////////////////////////////////
        }
        return serviceAssemblyStatisticsDataElement;
    }
    
    /**
     * 
     * @param document
     * @param list
     * @return
     */
    Element createServiceUnitStatisticsDataListElement(Document document, List<ServiceUnitStatisticsData> list) {
        Element serviceUnitStatisticsDataListElement = null;
        if ((document != null) && (list != null)) {
            // <ServiceUnitStatisticsDataList>
            serviceUnitStatisticsDataListElement = document.createElement(ServiceAssemblyStatisticsDataXMLConstants.UNIT_STATISTICS_DATA_LIST_KEY);
            // xmlns="http://java.sun.com/xml/ns/esb/management/ServiceUnitStatisticsDataList"
            serviceUnitStatisticsDataListElement.setAttribute(NAMESPACE_KEY, 
            ServiceAssemblyStatisticsDataXMLConstants.UNIT_NAMESPACE_VALUE);
            // version = "1.0"
            serviceUnitStatisticsDataListElement.setAttribute(VERSION_KEY, 
            ServiceAssemblyStatisticsDataXMLConstants.UNIT_VERSION_VALUE);
            
            for(ServiceUnitStatisticsData data : list) {
                // <ServiceUnitStatisticsData>
                Element serviceUnitDataElementChild = createServiceUnitStatisticsDataListElement(document, data);
                // </ServiceUnitStatisticsData>
                serviceUnitStatisticsDataListElement.appendChild(serviceUnitDataElementChild);
            }
        }
        return serviceUnitStatisticsDataListElement;
    }

    /**
     * 
     * @param document
     * @param data
     * @return
     */
    Element createServiceUnitStatisticsDataListElement(Document document, ServiceUnitStatisticsData data) {
        Element serviceUnitStatisticsDataElement = null;
        if ((document != null) && (data != null)) {
            // <ServiceUnitStatisticsData>
            serviceUnitStatisticsDataElement = document.createElement(ServiceAssemblyStatisticsDataXMLConstants.UNIT_STATISTICS_DATA_KEY);
            
            // <ServiceUnitName>
            Element serviceUnitElementChild = document.createElement(ServiceAssemblyStatisticsDataXMLConstants.SERVICE_UNIT_NAME_KEY);
            if (serviceUnitElementChild != null) {
                serviceUnitElementChild.setTextContent(data.getName());
            }
            // </ServiceAssemblyName>
            serviceUnitStatisticsDataElement.appendChild(serviceUnitElementChild);

            // <ServiceAssemblyStartupTimeAverage>
            Element serviceUnitStartupTimeAverageElementChild = document.createElement(ServiceAssemblyStatisticsDataXMLConstants.SERVICE_UNIT_STARTUP_TIME_AVERAGE_KEY);
            if (serviceUnitStartupTimeAverageElementChild != null) {
                serviceUnitStartupTimeAverageElementChild.setTextContent(data.getStartupTimeAverage()+"");
            }
            // </ServiceAssemblyStartupTimeAverage>
            serviceUnitStatisticsDataElement.appendChild(serviceUnitStartupTimeAverageElementChild);
            
            // <ServiceAssemblyShutdownTimeAverage>
            Element serviceUnitShutdownTimeAverageElementChild = document.createElement(ServiceAssemblyStatisticsDataXMLConstants.SERVICE_UNIT_SHUTDOWN_TIME_AVERAGE_KEY);
            if (serviceUnitShutdownTimeAverageElementChild != null) {
                serviceUnitShutdownTimeAverageElementChild.setTextContent(data.getShutdownTimeAverage()+"");
            }
            // </ServiceAssemblyShutdownTimeAverage>
            serviceUnitStatisticsDataElement.appendChild(serviceUnitShutdownTimeAverageElementChild);
            
            // <ServiceAssemblyStopTimeAverage>
            Element serviceUnitStopTimeAverageElementChild = document.createElement(ServiceAssemblyStatisticsDataXMLConstants.SERVICE_UNIT_STOP_TIME_AVERAGE_KEY);
            if (serviceUnitStopTimeAverageElementChild != null) {
                serviceUnitStopTimeAverageElementChild.setTextContent(data.getStopTimeAverage()+"");
            }
            // </ServiceAssemblyStopTimeAverage>
            serviceUnitStatisticsDataElement.appendChild(serviceUnitStopTimeAverageElementChild);
            
            // <EndpointNameList>
            Element serviceUnitEndpointListElement = document.createElement(ServiceAssemblyStatisticsDataXMLConstants.SERVICE_UNIT_ENDPOINT_NAME_LIST_KEY);
            
            for(String endpointName : data.getEndpointNameList()) {
                // <EndpointName>
                Element serviceUnitEndpointNameElement = document.createElement(ServiceAssemblyStatisticsDataXMLConstants.SERVICE_UNIT_ENDPOINT_NAME_KEY);
                if (serviceUnitEndpointNameElement != null) {
                    serviceUnitEndpointNameElement.setTextContent(endpointName);
                }
                // </EndpointName>
                serviceUnitEndpointListElement.appendChild(serviceUnitEndpointNameElement);
            }
            // </EndpointNameList>
            serviceUnitStatisticsDataElement.appendChild(serviceUnitEndpointListElement);
            
        }
        return serviceUnitStatisticsDataElement;
        
    }
    
    
    /**
     * @param document
     * @return
     * @throws TransformerException
     */
    protected String writeToString(Document document)
            throws TransformerException {
        // Use a Transformer for aspectOutput
        TransformerFactory tFactory = TransformerFactory.newInstance();
        Transformer transformer = tFactory.newTransformer();
        DOMSource source = new DOMSource(document);
        StringWriter writer = new StringWriter();
        StreamResult result = new StreamResult(writer);
        
        transformer.setOutputProperty(OutputKeys.METHOD, "xml");
        transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        transformer.setOutputProperty(OutputKeys.MEDIA_TYPE, "text/xml");
        transformer.setOutputProperty(OutputKeys.STANDALONE, "yes");
        
        // indent the aspectOutput to make it more legible...
        transformer.setOutputProperty(
                "{http://xml.apache.org/xslt}indent-amount", "4");
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.transform(source, result);
        
        return result.getWriter().toString();
    }    
    
    /**
     * @param args
     */
    public static void main(String[] args) {
        String uri = "C:/test/schema/assemblystatistics/ServiceAssemblyStatisticsData.xml";
        try {
            Map<String /* instanceName */, ServiceAssemblyStatisticsData> map = null;
            map = ServiceAssemblyStatisticsDataReader.parseFromFile(uri);
            for (String instanceName : map.keySet()) {
                System.out.println(map.get(instanceName).getDisplayString());
            }
            
            String content = ServiceAssemblyStatisticsDataWriter.serialize(map);
            System.out.println(content);
            ServiceAssemblyStatisticsDataWriter.setContents(new File(uri), content);
            
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }    }
    
}
