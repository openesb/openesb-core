/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ConsumingEndpointStatisticsDataCreator.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.esb.management.common.data.helper;

import java.util.ArrayList;
import java.util.Map;
import java.util.Set;

import javax.management.openmbean.CompositeData;
import javax.management.openmbean.CompositeDataSupport;
import javax.management.openmbean.CompositeType;
import javax.management.openmbean.OpenDataException;
import javax.management.openmbean.OpenType;
import javax.management.openmbean.SimpleType;
import javax.management.openmbean.TabularData;
import javax.management.openmbean.TabularDataSupport;
import javax.management.openmbean.TabularType;

import com.sun.esb.management.common.ManagementRemoteException;
import com.sun.esb.management.common.data.ConsumingEndpointStatisticsData;
import com.sun.esb.management.common.data.IEndpointStatisticsData;

/**
 * @author graj
 *
 */
public class ConsumingEndpointStatisticsDataCreator {
    /**  table index   */
    static String[] STATS_TABLE_INDEX = new String[] { "InstanceName" };
    
    
    /**
     * 
     */
    public ConsumingEndpointStatisticsDataCreator() {
        // TODO Auto-generated constructor stub
    }
    
    /**
     * 
     * @param data
     * @return
     * @throws ManagementRemoteException
     */
    public static TabularData createTabularData(Map<String /* instanceName */, IEndpointStatisticsData> map)
            throws ManagementRemoteException {
        Set<String> instanceNames = map.keySet();
        
        TabularType endpointStatsTableType = null;
        TabularData endpointsStatsTable = null;
        CompositeData[] endpointStats = new CompositeData[instanceNames.size()];
        int index = 0;
        for (String instanceName : instanceNames) {
            IEndpointStatisticsData value = map.get(instanceName);
            ConsumingEndpointStatisticsData data = (ConsumingEndpointStatisticsData) value;
            endpointStats[index++] = composeConsumingEndpointStats(data);
        }
        // the tabular type could be constructed only after we have the endpoint
        // stats composite type so we need atleast one entry
        try {
            if (index > 0 && endpointStats[0] != null) {
                endpointStatsTableType = new TabularType("EndpointStats",
                        "Endpoint Statistic Information", endpointStats[0]
                                .getCompositeType(), STATS_TABLE_INDEX);
                endpointsStatsTable = new TabularDataSupport(
                        endpointStatsTableType);
                
                for (int innerIndex = 0; innerIndex < index; innerIndex++) {
                    endpointsStatsTable.put(endpointStats[innerIndex]);
                }
            }
        } catch (OpenDataException e) {
            throw new ManagementRemoteException(e);
        }
        
        return endpointsStatsTable;
    }
    
    /**
     * This method is used to compose consuming endpoint stats
     * @param instanceName
     * @param commonValues values common to both consuming and consumer
     * @param consumingValues values specific to consuming
     * @param ojcStats table of performance data
     * @return CompositeData composite data
     *
     */
    protected static CompositeData composeConsumingEndpointStats(
            ConsumingEndpointStatisticsData data)
            throws ManagementRemoteException {
        
        TabularType ojcStatsType = null;
        if (data.getCategoryToPerformanceDataMap() != null) {
            //ojcStatsType = ojcStats.getTabularType();
        }
                    
        ArrayList<String> consumerItemNames = new ArrayList<String>();
        consumerItemNames.add("InstanceName");
        consumerItemNames.add("NumSentRequests");
        consumerItemNames.add("NumReceivedReplies");
        consumerItemNames.add("NumReceivedDONEs");
        consumerItemNames.add("NumSentDONEs");
        consumerItemNames.add("NumReceivedFaults");
        consumerItemNames.add("NumSentFaults");
        consumerItemNames.add("NumReceivedErrors");
        consumerItemNames.add("NumSentErrors");
        consumerItemNames.add("NumActiveExchanges");
        consumerItemNames.add("ComponentName");        
        consumerItemNames.add("ME-StatusTime-Avg");
        consumerItemNames.add("ME-ComponentTime-Avg");
        consumerItemNames.add("ME-DeliveryChannelTime-Avg");
        consumerItemNames.add("ME-MessageServiceTime-Avg");                         
        if ( ojcStatsType != null)
        {
            consumerItemNames.add("PerformanceMeasurements");
        }
            
        ArrayList<String> consumerItemDescriptions = new ArrayList<String>();
        consumerItemDescriptions.add("Instance Name");
        consumerItemDescriptions.add("Number of Sent Requests");
        consumerItemDescriptions.add("Number of Received Replies");
        consumerItemDescriptions.add("Number of Received DONEs");
        consumerItemDescriptions.add("Number of Sent DONEs");
        consumerItemDescriptions.add("Number of Received Faults");
        consumerItemDescriptions.add("Number of Sent Faults");
        consumerItemDescriptions.add("Number of Received Errors");
        consumerItemDescriptions.add("Number of Sent Errors");
        consumerItemDescriptions.add("Number of active exchanges");
        consumerItemDescriptions.add("Name of the owning component");
        consumerItemDescriptions.add("Message Exchange Status Time Avg in ns");
        consumerItemDescriptions.add("Message Exchange ComponentTime Avg in ns");
        consumerItemDescriptions.add("Message Exchange DeliveryChannelTime Avg in ns");
        consumerItemDescriptions.add("Message Exchange MessageServiceTime  Avg in ns");
        if ( ojcStatsType != null)
        {
            consumerItemDescriptions.add("Performance Measurements recorded by OJC Components");  
        };            
            
        ArrayList<OpenType> consumerItemTypes = new ArrayList<OpenType>();
        consumerItemTypes.add(SimpleType.STRING);
        consumerItemTypes.add(SimpleType.LONG);
        consumerItemTypes.add(SimpleType.LONG);
        consumerItemTypes.add(SimpleType.LONG);
        consumerItemTypes.add(SimpleType.LONG);
        consumerItemTypes.add(SimpleType.LONG);
        consumerItemTypes.add(SimpleType.LONG);
        consumerItemTypes.add(SimpleType.LONG);
        consumerItemTypes.add(SimpleType.LONG);    
        consumerItemTypes.add(SimpleType.LONG); 
        consumerItemTypes.add(SimpleType.STRING); 
        consumerItemTypes.add(SimpleType.LONG);
        consumerItemTypes.add(SimpleType.LONG);
        consumerItemTypes.add(SimpleType.LONG);
        consumerItemTypes.add(SimpleType.LONG);
        if ( ojcStatsType != null)
        {              
            consumerItemTypes.add(ojcStatsType);
        }
            
        ArrayList<Object> consumerItemValues = new ArrayList<Object>();
        consumerItemValues.add(data.getInstanceName());
        consumerItemValues.add(data.getNumberOfSentRequests());      
        consumerItemValues.add(data.getNumberOfReceivedReplies());  
        consumerItemValues.add(data.getNumberOfReceivedDones());         
        consumerItemValues.add(data.getNumberOfSentDones());
        consumerItemValues.add(data.getNumberOfReceivedFaults()); 
        consumerItemValues.add(data.getNumberOfSentFaults());
        consumerItemValues.add(data.getNumberOfReceivedErrors());
        consumerItemValues.add(data.getNumberOfSentErrors());
        consumerItemValues.add(data.getNumberOfActiveExchanges());    
        consumerItemValues.add(data.getComponentName());  
        consumerItemValues.add(data.getMessageExchangeStatusTimeAverage());
        consumerItemValues.add(data.getMessageExchangeComponentTimeAverage());     
        consumerItemValues.add(data.getMessageExchangeDeliveryChannelTimeAverage());       
        consumerItemValues.add(data.getMessageExchangeServiceTimeAverage());             

        if ( ojcStatsType != null)
        {        
            //consumerItemValues.add(ojcStats);
        }
        
        try
        {
            return 
                    new CompositeDataSupport(
                        new CompositeType(
                        "ConsumerEndpointStats",
                        "Consumer Endpoint Statistics",
                        (String[])consumerItemNames.toArray(new String[]{}),
                        (String[])consumerItemDescriptions.toArray(new String[]{}),
                        (SimpleType[])consumerItemTypes.toArray(new SimpleType[]{})),
                    (String[])consumerItemNames.toArray(new String[]{}),
                    (Object[])consumerItemValues.toArray(new Object[]{}));            
        }
        catch(OpenDataException ode)
        {
            throw new ManagementRemoteException(ode);
        }
       
    }
    
    /**
     * @param args
     */
    public static void main(String[] args) {
        // TODO Auto-generated method stub
        
    }
    
}
