/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)PerformanceDataXMLConstants.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.esb.management.common.data.helper;

import java.io.Serializable;

/**
 * XML constants required to parse or create Performance Measurement data
 *
 * @author graj
 */
public interface PerformanceDataXMLConstants extends Serializable {
    // XML TAGS
    public static final String VERSION_KEY                           = "version";

    public static final String VERSION_VALUE                         = "1.0";

    public static final String NAMESPACE_KEY                         = "xmlns";

    public static final String NAMESPACE_VALUE                       = "http://java.sun.com/xml/ns/esb/management/PerformanceMeasurementDataList";

    public static final String PERFORMANCE_MEASUREMENT_DATA_LIST_KEY = "PerformanceMeasurementDataList";

    public static final String PERFORMANCE_MEASUREMENT_DATA_KEY      = "PerformanceMeasurementData";

    public static final String CATEGORY_KEY                          = "category";

    public static final String ENDPOINT_KEY                          = "endpoint";

    public static final String SOURCE_CLASS_NAME_KEY                 = "sourceClassName";

    public static final String NUMBER_OF_MEASUREMENT_OBJECTS_KEY     = "numberOfMeasurementObjects";

    public static final String NUMBER_OF_MEASUREMENTS_KEY            = "numberOfMeasurements";

    public static final String AVERAGE_KEY                           = "average";

    public static final String AVERAGE_WITHOUT_FIRST_MEASUREMENT_KEY = "averageWithoutFirstMeasurement";

    public static final String FIRST_MEASUREMENT_TIME_KEY            = "firstMeasurementTime";

    public static final String LOAD_KEY                              = "load";

    public static final String MEDIAN_KEY                            = "median";

    public static final String THROUGHPUT_KEY                        = "throughput";

    public static final String TIME_TAKEN_KEY                        = "timeTaken";

    public static final String TOTAL_TIME_KEY                        = "totalTime";

}
