<!--
 DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 
 Copyright 1997-2008 Sun Microsystems, Inc. All rights reserved.
 
 The contents of this file are subject to the terms of either the GNU
 General Public License Version 2 only ("GPL") or the Common Development
 and Distribution License("CDDL") (collectively, the "License").  You
 may not use this file except in compliance with the License. You can obtain
 a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 language governing permissions and limitations under the License.
 
 When distributing the software, include this License Header Notice in each
 file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 Sun designates this particular file as subject to the "Classpath" exception
 as provided by Sun in the GPL Version 2 section of the License file that
 accompanied this code.  If applicable, add the following below the License
 Header, with the fields enclosed by brackets [] replaced by your own
 identifying information: "Portions Copyrighted [year]
 [name of copyright owner]"
 
 Contributor(s):
 
 If you wish your version of this file to be governed by only the CDDL or
 only the GPL Version 2, indicate your decision by adding "[Contributor]
 elects to include this software in this distribution under the [CDDL or GPL
 Version 2] license."  If you don't indicate a single choice of license, a
 recipient has the option to distribute your version of this file under
 either the CDDL, the GPL Version 2 or to extend the choice of license to
 its licensees as provided above.  However, if you add GPL Version 2 code
 and therefore, elected the GPL Version 2 license, then the option applies
 only if the new code is made subject to such option by the copyright
 holder.
-->
<!-- jbi/pe/showServiceUnit.jsf -->

<sun:page>
    <!beforeCreate
setResourceBundle(key="help" bundle="com.sun.enterprise.tools.admingui.resources.Helplinks")
setResourceBundle(key="i18n" bundle="com.sun.jbi.jsf.resources.Bundle")
setResourceBundle(key="i18n2" bundle="com.sun.enterprise.tools.admingui.resources.Strings")
getRequestValue(key="name", value=>$attribute{name});
getRequestValue(key="type", value=>$attribute{type});
getRequestValue(key="saname", value=>$attribute{saname});
getRequestValue(key="compType", value=>$attribute{compType});
getRequestValue(key="compName", value=>$attribute{compName});
getRequestValue(key="qs", value=>$attribute{qs});

//setSessionAttribute(key="show_serviceUnitName", value="$requestParameter{name}")
setSessionAttribute(key="show_serviceUnitName", value="$attribute{name}")
//setSessionAttribute(key="show_serviceAssemblyName", value="$requestParameter{saname}")
setSessionAttribute(key="show_serviceAssemblyName", value="$attribute{saname}")
//setSessionAttribute(key="show_componentType", value="$requestParameter{compType}")
setSessionAttribute(key="show_componentType", value="$attribute{compType}")
//setSessionAttribute(key="show_targetComponent", value="$requestParameter{compName}")
setSessionAttribute(key="show_targetComponent", value="$attribute{compName}")
//setSessionAttribute(key="show_type", value="$requestParameter{type}")
setSessionAttribute(key="show_type", value="$attribute{type}")
//setSessionAttribute(key="show_qs", value="$requestParameter{qs}")
setSessionAttribute(key="show_qs", value="$attribute{qs}")

jbiSetServiceUnitInfo (serviceUnitName="$session{show_serviceUnitName}", 
                       serviceAssemblyName="$session{show_serviceAssemblyName}",
                       componentType="$session{show_componentType}",
                       targetComponent="$session{show_targetComponent}")
      
<!-- This will cause the general tab to be selected first -->             
setSessionAttribute(key="showServiceUnitTabs", value="general")

    />
    
    <sun:html>
        <sun:head id="jbiShowServiceUnitHead">
            "<script language="JavaScript" src="../../js/adminjsf.js"></script>
	 </sun:head>
	 <sun:body>
	     <sun:form id="jbiShowServiceUnitBreadcrumbsForm"> 
		<sun:hidden id="helpKey" value="$resource{help.jbi.common.showServiceUnit}" />

#include treeBreadcrumbs.inc

                </sun:form>

             <sun:form id="tabsForm">
#include "jbi/pe/inc/showServiceUnitTabs.inc"
             </sun:form>

#include "jbi/pe/inc/alertBox.inc"
            <sun:form id="jbiShowServiceUnitPropertiesForm" >
                <sun:title id="propertyContentPageTitle"
                    title="$attribute{name} - $resource{i18n.jbi.show.service.unit.page.title.suffix.text}"
                    helpText="$resource{i18n.jbi.service.unit.show.page.help.inline.text}"
                    />

 <sun:propertySheet id="showServiceUnitPropertySheet">			 
 
    <sun:propertySheetSection id="showServiceUnitInfoPss" 
        label="$resource{i18n.jbi.show.service.unit.info.pss.label.text}">

        <sun:property id="showNameProperty"  
     	    overlapLabel="#{false}" 
            label="$resource{i18n.jbi.show.service.unit.name.property.label}" 
            labelAlign="left" 
            noWrap="#{true}" 
            >

            <sun:staticText id="showNameText" 
                text="#{ServiceUnitBean.name}"
                  />

        </sun:property>

        <sun:property id="showServiceUnitDescriptionProperty"  
            label="$resource{i18n.jbi.show.service.unit.description.property.label}"
            labelAlign="left" 
            noWrap="#{true}" 
            overlapLabel="#{false}" 
            >  

            <sun:staticText id="showServiceUnitDescriptionText"  
                text="#{ServiceUnitBean.description}"
                  /> 

        </sun:property>

        <sun:property id="showServiceUnitStateProperty"  
            label="$resource{i18n.jbi.show.service.unit.state.property.label}"
            labelAlign="left" 
            noWrap="#{true}" 
            overlapLabel="#{false}" 
            >  

            <sun:staticText id="showServiceUnitStateText"  
                text="#{ServiceUnitBean.state}"
                  /> 

        </sun:property>

     </sun:propertySheetSection>

    <sun:propertySheetSection id="showServiceUnitLinksPss"
        label="$resource{i18n.jbi.show.service.unit.links.pss.label.text}">

        <sun:property id="showServiceUnitDeploymentProperty"  
            label="$resource{i18n.jbi.show.service.unit.deployment.property.label}"
            labelAlign="left" 
            noWrap="#{true}" 
            overlapLabel="#{false}"
            >                           
            <sun:hyperlink id="deploymentLink"
                toolTip="$resource{i18n.jbi.show.service.unit.deployment.link.tooltip.text}" 
                url="showDeployment.jsf?type=service-assembly&name=#{ServiceUnitBean.serviceAssemblyName}"
                >
                <sun:image id="saImage"
                    url="/resource/images/jbi/JBIServiceAssembly.gif" />
                    
                <sun:staticText id="saText"
                    text="#{ServiceUnitBean.serviceAssemblyName}" />
            </sun:hyperlink>


        </sun:property> 

        <sun:property id="showServiceUnitComponentProperty"  
            label="$resource{i18n.jbi.show.service.unit.component.property.label}"
            labelAlign="left" 
            noWrap="#{true}" 
            overlapLabel="#{false}"
            >                           
            <sun:hyperlink id="deploymentLink"
                toolTip="$resource{i18n.jbi.show.service.unit.component.link.tooltip.text}" 
                url="showBindingOrEngine.jsf?type=#{ServiceUnitBean.componentType}&name=#{ServiceUnitBean.targetComponent}"
                >	
                <sun:image id="iconImage"
                    url="/resource/images/jbi/#{ServiceUnitBean.componentTypeIcon}" />

                <sun:staticText id="bcOrSeText"
                    text="#{ServiceUnitBean.targetComponent}" />
            </sun:hyperlink>

        </sun:property> 

     </sun:propertySheetSection>

 </sun:propertySheet>    



            </sun:form>
        </sun:body>
     
#include "changeButtonsJS.inc"

    </sun:html>
</sun:page>
