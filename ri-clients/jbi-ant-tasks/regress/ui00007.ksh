#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)ui00007.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

#regress setup
. ./regress_defs.ksh

JBI_ANT="$JBI_ANT -Djbi.task.fail.on.error=false"
echo $JBI_ANT

#
# test components
#

test_install_deploy_option_errors()
{

### install components/slibs to doamin #########################################
$JBI_ANT install-shared-library
$JBI_ANT install-component

### deploy service assembly  to doamin #########################################
$JBI_ANT deploy-service-assembly

### error check for both file and name
$JBI_ANT -Djbi.install.file=$UI_REGRESS_DIST_DIR/ant-test-sns1.jar -Djbi.shared.library.name="ant_test_sns1" install-shared-library
$JBI_ANT -Djbi.install.file=$UI_REGRESS_DIST_DIR/ant-test-binding1.jar -Djbi.component.name="ant_test_binding1" install-component

$JBI_ANT -Djbi.deploy.file=$UI_REGRESS_DIST_DIR/ant-test-au1.zip -Djbi.service.assembly.name="ant_test_assembly_unit_1" deploy-service-assembly
}


run_test()
{
# build_test_artifacts
test_install_deploy_option_errors
}

################## MAIN ##################
####
# Execute the test
####

#this is to correct for differences in ant behavior from version 1.5->1.6.  RT 6/18/05
run_test | tr -d '\r' | sed -e '/^$/d'

exit 0
