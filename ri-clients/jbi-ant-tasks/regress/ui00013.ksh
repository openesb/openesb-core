#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)ui00012.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

#regress setup
. ./regress_defs.ksh

#
# test component configuration
#


COMPONENT_ARCHIVE=${JV_SVC_BLD}/regress/dist/ant-config-binding.jar
COMPONENT_NAME=ant-config-binding
HTTP_COMPONENT_NAME=sun-http-binding

MY_JBI_ANT="asant -emacs -q -f $JV_SVC_REGRESS/ui00013.xml -Djbi.username=$AS_ADMIN_USER -Djbi.password=$AS_ADMIN_PASSWD -Djbi.port=$ASADMIN_PORT $JBI_ANT_ARGS "

run_test()
{

# Package and install a test binding component
ant -q -emacs -propertyfile "$JV_JBI_DOMAIN_PROPS" -lib "$REGRESS_CLASSPATH" -f ui00013.xml pkg.test.component

# Test against cluster
TARGET=
run_against_server

# Test against cluster
TARGET=ant_cluster
run_against_target

# Test against standalone instance
TARGET=instance13
run_against_target

# Test against cluster instance
# TARGET=instance11
# run_against_target

}

run_against_server()
{

$JBI_ANT -Djbi.install.file=$COMPONENT_ARCHIVE  install-component
$JBI_ANT -Djbi.component.name=$COMPONENT_NAME start-component

$JBI_ANT -Djbi.task.fail.on.error=true -Djbi.component.name=$COMPONENT_NAME list-application-variables
$JBI_ANT -Djbi.task.fail.on.error=true -Djbi.component.name=$COMPONENT_NAME -Djbi.app.variables.file=$JV_SVC_REGRESS/appvariables.properties create-application-variable
$JBI_ANT -Djbi.task.fail.on.error=true -Djbi.component.name=$COMPONENT_NAME -Djbi.show.full.length=true list-application-variables
$JBI_ANT -Djbi.task.fail.on.error=true -Djbi.component.name=$COMPONENT_NAME -Djbi.app.variables.file=$JV_SVC_REGRESS/setappvariables.properties update-application-variable
$JBI_ANT -Djbi.task.fail.on.error=true -Djbi.component.name=$COMPONENT_NAME list-application-variables
$JBI_ANT -Djbi.task.fail.on.error=true -Djbi.component.name=$COMPONENT_NAME -Djbi.app.variable.name="HostName Port" delete-application-variable
$JBI_ANT -Djbi.task.fail.on.error=true -Djbi.component.name=$COMPONENT_NAME list-application-variables

$JBI_ANT -Djbi.task.fail.on.error=true -Djbi.component.name=$COMPONENT_NAME list-application-configurations
$JBI_ANT -Djbi.task.fail.on.error=true -Djbi.component.name=$COMPONENT_NAME -Djbi.app.config.params.file=$JV_SVC_REGRESS/appconfig.properties  -Djbi.app.config.name=testConfig create-application-configuration
$JBI_ANT -Djbi.task.fail.on.error=true -Djbi.component.name=$COMPONENT_NAME list-application-configurations
$JBI_ANT -Djbi.task.fail.on.error=true -Djbi.component.name=$COMPONENT_NAME -Djbi.app.config.params.file=$JV_SVC_REGRESS/updated-appconfig.properties  -Djbi.app.config.name=testConfig update-application-configuration
$JBI_ANT -Djbi.task.fail.on.error=true -Djbi.component.name=$COMPONENT_NAME list-application-configurations
$JBI_ANT -Djbi.task.fail.on.error=true -Djbi.component.name=$COMPONENT_NAME -Djbi.app.config.name=testConfig delete-application-configuration
$JBI_ANT -Djbi.task.fail.on.error=true -Djbi.component.name=$COMPONENT_NAME list-application-configurations

$JBI_ANT -Djbi.component.name=$COMPONENT_NAME shut-down-component
$JBI_ANT -Djbi.component.name=$COMPONENT_NAME uninstall-component

# Tests for sun-http-binding
$JBI_ANT -Djbi.component.name=$HTTP_COMPONENT_NAME start-component

$MY_JBI_ANT -Djbi.component.name=$HTTP_COMPONENT_NAME create-application-configuration
$MY_JBI_ANT -Djbi.component.name=$HTTP_COMPONENT_NAME list-application-configurations
$MY_JBI_ANT -Djbi.component.name=$HTTP_COMPONENT_NAME update-application-configuration
$MY_JBI_ANT -Djbi.component.name=$HTTP_COMPONENT_NAME list-application-configurations
$MY_JBI_ANT -Djbi.component.name=$HTTP_COMPONENT_NAME delete-application-configuration

$JBI_ANT -Djbi.component.name=$HTTP_COMPONENT_NAME shut-down-component
}

run_against_target()
{

$JBI_ANT -Djbi.install.file=$COMPONENT_ARCHIVE  -Djbi.target=$TARGET install-component
$JBI_ANT -Djbi.component.name=$COMPONENT_NAME -Djbi.target=$TARGET start-component

$JBI_ANT -Djbi.task.fail.on.error=true -Djbi.component.name=$COMPONENT_NAME -Djbi.target=$TARGET list-application-variables
$JBI_ANT -Djbi.task.fail.on.error=true -Djbi.component.name=$COMPONENT_NAME -Djbi.app.variables.file=$JV_SVC_REGRESS/appvariables.properties -Djbi.target=$TARGET create-application-variable
$JBI_ANT -Djbi.task.fail.on.error=true -Djbi.component.name=$COMPONENT_NAME -Djbi.target=$TARGET -Djbi.show.full.length=true list-application-variables
$JBI_ANT -Djbi.task.fail.on.error=true -Djbi.component.name=$COMPONENT_NAME -Djbi.app.variables.file=$JV_SVC_REGRESS/setappvariables.properties -Djbi.target=$TARGET update-application-variable
$JBI_ANT -Djbi.task.fail.on.error=true -Djbi.component.name=$COMPONENT_NAME -Djbi.target=$TARGET list-application-variables
$JBI_ANT -Djbi.task.fail.on.error=true -Djbi.component.name=$COMPONENT_NAME -Djbi.app.variable.name=HostName -Djbi.target=$TARGET delete-application-variable
$JBI_ANT -Djbi.task.fail.on.error=true -Djbi.component.name=$COMPONENT_NAME -Djbi.target=$TARGET list-application-variables

$JBI_ANT -Djbi.task.fail.on.error=true -Djbi.component.name=$COMPONENT_NAME -Djbi.target=$TARGET list-application-configurations
$JBI_ANT -Djbi.task.fail.on.error=true -Djbi.component.name=$COMPONENT_NAME -Djbi.app.config.params.file=$JV_SVC_REGRESS/appconfig.properties  -Djbi.app.config.name=testConfig -Djbi.target=$TARGET create-application-configuration
$JBI_ANT -Djbi.task.fail.on.error=true -Djbi.component.name=$COMPONENT_NAME -Djbi.target=$TARGET list-application-configurations
$JBI_ANT -Djbi.task.fail.on.error=true -Djbi.component.name=$COMPONENT_NAME -Djbi.app.config.params.file=$JV_SVC_REGRESS/updated-appconfig.properties  -Djbi.app.config.name=testConfig -Djbi.target=$TARGET update-application-configuration
$JBI_ANT -Djbi.task.fail.on.error=true -Djbi.component.name=$COMPONENT_NAME -Djbi.target=$TARGET list-application-configurations
$JBI_ANT -Djbi.task.fail.on.error=true -Djbi.component.name=$COMPONENT_NAME -Djbi.app.config.name=testConfig -Djbi.target=$TARGET delete-application-configuration
$JBI_ANT -Djbi.task.fail.on.error=true -Djbi.component.name=$COMPONENT_NAME -Djbi.target=$TARGET list-application-configurations

$JBI_ANT -Djbi.component.name=$COMPONENT_NAME -Djbi.target=$TARGET shut-down-component
$JBI_ANT -Djbi.component.name=$COMPONENT_NAME -Djbi.target=$TARGET uninstall-component

# Tests for sun-http-binding
$JBI_ANT -Djbi.component.name=$HTTP_COMPONENT_NAME -Djbi.target=$TARGET start-component

$MY_JBI_ANT -Djbi.component.name=$HTTP_COMPONENT_NAME -Djbi.target=$TARGET create-application-configuration
$MY_JBI_ANT -Djbi.component.name=$HTTP_COMPONENT_NAME -Djbi.target=$TARGET list-application-configurations
$MY_JBI_ANT -Djbi.component.name=$HTTP_COMPONENT_NAME -Djbi.target=$TARGET update-application-configuration
$MY_JBI_ANT -Djbi.component.name=$HTTP_COMPONENT_NAME -Djbi.target=$TARGET list-application-configurations
$MY_JBI_ANT -Djbi.component.name=$HTTP_COMPONENT_NAME -Djbi.target=$TARGET delete-application-configuration

$JBI_ANT -Djbi.component.name=$HTTP_COMPONENT_NAME -Djbi.target=$TARGET shut-down-component

}

################## MAIN ##################
####
# Execute the test
####

#this is to correct for differences in ant behavior from version 1.5->1.6.  RT 6/18/05
run_test

exit 0
