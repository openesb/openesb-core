/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)AppVariable.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.ui.ant;

import java.lang.Comparable;

public class AppVariable implements Comparable
{
    /**
     * Holds value of property name.
     */
    private String mName;
    private String mType;
    private String mValue;
    
    /**
     * default public constructor
     */
    public AppVariable()
    {
	mName = "";
	mType = "";
	mValue = "";
    }

    /**
     * public constructor
     */
    public AppVariable(String theName, String theType, String theValue)
    {
	mName = theName;
	mType = theType;
	mValue = theValue;
    }
    
    /**
     * Getter for property name.
     * @return Value of property name.
     */
    public String getName()
    {
	return mName;
    }
    
    /**
     * Setter for property name.
     * @param key New value of property name.
     */
    public void setName(String name)
    {
	mName = name;
    }

    /**
     * Getter for property type.
     * @return Value of property type.
     */
    public String getType()
    {
	return mType;
    }

    /**
     * Setter for property type.
     * @param type New value of property type.
     */
    public void setType(String type)
    {
	mType = type;
    }

    /**
     * Getter for property value.
     * @return Value of property value.
     */
    public String getValue()
    {
	return mValue;
    }

    /**
     * Setter for property value.
     * @param value New value of property value.
     */
    public void setValue(String value)
    {
	mValue = value;
    }

   public int compareTo(Object T)
        throws ClassCastException
   {
        return (""+mName).compareTo(((AppVariable) T).getName());
   }
}
