/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JbiCreateApplicationVariablesTask.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.ui.ant;

import com.sun.jbi.ui.common.JBIManagementMessage;
import com.sun.jbi.ui.common.JBIResultXmlBuilder;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.Properties;
import java.util.TreeSet;
import java.util.SortedSet;
import org.apache.tools.ant.BuildException;

/** This class is an ant task for updating service engine or binding component.
 *
 * @author Sun Microsystems, Inc.
 */
public class JbiCreateApplicationVariablesTask extends JbiTargetTask
{
    /**
     * appvariable success msg key
     */
    private static final String APPVARIABLE_SUCCESS_STATUS_KEY =
				"jbi.ui.ant.add.appvariable.successful";

    /**
     * appvariable failure msg key
     */
    private static final String APPVARIABLE_FAILED_STATUS_KEY =
				"jbi.ui.ant.add.appvariable.failed";
   
    /**
     * appvariable success msg key
     */
    private static final String APPVARIABLE_PARTIAL_SUCCESS_STATUS_KEY =
				"jbi.ui.ant.add.appvariable.partial.success";
    

    /** Holds Param Nested elements */
    private List mAppVariableList;

    /** Holds Params File for AppVariables **/
    private String mAppVariablesFile = null;

    /** Holds value of property componentName. */
    private String mComponentName = null;
    
    /**
     * Getter for property componentName.
     * @return Value of property componentName.
     */
    public String getComponentName()
    {
        return this.mComponentName;
    }
    
    /**
     * Setter for property componentName.
     * @param componentName name of the component.
     */
    public void setComponentName(String componentName)
    {
        this.mComponentName = componentName;
    }
    
    /** Getter for property appvariables.
     * @return Value of property appvariables.
     *
     */
    public String getAppVariables()
    {
        return this.mAppVariablesFile;
    }
    
    /**
     * @param appVariablesFile path to set
     */
    
    public void setAppVariables(String appVariablesFile)
    {
        this.mAppVariablesFile = appVariablesFile;
    }
    
    private void debugPrintParams(Properties params) {
        if( params == null ) {
            this.logDebug("Set Configuration params are NULL");
            return;
        }
        StringWriter stringWriter = new StringWriter();
        PrintWriter out = new PrintWriter(stringWriter);
        params.list(out);
        out.close();
        this.logDebug(stringWriter.getBuffer().toString());
    }
    
    private String createFormatedSuccessJbiResultMessage(String i18nKey, Object[] args) {
        
        String msgCode = getI18NBundle().getMessage(i18nKey + ".ID");
        String msg = getI18NBundle().getMessage(i18nKey, args);
        
        String jbiResultXml =
            JBIResultXmlBuilder.getInstance()
            .createJbiResultXml("JBI_ANT_TASK_SET_CONFIG",
            JBIResultXmlBuilder.SUCCESS_RESULT,
            JBIResultXmlBuilder.INFO_MSG_TYPE,
            msgCode, msg, args, null);
        
        JBIManagementMessage mgmtMsg = null;
        mgmtMsg = JBIManagementMessage.createJBIManagementMessage(jbiResultXml);
        return (mgmtMsg != null) ? mgmtMsg.getMessage() : msg ;
    }
    
    private void executeCreateApplicationVariables(List appVariableList,
						String compName, String target)
        throws Exception
    {
        // Go throught the appvariable elements
        this.logDebug("Set application variables, component name: " +
							compName +
							" target: " +
							target);
	Properties varsProps = new Properties();

	Iterator itr = appVariableList.iterator();
	while (itr.hasNext())
	{
	    AppVariable appVar = (AppVariable) itr.next();
            String appName = (""+appVar.getName()).trim();
            if (appName.compareTo("") != 0)
            {
                String valueString = null;
                if ((appVar.getType() != null) && (appVar.getType().compareTo("") != 0))
                {
                    valueString = "[" + appVar.getType() + "]" + appVar.getValue();
		    varsProps.setProperty(appVar.getName(), valueString);
                }
                else
                {
                    valueString = "[STRING]" + appVar.getValue();
		    varsProps.setProperty(appVar.getName(), valueString);
                }
            }
	}

        String appVariablesFile = this.getAppVariables();
        if ((appVariablesFile != null) && (appVariablesFile.compareTo("") != 0))
        {
	    this.logDebug("the params file is " + appVariablesFile);

            Properties theProp = this.loadParamsFromFile(new File(appVariablesFile));
            // Check for the default type
            SortedSet keys = new TreeSet(theProp.keySet());
            for(Object key : keys) {
                String name = (String)key;
                String value = theProp.getProperty(name, "");
                if ((value+"").indexOf("[") < (value+"").indexOf("]"))
                {
                    // Do nothing
                }
                else
                {
                    value = "[STRING]" + value;
                    theProp.setProperty(name, value);
                }
            }

            varsProps.putAll(theProp);
        }

        if (varsProps.size() == 0)
        {
            String msg =
                createFailedFormattedJbiAdminResult(
                    "jbi.ui.ant.list.no.input.appvariable.data.found",
                    null);
            throw new BuildException(msg,getLocation());
        }

	debugPrintParams(varsProps);
        this.logDebug("Before calling common client addApplicationVariables method, component name: " +
							compName +
							" target: " + target +
                                                        " varsProps: " + varsProps);
        String rtnXml = this.getJBIAdminCommands().addApplicationVariables(compName,
                                        target,
                                        varsProps);

        JBIManagementMessage mgmtMsg = JBIManagementMessage.createJBIManagementMessage(rtnXml);
        if ( mgmtMsg.isFailedMsg() )
        {
            throw new Exception(rtnXml);
        }
        else
        {
            // print success message
            printTaskSuccess(mgmtMsg);
        }
    }

    /** executes the install task. Ant Task framework calls this method to
     * excute the task.
     * @throws BuildException if error or exception occurs.
     */
    public void executeTask() throws BuildException
    {        
	try
	{
	    String	compName	= getComponentName();
	    String	target		= getValidTarget();
            List	appVariableList = this.getAppVariableList();

	    if ((compName == null) || (compName.compareTo("") == 0))
	    {
		String errMsg = createFailedFormattedJbiAdminResult(
						"jbi.ui.ant.task.error.nullCompName",
						null);
		throw new BuildException(errMsg);
	    }

            this.logDebug("Executing create application variables Task....");
            executeCreateApplicationVariables(appVariableList, compName, target);
	}
	catch (Exception ex )
        {
            processTaskException(ex);
        }
    }
    
    /**
     * returns i18n key. tasks implement this method.
     * @return i18n key for the success status
     */
    protected String getTaskFailedStatusI18NKey()
    {
	return APPVARIABLE_FAILED_STATUS_KEY;
    }

    /**
     * returns i18n key. tasks implement this method.
     * @return i18n key for the failed status
     */
    protected String getTaskSuccessStatusI18NKey()
    {
        return APPVARIABLE_SUCCESS_STATUS_KEY;
    }

    /**
     * return i18n key for the partial success
     * @return i18n key for the partial success
     */
    protected String getTaskPartialSuccessStatusI18NKey() 
    {
        return APPVARIABLE_PARTIAL_SUCCESS_STATUS_KEY;
    }    

    /**
     * returns nested element list of &lt;appvariable>
     * @return AppVariable List
     */
    protected List getAppVariableList()
    {
        if ( this.mAppVariableList == null )
        {
            this.mAppVariableList = new ArrayList();
        }
        return this.mAppVariableList;
    }

    /**
     * load properties from a file
     * @return the Loaded properties
     * @param file file to load
     * @throws BuildException on error
     */
    protected Properties loadParamsFromFile(File file)
	throws BuildException
    {
        String absFilePath = null;
        String fileName = null;
        if ( file != null )
        {
            absFilePath = file.getAbsolutePath();
            fileName = file.getName();
        }
        if ( file == null || !file.exists() )
        {
            String msg =
                createFailedFormattedJbiAdminResult(
			"jbi.ui.ant.task.error.config.params.file.not.exist",
                	new Object[] {fileName});
            throw new BuildException(msg,getLocation());
        }
        
        if ( file.isDirectory() )
        {
            String msg =
                createFailedFormattedJbiAdminResult(
			"jbi.ui.ant.task.error.config.params.file.is.directory",
			null);
            throw new BuildException(msg,getLocation());
        }
        
        Properties props = new Properties();
        this.logDebug("Loading " + file.getAbsolutePath());
        try
        {
            FileInputStream fis = new FileInputStream(file);
            try
            {
                props.load(fis);
            }
            finally
            {
                if (fis != null)
                {
                    fis.close();
                }
            }
            return props;
        }
        catch (IOException ex)
        {
            throw new BuildException(ex, getLocation());
        }
    }

    /**
     * factory method for creating the nested element &lt;AppVariable>
     * @return AppVariable Object
     */
    public AppVariable createAppVariable()
    {
	AppVariable appVar = new AppVariable();
        this.getAppVariableList().add(appVar);
	return appVar;
    }
}
