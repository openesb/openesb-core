/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JbiUninstallComponentTask.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.ui.ant;

import org.apache.tools.ant.BuildException;


/** This class is an ant task for uninstalling a service engine or binding component
 *
 * @author Sun Microsystems, Inc.
 */
public class JbiUninstallComponentTask extends JbiTargetTask
{
    /**
     * success msg key
     */
    private static final String SUCCESS_STATUS_KEY = "jbi.ui.ant.uninstall.successful";
    /**
     * failure msg key
     */
    private static final String FAILED_STATUS_KEY = "jbi.ui.ant.uninstall.failed";

    /**
     * Holds value of property KeepArchive.
     */
    private boolean mKeepArchive = false; 
    
    /** Holds value of property componentName. */
    private String mComponentName;
    
    /**
     * Holds value of property force.
     */
    private boolean mForce = false; 
    
    /**
     * Getter for property forced.
     * @return Value of property forced.
     */
    public boolean isForce()
    {

        return this.mForce;
    }

    /**
     * Setter for property forced.
     * @param force New value of property forced.
     */
    public void setForce(boolean force)
    {

        this.mForce = force;
    }
    
    /** Getter for property componentName.
     * @return Value of property componentName.
     *
     */
    public String getName()
    {
        return this.mComponentName;
    }
    
    /**
     * Setter for property componentName.
     * @param name component name.
     */
    public void setName(String name)
    {
        this.mComponentName = name;
    }
    
    /**
     * Getter for property KeepArchive.
     * @return Value of property KeepArchive.
     */
    public boolean isKeepArchive()
    {

        return this.mKeepArchive;
    }

    /**
     * Setter for property KeepArchive.
     * @param keepArchive New value of property KeepArchive.
     */
    public void setKeepArchive(boolean keepArchive)
    {

        this.mKeepArchive = keepArchive;
    }
    
    /** executes the uninstall component task. Ant Task framework calls this method to
     * excute the task.
     * @throws BuildException if error or exception occurs.
     */
    public void executeTask() throws BuildException
    {
        String compName = getValidComponentName(getName());
        String target = getValidTarget();
        boolean force = isForce();
        
        boolean keepArchive = isKeepArchive();
        
        try
        {
            String result;
            
            result = this.getJBIAdminCommands().uninstallComponent(compName, force, keepArchive, target);
            
            printTaskSuccess( result );
            
        }
        catch (Exception ex )
        {
            // throwTaskBuildException(ex);
            processTaskException(ex);
        }
        
    }
    
    /**
     * returns i18n key. tasks implement this method.
     * @return i18n key for the success status
     */
    protected String getTaskFailedStatusI18NKey()
    {
        return FAILED_STATUS_KEY;
    }
    /**
     * returns i18n key. tasks implement this method.
     * @return i18n key for the failed status
     */
    protected String getTaskSuccessStatusI18NKey()
    {
        return SUCCESS_STATUS_KEY;
    }
    
}
