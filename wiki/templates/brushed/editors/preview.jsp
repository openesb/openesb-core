<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/jspwiki.tld" prefix="wiki"%>
<%@ page import="com.ecyrd.jspwiki.*" %>
<%@ page import="com.ecyrd.jspwiki.tags.*" %>
<%@ page import="com.ecyrd.jspwiki.ui.*" %>

<%--
        This is a special editor component for JSPWiki preview storage.
--%>
<% 
   WikiContext context = WikiContext.findContext( pageContext ); 
   String usertext = (String)pageContext.getAttribute( 
                       EditorManager.ATTR_EDITEDTEXT, 
                       PageContext.REQUEST_SCOPE ); 
   if( usertext == null ) usertext = ""; 

   String action = "comment".equals(request.getParameter("action")) ? 
                   context.getURL(WikiContext.COMMENT,context.getPage().getName()) : 
                   context.getURL(WikiContext.EDIT,context.getPage().getName());
%>

  PIPOPIPO

<form method="post" accept-charset="<wiki:ContentEncoding/>" 
      action="<%=action%>" 
        name="editForm" 
     enctype="application/x-www-form-urlencoded">
    <p>
        <%-- Edit.jsp & Comment.jsp rely on these being found.  So be careful, if you make changes. --%>
        <input type="hidden" name="author"   value="<%=session.getAttribute("author")%>" />
        <input type="hidden" name="link"     value="<%=session.getAttribute("link")%>" />
        <input type="hidden" name="remember" value="<%=session.getAttribute("remember")%>" />
        <input type="hidden" name="page"     value="<wiki:Variable var="pagename"/>" />
        <input type="hidden" name="action"   value="save" />
        <input type="hidden" name="edittime" value="<%=pageContext.getAttribute(
                                                       "lastchange",
                                                       PageContext.REQUEST_SCOPE )%>" />
    </p>
    <textarea style="display: none;" readonly="true"
                id="editorarea" 
              name="<%=EditorManager.REQ_EDITEDTEXT%>" rows="4" cols="80"><%=TextUtil.replaceEntities(usertext)%></textarea>

    <p>
    <div id="previewsavebutton" align="center">
        <input type="submit" name="edit" value="Keep editing" />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <input type="submit" name="ok" value="Save" />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <input type="submit" name="cancel" value="Cancel" />
     </div>
     </p>
</form>
