<%@ taglib uri="/WEB-INF/jspwiki.tld" prefix="wiki" %>

<%--
<wiki:TabbedSection>
  <wiki:Tab id="wikihelp" title="Help on Edit">  
    <wiki:NoSuchPage page="EditPageHelp">
      <div class="error">
      Ho hum, it seems that the EditPageHelp
      <wiki:EditLink page="EditPageHelp">?</wiki:EditLink>
      page is missing.  Someone must've done something to the installation...
      <br /><br />
      You can copy the text from the 
      <a href="http://www.jspwiki.org/Wiki.jsp?page=EditPageHelp">EditPageHelp page on jspwiki.org</a>.
      </div>
    </wiki:NoSuchPage>
    <wiki:InsertPage page="EditPageHelp" />
  </wiki:Tab>
  <wiki:Tab id="searchbarhelp"  title="Find And Replace Help">
    <wiki:InsertPage page="EditFindAndReplaceHelp" />
  </wiki:Tab>
</wiki:TabbedSection>
--%>
    <wiki:NoSuchPage page="EditPageHelp">
      <div class="error">
      Ho hum, it seems that the EditPageHelp
      <wiki:EditLink page="EditPageHelp">?</wiki:EditLink>
      page is missing.  Someone must've done something to the installation...
      <br /><br />
      You can copy the text from the 
      <a href="http://www.jspwiki.org/Wiki.jsp?page=EditPageHelp">EditPageHelp page on jspwiki.org</a>.
      </div>
    </wiki:NoSuchPage>
    <wiki:InsertPage page="EditPageHelp" />
