<%@ taglib uri="/WEB-INF/jspwiki.tld" prefix="wiki" %>
<%@ page import="com.ecyrd.jspwiki.*" %>
<%!
  /* add timezone support to wiki:PageDate */
  String wiki_PageDate( WikiPage page, String format, String timezone )
  {
    if( page == null ) return "" ;
    java.util.Date date = page.getLastModified();  
    if( date == null ) return "&lt;never&gt;" ;
    java.text.SimpleDateFormat fmt = new java.text.SimpleDateFormat( format );
    java.util.TimeZone tz = java.util.TimeZone.getDefault();
    try 
    {
      tz.setRawOffset( Integer.parseInt( timezone ) );
    }
    catch( Exception e) { /* dont care */ }
    fmt.setTimeZone( tz );     
    return fmt.format( date ) ;
  } ;
%>
<%!
  // FIXME: this should better be something like a wiki:Pagination TLD tag
  // 0 20 40 60
  // 0 20 40 60 80 next
  // previous 20 40 *60* 80 100 next
  // previous 40 60 80 100 120

  /* makePagination : return html string with pagination links 
   *    (eg:  previous 1 2 3 next)
   * startitem  : cursor
   * itemcount  : total number of items
   * pagesize   : number of items per page
   * maxpages   : number of pages directly accessible via a pagination link
   * linkAttr : html attributes of the generated links: use '%s' to replace with item offset
   */
  String wiki_Pagination( int startitem, int itemcount, int pagesize, int maxpages, 
                                  String linkAttr )
  {    
    if( itemcount <= pagesize ) return null;
  
    int maxs = pagesize * maxpages;
    int mids = pagesize * ( maxpages / 2 );

    StringBuffer pagination = new StringBuffer();
    pagination.append( "<div class='pagination'>Pagination: " ); 

    int cursor = 0;
    int cursormax = itemcount;
 
    if( itemcount > maxs )   //need to calculate real window ends
    { 
      if( startitem > mids ) cursor = startitem - mids;
      if( (cursor + maxs) > itemcount ) 
        cursor = ( ( 1 + itemcount/pagesize ) * pagesize ) - maxs ; 
      
      cursormax = cursor + maxs;
    }
               
    if( (startitem == -1) || (cursor > 0) ) 
      appendLink ( pagination, linkAttr, 0, pagesize, "first" );
    if( (startitem != -1 ) && (startitem-pagesize >= 0) ) 
      appendLink( pagination, linkAttr, startitem-pagesize, pagesize, "previous" );

    if( startitem != -1 )
    {
      while( cursor < cursormax )
      {
        if( cursor == startitem ) 
        { 
          pagination.append( "<span class='cursor'>" + (1+cursor/pagesize)+ "</span>&nbsp;&nbsp;" ); 
        } 
        else 
        { 
          appendLink( pagination, linkAttr, cursor, pagesize, Integer.toString(1+cursor/pagesize) );
        }
        cursor += pagesize;
      }     
    }

    if( (startitem != -1) && (startitem + pagesize < itemcount) ) 
      appendLink( pagination, linkAttr, startitem+pagesize, pagesize, "next" );

    if( (startitem == -1) || (cursormax < itemcount) ) 
      appendLink ( pagination, linkAttr, ( (itemcount/pagesize) * pagesize ), pagesize, "last" );

    if( startitem == -1 ) 
    { 
      pagination.append( "<span class='cursor'>all</span>&nbsp;&nbsp;" ); 
    } 
    else
    {
      appendLink ( pagination, linkAttr, -1 , -1, "all" );
    }
    
    pagination.append( " (Total items: " + itemcount + ")</div>" );
    
    return pagination.toString();
  } 

  // linkAttr : use '%s' to replace with cursor offset
  // eg :
  // linkAttr = "href='#' title='%s' onclick='$(start).value= %s; updateSearchResult();'";
  void appendLink( StringBuffer sb, String linkAttr, int linkFrom, int pagesize, String linkText )
  {
    String title = "Show all pages";
    if( linkFrom > -1 ) title = "Show page from " + (linkFrom+1) + " to "+ (linkFrom+pagesize) ;

    sb.append( "<a title=\"" + title + "\" " );
    sb.append( TextUtil.replaceString( linkAttr, "%s", Integer.toString(linkFrom) ) );
    sb.append( ">" + linkText + "</a>&nbsp;&nbsp;" );
  } ;

%>
<%
  /* see commonheader.jsp */
  String prefDateFormat = (String) session.getAttribute("prefDateFormat");
  String prefTimeZone   = (String) session.getAttribute("prefTimeZone");

  WikiContext wikiContext = WikiContext.findContext(pageContext);
  WikiPage wikiPage = wikiContext.getPage();

  String creationDate   ="";
  String creationAuthor ="";
  //FIXME -- seems not to work correctly for attachments !!
  WikiPage p = wikiContext.getEngine().getPage( wikiPage.getName(), 1 );
  if( p != null )
  {
    creationAuthor = p.getAuthor();
    creationDate   = wiki_PageDate( p, prefDateFormat, prefTimeZone );
  }

  int pagesize  = 20; //default #revisions shown per page
  int maxpages  = 9;  //max #paginations links -- odd figure
  int itemcount = 0;  //number of page versions

  try 
  { 
    itemcount = wikiPage.getVersion(); /* highest version */
  } 
  catch( Exception  e )  { /* dont care */ }

  int startitem = itemcount;  

  String parm_start = request.getParameter( "start");
  if( parm_start != null ) startitem = Integer.parseInt( parm_start ) ;
  if( startitem > itemcount ) startitem = itemcount;
  if( startitem < -1 ) startitem = 0;

  String parm_pagesize = request.getParameter( "pagesize"); 
  if( parm_pagesize != null ) pagesize = Integer.parseInt( parm_pagesize ) ;

  if( startitem > -1 ) startitem = ( (startitem/pagesize) * pagesize );

  String linkAttr = "href='#' onclick='location.href= $(\"moreinfo\").href + \"&start=%s\"; ' ";
  String pagination = wiki_Pagination(startitem, itemcount, pagesize, maxpages, linkAttr);
%>

<wiki:PageExists>
<%-- ***************** NORMAL PAGE ****************** --%>
<wiki:PageType type="page">
  <p>
  This page (revision-<wiki:PageVersion />) was last changed on 
  <a href="<wiki:DiffLink format='url' version='latest' newVersion='previous' />"
    title="Show changes of last page update" >
    <%--<wiki:PageDate format='<%= prefDateFormat %>'/>--%>
    <%= wiki_PageDate( wikiPage, prefDateFormat, prefTimeZone ) %>
  </a> by <wiki:Author />.
  <a href="<wiki:Link format="url" jsp="rss.jsp">
           <wiki:Param name="page" value="<%=wikiPage.getName()%>"/>
           <wiki:Param name="mode" value="wiki"/>
           </wiki:Link>"
    title="RSS page feed for <wiki:PageName />" ><img src="<wiki:Link jsp='images/xml.png' format='url'/>" border="0" alt="[RSS]"/></a>
    <wiki:CheckVersion mode="notfirst">
    <br />
    This page was created on 
    <wiki:Link version="1"><%= creationDate %></wiki:Link> by <%= creationAuthor %>.
    </wiki:CheckVersion>
  </p>    

  <wiki:Permission permission="rename">
  <p>
    <form action="<wiki:Link format='url' jsp='Rename.jsp'/>" 
           class="wikiform"
        onsubmit="return WikiForm.submitOnce( this );"
          method="post" accept-charset="<wiki:ContentEncoding />" >
      <input type="hidden"   name="page"     value="<wiki:Variable var='pagename' />" >
      <input type="submit"   name="rename"   value="Rename Page" style="display:none;"/>
      <input type="button"   name="proxy1"   value="Rename Page" onclick="this.form.rename.click();" />
      <input type="text"     name="renameto" value="<wiki:Variable var='pagename' />" size="32" >&nbsp;&nbsp;
      <input type="checkbox" name="references" checked="checked" >Update referrers?
    </form>
  </p>
  </wiki:Permission>

  <wiki:Permission permission="delete"> 
  <p>
    <form action="<wiki:Link format='url' context='<%=WikiContext.DELETE%>' />" 
            name="deleteForm"
          method="post" accept-charset="<wiki:ContentEncoding />" 
        onsubmit="return( confirm( 'Please confirm that you want to delete this page permanently!' )
                       && WikiForm.submitOnce( this ) );">
      <input type="submit" value="Delete" name="delete-all" id="delete-all" style="display:none;"/>
      <input type="button" value="Delete Page" name="proxy2" id="proxy2"
          <wiki:HasAttachments>disabled</wiki:HasAttachments>
          onclick="$('delete-all').click();" />
      <wiki:HasAttachments>
        <i>First delete all attachments of this page</i> 
      </wiki:HasAttachments>   
    </form>
  </p>
  </wiki:Permission>
   
  <wiki:CheckRequestContext context='!info'>
  <p><a href="<wiki:PageInfoLink format='url' />" id="moreinfo" >More info...</a></p>
  </wiki:CheckRequestContext>
  <wiki:CheckRequestContext context='info'>
  <a href="<wiki:PageInfoLink format='url' />" id="moreinfo" style="display:none;" ></a>
  <p>Back to <wiki:Link><wiki:PageName/></wiki:Link></p>
  </wiki:CheckRequestContext>

  <wiki:TabbedSection>
  <% if( itemcount > 1 ) { %>
  <wiki:Tab id="versionHistory" title="Version History" >

    <%= (pagination == null) ? "" : pagination %>
    <div class="zebra-table <wiki:CheckRequestContext context='info'>sortable table-filter</wiki:CheckRequestContext>">
    <table class="wikitable" >
      <tr>
        <th>Version</th>                        
        <th>Date</th>
        <th>Author</th>
        <th>Size</th>
        <th>Changes ...</th>
        <%-- HIDDEN FEATURE
        <wiki:CheckRequestContext context='info'>
        <th>Version Label</th>
        </wiki:CheckRequestContext>
        END OF HIDDEN FEATURE--%>
      </tr>

      <wiki:HistoryIterator id="currentPage">
      <% if( ( startitem == -1 ) || 
             (  ( currentPage.getVersion() >= startitem ) 
             && ( currentPage.getVersion() < startitem + pagesize ) ) ) 
         {  
       %>
      <tr>
        <td>
          <wiki:LinkTo version="<%=Integer.toString(currentPage.getVersion())%>">
            <wiki:PageVersion/>
          </wiki:LinkTo>
        </td>

        <td><%--><wiki:PageDate format="<%= prefDateFormat %>" /> --%>
          <%= wiki_PageDate( currentPage, prefDateFormat, prefTimeZone ) %>
        </td>
        <td><wiki:Author /></td>
        <td><wiki:PageSize /></td>

        <td>
          <wiki:CheckVersion mode="notfirst">
            <wiki:DiffLink version="current" newVersion="previous">to previous</wiki:DiffLink>
            <wiki:CheckVersion mode="notlatest"> | </wiki:CheckVersion>
          </wiki:CheckVersion>

          <wiki:CheckVersion mode="notlatest">
            <wiki:DiffLink version="latest" newVersion="current">to last</wiki:DiffLink>
          </wiki:CheckVersion>
        </td>

        <%--HIDDEN FEATURE
        <wiki:CheckRequestContext context='info'>
        <td>
        <%
          try 
          { 
            WikiContext cc = (WikiContext)wikiContext.clone();
            cc.setPage ( (WikiPage)currentPage) ;
            String pagedata = cc.getEngine().getPureText(currentPage);
            cc.getEngine().textToHTML(cc, pagedata);
          } 
          catch( Exception  e )  { /* dont care */ }     
        %>
        <wiki:Variable var='versionLabel' default="&nbsp;"/> 
        </td>
        </wiki:CheckRequestContext>
        END OF HIDDEN FEATURE--%>
        
      </tr>
      <% } %>
      </wiki:HistoryIterator>

    </table>  
    </div>
    <%= (pagination == null) ? "" : pagination %>

  </wiki:Tab>
  <% } /* itemcount > 1 */ %>

  <wiki:Tab id="incomingLinks" title="Incoming Links">
    <wiki:LinkTo><wiki:PageName /></wiki:LinkTo>
    <wiki:Plugin plugin="ReferringPagesPlugin" args="before='*' after='\n' " />
  </wiki:Tab>

  <wiki:Tab id="outgoingLinks" title="Outgoing Links">
    <wiki:CheckRequestContext context='!info'>
      <wiki:Plugin plugin="ReferredPagesPlugin" args="depth='1' type='local'" />
    </wiki:CheckRequestContext>
    <wiki:CheckRequestContext context='info'>
      <%--div class="collapse"--%>
      <wiki:Plugin plugin="ReferredPagesPlugin" args="depth='3' type='local'" />
      <%--/div--%>
    </wiki:CheckRequestContext>
  </wiki:Tab>

  <%-- HIDDEN FEATURE
       this functionality was removed from the ReferredPagesPlugin --here is it back! --
  <wiki:CheckRequestContext context='info'>
  <wiki:Tab id="externalLinks" title="External Links">
    <wiki:Plugin plugin="brushed.jspwiki.plugin.ReferredPagesPlugin" args="depth='1' type='external'" />
  </wiki:Tab>

  <wiki:Tab id="attachmentLinks" title="Attachment Links">
    <wiki:Plugin plugin="brushed.jspwiki.plugin.ReferredPagesPlugin" args="depth='1' type='attachment'" />
  </wiki:Tab>
  </wiki:CheckRequestContext>
  END OF HIDDEN FEATURE--%>
  
  </wiki:TabbedSection> <%-- end of .tabs --%>

</wiki:PageType>

<%-- ***************** ATTACHMENTS ****************** --%>
<wiki:PageType type="attachment"> 
  <p>
  This attachment (revision-<wiki:PageVersion />) was last changed on 
  <%--<wiki:LinkTo><wiki:PageDate format='<%= prefDateFormat %>'/></wiki:LinkTo>--%>
  <wiki:LinkTo><%= wiki_PageDate( wikiPage, prefDateFormat, prefTimeZone ) %></wiki:LinkTo>
  by <wiki:Author />.
  <a href="<wiki:Link format='url' jsp='rss.jsp' >
              <wiki:Param name='page' value='<%=wikiContext.getPage().getName()%>'/>
              <wiki:Param name='mode' value='wiki'/>
              </wiki:Link>"
    title="RSS page feed for <wiki:PageName />" ><img src="<wiki:Link jsp='images/xml.png' format='url'/>" border="0" alt="[RSS]"/></a>
  <%--
  <wiki:CheckVersion mode="notfirst">
  <br />
  This page was created on 
  <wiki:Link version="1"><%= creationDate %></wiki:Link> by <%= creationAuthor %>.
  </wiki:CheckVersion>
  --%>
  <br />
  Back to 
  <a href="<wiki:LinkToParent format='url' />&tab=attachments"><wiki:ParentPageName /></a> (parent page)
  </p>

  <wiki:Permission permission="upload">
  <h3>Add new version</h3>
    <form action="<wiki:Link context='att' format='url' absolute='true'/>" 
        onsubmit="return WikiForm.submitOnce( this );"
          method="post" accept-charset="<wiki:ContentEncoding/>"
         enctype="multipart/form-data" >

    <%-- Do NOT change the order of wikiname and content, otherwise the 
         servlet won't find its parts. --%>

    <input type="hidden" name="page" value="<wiki:Variable var='pagename' />" />
    <div id="uploadViewer">
    <input type="file"   name="content" size="40" />
    </div>
    <p>
    <input type="submit" name="upload" value="Upload" style="display:none;"/>
    <input type="button" name="proxy1" value="Upload" onclick="this.form.upload.click();" />
    <input type="hidden" name="action" value="upload" />
    <input type="hidden" name="nextpage" value="<wiki:PageInfoLink format='url'/>" />
    First select the file to attach, then click on "Upload"
    </form>
  </p>
  </wiki:Permission>

  <wiki:Permission permission="delete">
      <form action="<wiki:Link format='url' context='<%=WikiContext.DELETE%>' />"
              name="deleteForm"
            method="post" accept-charset="<wiki:ContentEncoding />" 
          onsubmit="return( confirm( 'Please confirm that you want to delete this attachment permanently!' )
                         && WikiForm.submitOnce( this ) );">
        <input type="submit" value="Delete" name="delete-all" id="delete-all" style="display:none;"/>
        <input type="button" value="Delete Attachment" name="proxy2" id="proxy2"
            onclick="$('delete-all').click();" />
      </form>
  </wiki:Permission> 

  <%-- similar to AttachmentTab.jsp --%>
  <div id="attachmentViewer">
    <div class="list">    
      <h3>Attachment Version History</h3>

      <form action='#'>
      <select id="attachSelect" size="16" 
            name="attachSelect" 
        onchange="Attach.showImage(this[this.selectedIndex], '%A4', 300, 300 )" >
        <option value="Attachment Info" selected="selected" >--- Select Image to preview ---</option>
      <wiki:HistoryIterator id="att">
      <%-- use %A4 as delimiter:  Name, Link-url, Info-url, Size, Version --%>
      <option value="<wiki:PageName />%A4<wiki:Link version='<%=Integer.toString(att.getVersion())%>' format='url' />%A4<wiki:PageInfoLink format='url' />%A4<wiki:PageSize /> bytes%A4<wiki:PageVersion />" >
        Revision-<wiki:PageVersion /> (<wiki:PageSize /> bytes) 
        on <%--<wiki:PageDate format='<%= prefDateFormat %>'/>--%>
        <%= wiki_PageDate( att, prefDateFormat, prefTimeZone ) %> by&nbsp;<wiki:Author /> 
      </option>
      </wiki:HistoryIterator>
      </select>
      </form>
    </div>

    <div class="preview">
      <h3>Image preview</h3>
      <div id="attachImg" title="No Image Selected">No Image Selected</div>
    </div>

    <div style="clear:both; height:0px;" > </div>
  </div>
</wiki:PageType>
</wiki:PageExists>

<wiki:NoSuchPage>
This page does not exist.  Why don't you go and <wiki:EditLink>create it</wiki:EditLink>?
</wiki:NoSuchPage>
