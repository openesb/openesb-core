/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ExtPropertyOption.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package org.openesb.tools.extpropertysheet.impl;

import org.openesb.tools.extpropertysheet.IExtPropertyOption;

/**
 *
 * @author rdwivedi
 */
public class ExtPropertyOption implements IExtPropertyOption {
    
    /** Creates a new instance of ExtPropertyOption */
    private String mLabel = null;
    private Object mValue = null;
    public ExtPropertyOption(String label, Object value) {
        setOptionLabel(label);
        setOptionValue(value);
        
    }

    public String getOptionLabel() {
        return mLabel;
    }

    public void setOptionLabel(String label) {
        mLabel = label;
    }

    public Object getOptionValue() {
        return mValue;
    }

    public void setOptionValue(Object value) {
        mValue = value;
    }

    public void toXML(StringBuffer buffer) {
    }
    
}
