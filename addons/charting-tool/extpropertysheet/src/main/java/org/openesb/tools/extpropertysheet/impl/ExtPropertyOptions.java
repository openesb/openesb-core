/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ExtPropertyOptions.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package org.openesb.tools.extpropertysheet.impl;

import org.openesb.tools.extpropertysheet.IExtPropertyOption;
import org.openesb.tools.extpropertysheet.IExtPropertyOptions;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;

/**
 *
 * @author rdwivedi
 */
public class ExtPropertyOptions implements IExtPropertyOptions {
    
    /** Creates a new instance of ExtPropertyOptions */
    private HashMap mPropOptions = new HashMap();
    public ExtPropertyOptions() {
    }
    public ExtPropertyOptions(String[] arrays,boolean useArrayIndexAsValues) {
         if(useArrayIndexAsValues) {
         for(int i = 0; i < arrays.length; i++) {
             addOption(arrays[i],""+i);
         }
         } 
    }
    public ExtPropertyOptions(String[] arrays,String[] displayNames) {
         for(int i = 0; i < arrays.length; i++) {
             addOption(displayNames[i],arrays[i]);
         }
    } 

    public void addOption(IExtPropertyOption opt) {
        mPropOptions.put(opt.getOptionLabel(), opt);
    }

    public Collection getAllOptions() {
         return Collections.unmodifiableCollection(mPropOptions.values());
    }

    public void addOption(String label, Object value) {
        addOption(new ExtPropertyOption(label,value));
    }

    public void toXML(StringBuffer buffer) {
    }
    
}
