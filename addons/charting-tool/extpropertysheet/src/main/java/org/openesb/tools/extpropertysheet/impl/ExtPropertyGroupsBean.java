/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ExtPropertyGroupsBean.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package org.openesb.tools.extpropertysheet.impl;

import org.openesb.tools.extpropertysheet.IExtPropertyGroup;
import org.openesb.tools.extpropertysheet.IExtPropertyGroupsBean;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;

/**
 *
 * @author rdwivedi
 */
public class ExtPropertyGroupsBean implements IExtPropertyGroupsBean {
    
    private String mName = null;
    private HashMap mGMap = new HashMap();
     
    /** Creates a new instance of ExtPropertyGroupsBean */
    public ExtPropertyGroupsBean() {
    }
     
    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }
    public void clear() {
        mGMap.clear();
    }

    public Collection getAllGroups() {
        return Collections.unmodifiableCollection(mGMap.values());
    }

    public void toXML(StringBuffer buffer) {
        buffer.append("<GROUPBEAN>//n");
        Iterator iter = getAllGroups().iterator();
        while(iter.hasNext()) {
            IExtPropertyGroup p = (IExtPropertyGroup)iter.next();
            p.toXML(buffer);
        }
        buffer.append("</GROUPBEAN>//n");
    }

    public IExtPropertyGroup getGroupByName(String name) {
        return (IExtPropertyGroup)mGMap.get(name);
    }
    
    public void addGroup(IExtPropertyGroup gp) {
        mGMap.put(gp.getGroupName(),gp);
               
    }
    public void removeGroupByName(String grpName) {
        mGMap.remove( grpName);
    }
    
}
