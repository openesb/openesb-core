<?xml version="1.0" encoding="UTF-8"?>
<jsp:root version="1.2" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:ui="http://www.sun.com/web/ui">
    <jsp:directive.page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"/>
    <jsp:directive.page import="org.openesb.tools.charting.persist.ChartApplicationBeansContainer,org.openesb.tools.charting.persist.ChartBean"/>
    <jsp:scriptlet>
                    
                    String id = request.getParameter("_daUID"); 
                    String cID = request.getParameter("_chartUID"); 
                    ChartApplicationBeansContainer container = (ChartApplicationBeansContainer)session.getAttribute("ChartApplicationBeansContainer");
                    if(id!= null) {
                        container.setCurrentDBABeanOnEditorID(id); 
                    }
                    if(cID != null) {
                    ChartBean bean = container.getChartBeanByID(cID);
                    container.setCurrentChartBeanOnEditor(bean); 
                    }
                    
                </jsp:scriptlet>
                
               <jsp:scriptlet> if(id!=null) { </jsp:scriptlet>
                <jsp:forward page="../configure/ConfigureNewDataAccess.jsp"/>
                <jsp:scriptlet>}</jsp:scriptlet> 
                
                <jsp:scriptlet> if(cID!=null) { </jsp:scriptlet>
                <jsp:forward page="../configure/SelectDatasetForChart.jsp"/>
                <jsp:scriptlet>}</jsp:scriptlet> 
 </jsp:root>