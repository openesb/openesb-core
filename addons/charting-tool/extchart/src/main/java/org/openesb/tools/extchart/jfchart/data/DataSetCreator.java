/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)DataSetCreator.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package org.openesb.tools.extchart.jfchart.data;


import org.openesb.tools.extchart.exception.ChartException;
import org.openesb.tools.extchart.property.ChartConstants;
import org.openesb.tools.extchart.property.JFChartConstants;
import java.sql.ResultSet;
import java.util.Properties;
import java.util.logging.Logger;
import org.jfree.data.general.Dataset;

/**
 *
 * @author rdwivedi
 */
public abstract class  DataSetCreator {
    
    
    private static Logger mLogger = Logger.getLogger(DataSetCreator.class.getName());
    public abstract String getQuery() throws ChartException ;
    public abstract void processResultSet(ResultSet rs) throws ChartException ;    
    public abstract Dataset  getDefaultDataSet()  ;
    
    public static DataSetCreator create(String datasetType) throws ChartException {
        DataSetCreator   create = null;
        if(datasetType.equals(JFChartConstants.CATEGORY_DATASET)) {
            create = new CategoryDataSetCreator();
        } else if(datasetType.equals(JFChartConstants.PIE_DATASET)) {
             
            create = new PieDataSetCreator();
        } else if(datasetType.equals(JFChartConstants.XY_DATASET)) {
            create = new XYDataSetCreator();
             
        } else if(datasetType.equals(JFChartConstants.VALUE_DATASET)) {
             
             create = new ValueDataSetCreator();
        } else {
            mLogger.info("This should never happen . Data set is not found ." + datasetType);
        }
        mLogger.info("The .. = " + create.getQuery());
        return create;
        
    }
    
    
}
