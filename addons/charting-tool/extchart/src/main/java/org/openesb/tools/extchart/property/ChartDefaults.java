/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ChartDefaults.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package org.openesb.tools.extchart.property;




/**
 * ChartDefaults.java
 *
 * @author Chris Johnston
 * @version :$Revision: 1.4 $
 */
public class ChartDefaults extends ViewDefaults implements ChartConstants {


    /** the RCS id */
    static final String RCS_ID =
        "$Id: ChartDefaults.java,v 1.4 2010/07/18 16:18:07 ksorokin Exp $";

    public static final String IS_CHART_IMAGE_KEY = "cd_isChartImage";
    public static final boolean DEFAULT_IS_CHART_IMAGE = false;

    public static final String CHART_IMAGE_LOCATION_KEY = "cd_chartImageLocation";
    public static final String DEFAULT_CHART_IMAGE_LOCATION = "";

    public static final String CHART_IMAGE_DRAW_ORIGIN_KEY = "cd_chartImageDrawOrigin";
    public static final int CHART_IMAGE_DRAW_ORIGIN_UPPERLEFT = 0;
    public static final int CHART_IMAGE_DRAW_ORIGIN_CENTERED  = 1;
    public static final int[] CHART_IMAGE_DRAW_ORIGIN_TYPES = {
        CHART_IMAGE_DRAW_ORIGIN_UPPERLEFT,
        CHART_IMAGE_DRAW_ORIGIN_CENTERED
    };
    public static final int DEFAULT_CHART_IMAGE_DRAW_ORIGIN = CHART_IMAGE_DRAW_ORIGIN_UPPERLEFT;

    public static final String CHART_IMAGE_FIT_KEY = "cd_chartImageFit";
    public static final int CHART_IMAGE_UNCHANGED = 0;
    public static final int CHART_IMAGE_STRETCH_TO_FIT = 1;
    public static final int[] CHART_IMAGE_FIT_TYPES = {
        CHART_IMAGE_UNCHANGED,
        CHART_IMAGE_STRETCH_TO_FIT
    };
    
    private static final String CHART_ID = "cdChartID";
    
    public static final int DEFAULT_CHART_IMAGE_FIT = CHART_IMAGE_UNCHANGED;

    /** defaultObjects is a list of default objects for this class */
    Object[] defaultObjects = {
        //Boolean.valueOf(DEFAULT_IS_CHART_IMAGE),
        //DEFAULT_CHART_IMAGE_LOCATION,
        new Integer(DEFAULT_CHART_IMAGE_DRAW_ORIGIN),
        new Integer(DEFAULT_CHART_IMAGE_FIT),
        Boolean.valueOf(DEFAULT_3D),
        Boolean.valueOf(DEFAULT_INCLUDE_LEGEND),
        new Integer(DEFAULT_IMAGE_WIDTH),
        new Integer(DEFAULT_IMAGE_HEIGHT),
        //new Integer(DEFAULT_REPORT_WIDTH),
        //new Integer(DEFAULT_REPORT_HEIGHT),
        //new Integer(DEFAULT_CHART_FILE_CREATION_FORMAT_TYPE),
        //Boolean.valueOf(DEFAULT_DRILLDOWN_IS_ENABLED),
        //DEFAULT_DRILLDOWN_CHART_NAME,
        //Boolean.valueOf(DEFAULT_DISPLAY_NAVIGATION_BUTTONS),
        //DEFAULT_NAVIGATION_CHART_NAME
    };

    /** MY_DEFAULT_KEYS is a list of keys for this class. */
    protected static final String[] MY_DEFAULT_KEYS = {
        ///IS_CHART_IMAGE_KEY,
        CHART_IMAGE_LOCATION_KEY,
        CHART_IMAGE_DRAW_ORIGIN_KEY,
        CHART_IMAGE_FIT_KEY,
        IS_3D_KEY,
        IS_INCLUDE_LEGEND_KEY,
        IMAGE_WIDTH_KEY,
        IMAGE_HEIGHT_KEY,
       //REPORT_WIDTH_KEY,
        //REPORT_HEIGHT_KEY,
        //CHART_FILE_CREATION_FORMAT_TYPE_KEY,
        //DRILLDOWN_IS_ENABLED_KEY,
        //DRILLDOWN_CHART_NAME_KEY,
        //DISPLAY_NAVIGATION_BUTTONS_KEY,
        //NAVIGATION_CHART_NAME_KEY
    };

    static final String[] CHART_KEYS;
    static {
        String[] superKeys = ViewDefaults.MY_DEFAULT_KEYS;
        String[] myKeys    = new String[superKeys.length + MY_DEFAULT_KEYS.length];
        for (int n = 0; n < myKeys.length; n++) {
            if (n < superKeys.length) {
                myKeys[n] = superKeys[n];
            } else {
                myKeys[n] = MY_DEFAULT_KEYS[n - superKeys.length];
            }
        }
        CHART_KEYS = myKeys;
    }

    /**
     * @see com.stc.ebam.server.chart.engine.view.ViewDefaults.getKeys()
     */
    public String[] getKeys() {
        return CHART_KEYS;
    }

    /**
     * The ChartDefaults constructor constructs a new chart defaults
     * instance.
     */
    public ChartDefaults() {
        super();
        initDefaults();
        this.setGroupName(JFChartConstants.CHART_COMMON_PROPERTIES);
    }

    



    private void initDefaults()  {
        //setChartImage(DEFAULT_IS_CHART_IMAGE);
        //setChartImageLocation(DEFAULT_CHART_IMAGE_LOCATION);
        //setChartImageDrawOrigin(DEFAULT_CHART_IMAGE_DRAW_ORIGIN);
        //setChartImageFit(DEFAULT_CHART_IMAGE_FIT);
        set3D(DEFAULT_3D);
        setIncludeLegend(DEFAULT_INCLUDE_LEGEND);
        setImageWidth(DEFAULT_IMAGE_WIDTH);
        setImageHeight(DEFAULT_IMAGE_HEIGHT);
        //setReportWidth(DEFAULT_REPORT_WIDTH);
       // setReportHeight(DEFAULT_REPORT_HEIGHT);
        //setChartFileCreationFormatType(DEFAULT_CHART_FILE_CREATION_FORMAT_TYPE);
        //setDrilldownEnabled(DEFAULT_DRILLDOWN_IS_ENABLED);
        //setDrilldownChartName(DEFAULT_DRILLDOWN_CHART_NAME);
        //setDisplayNavigationButtons(DEFAULT_DISPLAY_NAVIGATION_BUTTONS);
        //setNavigationChartName(DEFAULT_NAVIGATION_CHART_NAME);
    }

    /**
     * Method setChartImage is used to set the chart image on or off
     * @param val val
     */
    /**public void setChartImage(boolean val) {
        this.setProperty(IS_CHART_IMAGE_KEY,  new Boolean(val));
    }

    /**
     * Method isChartImage is used to determine if a chart image is to be used
     * @return boolean true if chart image is to be rendered
     */
    /**public boolean isChartImage() {
        Boolean val = (Boolean) this.getPropertyValue(IS_CHART_IMAGE_KEY);
        if (val != null) {
            return val.booleanValue();
        }
        return DEFAULT_IS_CHART_IMAGE;
    }

    /**
     * Method getChartImageLocation returns the location of the chart image
     * @return String of the chart image
     */
    /*
    public String getChartImageLocation() {
        String val = (String) this.getPropertyValue(CHART_IMAGE_LOCATION_KEY);
        if (val != null) {
            return val;
        }
        return DEFAULT_CHART_IMAGE_LOCATION;
    }
*/
    /**
     * Method setChartImageLocation sets the chart image location
     * @param sChartImageLocation is the location for the image
     */
    /*
    public void setChartImageLocation(String sChartImageLocation) {
        this.setProperty(CHART_IMAGE_LOCATION_KEY, sChartImageLocation);
    }
*/
    /**
     * Method getChartImageDrawOrigin
     * @return int of the draw origin
     */
    /*
    public int getChartImageDrawOrigin() {
        int     nChartImageDrawOrigin = DEFAULT_CHART_IMAGE_DRAW_ORIGIN;
        Integer val = (Integer) this.getPropertyValue(CHART_IMAGE_DRAW_ORIGIN_KEY);
        if (val != null) {
            nChartImageDrawOrigin= val.intValue();
        }
        return nChartImageDrawOrigin;
    }
*/
    /**
     * Method setChartImageDrawOrigin sets the draw origin for the chart image
     * @param nChartImageDrawOrigin
     */
  /*  public void setChartImageDrawOrigin(int nChartImageDrawOrigin) {
        if ((nChartImageDrawOrigin >= 0) && (nChartImageDrawOrigin < CHART_IMAGE_DRAW_ORIGIN_TYPES.length)) {
            this.setProperty(CHART_IMAGE_DRAW_ORIGIN_KEY,  new Integer(nChartImageDrawOrigin));
        }
    }
*/
    /**
     * Method getChartImageFit returns the fit for the chart image
     * @return int of the draw origin
     */
  /*  public int getChartImageFit() {
        int     nChartImageFit = DEFAULT_CHART_IMAGE_FIT;
        Integer val = (Integer) this.getPropertyValue(CHART_IMAGE_FIT_KEY);
        if (val != null) {
            nChartImageFit= val.intValue();
        }
        return nChartImageFit;
    }
*/
    /**
     * Method setChartImageFit sets the fit for the chart image
     * @param nChartImageFit
     */
   /* public void setChartImageFit(int nChartImageFit) {
        if ((nChartImageFit >= 0) && (nChartImageFit < CHART_IMAGE_FIT_TYPES.length)) {
            this.setProperty(CHART_IMAGE_FIT_KEY,  new Integer(nChartImageFit));
        }
    }
*/
    /**
     * set 3d
     * @param val val
     */
    public void set3D(boolean val) {
        this.setProperty(ChartDefaults.IS_3D_KEY,  new Boolean(val));
    }

    /**
     * is 3d
     * @return 3d
     */
    public boolean is3D() {
        Boolean val = (Boolean) this.getPropertyValue(ChartDefaults.IS_3D_KEY);
        if (val != null) {
            return val.booleanValue();
        }
        return DEFAULT_3D;
    }
    /**
     * Returns depth factor
     * @return depth factor
     */
    public double getDepthFactor() {
        Double val = (Double) this.getPropertyValue(DEPTH_FACTOR_KEY);
        if (val != null) {
            return val.doubleValue();
        }
        return DEFAULT_DEPTH_FACTOR;
    }
    /**
     * Sets depth factor
     * @param factor - depth factor
     */
    public void setDepthFactor(double factor) {
        
        this.setProperty(DEPTH_FACTOR_KEY,  new Double(factor));
    }
    /**
     * Returns if circular
     * @return circular
     */
    public boolean isCircular() {
        Boolean val = (Boolean) this.getPropertyValue(CIRCULAR_KEY);
        if (val != null) {
            return val.booleanValue();
        }
        return false;
    }
    /**
     * Sets circular
     * @param circular - circular
     */
    public void setCircular(boolean circular) {
        this.setProperty(CIRCULAR_KEY,  new Boolean(circular));
    }
    /**
     * Returns direction
     * @return direction
     */
    public int getDirection() {
        Integer val = (Integer) this.getPropertyValue(DIRECTION_KEY);
        if (val != null) {
            return val.intValue();
        }
        return CLOCKWISE;
    }
    /**
     * Sets direction
     * @param direction - direction
     */
    public void setDirection(int direction) {
        this.setProperty(DIRECTION_KEY, new Integer(direction));
    }
    /**
     * Returns interior gap
     * @return gap
     */
    public double getInteriorGap() {
        Double val = (Double) this.getPropertyValue(INTERIOR_GAP_KEY);
        if (val != null) {
            return val.doubleValue();
        }
        return 0.3;
    }
    /**
     * Sets interior gap
     * @param gap - gap
     */
    public void setInteriorGap(double gap)  {
        
        this.setProperty(INTERIOR_GAP_KEY, new Double(gap));
    }
    /**
     * set include legend
     * @param include include
     */
    public void setIncludeLegend(boolean include) {
        this.setProperty(IS_INCLUDE_LEGEND_KEY, new Boolean(include));
    }
    /**
     * is include legend
     * @return include legend
     */
    public boolean isIncludeLegend() {
        Boolean val = (Boolean) this.getPropertyValue(IS_INCLUDE_LEGEND_KEY);
        if (val != null) {
            return val.booleanValue();
        }
        return DEFAULT_INCLUDE_LEGEND;
    }
    /**
     * Returns legend anchor
     * @return legend anchor
     */
    public int getLegendAnchor() {
        Integer val = (Integer) this.getPropertyValue(LEGEND_ANCHOR_KEY);
        if (val != null) {
            return val.intValue();
        }
        return DEFAULT_LEGEND_ANCHOR;
    }
    /**
     * Sets legend anchor
     * @param anchor - legend anchor
     */
    public void setLegendAnchor(int anchor) {
        this.setProperty(LEGEND_ANCHOR_KEY,  new Integer(anchor));
    }
    /**
     * set image width
     * @param width width
     */
    public void setImageWidth(int width) {
       
        this.setProperty(ChartDefaults.IMAGE_WIDTH_KEY,  new Integer(width));
    }

    /**
     * get image width
     * @return width
     */
    public int getImageWidth() {
        Integer val = (Integer) this.getPropertyValue(ChartDefaults.IMAGE_WIDTH_KEY);
        if (val != null) {
            return val.intValue();
        }
        return DEFAULT_IMAGE_WIDTH;
    }

    /**
     * set image height
     * @param height height
     */
    public void setImageHeight(int height)  {
       
        this.setProperty(ChartDefaults.IMAGE_HEIGHT_KEY,  new Integer(height));
    }

    /**
     * get image height
     * @return image height
     */
    public int getImageHeight() {
        Integer val = (Integer) this.getPropertyValue(ChartDefaults.IMAGE_HEIGHT_KEY);
        if (val != null) {
            return val.intValue();
        }

        //return default??
        return DEFAULT_IMAGE_HEIGHT;
    }
    
    
    public String getChartID() {
        
                
        String val = (String) this.getPropertyValue(CHART_ID);
        if (val != null) {
            return val;
        } else {
            return null;
        }
    }
    
    public void setChartID(String i) {
        this.setProperty(CHART_ID, i);
    }

    
     public String getPropertyTemplateName() {
        return "CommonChart";
    }


   

    
}
