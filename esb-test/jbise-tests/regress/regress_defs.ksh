#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)regress_defs.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#
#common definitions for packaging regression tests.

#this are normally set by antbld/regress/common_defs.ksh:
SVC_LOC=esb-test/jbise-tests
SVC_ROOT=$SRCROOT/$SVC_LOC
SVC_REGRESS=$SVC_ROOT/regress
SVC_BLD=$SVC_ROOT/bld

export JBI_HOME
JBI_HOME="$JBISE_BASE/jbi"
