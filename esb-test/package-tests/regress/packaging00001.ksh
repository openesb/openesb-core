#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)packaging00001.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#
#get a copy of jbiroot for the compare
. ./regress_defs.ksh

cd $SVC_BLD

mkdir trash.$$
[ -r jbi-new ] && mv jbi-new trash.$$
rm -rf trash.$$ &

echo Copying jbiroot ...
cp -rp $AS8BASE/jbi jbi-new
[ $? -ne 0 ] && bldmsg -error cp -rp $AS8BASE/jbi jbi-new FAILED

#this directory is as the result of the test run - origin unknown.  RT 1/12/07
rm -rf jbi-new/components/JavaEEServiceEngine/install_root/workspace

#check that jbienv.bat is present on nt, and then remove it to prevent diff on other platforms..
if [ "$FORTE_PORT" = "nt" ]; then
    fn=jbi-new/templates/scripts/jbienv.bat
    if [ -r $fn ]; then
        rm -f $fn
    else
        echo "$fn is MISSING - was there a change to the bom?"
    fi
fi

echo Expanding jbiroot jars...
walkdir -qq -unjar jbi-new
walkdir -qq -unjar jbi-new
walkdir -qq -unjar jbi-new
walkdir -qq -unjar jbi-new

wait
exit $?
