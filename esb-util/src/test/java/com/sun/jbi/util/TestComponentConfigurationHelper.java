/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestComponentConfigurationHelper.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.util;

import java.util.Properties;
import javax.management.openmbean.TabularData;
import javax.management.openmbean.TabularDataSupport;
import javax.management.openmbean.CompositeData;
import javax.management.openmbean.CompositeDataSupport;
import javax.management.openmbean.CompositeType;
import javax.management.openmbean.SimpleType;
import javax.management.openmbean.OpenType;
import javax.management.openmbean.ArrayType;

import com.sun.jbi.EnvironmentContext;
import java.io.InputStream;
import java.io.StringBufferInputStream;
import org.w3c.dom.Document;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

/**
 * Tests for the ConfigurationHelper class.
 *
 * @author Sun Microsystems
 */
public class TestComponentConfigurationHelper
    extends junit.framework.TestCase
{
    private static final String CONFIG_XML =
                "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                "<componentConfig:Configuration name=\"sun-http-binding\" " +
                               "xmlns:componentConfig=\"http://com.sun.jbi.component/schema/configuration\"> " +
                        "<componentConfig:OutboundThreads displayName=\"Number of outbound threads\" " +
                                     "displayDescription=\"Number of threads to concurrently process outbound HTTP/SOAP requests\" " +
                                     "isPasswordField=\"false\">10</componentConfig:OutboundThreads> " +
                                     
                        "<componentConfig:HttpDefaultPort displayName=\"Default HTTP port number\" " +
                                     "displayDescription=\"Default HTTP port number for inbound HTTP/SOAP requests\" " +
                                     "isPasswordField=\"false\">1111</componentConfig:HttpDefaultPort> " +
                                     
                        "<componentConfig:HttpsDefaultPort displayName=\"Default HTTPS port number\" " +
                                    "displayDescription=\"Default HTTPS port number for inbound HTTP/SOAP requests\" " +
                                     "isPasswordField=\"false\">2222</componentConfig:HttpsDefaultPort> " +
                                     
                        "<componentConfig:UseJVMProxySettings displayName=\"Use default JVM proxy settings\" " +
                                     "displayDescription=\"Determines whether or not to use the default JVM system properties for proxy settings\" " +
                                     "isPasswordField=\"false\">true</componentConfig:UseJVMProxySettings> " +
                                     
                        "<componentConfig:ProxyType displayName=\"Proxy type\" " +
                                     "displayDescription=\"Specifies a valid proxy type\" " +
                                     "isPasswordField=\"false\">SOCKS</componentConfig:ProxyType>" +
                                     
                        "<componentConfig:ProxyHost displayName=\"Proxy host\" " +
                                     "displayDescription=\"Specifies a valid proxy host name\" " +
                                     "isPasswordField=\"false\">dummy</componentConfig:ProxyHost>" +
                                     
                        "<componentConfig:ProxyPort displayName=\"Proxy port\" " +
                                     "displayDescription=\"Specifies a valid proxy port\" " +
                                     "isPasswordField=\"false\">3333</componentConfig:ProxyPort>" +
                                     
                        "<componentConfig:NonProxyHosts displayName=\"Non proxy hosts\" " +
                                     "displayDescription=\"Specifies a list of server hosts whose connections do not require a proxy server.\" " +
                                     "isPasswordField=\"false\">dummy</componentConfig:NonProxyHosts>" +
                                     
                        "<componentConfig:ProxyUserName displayName=\"Proxy user name\" " +
                                     "displayDescription=\"Specifies a valid proxy user name\" " +
                                     "isPasswordField=\"false\">dummy</componentConfig:ProxyUserName>" +
                                     
                        "<componentConfig:ProxyPassword displayName=\"Proxy user password\" " +
                                     "displayDescription=\"Specifies a valid proxy user password.\" " +
                                     "isPasswordField=\"true\">dummy</componentConfig:ProxyPassword>" +
            
                        "<componentConfig:ApplicationConfiguration>" +

                            "<componentConfig:configurationName displayName=\"Application Configuration Name\" " + 
                                                 "displayDescription=\"Specifies the name of the application configuration.\" " +
                                                 "isPasswordField=\"false\" isRequiredField=\"true\">dummy</componentConfig:configurationName >" +

                            "<componentConfig:httpLocation displayName=\"HTTP URL Location\" " + 
                                                 "displayDescription=\"Specifies the HTTP URL location.\" " +
                                                 "isPasswordField=\"false\" isRequiredField=\"true\">http://</componentConfig:httpLocation>" +

                            "<componentConfig:userPassword displayName=\"User Password\" " + 
                                                 "displayDescription=\"Specifies the user password.\" " +
                                                 "isPasswordField=\"true\" isRequiredField=\"true\">secret</componentConfig:userPassword>" +

                        "</componentConfig:ApplicationConfiguration>" +
                        "</componentConfig:Configuration>";
    
    
    private static final String CONFIG_XML_NEW =
            
        "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
            
        "<config:Configuration xmlns:config='http://www.sun.com/jbi/Configuration/V1.0'>" + 

        "<config:Property name=\"A\" " +
                         "type=\"xsd:string\" " + 
                         "displayName=\"Property A\" " + 
                         "displayDescription=\"A test string property A\" " + 
                         "defaultValue=\"a\" " + 
                         "isApplicationRestartRequired=\"false\" " + 
                         "isComponentRestartRequired=\"false\" " + 
                         "isServerRestartRequired=\"false\" " + 
                         "showDisplay=\"all\" " +
                         "required=\"true\"/> " + 

        "<config:Property name=\"B\" " + 
                         "type=\"xsd:string\" " + 
                         "displayName=\"Property B\" " + 
                         "displayDescription=\"A test string property B\" " + 
                         "defaultValue=\"b\" " + 
                         "isApplicationRestartRequired=\"false\" " +  
                         "isComponentRestartRequired=\"false\" " + 
                         "isServerRestartRequired=\"false\" " + 
                         "showDisplay=\"all\"/> " + 

        "<config:Property name=\"C\" " + 
                         "type=\"xsd:string\" " + 
                         "displayName=\"Property C\" " + 
                         "displayDescription=\"A test string property C\" " + 
                         "defaultValue=\"c\" " + 
                         "isApplicationRestartRequired=\"false\" " + 
                         "isComponentRestartRequired=\"false\" " + 
                         "isServerRestartRequired=\"false\" " + 
                         "showDisplay=\"runtime\" " +
                         "encrypted=\"true\"/> " + 


         "<config:ApplicationConfiguration> " + 
                "<config:Property name=\"configurationName\" " + 
                             "displayName=\"Application Configuration Name\" " + 
                             "displayDescription=\"Name of the application configuration\" " + 
                             "type=\"xsd:string\"  " + 
                             "required=\"true\"/> " + 
                "<config:Property name=\"connectionURL\" " + 
                             "displayName=\"Connection URL\" " + 
                             "displayDescription=\"The connection URL\" " + 
                             "type=\"xsd:string\"  " + 
                             "required=\"true\"/> " + 
                             
                "<config:Property name=\"securityPrincipal\" " + 
                             "displayName=\"Security Principal\" " + 
                             "displayDescription=\"User Name\" " + 
                             "type=\"xsd:string\" " + 
                             "required=\"true\"/> " + 
                             
                "<config:Property name=\"securityCredential\" " + 
                             "displayName=\"Security Credential\" " + 
                             "displayDescription=\"User Password\" " + 
                             "type=\"xsd:string\" " + 
                             "required=\"true\" " + 
                             "encrypted=\"true\"/>  " + 

                "<config:Property name=\"jndienv\" " + 
                             "displayName=\"JNDI Environment Properties\" " + 
                             "displayDescription=\"The JNDI Environment Properties\" " + 
                             "type=\"xsd:string\" " + 
                             "required=\"false\" " + 
                             "maxOccurs=\"unbounded\"/>   " +                           

        "</config:ApplicationConfiguration>" + 

         "<!-- Example of Application Variable -->" + 
         "<config:ApplicationVariable isApplicationRestartRequired=\"true\">" + 
             "<config:name/>" + 
             "<config:type/>" + 
             "<config:value/>" + 
         "</config:ApplicationVariable>" + 
        "</config:Configuration>";
    
        private static final String CONFIG_XML_NEW_NO_APP_CONFIG =
            
        "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
            
        "<config:Configuration xmlns:config='http://www.sun.com/jbi/Configuration/V1.0'>" + 

        "<config:Property name=\"C\" " + 
                         "type=\"xsd:string\" " + 
                         "displayName=\"Property C\" " + 
                         "displayDescription=\"A test string property C\" " + 
                         "defaultValue=\"c\" " + 
                         "isApplicationRestartRequired=\"false\" " + 
                         "isComponentRestartRequired=\"false\" " + 
                         "isServerRestartRequired=\"false\" " + 
                         "showDisplay=\"runtime\" " +
                         "encrypted=\"true\"/> " +  

         "<!-- Example of Application Variable -->" + 
         "<config:ApplicationVariable isApplicationRestartRequired=\"true\">" + 
             "<config:name/>" + 
             "<config:type/>" + 
             "<config:value/>" + 
         "</config:ApplicationVariable>" + 
         "</config:Configuration>";
        
                
        private static final String CONFIG_XML_NEW_NO_APP_VAR =
            
        "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
            
        "<config:Configuration xmlns:config='http://www.sun.com/jbi/Configuration/V1.0'>" + 

        "<config:Property name=\"C\" " + 
                         "type=\"xsd:string\" " + 
                         "displayName=\"Property C\" " + 
                         "displayDescription=\"A test string property C\" " + 
                         "defaultValue=\"c\" " + 
                         "isApplicationRestartRequired=\"false\" " + 
                         "isComponentRestartRequired=\"false\" " + 
                         "isServerRestartRequired=\"false\" " + 
                         "showDisplay=\"runtime\" " +
                         "encrypted=\"true\"/> " +  

         "<config:ApplicationConfiguration> " + 
                "<config:Property name=\"configurationName\" " + 
                             "displayName=\"Application Configuration Name\" " + 
                             "displayDescription=\"Name of the application configuration\" " + 
                             "type=\"xsd:string\"  " + 
                             "required=\"true\"/> " + 
                "<config:Property name=\"connectionURL\" " + 
                             "displayName=\"Connection URL\" " + 
                             "displayDescription=\"The connection URL\" " + 
                             "type=\"xsd:string\"  " + 
                             "required=\"true\"/> " + 
                             
                "<config:Property name=\"securityPrincipal\" " + 
                             "displayName=\"Security Principal\" " + 
                             "displayDescription=\"User Name\" " + 
                             "type=\"xsd:string\" " + 
                             "required=\"true\"/> " + 
                             
                "<config:Property name=\"securityCredential\" " + 
                             "displayName=\"Security Credential\" " + 
                             "displayDescription=\"User Password\" " + 
                             "type=\"xsd:string\" " + 
                             "required=\"true\" " + 
                             "encrypted=\"true\"/>  " + 

                "<config:Property name=\"jndienv\" " + 
                             "displayName=\"JNDI Environment Properties\" " + 
                             "displayDescription=\"The JNDI Environment Properties\" " + 
                             "type=\"xsd:string\" " + 
                             "required=\"false\" " + 
                             "maxOccurs=\"unbounded\"/>   " +                           

        "</config:ApplicationConfiguration>" +
        "</config:Configuration>";
    
        private static final String CONFIG_XML_NEW_NO_COMP_CONFIG =
            
        "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
            
        "<config:Configuration xmlns:config='http://www.sun.com/jbi/Configuration/V1.0'>" + 

         "<config:ApplicationConfiguration> " + 
                "<config:Property name=\"configurationName\" " + 
                             "displayName=\"Application Configuration Name\" " + 
                             "displayDescription=\"Name of the application configuration\" " + 
                             "type=\"xsd:string\"  " + 
                             "required=\"true\"/> " + 
                "<config:Property name=\"connectionURL\" " + 
                             "displayName=\"Connection URL\" " + 
                             "displayDescription=\"The connection URL\" " + 
                             "type=\"xsd:string\"  " + 
                             "required=\"true\"/> " + 
                             
                "<config:Property name=\"securityPrincipal\" " + 
                             "displayName=\"Security Principal\" " + 
                             "displayDescription=\"User Name\" " + 
                             "type=\"xsd:string\" " + 
                             "required=\"true\"/> " + 
                             
                "<config:Property name=\"securityCredential\" " + 
                             "displayName=\"Security Credential\" " + 
                             "displayDescription=\"User Password\" " + 
                             "type=\"xsd:string\" " + 
                             "required=\"true\" " + 
                             "encrypted=\"true\"/>  " + 

                "<config:Property name=\"jndienv\" " + 
                             "displayName=\"JNDI Environment Properties\" " + 
                             "displayDescription=\"The JNDI Environment Properties\" " + 
                             "type=\"xsd:string\" " + 
                             "required=\"false\" " + 
                             "maxOccurs=\"unbounded\"/>   " +                           

        "</config:ApplicationConfiguration>" +
                
         "<!-- Example of Application Variable -->" + 
         "<config:ApplicationVariable isApplicationRestartRequired=\"true\">" + 
             "<config:name/>" + 
             "<config:type/>" + 
             "<config:value/>" + 
         "</config:ApplicationVariable>" + 
         "</config:Configuration>";
        
            
        private static final String CONFIG_XML_WITH_PROPERTY_GROUP =
            
        "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
            
        "<config:Configuration xmlns:config='http://www.sun.com/jbi/Configuration/V1.0'>" + 

        "<config:PropertyGroup> <config:Property name=\"C\" " + 
                         "type=\"xsd:string\" " + 
                         "displayName=\"Property C\" " + 
                         "displayDescription=\"A test string property C\" " + 
                         "defaultValue=\"c\" " + 
                         "isApplicationRestartRequired=\"false\" " + 
                         "isComponentRestartRequired=\"false\" " + 
                         "isServerRestartRequired=\"false\" " + 
                         "showDisplay=\"runtime\" " +
                         "encrypted=\"true\"/>  </config:PropertyGroup>" +  

         "<!-- Example of Application Variable -->" + 
         "<config:ApplicationVariable isApplicationRestartRequired=\"true\">" + 
             "<config:name/>" + 
             "<config:type/>" + 
             "<config:value/>" + 
         "</config:ApplicationVariable>" + 
         "</config:Configuration>";
    /**
     * The constructor for this testcase, forwards the test name to
     * the jUnit TestCase base class.
     * @param aTestName String with the name of this test.
     */
    public TestComponentConfigurationHelper(String aTestName)
    {
        super(aTestName);
    }

    /**
     * Test splitting property value into value and type
     */
    public void testGetAppVarType()
        throws Exception
    {
        ComponentConfigurationHelper cchlpr = new ComponentConfigurationHelper();
        
        assertEquals("PASSWORD", cchlpr.getAppVarType("[PASSWORD]5646vg", cchlpr.DEFAULT_APP_VAR_TYPE));
    }
    
    /**
     * Test splitting property value into value and type, when type goind in
     * is lower case, but coming out should be upper case.
     */
    public void testGetAppVarType1()
        throws Exception
    {
        ComponentConfigurationHelper cchlpr = new ComponentConfigurationHelper();
        
        assertEquals("STRING", cchlpr.getAppVarType("[String]5646vg", cchlpr.DEFAULT_APP_VAR_TYPE));
    } 
    
    /**
     * Test splitting property value into value and type, when type is not specified
     */
    public void testGetAppVarType2()
        throws Exception
    {
        ComponentConfigurationHelper cchlpr = new ComponentConfigurationHelper();
        
        assertEquals("STRING", cchlpr.getAppVarType("5646vg", cchlpr.DEFAULT_APP_VAR_TYPE));
    }
    
    /**
     * Test splitting property value into value and type, when type is not specified
     */
    public void testGetAppVarType3()
        throws Exception
    {
        ComponentConfigurationHelper cchlpr = new ComponentConfigurationHelper();
        
        assertEquals("STRING", cchlpr.getAppVarType("[ ]5646vg", cchlpr.DEFAULT_APP_VAR_TYPE));
    }
    
    
    /**
     * Test splitting property value into value and type
     */
    public void testGetAppVarValue()
        throws Exception
    {
        ComponentConfigurationHelper cchlpr = new ComponentConfigurationHelper();
        
        System.out.println(cchlpr.getAppVarValue("[PASSWORD]5646vg"));
        assertEquals("5646vg", cchlpr.getAppVarValue("[PASSWORD]5646vg"));
    }
    
        
    /**
     * Test Converting App. Var properties to Tabular data
     */
    public void testConvertToApplicationVariablesTable()
        throws Exception
    {
        ComponentConfigurationHelper cchlpr = new ComponentConfigurationHelper();
        
        Properties props = new Properties();
        props.put("Host", "[STRING]mystique01");
        props.put("Port", "[NUMBER]1056");
        props.put("User", "[STRING]perty");
        props.put("Pwd",  "[PASSWORD]678hjj");
        
        TabularData td = cchlpr.convertToApplicationVariablesTable(props);
        assertNotNull(td);
        
        Properties convProps = cchlpr.convertToApplicationVariablesProperties(td);
        
        assertTrue(convProps.containsKey("Host"));
        assertTrue(convProps.get("Host").equals("[STRING]mystique01"));
        
        assertTrue(convProps.containsKey("Port"));
        assertTrue(convProps.get("Port").equals("[NUMBER]1056"));
        
        assertTrue(convProps.containsKey("User"));
        assertTrue(convProps.get("User").equals("[STRING]perty"));
        
        assertTrue(convProps.containsKey("Pwd"));
        assertTrue(convProps.get("Pwd").equals("[PASSWORD]678hjj"));
    }
    
    /**
     * Test fix for issue 292, where a application variiable value is null.
     */
    public void testConvertToApplicationVariablesProperties()
        throws Exception
    {
         ComponentConfigurationHelper cchlpr = new ComponentConfigurationHelper();
         TabularData tabularData = new TabularDataSupport(cchlpr.getApplicationVariableTabularType());
         
         
             
         CompositeData cd = new CompositeDataSupport(
                 cchlpr.getApplicationVariablesType(),
                 cchlpr.getApplicationVariableItemNames(), 
                 new String[] {"testAppVar", null, "STRING"});
         tabularData.put(cd);
         
         Properties convProps = cchlpr.convertToApplicationVariablesProperties(tabularData);
         
         assertTrue(convProps.containsKey("testAppVar"));
         assertTrue(convProps.get("testAppVar").equals("[STRING]null"));
        
    }
    
    /**
     * Test fix for issue 292, where a application variiable value is null.
     */
    public void testConvertToApplicationVariableProperty()
        throws Exception
    {
         ComponentConfigurationHelper cchlpr = new ComponentConfigurationHelper();
         
         CompositeData cd = new CompositeDataSupport(
                 cchlpr.getApplicationVariablesType(),
                 cchlpr.getApplicationVariableItemNames(), 
                 new String[] {"testAppVar", null, "STRING"});
         
         Properties convProps = cchlpr.convertToApplicationVariableProperty(cd);
         
         assertTrue(convProps.containsKey("testAppVar"));
         assertTrue(convProps.get("testAppVar").equals("[STRING]null"));
        
    }
    
    /**
     *
     */
    public void testConvertPropertiesToCompositeData()
        throws Exception
    {
        ComponentConfigurationHelper cchlpr = new ComponentConfigurationHelper();
                
        Properties props = new Properties();
        props.put("configurationName", "myTestConfig");
        props.put("connectionURL",     "http://");
        props.put("jndienv.0", "n1=v1");
        props.put("jndienv.1", "n2=v2");
        props.put("jndienv.2", "n3=v3");
        props.put("ports.0", "1");
        props.put("ports.1", "2");
        
        CompositeData cd = cchlpr.convertPropertiesToCompositeData(props, createTestCompositeType());
        
        // Verify
        assertEquals("myTestConfig", cd.get("configurationName"));
        assertEquals("http://", cd.get("connectionURL"));
        String[] jndienv = (String[]) cd.get("jndienv");
        assertEquals(jndienv[0], "n1=v1");
        assertEquals(jndienv[1], "n2=v2");
        assertEquals(jndienv[2], "n3=v3");
        Integer[] ports = (Integer[]) cd.get("ports");
        assertEquals(ports[0].intValue(), 1);
        assertEquals(ports[1].intValue(), 2);
    }
    
    /**
     * Test converting a incomplete properties set to Composite Data
     */
    public void testConvertIncompPropertiesToCompositeData()
        throws Exception
    {
        ComponentConfigurationHelper cchlpr = new ComponentConfigurationHelper();
                
        Properties props = new Properties();
        props.put("configurationName", "myTestConfig");
        props.put("connectionURL",     "http://");
        props.put("jndienv.0", "n1=v1");
        props.put("jndienv.1", "n2=v2");
        props.put("jndienv.2", "n3=v3");
        props.put("subComposite", "\"host=myHost, maxUsers=2\"");
        
        CompositeData cd = cchlpr.convertPropertiesToCompositeData(props, createTestCompositeType());
        
        // Verify
        assertEquals("myTestConfig", cd.get("configurationName"));
        assertEquals("http://", cd.get("connectionURL"));
        String[] jndienv = (String[]) cd.get("jndienv");
        assertEquals(jndienv[0], "n1=v1");
        assertEquals(jndienv[1], "n2=v2");
        assertEquals(jndienv[2], "n3=v3");
        Integer[] ports = (Integer[]) cd.get("ports");
        assertNull(ports);
        
        CompositeData scd = (CompositeData) cd.get("subComposite");
        assertNotNull(scd);
        assertEquals(scd.get("host"), "myHost");
        assertEquals(scd.get("maxUsers"), "2");
    }
    
    /**
     * Test converting a CompositeData to properties
     */
    public void testConvertCompositeDataToProperties()
        throws Exception
    {
     
        String[] itemNames = new String[] { "configurationName",
                                          "connectionURL",
                                          "jndienv",
                                          "ports",
                                          "subComposite"
                                        };
                                        
        CompositeData scd = new CompositeDataSupport(createTestSubCompositeType(),
            new String[]{"host", "maxUsers"},
            new Object[]{"someHost", "100"});
        Object[] itemValues = new Object[] { "myConfig",
                                             "myConnectionURL",
                                             new String[]{"jndienv1", "jndienv2"},
                                             new Integer[]{1,2},
                                             scd};
                                             
        CompositeData cd = new CompositeDataSupport(
                    createTestCompositeType(),
                    itemNames,
                    itemValues); 
        ComponentConfigurationHelper cchlpr = new ComponentConfigurationHelper();
        Properties props = cchlpr.convertCompositeDataToProperties(cd);
        
        assertTrue(props.containsKey("configurationName"));
        assertEquals("myConfig", props.getProperty("configurationName"));
        assertTrue(props.containsKey("connectionURL"));
        assertEquals("myConnectionURL", props.getProperty("connectionURL"));
        assertTrue(props.containsKey("jndienv.0"));
        assertEquals("jndienv1", props.getProperty("jndienv.0"));
        assertTrue(props.containsKey("jndienv.1"));
        assertEquals("jndienv2", props.getProperty("jndienv.1"));
        assertTrue(props.containsKey("ports.0"));
        assertEquals("1", props.getProperty("ports.0"));
        assertTrue(props.containsKey("ports.1"));
        assertEquals("2", props.getProperty("ports.1"));
        assertTrue(props.containsKey("subComposite"));
        //System.out.println("Sub composite value " + props.getProperty("subComposite"));
        //assertEquals("\"host=someHost, maxUsers=100\"", props.getProperty("subComposite"));
    }
    
    /**
     * Test converting a CompositeData to properties
     */
    public void testConvertIncompleteCompositeDataToProperties()
        throws Exception
    {
     
        String[] itemNames = new String[] { "configurationName",
                                          "connectionURL",
                                          "jndienv",
                                          "ports",
                                          "subComposite"
                                        };
                                        
        Object[] itemValues = new Object[] { "myConfig",
                                             "myConnectionURL",
                                             new String[0],
                                             new Integer[0],
                                             null};
                                             
        CompositeData cd = new CompositeDataSupport(
                    createTestCompositeType(),
                    itemNames,
                    itemValues); 
        ComponentConfigurationHelper cchlpr = new ComponentConfigurationHelper();
        Properties props = cchlpr.convertCompositeDataToProperties(cd);
        
        //System.out.println("Converted composite data : " + props.toString());
        assertTrue(props.containsKey("configurationName"));
        assertEquals("myConfig", props.getProperty("configurationName"));
        assertTrue(props.containsKey("connectionURL"));
        assertEquals("myConnectionURL", props.getProperty("connectionURL"));
        assertFalse(props.containsKey("jndienv.0"));
        assertFalse(props.containsKey("jndienv.1"));
        assertFalse(props.containsKey("ports.0"));
        assertFalse(props.containsKey("ports.1"));
    }
        
    /**
     * Test determining if a app config field is a password field.
     */
    public void testIsEncrypted()
        throws Exception
    {   
        Document configDoc  = createConfigDataDoc(CONFIG_XML_NEW);
        
        ComponentConfigurationHelper cchlpr = new ComponentConfigurationHelper(); 
        boolean isPwd = cchlpr.isPassword("securityCredential", configDoc);
        
        assertTrue(isPwd);
    }
    
    /**
     * Test determining if a field is a password field.
     */
    public void testIsEncrypted2()
        throws Exception
    {   
        Document configDoc  = createConfigDataDoc(CONFIG_XML_NEW);
        
        ComponentConfigurationHelper cchlpr = new ComponentConfigurationHelper(); 
        boolean isPwd = cchlpr.isPassword("C", configDoc);
        
        assertTrue(isPwd);
    }
    
    /**
     * Test determining if a field is a password field.
     */
    public void testIsEncrypted3()
        throws Exception
    {   
        Document configDoc  = createConfigDataDoc(CONFIG_XML_NEW);
        
        ComponentConfigurationHelper cchlpr = new ComponentConfigurationHelper(); 
        boolean isPwd = cchlpr.isPassword("A", configDoc);
        
        assertFalse(isPwd);
    }
    
    /**
     * Test determining if a field is a password field. missing field case
     */
    public void testIsEncrypted4()
        throws Exception
    {   
        Document configDoc  = createConfigDataDoc(CONFIG_XML_NEW);
        
        ComponentConfigurationHelper cchlpr = new ComponentConfigurationHelper(); 
        boolean isPwd = cchlpr.isPassword("dummy", configDoc);
        
        assertFalse(isPwd);
    }
    
    /**
     * Test determining if a field is a required field. 
     */
    public void testIsRequired()
        throws Exception
    {   
        Document configDoc  = createConfigDataDoc(CONFIG_XML_NEW);
        
        ComponentConfigurationHelper cchlpr = new ComponentConfigurationHelper(); 
        boolean isRqd = cchlpr.isRequired("A", configDoc);
        
        assertTrue(isRqd);
    }
    
    /**
     * Test determining if a app config field is a required field. 
     */
    public void testIsRequired2()
        throws Exception
    {   
        Document configDoc  = createConfigDataDoc(CONFIG_XML_NEW);
        
        ComponentConfigurationHelper cchlpr = new ComponentConfigurationHelper(); 
        boolean isPwd = cchlpr.isRequired("jndienv", configDoc);
        
        assertFalse(isPwd);
    }
    
        
    /**
     * Test determining if a app config field is a required field. false case
     */
    public void testIsRequired3()
        throws Exception
    {   
        Document configDoc  = createConfigDataDoc(CONFIG_XML_NEW);
        
        ComponentConfigurationHelper cchlpr = new ComponentConfigurationHelper(); 
        boolean isPwd = cchlpr.isRequired("securityCredential", configDoc);
        
        assertTrue(isPwd);
    }
    
    
    /**
     * Test determining if a field is a required field. false case
     */
    public void testIsRequired4()
        throws Exception
    {   
        Document configDoc  = createConfigDataDoc(CONFIG_XML_NEW);
        
        ComponentConfigurationHelper cchlpr = new ComponentConfigurationHelper(); 
        boolean isPwd = cchlpr.isRequired("C", configDoc);
        
        assertFalse(isPwd);
    }   
    
    /**
     * Test determining if a field is a required field. missing field case
     */
    public void testIsRequired5()
        throws Exception
    {   
        Document configDoc  = createConfigDataDoc(CONFIG_XML_NEW);
        
        ComponentConfigurationHelper cchlpr = new ComponentConfigurationHelper(); 
        boolean isPwd = cchlpr.isRequired("dummy", configDoc);
        
        assertFalse(isPwd);
    }       
    
    
    /**
     * Test determining if a field is a password field.
     */
    public void testIsPassword()
        throws Exception
    {   
        Document configDoc  = createConfigDataDoc(CONFIG_XML);
        
        ComponentConfigurationHelper cchlpr = new ComponentConfigurationHelper(); 
        boolean isPwd = cchlpr.isPassword("ProxyPassword", configDoc);
        
        assertTrue(isPwd);
    }
    
    /**
     * Test determining if a field is a password field. false case.
     */
    public void testIsPassword2()
        throws Exception
    {   
        Document configDoc  = createConfigDataDoc(CONFIG_XML);
        
        ComponentConfigurationHelper cchlpr = new ComponentConfigurationHelper(); 
        boolean isPwd = cchlpr.isPassword("OutboundThreads", configDoc);
        
        assertFalse(isPwd);
    }
    
    /**
     * Test determining if a suffixed field is a password field. false case.
     */
    public void testIsPassword3()
        throws Exception
    {   
        Document configDoc  = createConfigDataDoc(CONFIG_XML);
        
        ComponentConfigurationHelper cchlpr = new ComponentConfigurationHelper(); 
        boolean isPwd = cchlpr.isPassword("OutboundThreads.0", configDoc);
        
        assertFalse(isPwd);
    }
    
    /**
     * Test determining if a field is a password field for a undefined
     *  field. false case.
     */
    public void testIsPasswordMissingField()
        throws Exception
    {   
        Document configDoc  = createConfigDataDoc(CONFIG_XML);
        
        ComponentConfigurationHelper cchlpr = new ComponentConfigurationHelper(); 
        boolean isPwd = cchlpr.isPassword("dummy", configDoc);
        
        assertFalse(isPwd);
    }
    
    /**
     * Test determining if a application configuration field is a password field.
     */
    public void testIsPasswordForAppConfig()
        throws Exception
    {
        Document configDoc = createConfigDataDoc(CONFIG_XML);
        
        ComponentConfigurationHelper cchlpr = new ComponentConfigurationHelper(); 
        boolean isPwd = cchlpr.isPassword("userPassword", configDoc);
        
        assertTrue(isPwd);
    }
    
    /**
     * Test determining if a suffixed application configurationfield is a password field.
     */
    public void testIsPasswordForAppConfig2()
        throws Exception
    {
        Document configDoc = createConfigDataDoc(CONFIG_XML);
        
        ComponentConfigurationHelper cchlpr = new ComponentConfigurationHelper(); 
        boolean isPwd = cchlpr.isPassword("userPassword.0", configDoc);
        
        assertTrue(isPwd);
    }
    
    /**
     * Test determining if a application configurationfield is a password field.
     * This is the case where a field is not a password field
     */
    public void testIsPasswordForAppConfig3()
        throws Exception
    {
        Document configDoc = createConfigDataDoc(CONFIG_XML);
        
        ComponentConfigurationHelper cchlpr = new ComponentConfigurationHelper(); 
        boolean isPwd = cchlpr.isPassword("httpLocation", configDoc);
        
        assertFalse(isPwd);
    }
    
    /**
     * Test determining if a suffixed application configuration field is a password field.
     * This is the case where a field is not a password field
     */
    public void testIsPasswordForAppConfig4()
        throws Exception
    {
        Document configDoc = createConfigDataDoc(CONFIG_XML);
        
        ComponentConfigurationHelper cchlpr = new ComponentConfigurationHelper(); 
        boolean isPwd = cchlpr.isPassword("httpLocation.0", configDoc);
        
        assertFalse(isPwd);
    }
    
    /**
     * Test determining if a application configurationfield is a password field.
     * This is the case where a field is missing
     */
    public void testIsPasswordForAppConfigMissingField()
        throws Exception
    {
        Document configDoc = createConfigDataDoc(CONFIG_XML);
        
        ComponentConfigurationHelper cchlpr = new ComponentConfigurationHelper(); 
        boolean isPwd = cchlpr.isPassword("dummy", configDoc);
        
        assertFalse(isPwd);
    }
           
    /**
     * Test determining if a component supports component configuration when 
     * the component configuration has a PropertyGroup.
     */
    public void testIsComponentConfigSupportedWithPropertyGroup()
        throws Exception
    {   
        Document configDoc  = createConfigDataDoc(CONFIG_XML_WITH_PROPERTY_GROUP);
        
        ComponentConfigurationHelper cchlpr = new ComponentConfigurationHelper(); 
        boolean supported = cchlpr.isComponentConfigSupported(configDoc);
        
        assertTrue(supported);
    }
    /**
     * Test determining if a component supports component configuration
     */
    public void testIsComponentConfigSupported()
        throws Exception
    {   
        Document configDoc  = createConfigDataDoc(CONFIG_XML_NEW);
        
        ComponentConfigurationHelper cchlpr = new ComponentConfigurationHelper(); 
        boolean supported = cchlpr.isComponentConfigSupported(configDoc);
        
        assertTrue(supported);
    }
    
    /**
     * Test determining if a component supports component configuration
     */
    public void testIsComponentConfigSupportedNegative()
        throws Exception
    {   
        Document configDoc  = createConfigDataDoc(CONFIG_XML_NEW_NO_COMP_CONFIG);
        
        ComponentConfigurationHelper cchlpr = new ComponentConfigurationHelper(); 
        boolean supported = cchlpr.isComponentConfigSupported(configDoc);
        
        assertFalse(supported);
    }    
    
    /**
     * Test determining if a component supports application variables
     */
    public void testIsAppVarsSupported()
        throws Exception
    {   
        Document configDoc  = createConfigDataDoc(CONFIG_XML_NEW);
        
        ComponentConfigurationHelper cchlpr = new ComponentConfigurationHelper(); 
        boolean supported = cchlpr.isAppVarsSupported(configDoc);
        
        assertTrue(supported);
    }
    
    /**
     * Test determining if a component supports application variables
     */
    public void testIsAppVarsSupportedNegative()
        throws Exception
    {   
        Document configDoc  = createConfigDataDoc(CONFIG_XML_NEW_NO_APP_VAR);
        
        ComponentConfigurationHelper cchlpr = new ComponentConfigurationHelper(); 
        boolean supported = cchlpr.isAppVarsSupported(configDoc);
        
        assertFalse(supported);
    }    
    
    /**
     * Test determining if a component supports application configuration
     */
    public void testIsAppConfigSupported()
        throws Exception
    {   
        Document configDoc  = createConfigDataDoc(CONFIG_XML_NEW);
        
        ComponentConfigurationHelper cchlpr = new ComponentConfigurationHelper(); 
        boolean supported = cchlpr.isAppConfigSupported(configDoc);
        
        assertTrue(supported);
    }

    /**
     * Test determining if a component supports application configuration
     */
    public void testIsAppConfigSupportedNegative()
        throws Exception
    {   
        Document configDoc  = createConfigDataDoc(CONFIG_XML_NEW_NO_APP_CONFIG);
        
        ComponentConfigurationHelper cchlpr = new ComponentConfigurationHelper(); 
        boolean supported = cchlpr.isAppConfigSupported(configDoc);
        
        assertFalse(supported);
    }    
    
    
    private Document createConfigDataDoc(String configData)
        throws Exception
    {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);
        DocumentBuilder db  = dbf.newDocumentBuilder();
        InputStream ios     = new StringBufferInputStream(configData);
        
        Document doc = db.parse(ios);
        ios.close(); 
        return doc;
    }
    
    
    /**
     *
     */
    private CompositeType createTestCompositeType()
        throws Exception
    {
        String[] itemNames = new String[] { "configurationName",
                                          "connectionURL",
                                          "jndienv",
                                          "ports",
                                          "subComposite"
                                        };
                                        
                                        
                                        
        OpenType[] itemTypes = new OpenType[] { SimpleType.STRING,
                                            SimpleType.STRING,
                                            new ArrayType(1, SimpleType.STRING),
                                            new ArrayType(1, SimpleType.INTEGER),
                                            createTestSubCompositeType()
                                           };
        
        String[] itemDescr = new String[] { "Configuration Name",
                                          "Connection URL",
                                          "JNDI env",
                                          "List of Ports",
                                          "Host and Users"
                                        };
                                        
        
       return new CompositeType("TestComposite",
                                "Test Composite Type",
                                itemNames,
                                itemDescr,
                                itemTypes);
    }
    
    
    /**
     *
     */
    private CompositeType createTestSubCompositeType()
        throws Exception
    {
        String[] itemNames = new String[] { "host", "maxUsers"};
                                        
                                        
                                        
        OpenType[] itemTypes = new OpenType[] { SimpleType.STRING, SimpleType.STRING };
        
        String[] itemDescr = new String[] { "Host Name",
                                            "Max Users"};
                                        
        
       return new CompositeType("TestSubComposite",
                                "Test Sub Composite Type",
                                itemNames,
                                itemDescr,
                                itemTypes);
    }
}
