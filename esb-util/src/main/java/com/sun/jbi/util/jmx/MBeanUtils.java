/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)MBeanUtils.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.util.jmx;

import com.sun.jbi.util.LocalStringKeys;
import com.sun.jbi.util.StringTranslator;

import javax.management.MBeanServer;
import javax.management.ObjectName;
import javax.management.StandardMBean;

/**
 * Helper class for frequently used MBean housekeeping. This is a static
 * class that must have a reference to the MBean server currently being used.
 * Early in the initialization of the JBI framework, the MBean server reference
 * is set by the framework, before any other JBI services are initialized. This
 * ensures that the MBean server reference will always be available. While not
 * an ideal design, in the context of the JBI runtime, this is a workable
 * solution and prevents every caller from having to have a reference to the
 * MBean server.
 *
 * @author Sun Microsystems, Inc.
 */
public class MBeanUtils
{
    /**
     * Local handle to MBean Server.
     */
    private static MBeanServer sMBeanServer;

    /**
     * Local handle to StringTranslator.
     */
    private static StringTranslator sTranslator;

    /**
     * Initialize the static data. This method is called by the framework
     * during initialization so that all other code can use the other methods
     * in the class at any time. This is also used by the ProxyBinding as it
     * has its own classloader so it needs to initialize the same static data.
     * @param server The MBean Server reference to be used.
     */
    public static void init(MBeanServer server)
    {
        if ( null == server )
        {
            throw new java.lang.IllegalArgumentException(sTranslator.getString(
                LocalStringKeys.NULL_ARGUMENT, "server"));
        }

        sMBeanServer = server;
        sTranslator = new StringTranslator("com.sun.jbi.util", null);
    }

    /**
     * Create and register a standard MBean with the main MBean Server.
     * @param interfaceClass the MBean interface implemented by the instance.
     * @param instance the MBean implementation instance.
     * @param mbeanName the JMX ObjectName for the MBean.
     * @param replace indicates whether an existing registration should be
     * replaced or cause an error. If true, an existing registration should be
     * replaced; if false, an existing registration should cause an error.
     * @throws javax.jbi.JBIException If the MBean creation or registration
     * fails.
     */
    public static void registerStandardMBean(
        Class interfaceClass,
        Object instance,
        ObjectName mbeanName,
        boolean replace)
        throws javax.jbi.JBIException
    {
        if ( null == sMBeanServer )
        {
            return;
        }

        // Create a StandardMBean for the MBean instance.

        StandardMBean mbean = null;
        try
        {
            mbean = new StandardMBean(instance, interfaceClass);
        }
        catch ( javax.management.NotCompliantMBeanException ncEx )
        {
            throw new javax.jbi.JBIException(sTranslator.getString(
                LocalStringKeys.MBEAN_CREATION_NOT_JMX_COMPLIANT,
                mbeanName, instance.getClass().getName()));
        }

        // If an existing registration should be replaced, remove any existing
        // registration first.

        if ( replace )
        {
            if ( sMBeanServer.isRegistered(mbeanName) )
            {
                try
                {
                    sMBeanServer.unregisterMBean(mbeanName);
                }
                catch ( javax.management.InstanceNotFoundException infEx )
                {
                    ; // Just ignore this error
                }
                catch ( javax.management.MBeanRegistrationException mbrEx )
                {
                    String msg = mbrEx.getMessage();
                    Throwable cause = mbrEx.getCause();
                    String causeName = cause.getClass().getName();
                    throw new javax.jbi.JBIException(sTranslator.getString(
                        LocalStringKeys.MBEAN_UNREGISTRATION_EXCEPTION,
                        mbeanName, causeName, null == msg ? "" : msg), mbrEx);
                }
            }
        }

        // Register the MBean.

        try
        {
            sMBeanServer.registerMBean(mbean, mbeanName);
        }
        catch ( javax.management.InstanceAlreadyExistsException iaeEx )
        {
            throw new javax.jbi.JBIException(sTranslator.getString(
                LocalStringKeys.MBEAN_ALREADY_REGISTERED,
                mbeanName));
        }
        catch ( javax.management.MBeanRegistrationException mbrEx )
        {
            String msg = mbrEx.getMessage();
            Throwable cause = mbrEx.getCause();
            String causeName = cause.getClass().getName();
            throw new javax.jbi.JBIException(sTranslator.getString(
                LocalStringKeys.MBEAN_REGISTRATION_EXCEPTION,
                mbeanName, causeName, null == msg ? "" : msg), mbrEx);
        }
        catch ( javax.management.NotCompliantMBeanException ncEx )
        {
            throw new javax.jbi.JBIException(sTranslator.getString(
                LocalStringKeys.MBEAN_REGISTRATION_NOT_JMX_COMPLIANT,
                mbeanName, instance.getClass().getName()));
        }
    }

    /**
     * Unregister an MBean from the MBean server.
     * @param mbeanName the JMX ObjectName for the MBean.
     * @throws javax.jbi.JBIException if the unregistration fails.
     */
    public static void unregisterMBean(
        ObjectName mbeanName)
        throws javax.jbi.JBIException
    {
        if ( null == sMBeanServer )
        {
            return;
        }

        // Unregister the MBean

        try
        {
            sMBeanServer.unregisterMBean(mbeanName);
        }
        catch ( javax.management.InstanceNotFoundException infEx )
        {
            ; // Just ignore this error
        }
        catch ( javax.management.MBeanRegistrationException mbrEx )
        {
            String msg = mbrEx.getMessage();
            Throwable cause = mbrEx.getCause();
            String causeName = cause.getClass().getName();
            throw new javax.jbi.JBIException (sTranslator.getString(
                LocalStringKeys.MBEAN_UNREGISTRATION_EXCEPTION,
                mbeanName, causeName, null == msg ? "" : msg), mbrEx);
        }
    }

}
