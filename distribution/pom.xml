<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
    <parent>
        <artifactId>open-esb-top</artifactId>
        <groupId>net.open-esb.core</groupId>
        <version>3.2.5-SNAPSHOT</version>
    </parent>
       
    <modelVersion>4.0.0</modelVersion>
    <artifactId>core-distribution</artifactId>
    <packaging>pom</packaging>
    <name>core-distribution</name>

    <build>
        <plugins>
            <plugin>
                <artifactId>maven-antrun-plugin</artifactId>
                <executions>
                    <execution>
                        <id>generate-jbi-version</id>
                        <phase>initialize</phase>
                        <goals>
                            <goal>run</goal>
                        </goals>
                        <configuration>
                            <target>
                                <echo file="${final.build.directory}/jbi.ver" append="false">
                                    FULL_PRODUCT_NAME="Open Enterprise Service Bus"
                                    SHORT_PRODUCT_NAME="Open_ESB"
                                    FULL_VERSION="${project.version}"
                                    MAJOR_VERSION="${parsedVersion.majorVersion}"
                                    MINOR_VERSION="${parsedVersion.minorVersion}.${parsedVersion.incrementalVersion}-${parsedVersion.qualifier}"
                                    BUILD_NUMBER="${maven.build.timestamp}"
                                    CODELINE="${env.CODELINE}"
                                    TIMESTAMP="${maven.build.timestamp}"
                                </echo>
                                
                                <echo message="Making dir ${final.build.directory}/bin" />
                                <mkdir dir="${final.build.directory}/bin" />
                                <chmod dir="${final.build.directory}/bin" perm="0755" />

                                <echo message="Making dir ${final.build.directory}/lib" />
                                <mkdir dir="${final.build.directory}/lib" />
                                <chmod dir="${final.build.directory}/lib" perm="0755" />

                                <echo message="Making dir ${final.build.directory}/schemas" />
                                <mkdir dir="${final.build.directory}/schemas" />
                                <chmod dir="${final.build.directory}/schemas" perm="0755" />
                            </target>
                        </configuration>
                    </execution>
                    
                    <execution>
                        <id>copy-schemas</id>
                        <phase>package</phase>
                        <goals>
                            <goal>run</goal>
                        </goals>
                        <configuration>
                            <target>
                                <resources id="admin.schema">
                                    <fileset dir="${final.build.directory}/tmp/admin/schema">
                                        <include name="component-info-list*" />
                                        <include name="service-assembly-info-list*" />
                                        <exclude name="*.xml" />
                                    </fileset>
                                </resources>

                                <copy todir="${final.build.directory}/schemas" flatten="yes">
                                    <mappedresources>
                                        <resources refid="admin.schema" />
                                    </mappedresources>
                                </copy>
                                
                                <pathconvert pathsep="," property="admin.schema.chmod">
                                    <resources refid="admin.schema" />
                                    <regexpmapper from="^(.*)/([^/]*)$$" to="${final.build.directory}/schemas/\2" />
                                </pathconvert>

                                <chmod perm="0444">
                                    <files includes="${admin.schema.chmod}" />
                                </chmod>
                                
                                <resources id="manage.schema">
                                    <fileset dir="${final.build.directory}/tmp/manage/schemas">
                                        <include name="jbi.*" />
                                        <include name="componentConfiguration*" />
                                        <include name="componentIdentification*" />
                                        <include name="componentLogging*" />
                                        <include name="managementMessage*" />
                                        <include name="jbi-registry.xsd" />
                                    </fileset>
                                </resources>

                                <copy todir="${final.build.directory}/schemas" flatten="yes">
                                    <resources refid="manage.schema" />
                                </copy>

                                <pathconvert pathsep="," property="manage.schema.chmod">
                                    <resources refid="manage.schema" />
                                    <regexpmapper from="^(.*)/([^/]*)$$" to="${final.build.directory}/schemas/\2" />
                                </pathconvert>

                                <chmod perm="0444">
                                    <files includes="${manage.schema.chmod}" />
                                </chmod>
                            </target>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            
            <plugin>
                <artifactId>maven-assembly-plugin</artifactId>
                <version>2.4</version>
                <executions>
                    <execution>
                        <id>dist-assembly</id>
                        <phase>package</phase>
                        <goals>
                            <goal>single</goal>
                        </goals>
                        <configuration>
                            <appendAssemblyId>false</appendAssemblyId>
                            <descriptors>
                                <descriptor>src/assemble/distribution.xml</descriptor>
                            </descriptors>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <artifactId>maven-dependency-plugin</artifactId>
                <executions>
                    <execution>
                        <id>copy-dependencies</id>
                        <phase>prepare-package</phase>
                        <goals>
                            <goal>copy-dependencies</goal>
                        </goals>
                        <configuration>
                            <outputDirectory>${final.build.directory}/lib</outputDirectory>
                            <excludeTransitive>true</excludeTransitive>
                            <overWriteReleases>true</overWriteReleases>
                            <overWriteSnapshots>true</overWriteSnapshots>
                            <overWriteIfNewer>true</overWriteIfNewer>
                            <stripVersion>true</stripVersion>
                        </configuration>
                    </execution>
                    <execution>
                        <id>unpack-admin-schemas</id>
                        <phase>prepare-package</phase>
                        <goals>
                            <goal>unpack</goal>
                        </goals>
                        <configuration>
                            <artifactItems>
                                <artifactItem>
                                    <groupId>net.open-esb.core</groupId>
                                    <artifactId>jbi-admin-common</artifactId>
                                    <version>${project.version}</version>
                                    <type>jar</type>
                                    <overWrite>true</overWrite>
                                    <outputDirectory>${final.build.directory}/tmp/admin</outputDirectory>
                                    <includes>schema/*</includes>
                                </artifactItem>
                            </artifactItems>
                        </configuration>
                    </execution>
                    <execution>
                        <id>unpack-manage-schemas</id>
                        <phase>prepare-package</phase>
                        <goals>
                            <goal>unpack</goal>
                        </goals>
                        <configuration>
                            <artifactItems>
                                <artifactItem>
                                    <groupId>net.open-esb.core</groupId>
                                    <artifactId>manage</artifactId>
                                    <version>${project.version}</version>
                                    <type>jar</type>
                                    <classifier>resources</classifier>
                                    <overWrite>true</overWrite>
                                    <outputDirectory>${final.build.directory}/tmp/manage</outputDirectory>
                                </artifactItem>
                            </artifactItems>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>

    <dependencies>
        <!-- OpenESB dependencies-->
        <dependency>
            <groupId>net.open-esb.core</groupId>
            <artifactId>jbi</artifactId>
            <version>${project.version}</version>
        </dependency>
        <dependency>
            <groupId>net.open-esb.core</groupId>
            <artifactId>jbi-ext</artifactId>
            <version>${project.version}</version>
        </dependency>
        <dependency>
            <groupId>net.open-esb.core</groupId>
            <artifactId>jbi_rt</artifactId>
            <version>${project.version}</version>
        </dependency>
        <dependency>
            <groupId>net.open-esb.core</groupId>
            <artifactId>jbi_framework</artifactId>
            <version>${project.version}</version>
        </dependency>
        <dependency>
            <groupId>net.open-esb.core</groupId>
            <artifactId>jbi-ant-tasks</artifactId>
            <version>${project.version}</version>
        </dependency>
        <dependency>
            <groupId>net.open-esb.core</groupId>
            <artifactId>jbi-admin-common</artifactId>
            <version>${project.version}</version>
        </dependency>
        
        
        <!-- Other dependencies -->
        <dependency>
            <groupId>xmlbeans</groupId>
            <artifactId>xbean</artifactId>
        </dependency>
        <dependency>
            <groupId>javax.xml.bind</groupId>
            <artifactId>jsr173_api</artifactId>
        </dependency>
        <dependency>
            <groupId>javax.xml.bind</groupId>
            <artifactId>jaxb-api</artifactId>
        </dependency>
        <dependency>
            <groupId>javax.activation</groupId>
            <artifactId>activation</artifactId>
        </dependency>
        <dependency>
            <groupId>com.sun.xml.bind</groupId>
            <artifactId>jaxb-impl</artifactId>
        </dependency>
        <dependency>
            <groupId>jta</groupId>
            <artifactId>jta-1_1-classes</artifactId>
            <type>zip</type>
        </dependency>
    </dependencies>
    
    <properties>
        <final.build.directory>${project.build.directory}/distribution</final.build.directory>
    </properties>
    
    <profiles>
        <profile>
            <id>gravitee-release</id>
            <activation>
                <property>
                    <name>performRelease</name>
                    <value>true</value>
                </property>
            </activation>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-gpg-plugin</artifactId>
                        <executions>
                            <execution>
                                <id>sign-artifacts</id>
                                <phase>install</phase>
                                <goals>
                                    <goal>sign</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>
                </plugins>
            </build>
        </profile>
    </profiles>
</project>
