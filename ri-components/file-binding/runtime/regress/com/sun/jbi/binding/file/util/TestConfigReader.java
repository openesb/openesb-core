/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestConfigReader.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.file.util;

import com.sun.jbi.binding.file.EndpointBean;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.w3c.dom.Document;

/**
 * DOCUMENT ME!
 *
 * @author Sun Microsystems, Inc.
 */
public class TestConfigReader extends TestCase
{
    /**
     * DOCUMENT ME!
     */
    private ConfigFileValidator mValidator;

    /**
     * DOCUMENT ME!
     */
    private ConfigReader mConfigReader;

    /**
     * Creates a new TestConfigReader object.
     *
     * @param testName DOCUMENT ME!
     */
    public TestConfigReader(java.lang.String testName)
    {
        super(testName);
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public static Test suite()
    {
        TestSuite suite = new TestSuite(TestConfigReader.class);

        return suite;
    }

    /**
     * Sets up tests.
     */
    public void setUp()
    {
        mConfigReader = new ConfigReader();
        System.out.println("testInit");

        String srcroot = System.getProperty("junit.srcroot");

        try
        {
            mValidator =
                new ConfigFileValidator(srcroot
                    + "/binding/file/schema/endpoints.xsd",
                    srcroot + "/binding/file/config/endpoints.xml");
            mValidator.validate();
            mConfigReader.init(mValidator.getDocument());
        }
        catch (Exception jbiException)
        {
            jbiException.printStackTrace();
            fail("Cannot Init Config Reader");
        }
    }

    /**
     * Test of getBean method, of class
     * com.sun.jbi.binding.file.util.ConfigReader.
     */
    public void testGetBean()
    {
        System.out.println("testGetBean");

        String ep = "PullEndpoint";
        EndpointBean eb =
            mConfigReader.getBean("OnlineStockServicestockEndpoint");
        assertNotNull("Endpoint Bean is Null ", eb);
        assertEquals("Invalid Bean object ", eb.getValue("endpoint-name"),
            "stockEndpoint");
    }

    /**
     * Test of getEndpoint method, of class
     * com.sun.jbi.binding.file.util.ConfigReader.
     */
    public void testGetEndpoint()
    {
        System.out.println("testGetEndpoint");
        assertNotNull("Endpointlist is null", mConfigReader.getEndpoint());
    }

    /**
     * Test of getEndpointCount method, of class
     * com.sun.jbi.binding.file.util.ConfigReader.
     */
    public void testGetEndpointCount()
    {
        System.out.println("testGetEndpointCount");
        assertEquals("Failed getting endpoint count ",
            mConfigReader.getEndpointCount(), 1);
    }

    /**
     * Test of init method, of class
     * com.sun.jbi.binding.file.util.ConfigReader.
     */
    public void testInit()
    {
        System.out.println("testInit");
    }

    // Add test methods here, they have to start with 'test' name.
    // for example:
    // public void testHello() {}
}
