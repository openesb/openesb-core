/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)EndpointBean.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.file;

import com.sun.jbi.binding.file.util.ConfigData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

import javax.jbi.servicedesc.ServiceEndpoint;

import javax.xml.namespace.QName;


/**
 * This Bean object holds all the attributes of an endpoint.
 *
 * @author Sun Microsystems, Inc.
 */
public final class EndpointBean
{
    /**
     * File thread corresponding to this endpoint.
     */
    private FileThreads mFileThreads;

    /**
     * Table that stores the opreation and corresponding MEP.
     */
    private HashMap mMessagePatterns = null;

    /**
     * Table for storing the xml tags and values  present in the config file.
     * This table stores  throws filebinding sepcific configuration
     * information correspondint to each endpoint.
     */
    private Hashtable mConfigTable = null;

    /**
     * Operations.
     */
    private List mOperations = null;

    /**
     * Endpoint Reference as returned by the NMS.
     */
    private ServiceEndpoint mServiceEndpoint;

    /**
     * Deployment Id.
     */
    private String mDeploymentId;

    /**
     * Receiver thread.
     */
    private Thread mThread;

    /**
     * Denotes if an endpoint is consumer or provider
     */
    private int mRole;

    private Object mDefinition;
    
    private String mDeploymentType;
    /**
     * Constructor. Creates a new Table, list.
     */
    public EndpointBean()
    {
        mConfigTable = new Hashtable();
        mOperations = new ArrayList();
        mMessagePatterns = new HashMap();
    }
    /**
     * Gets a unique combination key
     * 
     * @return key
     */
    public String getKey()
    {     
        String servicenamespace = getValue(ConfigData.SERVICE_NAMESPACE);
        String servicename = getValue(ConfigData.SERVICE_LOCALNAME);
        String interfacenamespace = getValue(ConfigData.INTERFACE_NAMESPACE);        
        String interfacename = getValue(ConfigData.INTERFACE_LOCALNAME);
        String endpointname = getValue(ConfigData.ENDPOINTNAME);   
        
        QName serqname = new QName(servicenamespace, servicename);
        QName interqname = new QName(interfacenamespace, interfacename);
        
        if (mRole == ConfigData.PROVIDER)
        {
            return serqname.toString() + interqname.toString() + endpointname
                    + ConfigData.PROVIDER_STRING;
        }
        else
        {            
            return serqname.toString() + interqname.toString() + endpointname
                    + ConfigData.CONSUMER_STRING;
        }        
    }

    public void setWsdlDefintion(Object def)
    {
        mDefinition = def;
    }
    
    public Object getWsdlDefinition()
    {
        return mDefinition;
    }
    
    /**
     * Returns the default operation, which is normally the first operation
     * defined in the XML or WSDL.
     *
     * @return QName name of the operation.
     */
    public QName getDefaultOperation()
    {
        QName operation = null;

        try
        {
            OperationBean op = (OperationBean) mOperations.get(0);
            operation = new QName(op.getNamespace(), op.getName());
        }
        catch (Exception e)
        {
            ;
        }

        return operation;
    }

    /**
     * Sets the deployment Id corresponding to this endpoint Bean.
     *
     * @param suId Service unit Id.
     */
    public void setDeploymentId(String suId)
    {
        mDeploymentId = suId;
    }

    /**
     * Fetches the deployment Id coresponding to this bean.
     *
     * @return Service unit ID.
     */
    public String getDeploymentId()
    {
        return mDeploymentId;
    }

    /**
     * Gets the file extension corresponding to an operation.
     *
     * @param operation operation name.
     *
     * @return file extension corresponding to operation.
     */
    public String getExtension(String operation)
    {
        String ext = null;
        Iterator iter = mOperations.iterator();

        while (iter.hasNext())
        {
            OperationBean op = (OperationBean) iter.next();

            if (op.getName().trim().equals(operation))
            {
                ext = op.getFileExtension();

                break;
            }
        }

        return ext;
    }

    /**
     * Sets the file thread.
     *
     * @param ft File thread object.
     */
    public void setFileThread(FileThreads ft)
    {
        mFileThreads = ft;
    }

    /**
     * Gets the file thread.
     *
     * @return file thread object.
     */
    public FileThreads getFileThread()
    {
        return mFileThreads;
    }

    /**
     * Returns the input type corresponding to an operation name.
     *
     * @param operation operation name.
     *
     * @return input message type for the operation.
     */
    public String getInputType(String operation)
    {
        String inputtype = null;
        Iterator iter = mOperations.iterator();

        while (iter.hasNext())
        {
            OperationBean op = (OperationBean) iter.next();

            if (op.getName().trim().equals(operation))
            {
                inputtype = op.getInputType();

                break;
            }
        }

        return inputtype;
    }

    /**
     * Gets the MEP corresponding to the operation name.
     *
     * @param operation operation name.
     *
     * @return String representing the operation , if not present then null
     */
    public String getMEP(String operation)
    {
        String mep = null;
        Iterator iter = mOperations.iterator();

        while (iter.hasNext())
        {
            OperationBean op = (OperationBean) iter.next();

            if (op.getName().trim().equals(operation))
            {
                mep = op.getMep();

                break;
            }
        }

        return mep;
    }

    /**
     * Gets the operation name corresponding to the input type in WSDL.
     *
     * @param input name of the input type.
     *
     * @return String representing the operation , if not present then null
     */
    public QName getOperation(String input)
    {
        QName operation = null;

        Iterator iter = mOperations.iterator();

        while (iter.hasNext())
        {
            OperationBean op = (OperationBean) iter.next();

            if (op.getQName().trim().equals(input))
            {
                operation = new QName(op.getNamespace(), op.getName());

                break;
            }
        }

        return operation;
    }

    /**
     * Gets the fully qualified name of operation given the position in  XML or
     * WSDL.
     *
     * @param index occurence of operation inXML or WSDL.
     *
     * @return the QName of operation.
     */
    public QName getOperationQName(int index)
    {
        String local = ((OperationBean) mOperations.get(index)).getName();
        String namespace =
            ((OperationBean) mOperations.get(index)).getNamespace();

        return new QName(namespace, local);
    }

    /**
     * Returns the number of operations in XML or WSDL.
     *
     * @return number of operations for this endpoint.
     */
    public int getOperationsCount()
    {
        return mOperations.size();
    }

    /**
     * Returns the output type corresponding to an operation.
     *
     * @param operation operation name.
     *
     * @return output message type.
     */
    public String getOutputType(String operation)
    {
        String outputtype = null;
        Iterator iter = mOperations.iterator();

        while (iter.hasNext())
        {
            OperationBean op = (OperationBean) iter.next();

            if (op.getName().trim().equals(operation))
            {
                outputtype = op.getOutputType();

                break;
            }
        }

        return outputtype;
    }

    /**
     * Gets the file prefix corresponding to an operation.
     *
     * @param operation operation name.
     *
     * @return output file prefix.
     */
    public String getPrefix(String operation)
    {
        String prefix = null;
        Iterator iter = mOperations.iterator();

        while (iter.hasNext())
        {
            OperationBean op = (OperationBean) iter.next();

            if (op.getName().trim().equals(operation))
            {
                prefix = op.getOutputFilePrefix();

                break;
            }
        }

        return prefix;
    }

    /**
     * Sets the receiver thread.
     *
     * @param t thread
     */
    public void setReceiverThread(Thread t)
    {
        mThread = t;
    }

    /**
     * Returns the receiver thread.
     *
     * @return receiver thread object.
     */
    public Thread getReceiverThread()
    {
        return mThread;
    }

    /**
     * Sets the role of this endpoint, provider or consumer.
     *
     * @param role should be provider or comsumer, 0 or 1.
     */
    public void setRole(int role)
    {
        if ((role != ConfigData.PROVIDER) && (role != ConfigData.CONSUMER))
        {
            mRole = ConfigData.CONSUMER;
        }
        else
        {
            mRole = role;
        }
    }

    /**
     * Returns the role.
     *
     * @return role
     */
    public int getRole()
    {
        return mRole;
    }

    /**
     * Sets the endpoint reference once the endpoitn has been activated  with
     * the NMS.
     *
     * @param ref endpoint reference
     */
    public void setServiceEndpoint(ServiceEndpoint ref)
    {
        mServiceEndpoint = ref;
    }

    /**
     * Gets the enpoint reference corresponding to this endpoint.
     *
     * @return endpoint reference.
     */
    public ServiceEndpoint getServiceEndpoint()
    {
        return mServiceEndpoint;
    }

    /**
     * Returns a unique name which is a combination of service name and
     * endpoint name.
     *
     * @return unique name of this endpoint. Its a combination of service  name
     *         and endpoint name.
     */
    public String getUniqueName()
    {
        String servicenamespace = getValue(ConfigData.SERVICE_NAMESPACE);
        String servicename = getValue(ConfigData.SERVICE_LOCALNAME);
        String endpointname = getValue(ConfigData.ENDPOINTNAME);

        if ((servicenamespace != null) && (endpointname != null))
        {
            if (servicenamespace.trim().equals(""))
            {
                return servicename + endpointname;
            }
            else
            {
                return "{" + servicenamespace + "}" + servicename
                + endpointname;
            }
        }

        return null;
    }

    /**
     * Sets the value for the key in the table.
     *
     * @param key for which value has to be retrieved.
     * @param value corresponding to the key
     */
    public void setValue(
        String key,
        String value)
    {
        if (key == null)
        {
            return;
        }

        if (value == null)
        {
            value = "";
        }

        mConfigTable.put(key, value);
    }

    /**
     * Returns the value associated with the key from the table.
     *
     * @param key the tag name for which value is required.
     *
     * @return value corresponding to the tag as in config file.
     */
    public String getValue(String key)
    {
        if (key == null)
        {
            return null;
        }

        return (String) mConfigTable.get(key);
    }

    /**
     * Adds an entry for operation-mep in table.
     *
     * @param oper operation name.
     * @param mep corresponding MEP.
     */
    public void addMessagePattern(
        String oper,
        String mep)
    {
        try
        {
            mMessagePatterns.put(oper, mep);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Adds an operation to the endpoint.
     *
     * @param namespace namespace uri
     * @param name local name
     * @param mep exchange pattern.
     * @param input input message type.
     * @param output output message type.
     * @param ext file extension.
     * @param prefix output file prefix.
     */
    public void addOperation(
        String namespace,
        String name,
        String mep,
        String input,
        String output,
        String ext,
        String prefix)
    {
        OperationBean op =
            new OperationBean(namespace, name, mep, input, output, ext, prefix);
        mOperations.add(op);
    }
    
    /**
     * Getter for property mDeploymentType.
     * @return Value of property mDeploymentType.
     */
    public java.lang.String getDeploymentType() {
        return mDeploymentType;
    }
    
    /**
     * Setter for property mDeploymentType.
     * @param mDeploymentType New value of property mDeploymentType.
     */
    public void setDeploymentType(java.lang.String mDeploymentType) {
        this.mDeploymentType = mDeploymentType;
    }
    
}
