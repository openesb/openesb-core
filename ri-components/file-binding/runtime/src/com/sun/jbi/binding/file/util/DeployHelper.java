/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)DeployHelper.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.file.util;

import com.sun.jbi.binding.file.DeploymentRegistry;
import com.sun.jbi.binding.file.EndpointBean;
import com.sun.jbi.binding.file.FileBindingContext;
import com.sun.jbi.binding.file.FileBindingResolver;
import com.sun.jbi.binding.file.FileBindingResources;

import org.w3c.dom.Document;

import java.io.File;

import java.util.logging.Logger;

import javax.xml.namespace.QName;


/**
 * This class ia  a Helper clas to do deployment. This class does the work of
 * deploying, and registering the endpoint in the in memory deployment cache.
 *
 * @author Sun Microsystems, Inc.
 */
public class DeployHelper
    extends UtilBase
    implements FileBindingResources
{
    /**
     * Reader object to read config XML file.
     */
    private ConfigReader mConfigReader;

    /**
     * Deploy descriptor reader
     */
    private DeployDescriptorReader mDeployDescriptorReader;

    /**
     * Resolver object.
     */
    private FileBindingResolver mResolver;

    /**
     * Logger object.
     */
    private Logger mLog;

    /**
     * Deployment descriptor schema.
     */
    private String mDDSchema;

    /**
     * Jbi.xml deployment descriptor file.
     */
    private String mDeploymentDescriptor;

    /**
     * Name of the endpoint config XMl file.
     */
    private String mEndpointFile;

    /**
     * Application Sub assembly of deployment.
     */
    private String mSUid;

    /**
     * Name of the schema file.
     */
    private String mSchemaFile;

    /**
     * Wsdl file name.
     */
    private String mWsdlFile;

    /**
     * Helper class for i18n.
     */
    private StringTranslator mTranslator;

    /**
     * Wsdl reader object.
     */
    private WSDLFileReader mWsdlReader;
    
    /**
     * WSDL 1.1 reader
     */
    private WSDL11FileReader mWsdl11Reader;

    /**
     * List of endpoints.
     */
    private EndpointBean [] mEndpointList;

    /**
     * Creates a new DeployHelper object.
     *
     * @param suId application sub assembly Id.
     * @param suPath path of SU.
     * @param reslv RESOLVER
     */
    public DeployHelper(
        String suId,
        String suPath,
        FileBindingResolver reslv)
    {
        mSUid = suId;
        mResolver = reslv;
        mEndpointFile = FileListing.getXMLFile(suPath);
        mWsdlFile = FileListing.getWSDLFile(suPath);
        mDeploymentDescriptor =
            suPath + File.separatorChar + "META-INF" + File.separatorChar
            + ConfigData.DEPLOY_DESCRIPTOR;
        mDDSchema =
            FileBindingContext.getInstance().getContext().getInstallRoot()
            + File.separatorChar + ConfigData.JBI_SCHEMA_FILE;
        mSchemaFile =
            FileBindingContext.getInstance().getContext().getInstallRoot()
            + File.separatorChar + ConfigData.SCHEMA_FILE;

        mLog = FileBindingContext.getInstance().getLogger();
        mTranslator = new StringTranslator();
        setValid(true);
    }

    /**
     * Returns the consumer endpoint count.
     *
     * @return  consumer endpoints.
     */
    public int getConsumerCount()
    {
        int count = 0;

        for (int i = 0; i < mEndpointList.length; i++)
        {
            EndpointBean eb = mEndpointList[i];

            if (eb.getRole() == ConfigData.CONSUMER)
            {
                count++;
            }
        }

        return count;
    }

    /**
     * Returns the number of provider endpoints in the artifacts file.
     *
     * @return  provider endpoint count.
     */
    public int getProviderCount()
    {
        int count = 0;

        for (int i = 0; i < mEndpointList.length; i++)
        {
            EndpointBean eb = mEndpointList[i];

            if (eb.getRole() == ConfigData.PROVIDER)
            {
                count++;
            }
        }

        return count;
    }
    /**
     * Deployment for a WSDL 1.1 file.
     */
    private void doWSDL11Deploy()
    {
        WSDL11FileValidator validator = new WSDL11FileValidator(mWsdlFile);
        validator.validate();

        javax.wsdl.Definition d = validator.getDocument();

        if ((!validator.isValid()) || (d == null))
        {
            mLog.severe(mTranslator.getString(FBC_INVALID_WSDL_FILE, mWsdlFile));
            setError(mTranslator.getString(FBC_INVALID_WSDL_FILE, mWsdlFile)
                + " " + validator.getError());
            setValid(false);

            return;
        }
        mWsdl11Reader = new WSDL11FileReader(mResolver, mWsdlFile);
        mWsdl11Reader.init(d);

        if (!mWsdl11Reader.isValid())
        {
            mLog.severe(mTranslator.getString(FBC_INVALID_WSDL_FILE,
                    mWsdlFile));
            setError(mTranslator.getString(FBC_INVALID_WSDL_FILE,
                    mWsdlFile) + " " + mWsdl11Reader.getError());

            return;
        }

        setWarning(mWsdl11Reader.getWarning());

        mEndpointList = mWsdl11Reader.getEndpoint();

        for (int i = 0; i < mEndpointList.length; i++)
        {
            mEndpointList[i].setDeploymentId(mSUid);
        }
    }
    
    /**
     * Checks the consistency of information between the jbi.xml and
     * a configuration file.
     */  
    
    private void checkConsistency()
    {
        
        
        if (getProviderCount() < mDeployDescriptorReader.getProviderCount())
        {
            setError(mTranslator.getString(FBC_INCONSISTENT_PROVIDER_DATA));

            return;
        }

        if (getConsumerCount() < mDeployDescriptorReader.getConsumerCount())
        {
            setError(mTranslator.getString(FBC_INCONSISTENT_CONSUMER_DATA));

            return;
        }
        
        java.util.List keylist = mDeployDescriptorReader.getKeyList();
        
        if (mEndpointList != null)
        {
            for (int j = 0; j < keylist.size(); j++)
            {
                String key = (String) keylist.get(j);
                boolean found = false; 
                for (int i = 0; i < mEndpointList.length; i++)
                {
                    if (mEndpointList[i].getKey().equals(key))
                    {
                        found = true;                        
                    }
                }
                if (!found)
                {
                    setError(mTranslator.getString(FBC_DD_DATA_NOT_FOUND, key));
                }
            
            }
        }
    }
    /**
     * Method which does the actual deployment.
     *
     * @param isDeploy denotes if its during deployment or start.
     */
    public void doDeploy(boolean isDeploy)
    {
        try
        {
         super.clear();
       
            if ((mDeploymentDescriptor != null)
                    && (new File(mDeploymentDescriptor)).exists())
            {
                doDDParsing();                
                if ((!isValid()) && isDeploy)
                {
                    return;
                } 
            }
            
            String type = "WSDL20";
            
            if (mDeployDescriptorReader != null)
            {
                type = mDeployDescriptorReader.getType();
            }         
         
         
            if (type.equalsIgnoreCase("WSDL20") &&
                (mWsdlFile != null) && (new File(mWsdlFile)).exists())
            {
                doWSDLDeploy();
            }
            else if (type.equalsIgnoreCase("WSDL11") &&
                (mWsdlFile != null) && (new File(mWsdlFile)).exists())
            {
                doWSDL11Deploy();
            }
            else if ((mEndpointFile != null)
                    && (new File(mEndpointFile)).exists())
            {
                doConfigDeploy();
            }           
            else
            {
                setError(mTranslator.getString(FBC_ARTIFACT_NOT_FOUND));
                mLog.info(mTranslator.getString(FBC_ARTIFACT_NOT_FOUND));
                return;
            }
            
            if (mDeployDescriptorReader != null)
            {
                if (isValid())
                {
                    checkConsistency();
                }         
            }
            /**
             * Here we have a violation of spec 1.0. The spec says that
             * a jbi.xml must be present in an SU, 
             * But we are making it optional here, just to make a backward
             * compatibility.  
             * This optionality will be made mandatory in future.
             *  
             */
            
           /*
            else
            {
                setError(mTranslator.getString(FBC_NO_DD) + "\n" + getError());
                mLog.info(mTranslator.getString(FBC_NO_DD));
                setValid(false);

                return;
            }
            */

            if ((!isValid()) && isDeploy)
            {
                return;
            }
            registerEndpoints();

        }
        catch (Exception de)
        {
            setError(getError() + de.getMessage());
            setException(de);
            de.printStackTrace();
        }
    }

    /**
     * Registers a bean and service name with registry.
     */
    public void registerEndpoints()
    {
        DeploymentRegistry dr = DeploymentRegistry.getInstance();

        for (int i = 0; i < mEndpointList.length; i++)
        {
            String ser = null;

            if (mEndpointList[i].getRole() == ConfigData.PROVIDER)
            {
                ser = mEndpointList[i].getUniqueName();
            }
            else if (mEndpointList[i].getRole() == ConfigData.CONSUMER)
            {
                QName qnam =
                    new QName(mEndpointList[i].getValue(
                            ConfigData.SERVICE_NAMESPACE),
                        mEndpointList[i].getValue(ConfigData.SERVICE_LOCALNAME));
                ser = qnam.toString();
            }

            if (dr.isDeployed(ser))
            {
                mLog.warning(mTranslator.getString(FBC_DUPLICATE_ENDPOINT, ser));
                setWarning(mTranslator.getString(FBC_DUPLICATE_ENDPOINT, ser));

                continue;
            }

            dr.registerEndpoint(ser, mEndpointList[i]);
        }
    }

    /**
     * Verifies the registry for duplicate entries.
     */
    private void checkRegistry()
    {
        DeploymentRegistry dr = DeploymentRegistry.getInstance();

        for (int i = 0; i < mEndpointList.length; i++)
        {
            String ser = mEndpointList[i].getUniqueName();

            if (dr.isDeployed(ser))
            {
                mLog.warning(mTranslator.getString(FBC_DUPLICATE_ENDPOINT, ser));
            }
        }
    }

    /**
     * Perform deployment of configuration XML file.
     */
    private void doConfigDeploy()
    {
        ConfigFileValidator validator =
            new ConfigFileValidator(mSchemaFile, mEndpointFile);
        validator.setValidating();
        validator.validate();

        Document d = validator.getDocument();

        if ((!validator.isValid()) || (d == null))
        {
            mLog.severe(mTranslator.getString(FBC_INVALID_CONFIG_FILE,
                    mEndpointFile));
            setError(mTranslator.getString(FBC_INVALID_CONFIG_FILE,
                    mEndpointFile) + " " + validator.getError());
            setValid(false);

            return;
        }

        mConfigReader = new ConfigReader();
        mConfigReader.init(d);

        if (!mConfigReader.isValid())
        {
            mLog.severe(mTranslator.getString(FBC_INVALID_CONFIG_FILE,
                    mEndpointFile));
            setError(mTranslator.getString(FBC_INVALID_CONFIG_FILE,
                    mEndpointFile) + " " + mConfigReader.getError());
            setValid(false);

            return;
        }

        mEndpointList = mConfigReader.getEndpoint();

        for (int i = 0; i < mEndpointList.length; i++)
        {
            mEndpointList[i].setDeploymentId(mSUid);
        }
    }

    /**
     * Parses the deployment descriptor jbi.xml.
     */
    private void doDDParsing()
    {
        ConfigFileValidator validator =
            new ConfigFileValidator(mDDSchema, mDeploymentDescriptor);
        validator.setValidating();
        validator.validate();

        Document d = validator.getDocument();

        if ((!validator.isValid()) || (d == null))
        {
            mLog.severe(mTranslator.getString(FBC_INVALID_DD,
                    mDeploymentDescriptor));
            setError(mTranslator.getString(FBC_INVALID_DD, mDeploymentDescriptor)
                + " " + validator.getError());
            setValid(false);

            return;
        }

        /**
         * Match service names in artifacts file ( endpoints.xml) and the DD.
         * Every endpoint in the the DD file should be present in the
         * artifacts file. We dont care if the artifacts file has more
         * endpoints. Those will be ignored. The condition is the DD XML file
         * should be a sub-set of artifacts file. The spec does not govern
         * this logic so it is upto the component to decide how to veryify
         * consistence between  DD and its artifacts.
         */
        /**
         * A valid question that arises here is why are we having 2 XML files
         * to decorate an endpoint. The artifacts XML file is the older form
         * which was used when the  spec did not accomodate any DD for an SU.
         * So that is still around.  Any component specific decoration for the
         * endpoints can be done  through the DD jbi.xml also. And that would
         * be the correct way to do it. We dont do it that way because there
         * are other modules that might           depend on the endpoints.xml
         * file, so to maintain their functionality  the older endpoints.xml
         * file is still used to decorate endpoints.
         */
        mDeployDescriptorReader = new DeployDescriptorReader();
        mDeployDescriptorReader.init(d);

        if (!mDeployDescriptorReader.isValid())
        {
            mLog.severe(mTranslator.getString(FBC_INVALID_DD, mEndpointFile));
            setError(mTranslator.getString(FBC_INVALID_DD, mEndpointFile) + " "
                + mDeployDescriptorReader.getError());
            setValid(false);

            return;
        }
    }

    /**
     * Perform WSDL file deployment.
     */
    private void doWSDLDeploy()
    {
        WSDLFileValidator validator = new WSDLFileValidator(mWsdlFile);
        validator.validate();

        com.sun.jbi.wsdl2.Description d = validator.getDocument();

        if ((!validator.isValid()) || (d == null))
        {
            mLog.severe(mTranslator.getString(FBC_INVALID_WSDL_FILE, mWsdlFile));
            setError(mTranslator.getString(FBC_INVALID_WSDL_FILE, mWsdlFile)
                + " " + validator.getError());
            setValid(false);

            return;
        }

        mWsdlReader = new WSDLFileReader(mResolver);
        mWsdlReader.init(d);

        if (!mWsdlReader.isValid())
        {
            mLog.severe(mTranslator.getString(FBC_INVALID_WSDL_FILE, mWsdlFile));
            setError(mTranslator.getString(FBC_INVALID_WSDL_FILE, mWsdlFile)
                + " " + mWsdlReader.getError());
            setValid(false);

            return;
        }

        mEndpointList = mWsdlReader.getEndpoint();

        for (int i = 0; i < mEndpointList.length; i++)
        {
            mEndpointList[i].setDeploymentId(mSUid);
        }
    }
}
