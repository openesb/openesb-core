/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)WSDL11FileReader.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.file.util;

import com.sun.jbi.binding.file.EndpointBean;
import com.sun.jbi.binding.file.FileBindingResolver;
import com.sun.jbi.binding.file.FileBindingResources;

import com.sun.jbi.binding.file.MessageProcessor;



import javax.wsdl.Binding;
import javax.wsdl.Definition;
import javax.wsdl.OperationType;
import javax.wsdl.Port;
import javax.wsdl.PortType;
import javax.wsdl.extensions.UnknownExtensibilityElement;
import javax.wsdl.factory.WSDLFactory;
import javax.wsdl.xml.WSDLReader;

import org.w3c.dom.DocumentFragment;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Element;

import java.io.File;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;
import java.util.Iterator;
import java.util.Collection;

import java.util.Map;

import javax.xml.namespace.QName;


/**
 * Reads the configuration WSDL file and loads the data into the EndpointBean
 * objects.
 *
 * @author Sun Microsystems, Inc.
 */
public class WSDL11FileReader
    extends UtilBase
    implements FileBindingResources
{
    /**
     * WSDL Definitions object
     */
    private Definition mDefinition;

    /**
     * Resolver object.
     */
    private FileBindingResolver mResolver;

    /**
     * Helper class for i18n.
     */
    private StringTranslator mTranslator;

    /**
     * The list of endpoints as configured in the config file. This list
     * contains  all the endpoints and their attibutes
     */
    private EndpointBean [] mEndpointInfoList = null;

    /**
     * The total number of end points in the config file
     */
    private int mTotalEndpoints = 0;
    
    /**
     * Wsdl 1.1 file name
     */
    private String mWsdlFile;

    /**
     * Creates a new WSDL11FileReader object.
     *
     * @param rslv resolver object
     */
    public WSDL11FileReader(FileBindingResolver rslv, String wsdlfile)
    {
        setValid(true);
        mResolver = rslv;
        mWsdlFile = wsdlfile;
        mTranslator = new StringTranslator();
    }

    /**
     * Returns the Bean object corresponding to the endpoint.
     *
     * @param endpoint endpoint name.
     *
     * @return endpoint Bean object.
     */
    public EndpointBean getBean(String endpoint)
    {
        /*Search for the bean corresponding to the service and endpoint name
         */
        for (int j = 0; j < mEndpointInfoList.length; j++)
        {
            String tmp = mEndpointInfoList[j].getUniqueName();

            if (tmp.trim().equals(endpoint))
            {
                return mEndpointInfoList[j];
            }
        }

        return null;
    }

    /**
     * Gets the endpoint list corresponding to a config file.
     *
     * @return End point Bean list
     */
    public EndpointBean [] getEndpoint()
    {
        return mEndpointInfoList;
    }

    /**
     * Returns the total number of endopoints in the wsld file.
     *
     * @return int number of endpoints.
     */
    public int getEndpointCount()
    {
        return mTotalEndpoints;
    }

    /**
     * Initializes the config file and loads services.
     *
     * @param doc Name of the config file.
     */
    public void init(Definition doc)
    {
        mDefinition = doc;
        loadServicesList();
    }

    /**
     * Util method that gets the  attribute value.
     *
     * @param map attribute node map
     * @param attr attribute name
     *
     * @return attribute value
     */
    private String getAttrValue(
        NamedNodeMap map,
        String attr)
    {
        Node attrnode = map.getNamedItem(attr);
        String value = null;

        try
        {
            value = attrnode.getNodeValue();
        }
        catch (Exception e)
        {
            ;
        }

        return value;
    }

    /**
     * Sets the direction to inbound or outbound.
     *
     * @param map node map.
     * @param eb endpoint bean.
     */
    private void setDirection(
        NamedNodeMap map,
        EndpointBean eb)
    {
        String val = getValue(map, ConfigData.ENDPOINT_TYPE);

        if (val.trim().equalsIgnoreCase(ConfigData.PROVIDER_STRING))
        {
            eb.setRole(ConfigData.PROVIDER);
            eb.setValue(ConfigData.ENDPOINT_TYPE, val);
        }
        else if (val.trim().equalsIgnoreCase(ConfigData.CONSUMER_STRING))
        {
            eb.setRole(ConfigData.CONSUMER);
            eb.setValue(ConfigData.ENDPOINT_TYPE, val);
        }
        else
        {
            setError(getError() + "\n\t"
                + mTranslator.getString(FBC_WSDL_INVALID_DIRECTION, val));
            eb.setValue(ConfigData.ENDPOINT_TYPE, "");
            setValid(false);

            return;
        }
    }

   /**
     * Checks if a binging is file binding
     *
     * @param df Document fragment
     *
     * @return true if file binding
     */
    private boolean isFileBinding(Binding aBinding)
    {

        java.util.List extList = aBinding.getExtensibilityElements();
        List boper = aBinding.getBindingOperations();
        if (extList != null)
        {
            for (int i = 0; i < extList.size(); i++)
            {
                if (extList.get(i) instanceof javax.wsdl.extensions.UnknownExtensibilityElement)
                {
                    UnknownExtensibilityElement sb =
                        (UnknownExtensibilityElement) extList.get(i);

                    if ((sb.getElementType().getNamespaceURI().equals(ConfigData.FILENAMESPACE))
                            && (sb.getElementType().getLocalPart().equals("binding")))
                    {
                        return true;
                    }
                }
            }
        }
        return false;
    }


    /**
     * Sets the input pattern.
     *
     * @param map Map containing endpoint info.
     * @param eb Bean attributes.
     */
    private void setInputPattern(
        NamedNodeMap map,
        EndpointBean eb)
    {
        String val = getValue(map, ConfigData.INPUTPATTERN);

        if (val == null)
        {
            val = "";
        }

        eb.setValue(ConfigData.INPUTPATTERN, val);
    }

    /**
     * Sets the input, output and processed location.
     *
     * @param map Map containing enpoint info
     * @param eb bean attributes.
     * @param attribute attribute name.
     * @param configdata config data.
     */
    private void setLocation(
        NamedNodeMap map,
        EndpointBean eb,
        String attribute,
        String configdata)
    {
        String val = getValue(map, attribute);

        File f = null;

        try
        {
            f = new File(new URI(val));
        }
        catch (IllegalArgumentException ie)
        {
            setValid(false);
            setError(getError() + " "
                + mTranslator.getString(FBC_NOT_VALID, val) + "\n"
                + ie.getMessage());
            setException(ie);

            return;
        }
        catch (URISyntaxException use)
        {
            setValid(false);
            setError(getError() + "\n\t"
                + mTranslator.getString(FBC_WSDL_INVALID_URI, val) + "\n"
                + use.getMessage());
            setException(use);

            return;
        }

        eb.setValue(configdata, f.getAbsolutePath());
    }

    
    /**
     * DOCUMENT ME!
     *
     * @param inputext
     *
     * @return
     */
    private String getMessageType(List inputext)
    {
        String type = null;

        try
        {
            for (int i = 0; i < inputext.size(); i++)
            {
                Object obj = inputext.get(i);

                if (obj instanceof UnknownExtensibilityElement)
                {
                    UnknownExtensibilityElement fileelement =
                        (UnknownExtensibilityElement) obj;

                    if (fileelement.getElementType().getNamespaceURI().equals(ConfigData.FILENAMESPACE)
                            && fileelement.getElementType().getLocalPart()
                                             .equals("message"))
                    {
                        type =
                            getValue(fileelement.getElement().getAttributes(),
                                "type");
                    }
                }
            }
        }
        catch (Exception e)
        {
            return null;
        }

        return type;
    }
    
    private String getOperationExt(List  inputext, String attr)
    {
         String type = null;

        try
        {
            for (int i = 0; i < inputext.size(); i++)
            {
                Object obj = inputext.get(i);

                if (obj instanceof UnknownExtensibilityElement)
                {
                    UnknownExtensibilityElement fileelement =
                        (UnknownExtensibilityElement) obj;

                    if (fileelement.getElementType().getNamespaceURI().equals(ConfigData.FILENAMESPACE)
                            && fileelement.getElementType().getLocalPart()
                                             .equals("artifacts"))
                    {
                        type =
                            getValue(fileelement.getElement().getAttributes(),
                                attr);
                    }
                }
            }
        }
        catch (Exception e)
        {
            return null;
        }

        return type;       
        
    }
    
    /**
     * Sets operations from the WSLD doc.
     *
     * @param eb Endpoint bean
     * @param b binding
     */
    private void setOperations(
        EndpointBean eb,
        Binding b)
    {
       try
        {
            List bopers = b.getBindingOperations();
            PortType bindingif = b.getPortType();
            
            Iterator boperiter = null;

            if (bopers != null)
            {
                boperiter = bopers.iterator();
            }

            while (boperiter.hasNext())
            {
                javax.wsdl.BindingOperation boper =
                    (javax.wsdl.BindingOperation) boperiter.next();
                String bindingopername = boper.getOperation().getName();
                String input = null;
                String output = null;
                String mep = null;
                String outfileext = null;
                String outprefix  = null;
                List inputext = null;
                
                outfileext = getOperationExt(boper.getExtensibilityElements(), 
                                                    ConfigData.OUTPUTEXTENSION);
                
                outprefix = getOperationExt(boper.getExtensibilityElements(), 
                                                    ConfigData.OUTPUTPREFIX);
                
                if (!boper.getOperation().getStyle().equals(OperationType.NOTIFICATION))
                {
                    inputext = boper.getBindingInput().getExtensibilityElements();
                    input = getMessageType(inputext);                    
                    if ((input == null) || (input.trim().equals("")))
                    {
                        input = ConfigData.DEFAULT_MESSAGE_TYPE;
                    }
                }              

                if (!boper.getOperation().getStyle().equals(OperationType.ONE_WAY))
                {
                    List outputext =
                            boper.getBindingOutput().getExtensibilityElements();
                    output = getMessageType(outputext);
                    if ((output == null) || (output.trim().equals("")))
                    {
                        output = ConfigData.DEFAULT_MESSAGE_TYPE;
                    }
                }
                mep = resolveMep(boper.getOperation().getStyle());
                if (isValid())
                {                   
                    eb.addOperation("", bindingopername, mep, input, output, 
                                    outfileext, outprefix);
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            setError("Cannot set operations - " + e.getMessage());
        }
    }

    /**
     * Sets the output extension.
     *
     * @param map Node map.
     * @param eb endpoint bean.
     */
    private void setOutputExtension(
        NamedNodeMap map,
        EndpointBean eb)
    {
        String val = getValue(map, ConfigData.OUTPUTEXTENSION);

        if (val == null)
        {
            val = "xml";
        }

        eb.setValue(ConfigData.OUTPUTEXTENSION, val);
    }

    /**
     * Sets the output file prefix.
     *
     * @param map Node map.
     * @param eb bean attributes.
     */
    private void setOutputPrefix(
        NamedNodeMap map,
        EndpointBean eb)
    {
        String val = getValue(map, ConfigData.OUTPUTPREFIX);

        if (val == null)
        {
            val = "out";
        }

        eb.setValue(ConfigData.OUTPUTPREFIX, val);
    }

    /**
     * Gets the valus specified by attr from the Map.
     *
     * @param map Node map.
     * @param attr attribute.
     *
     * @return value.
     */
    private String getValue(
        NamedNodeMap map,
        String attr)
    {
        Node attrnode = map.getNamedItem(attr);
        String value = null;

        try
        {
            value = attrnode.getNodeValue();
        }
        catch (Exception e)
        {
            ;
        }

        return value;
    }

    /**
     * Loads the endpoint information.
     *
     * @param ep Endpoint object
     * @param eb endpoint bean.
     */
    private void loadEndpointBean(
        Port ep,
        EndpointBean eb)
    {      List extList = ep.getExtensibilityElements();

        if (extList != null)
        {
            for (int i = 0; i < extList.size(); i++)
            {
                if (extList.get(i) instanceof javax.wsdl.extensions.UnknownExtensibilityElement)
                {
                    UnknownExtensibilityElement sb =
                        (UnknownExtensibilityElement) extList.get(i);

                    if (sb.getElementType().getNamespaceURI().equals(ConfigData.FILENAMESPACE)
                            && sb.getElementType().getLocalPart().equals("artifacts"))
                    {
                        Element epnode = sb.getElement();
                        NamedNodeMap epattributes = epnode.getAttributes(); 
                        setDirection(epattributes, eb);
                        if (eb.getRole() == ConfigData.CONSUMER)
                        {
                            setLocation(epattributes, eb, ConfigData.WSDL_INPUT_LOCATION,
                                                    ConfigData.INPUTDIR);
                            setLocation(epattributes, eb, ConfigData.WSDL_PROCESSED_LOCATION,
                                                    ConfigData.PROCESSEDDIR);
                        }
                        setLocation(epattributes, eb, ConfigData.WSDL_OUTPUT_LOCATION,
                                                     ConfigData.OUTPUTDIR);
                        setInputPattern(epattributes, eb);
                        if (!isValid())
                        {
                            setError("\n\tEndpoint : " + ep.getName() + getError());
                        }
                    }
                }
            }
        }
    }

    /**
     * Parses the config files and loads them into bean objects.
     */
    private void loadServicesList()
    {
        java.util.Map servicemap = mDefinition.getServices();
        List eplist = new LinkedList();
        Collection services = servicemap.values();

        Iterator iter = services.iterator();

        while (iter.hasNext())
        {
            try
            {
                javax.wsdl.Service ser = (javax.wsdl.Service) iter.next();

                Map endpointsmap = ser.getPorts();
                Collection endpoints = endpointsmap.values();

                Iterator epiter = endpoints.iterator();

                while (epiter.hasNext())
                {
                    javax.wsdl.Port ep = (javax.wsdl.Port) epiter.next();

                    javax.wsdl.Binding b = ep.getBinding();

                    if (!isFileBinding(b))
                    {
                        continue;
                    }

                    EndpointBean eb = new EndpointBean();
                    loadEndpointBean(ep, eb);
                    eb.setValue(ConfigData.SERVICE_NAMESPACE,
                        ser.getQName().getNamespaceURI());
                    eb.setValue(ConfigData.SERVICE_LOCALNAME,
                        ser.getQName().getLocalPart());
                    eb.setValue(ConfigData.SERVICENAME,
                        ser.getQName().toString());
                    eb.setValue(ConfigData.BINDING_NAME, b.getQName().toString());
                    eb.setValue(ConfigData.ENDPOINTNAME, ep.getName());
                    eb.setValue(ConfigData.INTERFACE_LOCALNAME,
                        ep.getBinding().getPortType().getQName().getLocalPart());
                    eb.setValue(ConfigData.INTERFACE_NAMESPACE,
                        ep.getBinding().getPortType().getQName()
                          .getNamespaceURI());
                    eb.setWsdlDefintion(mDefinition);
                    eb.setDeploymentType("WSDL11");
                    setOperations(eb, b);

                    if (!isValid())
                    {
                        setError(("\n\t" + "Service : "
                            + ser.getQName().toString() + getError()));
                        eplist.clear();

                        return;
                    }

                    eplist.add(eb);                    
                    mResolver.addEndpointDoc(ser.getQName().toString()
                        + ep.getName(), mWsdlFile );
                    
                }
            }
            catch (Exception e)
            {
                setError(mTranslator.getString(FBC_WSDL_ENDPOINT_ERROR,
                        e.getMessage()));
                setException(e);
                setValid(false);
                e.printStackTrace();

                return;
            }
        }

        mEndpointInfoList = new EndpointBean[eplist.size()];

        for (int x = 0; x < eplist.size(); x++)
        {
            mEndpointInfoList[x] = (EndpointBean) eplist.get(x);
        }

        mTotalEndpoints = eplist.size();
    }
    
       /**
     *
     *
     * @param opstyle  
     *
     * @return  
     */
    private String resolveMep(OperationType opstyle)
    {
        String mep = null;

        if (opstyle.equals(OperationType.ONE_WAY))
        {
            mep = MessageProcessor.IN_ONLY;
        }
        else if (opstyle.equals(OperationType.REQUEST_RESPONSE))
        {
            mep = MessageProcessor.IN_OUT;
        }
        else if (opstyle.equals(OperationType.NOTIFICATION))
        {
            mep = MessageProcessor.IN_ONLY;
        }
        else if (opstyle.equals(OperationType.SOLICIT_RESPONSE))
        {
            mep = MessageProcessor.IN_OUT;
        }
        else
        {
            setError("Unknown Style of operation " + opstyle);
        }

        return mep;
    }

}
