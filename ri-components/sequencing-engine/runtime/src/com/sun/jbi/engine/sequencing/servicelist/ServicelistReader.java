/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ServicelistReader.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.engine.sequencing.servicelist;

import com.sun.jbi.engine.sequencing.SequencingEngineContext;
import com.sun.jbi.engine.sequencing.SequencingEngineResources;
import com.sun.jbi.engine.sequencing.util.ConfigData;
import com.sun.jbi.engine.sequencing.util.StringTranslator;
import com.sun.jbi.engine.sequencing.util.UtilBase;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.File;

import java.util.StringTokenizer;
import java.util.logging.Logger;


/**
 * Class ServicelistReader.
 *
 * @author Sun Microsystems, Inc.
 */
public class ServicelistReader
    extends UtilBase
    implements SequencingEngineResources
{
    /**
     * Document object.
     */
    private Document mDoc;

    /**
     * Logger object.
     */
    private Logger mLog;

    /**
     * List of services.
     */
    private ServicelistBean mListofService = null;

    /**
     * Interface name.
     */
    private String mListInterfaceName;

    /**
     * Interface namespace.
     */
    private String mListInterfaceNameSpace;

    /**
     * List desc
     */
    private String mServicelistDesc;

    /**
     * Endpoint name for this service.
     */
    private String mServicelistEndpoint;

    /**
     * Servicelist MEP
     */
    private String mServicelistMep;

    /**
     * List name.
     */
    private String mServicelistName;

    /**
     * Namespace of this service.
     */
    private String mServicelistNameSpace;

    /**
     * List opereation.
     */
    private String mServicelistOper;

    /**
     * Namespace of the operation in this service.
     */
    private String mServicelistOperNamespace;

    /**
     * Translator object for internationalization.
     */
    private StringTranslator mTranslator;

    /**
     * Service count.
     */
    private int mServiceCount = 0;

    /**
     * Creates a new ServicelistReader object.
     */
    public ServicelistReader()
    {
        mLog = SequencingEngineContext.getInstance().getLogger();
        mTranslator = new StringTranslator();
        setValid(true);
    }

    /**
     * Gets the service count.
     *
     * @return number of services.
     */
    public int getServiceCount()
    {
        return mServiceCount;
    }

    /**
     * Gets the list bean.
     *
     * @return list bean.
     */
    public ServicelistBean getServicelistBean()
    {
        return mListofService;
    }

    /**
     * Initializes the config file and loads services.
     *
     * @param doc Name of the config file.
     */
    public void init(Document doc)
    {
        try
        {
            mDoc = doc;
            mDoc.getDocumentElement().normalize();
            loadServices();
        }
        catch (Exception genException)
        {
            mLog.severe(mTranslator.getString(SEQ_CONFIGDATA_ERROR));
            mLog.severe(genException.getMessage());
            setException(genException);
            setError(getError() + "\n\t"
                + mTranslator.getString(SEQ_CONFIGDATA_ERROR) + "\n"
                + genException.getMessage());
            setValid(false);
        }
    }

    /**
     * Loads the services.
     */
    public void loadServices()
    {
        mListofService = new ServicelistBean();
        setListAttributes(mDoc);
        mListofService.setServicename(mServicelistName);
        mListofService.setNamespace(mServicelistNameSpace);
        mListofService.setInterfaceNamespace(mListInterfaceNameSpace);
        mListofService.setInterfaceName(mListInterfaceName);
        mListofService.setListdescription(mServicelistDesc);
        mListofService.setOperationNamespace(mServicelistOperNamespace);
        mListofService.setOperation(mServicelistOper);
        mListofService.setMep(mServicelistMep);
        mListofService.setEndpointName(mServicelistEndpoint);

        NodeList list = mDoc.getElementsByTagName(ConfigData.SERVICE);
        mServiceCount = list.getLength();

        try
        {
            for (int i = 0; i < mServiceCount; i++)
            {
                Node node = list.item(i);
                ServiceBean sb = new ServiceBean();

                if (node.getNodeType() == Node.ELEMENT_NODE)
                {
                    setService(node, sb);
                    setInterface(node, sb);
                    setOperation(node, sb);
                    setByTagName(node, sb, ConfigData.SERVICE_ENDPOINT);
                    setByTagName(node, sb, ConfigData.SERVICE_DESCRIPTION);
                    setByTagName(node, sb, ConfigData.TIMEOUT);
                    setByTagName(node, sb, ConfigData.SERVICEID);
                    setByTagName(node, sb, ConfigData.SERVICE_MEP);
                }

                mListofService.addService(sb);
            }
        }
        catch (Exception ee)
        {
            setError(getError() + ee.getMessage());
            setException(ee);
            ee.printStackTrace();
            setValid(false);
        }
    }

    /**
     * Utility method for setting the Bean object from the XML Nodes.
     *
     * @param node The node which needs to be
     * @param sb The end point bean object which has to be updated
     * @param tagName the tag name which needs to be read.
     */
    private void setByTagName(
        Node node,
        ServiceBean sb,
        String tagName)
    {
        Element ele = (Element) node;
        NodeList namelist = ele.getElementsByTagName(tagName);
        Element name = (Element) namelist.item(0);
        String sValue = null;

        try
        {
            sValue =
                ((Node) (name.getChildNodes().item(0))).getNodeValue().trim();
        }
        catch (NullPointerException ne)
        {
            return;
        }

        if (tagName.equalsIgnoreCase(ConfigData.SERVICE_NAMESPACE))
        {
            sb.setNamespace(sValue);
        }
        else if (tagName.equalsIgnoreCase(ConfigData.SERVICE_LOCALNAME))
        {
            sb.setName(sValue);
        }

        else if (tagName.equalsIgnoreCase(ConfigData.SERVICE_ENDPOINT))
        {
            sb.setEndpointName(sValue);
        }
        else if (tagName.equalsIgnoreCase(ConfigData.SERVICE_DESCRIPTION))
        {
            sb.setDescription(sValue);
        }
        else if (tagName.equalsIgnoreCase(ConfigData.OPERATION_NAMESPACE))
        {
            sb.setOperationNamespace(sValue);
        }
        else if (tagName.equalsIgnoreCase(ConfigData.OPERATION))
        {
            sb.setOperation(sValue);
        }
        else if (tagName.equalsIgnoreCase(ConfigData.SERVICE_MEP))
        {
            sb.setMep(sValue);
        }
        else if (tagName.equalsIgnoreCase(ConfigData.TIMEOUT))
        {
            long timeout = 0;

            try
            {
                timeout = Integer.parseInt(sValue);
                sb.setTimeout(timeout);
            }
            catch (NumberFormatException ne)
            {
                mLog.warning(mTranslator.getString(SEQ_CONFIGDATA_ERROR,
                        sb.getName()));
            }
        }
        else if (tagName.equalsIgnoreCase(ConfigData.SERVICEID))
        {
            sb.setServiceid(sValue);
        }
    }

    /**
     * Sets interface.
     *
     * @param node node
     * @param sb bean.
     */
    private void setInterface(
        Node node,
        ServiceBean sb)
    {
        Element ele = (Element) node;
        NodeList namelist = ele.getElementsByTagName(ConfigData.INTERFACE);

        try
        {
            Node nd = (Node) namelist.item(0);
            String namespace = getValue(nd, ConfigData.NAMESPACE_URI);
            String name = getValue(nd, ConfigData.LOCAL_PART);
            sb.setInterfaceNamespace(namespace);
            sb.setInterfaceName(name);
        }
        catch (Exception e)
        {
            mLog.severe(mTranslator.getString(SEQ_LOAD_CONFIG_FAILED));
        }
    }

    /**
     * Sets the list atttributes.
     *
     * @param doc Document.
     */
    private void setListAttributes(Document doc)
    {
        NodeList nl = doc.getElementsByTagName(ConfigData.SERVICELISTATTR);
        Node node = nl.item(0);
        setListService(node);
        setListInterface(node);
        setListOperation(node);
        mServicelistDesc = getValue(node, ConfigData.SERVICELIST_DESCRIPTION);
        mServicelistMep = getValue(node, ConfigData.LIST_MEP);
        mServicelistEndpoint = getValue(node, ConfigData.LIST_ENDPOINT);
    }

    /**
     * Sets the service list interface.
     *
     * @param nd node.
     */
    private void setListInterface(Node nd)
    {
        Element ele = (Element) nd;
        NodeList namelist = ele.getElementsByTagName(ConfigData.LIST_INTERFACE);

        try
        {
            Node node = (Node) namelist.item(0);
            mListInterfaceNameSpace = getValue(node, ConfigData.NAMESPACE_URI);
            mListInterfaceName = getValue(node, ConfigData.LOCAL_PART);
        }
        catch (Exception e)
        {
            mLog.severe(mTranslator.getString(SEQ_LOAD_CONFIG_FAILED));
        }
    }

    /**
     * Sets the list operation.
     *
     * @param nd node.
     */
    private void setListOperation(Node nd)
    {
        Element ele = (Element) nd;
        NodeList namelist = ele.getElementsByTagName(ConfigData.LIST_OPERATION);

        try
        {
            Node node = (Node) namelist.item(0);
            mServicelistOperNamespace =
                getValue(node, ConfigData.NAMESPACE_URI);
            mServicelistOper = getValue(node, ConfigData.LOCAL_PART);
        }
        catch (Exception e)
        {
            mLog.severe(mTranslator.getString(SEQ_LOAD_CONFIG_FAILED));
        }
    }

    /**
     * Sets the service list service.
     *
     * @param nd node.
     */
    private void setListService(Node nd)
    {
        Element ele = (Element) nd;
        NodeList namelist = ele.getElementsByTagName(ConfigData.LIST_SERVICE);

        try
        {
            Node node = (Node) namelist.item(0);
            mServicelistNameSpace = getValue(node, ConfigData.NAMESPACE_URI);
            mServicelistName = getValue(node, ConfigData.LOCAL_PART);
        }
        catch (Exception e)
        {
            mLog.severe(mTranslator.getString(SEQ_LOAD_CONFIG_FAILED));
        }
    }

    /**
     * Gets the local name from the quname.
     *
     * @param qname Qualified name of service.
     *
     * @return String local name
     */
    private String getLocalName(String qname)
    {
        StringTokenizer tok = new StringTokenizer(qname, ":");

        try
        {
            if (tok.countTokens() == 1)
            {
                return qname;
            }

            tok.nextToken();

            return tok.nextToken();
        }
        catch (Exception e)
        {
            return "";
        }
    }

    /**
     * Gets the namespace from the qname.
     *
     * @param qname Qname of service
     *
     * @return namespace namespace of service
     */
    private String getNamespace(String qname)
    {
        StringTokenizer tok = new StringTokenizer(qname, ":");
        String prefix = null;

        try
        {
            if (tok.countTokens() == 1)
            {
                return "";
            }

            prefix = tok.nextToken();

            NamedNodeMap map = mDoc.getDocumentElement().getAttributes();

            for (int j = 0; j < map.getLength(); j++)
            {
                Node n = map.item(j);

                if (n.getLocalName().trim().equals(prefix.trim()))
                {
                    return n.getNodeValue();
                }
            }
        }
        catch (Exception e)
        {
            ;
        }

        return "";
    }

    /**
     * Sets the operation.
     *
     * @param node node.
     * @param sb service bean.
     */
    private void setOperation(
        Node node,
        ServiceBean sb)
    {
        Element ele = (Element) node;
        NodeList namelist = ele.getElementsByTagName(ConfigData.OPERATION);

        try
        {
            Node nd = (Node) namelist.item(0);
            String namespace = getValue(nd, ConfigData.NAMESPACE_URI);
            String name = getValue(nd, ConfigData.LOCAL_PART);
            sb.setOperationNamespace(namespace);
            sb.setOperation(name);
        }
        catch (Exception e)
        {
            mLog.severe(mTranslator.getString(SEQ_LOAD_CONFIG_FAILED));
        }
    }

    /**
     * Sets service.
     *
     * @param node node
     * @param sb bean.
     */
    private void setService(
        Node node,
        ServiceBean sb)
    {
        Element ele = (Element) node;
        NodeList namelist = ele.getElementsByTagName(ConfigData.SERVICE_NAME);

        try
        {
            Node nd = (Node) namelist.item(0);
            String namespace = getValue(nd, ConfigData.NAMESPACE_URI);
            String name = getValue(nd, ConfigData.LOCAL_PART);
            sb.setNamespace(namespace);
            sb.setName(name);
        }
        catch (Exception e)
        {
            mLog.severe(mTranslator.getString(SEQ_LOAD_CONFIG_FAILED));
        }
    }

    /**
     * Gets the value of the atribute.
     *
     * @param n node
     * @param name name of the node.
     *
     * @return value of node.
     */
    private String getValue(
        Node n,
        String name)
    {
        String s = null;

        try
        {
            Element ele = (Element) n;
            NodeList list = ele.getElementsByTagName(name);
            Element found = (Element) list.item(0);
            s = (String) found.getFirstChild().getNodeValue();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return s;
    }
}
