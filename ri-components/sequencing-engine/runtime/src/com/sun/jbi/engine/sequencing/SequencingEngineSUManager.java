/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)SequencingEngineSUManager.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.engine.sequencing;

import com.sun.jbi.common.Util;
import com.sun.jbi.common.management.ComponentMessageHolder;
import com.sun.jbi.common.management.ManagementMessageBuilder;
import com.sun.jbi.engine.sequencing.servicelist.ServicelistBean;
import com.sun.jbi.engine.sequencing.util.DeployHelper;
import com.sun.jbi.engine.sequencing.util.StringTranslator;

import java.util.logging.Logger;

import javax.jbi.component.ComponentContext;
import javax.jbi.management.DeploymentException;


/**
 * Service unit manager for SE.
 *
 * @author Sun Microsystems, Inc.
 */
public class SequencingEngineSUManager
    implements javax.jbi.component.ServiceUnitManager,
        SequencingEngineResources
{
    /**
     * Component Context.
     */
    private ComponentContext mContext;

    /**
     * Helper class for deployment.
     */
    private DeployHelper mHelper;

    /**
     * Logger.
     */
    private Logger mLog;

    /**
     * Deployer messages.
     */
    private ManagementMessageBuilder mBuildManagementMessage;

    /**
     * Service manager which takes care of starting/stopping services.
     */
    private ServiceManager mManager;

    /**
     * i18n.
     */
    private StringTranslator mTranslator;

    /**
     * Creates a new instance of FileBindingSUManager.
     */
    public SequencingEngineSUManager()
    {
        mLog = SequencingEngineContext.getInstance().getLogger();
        mTranslator = new StringTranslator();
    }

    /**
     * Returns a list of all application sub-assemblies (ASA's) currently
     * deployed in to the named component.  This method is a convenient
     * wrapper for the same operation in the DeployerMBean.
     *
     * @return array of SU UUID strings.
     */
    public String [] getDeployments()
    {
        DeploymentRegistry dr = DeploymentRegistry.getInstance();

        return dr.getAllDeployments();
    }

    /**
     * Sets the component context, and SU manager.
     *
     * @param ctx component context.
     * @param manager su manager.
     */
    public void setValues(
        ComponentContext ctx,
        ServiceManager manager)
    {
        mContext = ctx;
        mManager = manager;
        mBuildManagementMessage = Util.createManagementMessageBuilder();
    }

    /**
     * This method is called by the framework when a deployment request comes
     * for the file binding.
     *
     * @param suId Service unit id.
     * @param suPath Service unit path.
     *
     * @return status message.
     *
     * @throws javax.jbi.management.DeploymentException deploy exception.
     * @throws DeploymentException
     */
    public String deploy(
        String suId,
        String suPath)
        throws javax.jbi.management.DeploymentException
    {
        DeploymentRegistry dr = DeploymentRegistry.getInstance();
        String retMsg = null;

        if (dr.getDeploymentStatus(suId))
        {
            mLog.severe(mTranslator.getString(SEQ_DUPLICATE_DEPLOYMENT, suId));
            throw new DeploymentException(createExceptionMessage(
                    mContext.getComponentName(), "deploy", "FAILED",
                    "SEQ_30001", suId,
                    mTranslator.getString(SEQ_DUPLICATE_DEPLOYMENT, suId), null));
        }

        try
        {
            initializeDeployment(suId, suPath);
        }
        catch (DeploymentException de)
        {
            throw de;
        }
        catch (Exception e)
        {
            throw new DeploymentException(createExceptionMessage(
                    mContext.getComponentName(), "deploy", "FAILED",
                    "SEQ_30009", suId,
                    mTranslator.getString(SEQ_XML_STRING_CREATION_FAILED, suId),
                    e));
        }

        try
        {
            ComponentMessageHolder compMsgHolder =
                new ComponentMessageHolder("STATUS_MSG");
            compMsgHolder.setComponentName(mContext.getComponentName());
            compMsgHolder.setTaskName("deploy");
            compMsgHolder.setTaskResult("SUCCESS");

            retMsg =
                mBuildManagementMessage.buildComponentMessage(compMsgHolder);
        }
        catch (Exception e)
        {
            mLog.severe(mTranslator.getString(SEQ_XML_STRING_CREATION_FAILED));
            mLog.severe(e.getMessage());
        }


        return retMsg;
    }

    /**
     * This method is called by the framework when the deployment has to be \
     * initialised , just before starting it.
     *
     * @param suId service unit id.
     * @param suPath service unit path.
     *
     * @throws javax.jbi.management.DeploymentException DeploymentException
     * @throws DeploymentException
     */
    public void init(
        String suId,
        String suPath)
        throws javax.jbi.management.DeploymentException
    {
        DeploymentRegistry dr = DeploymentRegistry.getInstance();

        if (!dr.getDeploymentStatus(suId))
        {
            try
            {
                initializeDeployment(suId, suPath);
            }
            catch (DeploymentException de)
            {
                throw de;
            }
            catch (Exception e)
            {
                throw new DeploymentException(createExceptionMessage(
                        mContext.getComponentName(), "deploy", "FAILED",
                        "SEQ_30009", suId,
                        mTranslator.getString(SEQ_XML_STRING_CREATION_FAILED,
                            suId), e));
            }
        }

    }

    /**
     * Shuts down the service unit.
     *
     * @param str su id.
     *
     * @throws javax.jbi.management.DeploymentException depl exception
     */
    public void shutDown(String str)
        throws javax.jbi.management.DeploymentException
    {
    }

    /**
     * Starts an SU.
     *
     * @param suId su id.
     *
     * @throws javax.jbi.management.DeploymentException DeploymentException
     * @throws DeploymentException
     */
    public void start(String suId)
        throws javax.jbi.management.DeploymentException
    {

        try
        {
            mManager.startDeployment(suId);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw new DeploymentException(createExceptionMessage(
                    mContext.getComponentName(), "deploy", "FAILED",
                    "SEQ_30009", suId,
                    mTranslator.getString(SEQ_START_DEPLOYMENT_FAILED, suId), e));
        }

        if (!mManager.isValid())
        {
            mLog.severe(mTranslator.getString(SEQ_START_DEPLOYMENT_FAILED, suId));
            mLog.severe(mManager.getError());
            throw new DeploymentException(createExceptionMessage(
                    mContext.getComponentName(), "deploy", "FAILED",
                    "SEQ_30009", suId, mManager.getError(), null));
        }

    }

    /**
     * Stops an SU.
     *
     * @param suId su id.
     *
     * @throws javax.jbi.management.DeploymentException DeploymentException
     * @throws DeploymentException
     */
    public void stop(String suId)
        throws javax.jbi.management.DeploymentException
    {
        mManager.stopDeployment(suId);

        if (!mManager.isValid())
        {
            throw new DeploymentException(createExceptionMessage(
                    mContext.getComponentName(), "deploy", "FAILED",
                    "SEQ_30009", suId, mManager.getError(), null));
        }
    }

    /**
     * Undeploys an SU.
     *
     * @param suId su id.
     * @param suPath su path.
     *
     * @return management message string.
     *
     * @throws javax.jbi.management.DeploymentException DeploymentException
     * @throws DeploymentException
     */
    public String undeploy(
        String suId,
        String suPath)
        throws javax.jbi.management.DeploymentException
    {
        DeploymentRegistry dr = DeploymentRegistry.getInstance();
        ServicelistBean slb = dr.getService(suId);

        if (slb == null)
        {
            throw new DeploymentException(createExceptionMessage(
                    mContext.getComponentName(), "undeploy", "FAILED",
                    "SEQ_30006", "", "SU not deployed", null));
        }

        try
        {
            dr.deregisterService(slb.getNamespace() + slb.getServicename()
                + slb.getEndpointName() + slb.getOperation());
        }
        catch (Exception e)
        {
            throw new DeploymentException(createExceptionMessage(
                    mContext.getComponentName(), "undeploy", "FAILED",
                    "SEQ_30006", "", e.getMessage(), e));
        }

        String retMsg = null;

        try
        {
            ComponentMessageHolder compMsgHolder =
                new ComponentMessageHolder("STATUS_MSG");
            compMsgHolder.setComponentName(mContext.getComponentName());
            compMsgHolder.setTaskName("undeploy");
            compMsgHolder.setTaskResult("SUCCESS");

            retMsg =
                mBuildManagementMessage.buildComponentMessage(compMsgHolder);
        }
        catch (Exception e)
        {
            mLog.severe(mTranslator.getString(SEQ_XML_STRING_CREATION_FAILED));
            mLog.severe(e.getMessage());
        }

        return retMsg;
    }

    /**
     * helper method to create XML exception string.
     *
     * @param compid Component id.
     * @param oper operation like deploy or undeploy
     * @param status success r failure
     * @param loctoken some failure string token like SEQ_300001
     * @param locparam parameters for error message.
     * @param locmessage error message.
     * @param exObj stack trace of exception.
     *
     * @return XML string.
     */
    private String createExceptionMessage(
        String compid,
        String oper,
        String status,
        String loctoken,
        String locparam,
        String locmessage,
        Throwable exObj)
    {
        String [] locParams = new String[1];
        locParams[0] = locparam;

        ComponentMessageHolder msgMap =
            new ComponentMessageHolder("EXCEPTION_MSG");

        msgMap.setComponentName(compid);
        msgMap.setTaskName(oper);
        msgMap.setTaskResult(status);
        msgMap.setLocToken(1, loctoken);
        msgMap.setLocParam(1, locParams);
        msgMap.setLocMessage(1, locmessage);
        msgMap.setExceptionObject(exObj);

        String retMsg = null;

        try
        {
            retMsg = mBuildManagementMessage.buildComponentMessage(msgMap);
        }
        catch (Exception e)
        {
            mLog.severe(mTranslator.getString(SEQ_XML_STRING_CREATION_FAILED));
            mLog.severe(e.getMessage());
        }

        return retMsg;
    }

    /**
     * initalises deployment.
     *
     * @param suId su id.
     * @param suPath su path.
     *
     * @throws javax.jbi.management.DeploymentException DeploymentException
     * @throws DeploymentException
     */
    private void initializeDeployment(
        String suId,
        String suPath)
        throws javax.jbi.management.DeploymentException
    {
        mHelper = new DeployHelper(suId, suPath); // , mResolver);
        mHelper.doDeploy(true);

        if (!mHelper.isValid())
        {
            mLog.severe(mHelper.getError());

            Exception e = mHelper.getException();

            if (e != null)
            {
                throw new DeploymentException(createExceptionMessage(
                        mContext.getComponentName(), "deploy", "FAILED",
                        "SEQ_30005", "", mHelper.getError(), e));
            }
            else
            {
                throw new DeploymentException(createExceptionMessage(
                        mContext.getComponentName(), "deploy", "FAILED",
                        "SEQ_30005", "", mHelper.getError(), null));
            }
        }
    }

    /**
     * Starts a deployment.
     *
     * @param suId su id.
     *
     * @return management message string.
     */
    private String startDeployment(String suId)
    {
        String retMsg = null;
        mManager.startDeployment(suId);

        try
        {
            ComponentMessageHolder compMsgHolder =
                new ComponentMessageHolder("STATUS_MSG");
            compMsgHolder.setComponentName(mContext.getComponentName());
            compMsgHolder.setTaskName("deploy");
            compMsgHolder.setTaskResult("SUCCESS");

            if (!mManager.isValid())
            {
                compMsgHolder.setTaskResult("FAILED");
                compMsgHolder.setLocMessage(1, mManager.getError());
            }
            else if (!mManager.getWarning().trim().equals(""))
            {
                compMsgHolder.setStatusMessageType("WARNING");
                compMsgHolder.setLocMessage(1, mManager.getWarning());
            }

            retMsg =
                mBuildManagementMessage.buildComponentMessage(compMsgHolder);
        }
        catch (Exception e)
        {
            mLog.severe(mTranslator.getString(SEQ_XML_STRING_CREATION_FAILED));
            mLog.severe(e.getMessage());
        }

        return retMsg;
    }
}
