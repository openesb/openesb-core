/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ServiceListValidator.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.engine.xslt.util;

import com.sun.jbi.engine.xslt.TEResources;

import org.w3c.dom.Document;

import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;


/**
 * DOCUMENT ME!
 *
 * @author Sun Microsystems, Inc.
 */
public final class ServiceListValidator
    extends DefaultHandler
    implements TEResources
{
    /**
     * DOCUMENT ME!
     */
    static final String JAXP_SCHEMA_LANGUAGE =
        "http://java.sun.com/xml/jaxp/properties/schemaLanguage";

    /**
     * DOCUMENT ME!
     */
    static final String W3C_XML_SCHEMA = "http://www.w3.org/2001/XMLSchema";

    /**
     * DOCUMENT ME!
     */
    static final String JAXP_SCHEMA_SOURCE =
        "http://java.sun.com/xml/jaxp/properties/schemaSource";

    /**
     * DOCUMENT ME!
     */
    private DocumentBuilderFactory mDocFactory = null;

    /**
     * DOCUMENT ME!
     */
    private String mErrorMsg = null;

    /**
     * DOCUMENT ME!
     */
    private String mFileName = "servicelist.xml";

    /**
     * DOCUMENT ME!
     */
    private String mSchemaFile = "servicelist.xsd";

    /**
     *    
     */
    private StringTranslator mTranslator = null;

    /**
     * DOCUMENT ME!
     */
    private boolean mValid = true;
    
    /**
     * DOCUMENT ME!
     */
    private Document mDocument = null;

    /**
     * Creates a new ServiceListValidator object.
     *
     * @param schema DOCUMENT ME!
     * @param xmlfile DOCUMENT ME!
     */
    public ServiceListValidator(
        String schema,
        String xmlfile)
    {
        mTranslator =
            new StringTranslator("com.sun.jbi.engine.xslt",
                this.getClass().getClassLoader());
        mDocFactory = DocumentBuilderFactory.newInstance();
        mDocFactory.setNamespaceAware(true);
        mDocFactory.setValidating(false);
        mSchemaFile = schema;
        mFileName = xmlfile;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String getError()
    {
        return mErrorMsg;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public boolean isValid()
    {
        return mValid;
    }

    /**
     * DOCUMENT ME!
     *
     * @param se DOCUMENT ME!
     */
    public void error(SAXParseException se)
    {
        //System.out.println(se.getMessage());
        mValid = false;
    }

    /**
     * DOCUMENT ME!
     *
     * @param se DOCUMENT ME!
     */
    public void fatalError(SAXParseException se)
    {
        //System.out.println(se.getMessage());
        mValid = false;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public Document parse()
    {
        DocumentBuilder builder = null;

        try
        {
            builder = mDocFactory.newDocumentBuilder();
        }
        catch (ParserConfigurationException pce)
        {
            setError(pce.getMessage());
        }

        builder.setErrorHandler(this);

        Document document = null;

        try
        {
            document = builder.parse(mFileName);
        }
        catch (SAXException se)
        {
            setError(se.getMessage());
            se.printStackTrace();
        }
        catch (IOException ioe)
        {
            setError(ioe.getMessage());
            ioe.printStackTrace();
        }

        return document;
    }

    /**
     * DOCUMENT ME!
     */
    public void validate()
    {
        setAttributes();

        Document d = parse();

        if (!isValid())
        {
            //System.out.println(getError());
        }
        mDocument = d;
    }
    
    /**
     * DOCUMENT ME!
     */
    public Document getDocument()
    {
        return mDocument;
    }

    /**
     * DOCUMENT ME!
     *
     * @param se DOCUMENT ME!
     */
    public void warning(SAXParseException se)
    {
        //System.out.println("WARNIGN " + se.getMessage());
    }

    /**
     * DOCUMENT ME!
     */
    private void setAttributes()
    {
        mDocFactory.setValidating(true);

        try
        {
            mDocFactory.setAttribute(JAXP_SCHEMA_LANGUAGE, W3C_XML_SCHEMA);
            mDocFactory.setAttribute(JAXP_SCHEMA_SOURCE, mSchemaFile);
        }
        catch (IllegalArgumentException iae)
        {
            setError(iae.getMessage());
            iae.printStackTrace();
        }
    }

    /**
     * DOCUMENT ME!
     *
     * @param msg DOCUMENT ME!
     */
    private void setError(String msg)
    {
        mErrorMsg = msg;
    }
}
