/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Receiver.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.engine.xslt;

import com.sun.jbi.engine.xslt.framework.WorkManager;
import com.sun.jbi.engine.xslt.util.StringTranslator;

import java.util.logging.Logger;

import javax.jbi.component.ComponentContext;
import javax.jbi.messaging.DeliveryChannel;
import javax.jbi.messaging.MessageExchange;
import javax.jbi.messaging.MessagingException;


//import javax.jbi.engine.EngineContext;

/**
 * This class implements runnable. Its run in a seprate thread. This blocks and
 * pulls messages from NMS. It then submits the received normalized messages
 * for further processing.
 *
 * @author Sun Microsystems Inc.
 */
public class Receiver
    implements Runnable, TEResources
{
    /**
     *    
     */
    private ComponentContext mContext = null;

    /**
     * Refernce to a DeliveryChannel passed to command object. This is used by
     * command object to send reply to sender.
     */
    private DeliveryChannel mChannel = null;

    /**
     * Logger for logging information.
     */
    private Logger mLogger = null;

    /**
     *    
     */
    private MessageExchange mExchange = null;

    /**
     *    
     */
    private StringTranslator mTranslator = null;

    /**
     *    
     */
    private WorkManager mWorkManager = null;

    /**
     * Boolean flag for controlling life cycle of this thread.
     */
    private boolean mRunFlag = true;

    /**
     * Constructor for creating instance of this class.
     *
     * @param context for receiving environment parameters.
     */
    public Receiver(ComponentContext context)
    {
        mContext = context;
        mTranslator =
            new StringTranslator("com.sun.jbi.engine.xslt",
                this.getClass().getClassLoader());
        mLogger = TransformationEngineContext.getInstance().getLogger("");
        mLogger.info(mTranslator.getString(TEResources.RECEIVER_CONSTRUCTOR));
        
        init();
    }

    /**
     * This is called to gracefully stop the ReceiverThread.
     */
    public synchronized void cease()
    {
        mLogger.info(mTranslator.getString(TEResources.RECEVIER_CEASE));
        mRunFlag = false;

        if (mWorkManager != null)
        {
            mWorkManager.cease();
        }

        mLogger.info(mTranslator.getString(TEResources.RECEVIER_CEASE_END));
    }

    /**
     * It polls NMS periodically for input messages. It blocks for 6 sec on
     * receive call to receive messages.Depending on runFlag staus either it
     * tries to again poll NMS or exits.
     */
    public void exec()
    {
        mLogger.info(mTranslator.getString(TEResources.RECEVIER_EXEC));
        mWorkManager.start();

        try
        {
            mChannel = mContext.getDeliveryChannel();

            if (mChannel == null)
            {
                mLogger.severe(mTranslator.getString(TEResources.CHANNEL_NULL));
            }
        }
        catch (MessagingException ex)
        {
            mLogger.severe(mTranslator.getString(TEResources.RECEIVER_TERMINATED));
            ex.printStackTrace();

            return;
        }

        while (mRunFlag)
        {
            try
            {
                mExchange = mChannel.accept();
                if (mExchange != null)
                {
                    mLogger.info(mTranslator.getString(
                        TEResources.GOT_MESSAGEEXCHANGE, mExchange.getExchangeId()));
                    ServiceExecutor executor =
                        new ServiceExecutor(mChannel, mExchange);

                    while (!mWorkManager.processCommand(executor))
                    {
                        //block till command is processed
                    }
                }
                /*else
                {
                    mLogger.severe(mTranslator.getString(
                            TEResources.TE_ERR_MESSEXC_RCVD_NULL));
                }*/
            }
            catch (MessagingException mex)
            {
                mLogger.info(mTranslator.getString(TEResources.RECEIVER_EXIT));

                //mex.printStackTrace(); 
                break;
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
            }
        }

        mLogger.info(mTranslator.getString(TEResources.RECECIVER_FINISHED));
    }

    /**
     * This is called for starting the ReceiverThread.
     */
    public void run()
    {
        mLogger.info(mTranslator.getString(TEResources.RECEIVER_START));
        exec();
        mLogger.info(mTranslator.getString(TEResources.RECEIVER_START_END));
    }

    /**
     *
     */
    private void init()
    {
        mLogger.info(mTranslator.getString(TEResources.RECEIVER_INIT));
        mWorkManager =
            WorkManager.getWorkManager(mTranslator.getString(TEResources.XSLT));
        mLogger.info(mTranslator.getString(TEResources.RECEIVER_INIT_END));
    }
}
