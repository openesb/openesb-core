/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TransformationProcessor.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.engine.xslt;

import javax.xml.transform.Result;
import javax.xml.transform.Source;


/**
 * This class provides interfaces to process the messages received by the
 * Transformation implementations. It also contains interfaces to create the
 * outbound response and fault message.
 *
 * @author Sun Microsystems, Inc.
  */
public interface TransformationProcessor
{
    /**
     * Does transformation on the input DOM object using the stylesheet.
     *
     * @param input - Input for Transformation.
     * @param xsltfile - XSLT Stylesheet.
     * @param result - a Result object containing the message response.
     *
     * @return boolean
     *
     * @throws Exception exception
     */
    boolean doTransform(
        Source input,
        String xsltfile,
        Result result)
        throws Exception;

    /**
     * Does transformation on the input DOM object using the stylesheet.
     *
     * @param input - Input for Transformation.
     * @param result - a Result object containing the message response.
     *
     * @return boolean
     *
     * @throws Exception exception
     */
    boolean transform(
        Source input,
        Result result)
        throws Exception;
}
