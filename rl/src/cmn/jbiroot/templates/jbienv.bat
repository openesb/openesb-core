rem
rem WARNING - THIS FILE IS GENERATED.  DO NOT MODIFY
rem

set AS_INSTALL=@com.sun.aas.installRoot@
set AS_JMX_REMOTE_URL=service:jmx:rmi:///jndi/rmi://@com.sun.aas.hostName@:@com.sun.jbi.management.JmxRmiPort@/management/rmi-jmx-connector
set JBI_HOME=@com.sun.jbi.home@
set JBI_INSTANCE_NAME=@com.sun.aas.instanceName@
set JBI_ADMIN_PORT=@com.sun.jbi.management.JmxRmiPort@
set JBI_ADMIN_HOST=@com.sun.aas.hostName@
set JBI_DOMAIN_ROOT=@com.sun.jbi.domain.root@
set JBI_DOMAIN_DIR=@com.sun.aas.domainsRoot@
set JBI_DOMAIN_NAME=@com.sun.jbi.domain.name@
set JBI_HADAPTOR_PORT=@com.sun.jbi.management.HtmlAdaptorPort@
set JBI_DOMAIN_PROPS=%JBI_DOMAIN_ROOT%\jbi\config\jbienv.properties
set JBI_DOMAIN_STARTED=%JBI_DOMAIN_ROOT%\jbi\tmp\.jbi_admin_running
set JBI_DOMAIN_STOPPED=%JBI_DOMAIN_ROOT%\jbi\tmp\.jbi_admin_stopped
